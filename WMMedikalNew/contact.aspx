﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="contact.aspx.cs" Inherits="WMMedikalNew.contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBbDKtGQoXfF_vI7O4wNMubMVwXBAGj86s"></script>
    <script type="text/javascript">
        function CloseMessage() {
            document.getElementById('divMessageResult').style.display = "none";
        }

        function CheckSecurity() {
            var hdValue = document.getElementById('Content_hdnCaptcha').value;
            var secValue = document.getElementById('Content_txtCaptcha').value;

            if (hdValue == secValue) {
                document.getElementById('Content_lblSecErrorr').style.display = "none";
                return true;
            }
            else {
                document.getElementById('Content_lblSecErrorr').style.display = "block";
                return false;
            }
        }
    </script>
    <style>
        #map_canvas {
            width: 100% !important;
            height: 320px !important;
        }

        .accordion-content {
            padding: 30px;
            box-sizing: border-box;
            background: #fdf2f5;
            background-size: 1px 100%;
            display: block;
            position: relative;
            padding-bottom: 0px;
            float: left;
            width: 100%;
        }

        .accordion .accordion-content p {
            padding-left: 0;
        }

        .accordion h5 {
            margin-bottom: 15px;
        }



        .half-col .form {
            margin-top: -16px;
            margin-left: 36px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <div class="sub-page-title" runat="server" id="topbanner">

        <div class="container">
        </div>
    </div>


    <div class="section-line">
        <div class="breadcrumb2">
            <%-- <a href="/"><%=Resources.Lang.Anasayfa %> | </a>  <a href="/tibbi-hizmetlerimiz/<%#WMMedikalNew.cBase.Lang%>"><%=Resources.Lang.TibbiHizmetlerimizLink %></a> --%>
        </div>
    </div>



    <div class="container content">
        <div class="accordion">
            <%-- <asp:Repeater ID="rptContactList" runat="server" OnItemDataBound="rptContactList_ItemDataBound">
                <ItemTemplate>--%>
            <div class="accordion-item secordion">
                <div class="accordion-title">
                    <h2>İletişim</h2>
                </div>
                <div class="accordion-content" style="display: block;">
                    <div class="half-col fleft">

                        <div id="map" style="width: 100%; height: 320px;"></div>
                        <h5><%=Resources.Lang.Adres %> </h5>
                        <p>
                            <asp:Literal ID="ltrAdres" runat="server"></asp:Literal>
                        </p>
                        
                        <ul class="addr-info">
                            <li>
                                <h5><%=Resources.Lang.EPosta %></h5>
                                <p>
                                    <a runat="server" id="lbleposta"></a>
                                </p>
                            </li>
                            <li>
                                <h5><%=Resources.Lang.CagriMerkezi %></h5>
                                <p>
                                    <asp:Label ID="lblCallCenter" runat="server" Text="Label"></asp:Label>
                                </p>
                            </li>
                            <li>
                                <h5><%=Resources.Lang.Telefon %></h5>
                                <p>
                                    <asp:Label ID="lblTelefon" runat="server" Text="Label"></asp:Label>
                                </p>
                            </li>
                        </ul>

                        <h3>Ulaşım</h3>
                        <p><strong>İstinye &Uuml;niversite Hastanesi</strong>&nbsp;İstanbul&rsquo;un en merkezi noktalarından birinde yer alıyor. &Uuml;&ccedil;&uuml;nc&uuml; Havalimanı, Yavuz Sultan Selim K&ouml;pr&uuml;s&uuml;, TEM ve E5 bağlantı yollarına yakınlığıyla &ccedil;ok sayıda ulaşım se&ccedil;eneğine sahip olan hastane, Akbatı AVM/Residence karşısındaki lokasyonuyla da toplu ulaşımın en kolay adreslerinden birinde hizmet veriyor.</p>

                        <div class="send_message_acc"></div>
                    </div>
                    <div class="half-col fleft">
                        <div class="form" id="mesajSend">
                            <asp:HiddenField ID="hdnActiveTab" runat="server" />
                            <div class="column">
                                <div class="form-group">
                                    <label for="iname" class="input-label"><%=Resources.Lang.AdSoyad %> *</label>

                                    <asp:TextBox runat="server" ID="txtNameSurname" CssClass="itext"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="req_txtNameSurname" ControlToValidate="txtNameSurname" Width="200px" Style="margin-left: 130px;"
                                        ErrorMessage="'ad soyad' alanı zorunludur." ForeColor="Red" runat="server"></asp:RequiredFieldValidator>
                                </div>
                                <div class="form-group">
                                    <label for="imail" class="input-label"><%=Resources.Lang.EPosta %> *</label>

                                    <asp:TextBox runat="server" ID="txtEMail" CssClass="itext"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="req_txtEmail" ControlToValidate="txtEmail" Width="200px" Style="margin-left: 130px;"
                                        ErrorMessage="'e-posta' alanı zorunludur." ForeColor="Red" runat="server"></asp:RequiredFieldValidator>
                                </div>


                                <div class="form-group">
                                    <label for="iinterest" class="input-label"><%=Resources.Lang.Konu %> </label>

                                    <asp:DropDownList runat="server" ID="drpSubjectList" CssClass=" select-alt" Style="width: 270px">
                                        <asp:ListItem Text="Seçiniz" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Şikayet" Value="Şikayet"></asp:ListItem>
                                        <asp:ListItem Text="İstek" Value="İstek"></asp:ListItem>
                                        <asp:ListItem Text="Teşekkür" Value="Teşekkür"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                            </div>

                            <div class="column">

                                <div class="form-group">
                                    <label for="itel" class="input-label"><%=Resources.Lang.Telefon %></label>

                                    <asp:TextBox runat="server" ID="txtPhone" CssClass="itext"></asp:TextBox>
                                    <br />
                                    <br />
                                    <br />
                                </div>

                                <div class="form-group">
                                    <label for="isurname" class="input-label"><%=Resources.Lang.Meslek %> </label>
                                    <asp:TextBox runat="server" ID="txtJob" CssClass="itext"></asp:TextBox>
                                    <br />
                                    <br />
                                </div>
                            </div>

                            <div class="form-group clear">
                                <label for="imsg" class="input-label"><%=Resources.Lang.Mesajiniz %> </label>
                                <asp:TextBox ID="txtMessage" CssClass="itext" TextMode="MultiLine" Rows="3" runat="server"></asp:TextBox>
                                <br />
                                <br />
                            </div>

                            <div class="form-group clear">
                                <asp:ScriptManager runat="server"></asp:ScriptManager>
                                <label for="imsg" class="input-label"><%=Resources.Lang.GuvenlikKodu %>  </label>
                                <asp:UpdatePanel ID="upCaptcha" runat="server">
                                    <ContentTemplate>

                                        <div class="g-recaptcha" data-sitekey="6LfIfRoUAAAAAMIgFhtRof0_p0An5M_UxOwbLR-c"></div>
                                        <%-- <table>
                                            <tr>
                                                <td>
                                                    <asp:Image ID="imgCaptcha" BorderWidth="1px" CssClass="captcha-image" BorderColor="DarkBlue" runat="server" />
                                                </td>
                                                <td style="margin-left: 11px; margin-right: -15px;">
                                                    <asp:ImageButton ID="btnYenile" ValidationGroup="2" ImageUrl="/assets/img/captcha-refresh.png" runat="server"
                                                        CssClass="captcha-button" OnClick="btnYenile_Click" />
                                                </td>
                                                <td colspan="2">
                                                    <asp:TextBox ID="txtCaptcha" runat="server" CssClass="itext" Width="98px"></asp:TextBox></td>
                                           
                                                <td>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtCaptcha" Width="200px" Style="margin-left: 5px;"
                                                        ErrorMessage="'Güvenlik Kodu' alanı zorunludur." ForeColor="Red" runat="server"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="RequiredFieldValidator1" ControlToValidate="txtCaptcha" ControlToCompare="hdnCaptcha" Width="200px"
                                                        Style="margin-left: 5px;" ErrorMessage="Güvenlik Kodu doğrulanamadı!" runat="server"></asp:CompareValidator>
                                                </td>
                                            </tr>
                                        </table>--%>
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                            </div>
                            <br />
                            <div class="clear align-center">
                                <asp:Button runat="server" ID="bntSend" CssClass="btn btn-type2" OnClick="bntSend_Click" />
                                <br />
                                <br />
                                <asp:Label ID="ltrlThanks" runat="server"></asp:Label>
                            </div>
                            <br />
                            <br />
                        </div>
                    </div>
                    <div>

                        <%--<p>
                            <strong>İstinye &Uuml;niversitesi Sağlık Uygulama ve Araştırma Merkezi</strong>&nbsp;<br />
                            Aşık Veysel Mah. S&uuml;leyman Demirel Cad. No:1 Esenyurt-İstanbul Davutpaşa Vd. 481 061 1692&nbsp;<br />
                            <strong>&Ccedil;ağrı Merkezi:</strong>&nbsp;<a href="tel:+902124446623">4446623</a>&nbsp;<br />
                            <strong>Tel:</strong>&nbsp;<a href="tel:+902129794000">0212 979 40 00</a>
                        </p>--%>
                        <style type="text/css">
                            .google-maps {
                                position: relative;
                                padding-bottom: 75%;
                                // This is the aspect ratio height: 0;
                                overflow: hidden;
                            }

                                .google-maps iframe {
                                    position: absolute;
                                    top: 0;
                                    left: 0;
                                    width: 100% !important;
                                    height: 100% !important;
                                    max-width: 500px;
                                    max-height: 500px;
                                }
                        </style>
                        <div class="google-maps">
                            <iframe allowfullscreen="" frameborder="0" height="450" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12034.323466704978!2d28.6720956!3d41.056294!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7d20f9384168951f!2s%C4%B0stinye+%C3%9Cniversitesi+Liv+Hastanesi!5e0!3m2!1str!2str!4v1490104126958" style="border: 0" width="600"></iframe>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <script type="text/javascript">

            var map;
            var cen = new google.maps.LatLng(<asp:Literal ID="ltrMetrics34Page" runat="server" />);
            function initMap() {
                var mapProp = {
                    center: cen,
                    zoom: 12,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,

                };
                map = new google.maps.Map(document.getElementById("map"), mapProp);
                placeMarker(map, cen);

            }
            function placeMarker(map, location) {
                var marker = new google.maps.Marker({
                    position: location,
                    map: map,

                });

                var infowindow = new google.maps.InfoWindow({

                    content: "İstinye Üniversitesi Hastanesi"
                });
                infowindow.open(map, marker);
            }

            $(document).ready(function () {

                initMap();



            });
        </script>
        <%--                </ItemTemplate>
            </asp:Repeater>--%>
        <%--   <script>
                $(document).ready(function () {
                    var asd = $("#mesajSend").clone();
                    $(".send_msg").remove();
                    $(".send_message_acc").append(asd)
                })
            </script>--%>


        <%--<div class="accordion-item send_msg">
                <div class="accordion-title">
                    Mesaj Gönder
                            <br />
                    <img src="/assets/img/site/ico-accordion-arrow.png" alt="" />
                </div>
                <div class="accordion-content">
                </div>
            </div>--%>
        <div id="divMessageResult" runat="server" style="text-align: center; color: green; font-weight: bold; font-size: 15px; padding: 5px; display: none;"></div>
    </div>
    </div>

    <script src="/assets/js/plugins.js"></script>
    <script src="/assets/js/liv_global.js"></script>



</asp:Content>
