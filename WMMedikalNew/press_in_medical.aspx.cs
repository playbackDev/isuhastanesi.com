﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class WebForm3 :cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var PressList = db.press.Where(q=> q.is_active).ToList();

            rptPress.DataSource = PressList;
            rptPress.DataBind();
        }
    }
}