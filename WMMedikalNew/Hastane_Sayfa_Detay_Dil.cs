//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WMMedikalNew
{
    using System;
    using System.Collections.Generic;
    
    public partial class Hastane_Sayfa_Detay_Dil
    {
        public int id { get; set; }
        public string video_url { get; set; }
        public string description { get; set; }
        public string lang { get; set; }
        public Nullable<int> sayfa_id { get; set; }
        public string top_banner { get; set; }
        public string meta_description { get; set; }
        public string page_url { get; set; }
    
        public virtual Hastane_Sayfalar Hastane_Sayfalar { get; set; }
    }
}
