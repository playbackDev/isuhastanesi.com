﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class our_hospital1 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //var HospitalList = db.our_hospital_lang.Select(s => new
            //{
            //    s.id,
            //    s.hospital_name,
            //    s.our_hospital.hospital_image,
            //    s.our_hospital.is_active,
            //    s.lang,
            //    PageID =s.our_hospital.pages.Where(q2=> q2.page_name == "Hastanemiz").FirstOrDefault().id
            //}).Where(q => q.is_active && q.lang == Lang).ToList();

            var HospitalList = db.Hastaneler.Select(s => new { 
            s.HastaneAdi,
            s.id,
            sayfa_id = s.Hastane_Sayfalar.FirstOrDefault(f=> f.sayfa_tipi == 1).id,
            sayfa_adi = s.Hastane_Sayfalar.FirstOrDefault(f=> f.sayfa_tipi == 1).sayfa_adi

            }).ToList();
        

            if(HospitalList.Count > 0)
            {
                rptHospital.DataSource = HospitalList;
             rptHospital.DataBind();

            }
            
        }
    }
}