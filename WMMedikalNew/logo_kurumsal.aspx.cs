﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class WebForm2 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var LogoList = db.logo.Where(q => q.is_active).ToList();

            if (LogoList.Count > 0)
            {
                rptLogo.DataSource = LogoList;
                rptLogo.DataBind();
            }
        }
    }
}