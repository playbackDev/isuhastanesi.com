﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class photo_gallery : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var _galeri = db.gallery.Where(q => q.is_active && q.page_id == null).ToList();

            if (_galeri.Count > 0)
            {
                rptGaleri.DataSource = _galeri;
                rptGaleri.DataBind();
            }
        }
    }
}