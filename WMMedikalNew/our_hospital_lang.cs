//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WMMedikalNew
{
    using System;
    using System.Collections.Generic;
    
    public partial class our_hospital_lang
    {
        public int id { get; set; }
        public string hospital_name { get; set; }
        public string lang { get; set; }
        public string hospital_detail { get; set; }
        public int hospital_id { get; set; }
    
        public virtual our_hospital our_hospital { get; set; }
    }
}
