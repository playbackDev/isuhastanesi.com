﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class WebForm16 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetSearchResultList();
        }


        protected void GetSearchResultList()
        {
            if (Request.QueryString["text"] != null)
            {
                string strSearch = Request.QueryString["text"].ToString();

                List<SearchResultList> srList = new List<SearchResultList>();


                #region doktorlarımız

                List<DoctorListModel> doctorList = db.our_doctors.Select(s => new DoctorListModel
                {
                    id = s.id,
                    name_surname = s.name_surname,
                    description = s.our_doctors_lang.FirstOrDefault().description,
                    title = s.our_doctors_lang.FirstOrDefault().title,
                    is_active = s.is_active
                }).Where(q => (q.name_surname.Contains(strSearch)
                || q.title.Contains(strSearch)
                || q.description.Contains(strSearch))
                && q.is_active).ToList<DoctorListModel>();

                foreach (DoctorListModel item_doctor in doctorList)
                {
                    SearchResultList sr = new SearchResultList()
                    {
                        Link = "/doktorlarimiz/" + item_doctor.name_surname.ToURL() + "-" + item_doctor.id + "/" + Lang,
                        Parameters = "/" + ReplaceText(item_doctor.title + " " + item_doctor.name_surname) + "/" + item_doctor.id.ToString(),
                        Title = item_doctor.title + " " + item_doctor.name_surname
                    };
                    srList.Add(sr);
                }

                #endregion

                #region tıbbi hizmetlerimiz

                List<MedicalServicesListModel> servicesList = db.our_medical_services.Select(s => new MedicalServicesListModel
                {
                    id = s.id,
                    services_name = s.medical_service_name,
                    service_description = s.our_medical_services_lang.FirstOrDefault().service_description,
                    is_active = s.our_medical_services_lang.FirstOrDefault().is_active

                }).Where(q => (q.services_name.Contains(strSearch)
              || q.service_description.Contains(strSearch))
              && q.is_active).ToList<MedicalServicesListModel>();

                foreach (MedicalServicesListModel item_services in servicesList)
                {
                    SearchResultList sr = new SearchResultList()
                    {

                        Link = "/detay/tibbi-hizmetlerimiz/" + item_services.services_name.ToURL() + "-" + item_services.id + "/" + Lang,
                        Parameters = "/" + ReplaceText(item_services.services_name) + "/" + item_services.id.ToString(),
                        Title = item_services.services_name
                    };
                    srList.Add(sr);
                }

                #endregion

                #region haberler

                List<NewsListModel> newsList = db.news.Select(s => new NewsListModel
                {
                    id = s.id,
                    news_title = s.news_lang.FirstOrDefault().news_title,
                    news_content = s.news_lang.FirstOrDefault().news_content,
                    is_active = s.news_lang.FirstOrDefault().is_active

                }).Where(q => (q.news_title.Contains(strSearch)
                  || q.news_content.Contains(strSearch))
                  && q.is_active).ToList<NewsListModel>();

                foreach (NewsListModel item_news in newsList)
                {
                    SearchResultList sr = new SearchResultList()
                    {
                        Link = "haberler/" + Lang,
                        Parameters = "/" + ReplaceText(item_news.news_title) + "/" + item_news.id.ToString(),
                        Title = item_news.news_title
                    };
                    srList.Add(sr);
                }

                #endregion


                rptSearchList.DataSource = srList;
                rptSearchList.DataBind();

            }
            else
                Response.Redirect("/",false);
        }

        public string ReplaceText(string text)
        {
            string replaceText = string.Empty;
            if (text != "")
            {
                replaceText = text.ToLower().Replace("ç", "c").Replace("Ç", "C").Replace("ğ", "g").Replace("Ğ", "G").
                                             Replace("ı", "i").Replace("İ", "I").Replace("ö", "o").Replace("Ö", "O").
                                             Replace("ş", "s").Replace("Ş", "S").Replace("ü", "u").Replace("Ü", "U").
                                             Replace("  ", "-").Replace(" ", "-");
            }
            return replaceText;
        }

        public class SearchResultList
        {
            public string Link { get; set; }
            public string Parameters { get; set; }
            public string Title { get; set; }
        }

        public class DoctorListModel
        {
            public int id { get; set; }
            public string name_surname { get; set; }
            public string description { get; set; }
            public string title { get; set; }
            public bool is_active { get; set; }
        }

        public class MedicalServicesListModel
        {
            public int id { get; set; }
            public string services_name { get; set; }
            public string service_description { get; set; }
            public bool is_active { get; set; }
        }

        public class NewsListModel
        {
            public int id { get; set; }
            public string news_title { get; set; }
            public string news_content { get; set; }
            public bool is_active { get; set; }
        }
    }
}