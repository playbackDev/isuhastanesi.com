﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class eventDetails : cBase
    {
        int NewsID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["news_id"] != null && Request.QueryString["news_id"] != "")
            {
                NewsID = Request.QueryString["news_id"].toint();

                var _eventList = db.news_lang.FirstOrDefault(q => q.is_active && q.news.type == 2 && q.id == NewsID);
                txtTitle.InnerText = _eventList.news_title;
                txtTime.InnerText = _eventList.created_time.ToString();
                lblContent.Text = _eventList.news_content;
                SetMetaTags(this.Page, _eventList.news.seoPageTitle, _eventList.news.seoMetaDesc, _eventList.news.seoKeywords);

                if (_eventList.news.bigImg != null || _eventList.news.bigImg != "")
                    ltrImgBig.Text = "<img src='" + _eventList.news.bigImg + "' />";
            }
            //var _eventList2 = db.news.FirstOrDefault(q => q.type == 2);

            //if (_eventList2.img_path != null && _eventList2.img_path != "")
            //{
            //    topbanner.Style.Add("background-image", "../" + _eventList2.img_path);
            //    topbanner.Style.Add("background-position-y", "-31px");
            //}
            //else
            //    topbanner.Style.Add("height", "0");
        }
    }
}