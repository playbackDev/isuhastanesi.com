﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WMMedikalNew.LivHospitalERandevu1;
using WMMedikalNew.LivHospitalESonuc1;

namespace WMMedikalNew
{
    public partial class WebForm5 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                getMedicalServices();
            }

            DataLoad();

            var _HakkimizdDetay = db.Hastane_Sayfalar_Dil.Select(s => new
            {
                s.sayfa_adi,
                s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == "TR").description,
                s.sayfa_id,
                s.Hastane_Sayfalar.Hastaneler.HastaneAdi,
                hastane_id = s.Hastane_Sayfalar.Hastaneler.id,
                s.dil,
                s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == "TR").top_banner,
                s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == "TR").meta_description,
                GalleryImage = s.Hastane_Sayfalar.gallery.Select(s2 => new
                {
                    ImageID = s2.id,
                    s2.image_name,
                    active_status = (s2.is_active) ? "Aktif" : "Pasif",
                    s2.Hastaneler.HastaneAdi
                })
            }).FirstOrDefault(q => q.sayfa_id == 3);
        }

        private void getMedicalServices()
        {
            var _medicalServiceList = db.TibbiBolumler_Dil.Where(q => q.Dil == "TR" && (q.TibbiBolumler.Doktorlar.Count > 0 || q.TibbiBolumler.doktorlar_ek_bolumler.Count > 0)).OrderBy(s => s.BolumAdi).ToList();

            if (_medicalServiceList != null)
            {
                dlFilters.DataSource = _medicalServiceList;
                dlFilters.DataTextField = "BolumAdi";
                dlFilters.DataValueField = "BolumID";
                dlFilters.DataBind();
                dlFilters.Items.Insert(0, new ListItem("Tıbbi birim seçin", "0"));
            }
        }
        private void DataLoad(int BirimID = 0)
        {
            try
            {
                if (BirimID == 0)
                {
                    #region All Doctors
                    var all_doctors = db.Doktorlar_Dil
                        .Where(q => q.Doktorlar.is_active)
                        .Select(s => new
                        {
                            s.doktor_id,
                            DoktorServisID = s.Doktorlar.ServisID,
                            BolumServisID = s.Doktorlar.TibbiBolumler.ServisID,
                            HastaneServisID = s.Doktorlar.Hastaneler.ServisHastaneID,
                            s.Doktorlar.Unvan,
                            s.Doktorlar.DoktorAdi,
                            s.Doktorlar.ResimAdi,
                            s.dil,
                            s.Doktorlar.HastaneID,
                            s.Doktorlar.TibbiBolumID,
                            s.Doktorlar.Hastaneler.HastaneAdi,
                            s.Doktorlar.is_active,
                            s.Doktorlar.TibbiBolumler.TibbiBolumler_Dil.Where(q => q.Dil == "TR").FirstOrDefault().BolumAdi,
                            s.Doktorlar.displayOrder,
                            ekbolumler = s.Doktorlar.doktorlar_ek_bolumler
                                .Select(s2 => new
                                {
                                    s2.TibbiBolumler.BolumAdi
                                })
                        });


                    var results = all_doctors.Where(q => q.dil == "TR").OrderBy(o => o.BolumAdi).ThenBy(o => o.displayOrder).ToList();

                    if (results.Count > 0)
                    {
                        ltrUyari.Text = "";
                        rptDoctorList.DataSource = results;
                        rptDoctorList.DataBind();
                    }
                    else
                    {
                        ltrUyari.Text = "Sonuç Bulunamadı";
                        rptDoctorList.ClearData();
                    }
                    #endregion
                }
                else
                {
                    #region Filtered Doctors
                    var all_doctors = db.Doktorlar_Dil
                        .Where(q => q.Doktorlar.is_active && (q.Doktorlar.TibbiBolumID == BirimID || q.Doktorlar.doktorlar_ek_bolumler.Where(q2 => q2.bolumId == BirimID).Count() > 0))
                        .Select(s => new
                        {
                            s.doktor_id,
                            DoktorServisID = s.Doktorlar.ServisID,
                            BolumServisID = s.Doktorlar.TibbiBolumler.ServisID,
                            HastaneServisID = s.Doktorlar.Hastaneler.ServisHastaneID,
                            s.Doktorlar.Unvan,
                            s.Doktorlar.DoktorAdi,
                            s.Doktorlar.ResimAdi,
                            s.dil,
                            s.Doktorlar.HastaneID,
                            s.Doktorlar.TibbiBolumID,
                            s.Doktorlar.Hastaneler.HastaneAdi,
                            s.Doktorlar.is_active,
                            s.Doktorlar.TibbiBolumler.TibbiBolumler_Dil.Where(q => q.Dil == "TR").FirstOrDefault().BolumAdi,
                            s.Doktorlar.displayOrder,
                            ekbolumler = s.Doktorlar.doktorlar_ek_bolumler
                                .Select(s2 => new
                                {
                                    s2.TibbiBolumler.BolumAdi
                                })
                        });

                    var results = all_doctors.Where(q => q.dil == "TR").OrderBy(o => o.displayOrder).ToList();

                    if (results.Count > 0)
                    {
                        ltrUyari.Text = "";
                        rptDoctorList.DataSource = results;
                        rptDoctorList.DataBind();
                    }
                    else
                    {
                        ltrUyari.Text = "Sonuç Bulunamadı";
                        rptDoctorList.ClearData();
                    }
                    #endregion
                }
            }
            catch (Exception ex) { Response.Write(ex.Message); }
        }

        protected void lnkFiltrele_Command(object sender, CommandEventArgs e)
        {
            int MedicalID = e.CommandArgument.toint();

            try
            {
                var all_doctors = db.Doktorlar_Dil.Select(s => new
                {
                    s.doktor_id,
                    DoktorServisID = s.Doktorlar.ServisID,
                    BolumServisID = s.Doktorlar.TibbiBolumler.ServisID,
                    HastaneServisID = s.Doktorlar.Hastaneler.ServisHastaneID,
                    s.Doktorlar.Unvan,
                    s.Doktorlar.DoktorAdi,
                    s.Doktorlar.ResimAdi,
                    s.dil,
                    s.Doktorlar.HastaneID,
                    s.Doktorlar.TibbiBolumID,
                    s.Doktorlar.Hastaneler.HastaneAdi,
                    s.Doktorlar.is_active,
                    s.Doktorlar.TibbiBolumler.BolumAdi,
                    s.Doktorlar.displayOrder
                });


                if (MedicalID == 0)
                {
                    all_doctors = all_doctors.OrderBy(o => o.displayOrder);
                }
                else
                {
                    all_doctors = all_doctors.Where(q => q.TibbiBolumID == MedicalID).OrderBy(o => o.displayOrder);
                }

                var results = all_doctors.Where(q => q.dil == "TR").OrderBy(o => o.displayOrder).ToList();

                if (results.Count > 0)
                {
                    ltrUyari.Text = "";
                    rptDoctorList.DataSource = results;
                    rptDoctorList.DataBind();
                }
                else
                {
                    ltrUyari.Text = "Sonuç Bulunamadı";
                    rptDoctorList.ClearData();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }

        }

        protected void dlFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataLoad(dlFilters.SelectedValue.toint());
            }
            catch { }
        }
    }
}