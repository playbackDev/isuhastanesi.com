﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="doctor_detail.aspx.cs" Inherits="WMMedikalNew.WebForm6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--    <div class="sub-page-title" runat="server" id="topbanner">

        <div class="container">
        </div>
    </div>--%>

    <div class="section-line">
        <div class="breadcrumb2">
            <a href="/">Anasayfa | </a><a href="/doktorlarimiz">Doktorlarımız | </a>
            <asp:HyperLink ID="hlDoctorBC" runat="server" />
        </div>
    </div>

    <div class="container content">


        <div class="profile-section" style="display: inline-table;">

            <div class="doktor-img">
                <asp:Literal ID="ltrImage" runat="server"></asp:Literal>
            </div>
            <div class="doctor-desc">
                <h1>
                    <asp:Literal ID="ltrTitle" runat="server"></asp:Literal>
                    <asp:Literal ID="ltrNameSurname" runat="server"></asp:Literal></h1>
                <h2>
                    <asp:Literal ID="ltrMedical" runat="server"></asp:Literal></h2>
                <p>
                    <asp:Literal ID="ltrEmail" runat="server"></asp:Literal>
                </p>
                <p>
                    <asp:Literal ID="ltrDescription" runat="server"></asp:Literal>
                </p>
                <asp:LinkButton Text="Randevu Al" runat="server" CssClass="randevu-al_btn" OnClick="Unnamed_Click" />
                <br /><br />
            </div>

        </div>
        <style>
            .description {
                margin: 1em auto 2.25em;
            }


            h1 {
                /*font-family: 'Pacifico', cursive;*/
                font-weight: 400;
                font-size: 2.5em;
            }

            

            .inner {
                /*padding-left: 0;*/
                overflow: hidden;
                /*display: none;*/
                width: 101%;
                margin: -10px 0 -10px -30px !important;
                padding: 20px 0 20px;
                background: rgba(255,255,255,.5);
            }

                .inner .show {
                    /*display: block;*/
                }


            .accordion li {
                margin: .5em 0;
                list-style: none;
            }

            a.toggle {
                width: 100%;
                /*display: block;*/
                background: #7b262a/*eaeaea*/;
                color: #f2f2f2/*7b262a*/;
                padding: .75em;
                border-radius: 0.15em;
                transition: background .5s ease;
                margin: 0 auto;
                float: right;
                margin: 5px 12px;
                text-align: center;
                font-size: 14px;
                border: 1px solid #90032B;
            }

                a.toggle:hover {
                    background: #7b262a;
                    transition: background .5s ease;
                    color: #f2f2f2;
                    cursor: default;
                }
        </style>
        <div class="descr-section">
            <div class="col-1">

                <ul class="accordion">
                    <li runat="server" id="uzmanlikalanlari" visible="false">
                        <a class="toggle" href="javascript:void(0);"><%=Resources.Lang.UzmanlikAlanlari %></a>
                        <div class="inner">
                            <div class="dr-descr">
                                <div class="dr-descr">
                                    <p>
                                        <asp:Literal ID="ltrExpertise" runat="server"></asp:Literal></p>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li runat="server" id="egitim" visible="false">
                        <a class="toggle" href="javascript:void(0);"><%=Resources.Lang.Egitim %></a>
                        <div class="inner">
                            <div class="dr-descr">
                                <p>
                                    <asp:Literal ID="ltrEducation" runat="server"></asp:Literal></p>
                            </div>
                        </div>
                    </li>

                    <li runat="server" id="deneyim" visible="false">
                        <a class="toggle" href="javascript:void(0);"><%=Resources.Lang.Deneyim %></a>
                        <div class="inner">
                            <div class="dr-descr">
                                <p>
                                    <asp:Literal ID="ltrExperience" runat="server"></asp:Literal></p>
                            </div>
                        </div>
                    </li>

                    <li id="oduller" visible="false" runat="server">
                        <a class="toggle" href="javascript:void(0);"><%=Resources.Lang.Oduller %></a>
                        <div class="inner">
                            <div class="dr-descr">
                                <p>
                                    <asp:Literal ID="ltrAwards" runat="server"></asp:Literal></p>
                            </div>
                        </div>
                    </li>

                    <li id="uyelikler" visible="false" runat="server">
                        <a class="toggle" href="javascript:void(0);"><%=Resources.Lang.Uyelikler %></a>
                        <div class="inner">
                            <div class="dr-descr">
                                <p>
                                    <asp:Literal ID="ltrMemberShip" runat="server"></asp:Literal></p>
                            </div>
                        </div>
                    </li>


                    <li id="sertifikalar" visible="false" runat="server">
                        <a class="toggle" href="javascript:void(0);"><%=Resources.Lang.Sertifikalar %></a>
                        <div class="inner">
                            <div class="dr-descr">
                                <p>
                                    <asp:Literal ID="ltrCert" runat="server"></asp:Literal></p>
                            </div>
                        </div>
                    </li>

                    <li runat="server" id="yayinlar" visible="false">
                        <a class="toggle toggle2" href="javascript:void(0);">
                            <img src="/assets/img/arr-down.png" class="research" /> 
                            <%=Resources.Lang.Yayinlar %> 
                            <img src="/assets/img/arr-down.png" class="research" />
                        </a>
                        <div class="inner hide-temp">
                            <div class="dr-descr">
                                <p>
                                    <asp:Literal ID="ltrResource" runat="server"></asp:Literal></p>
                            </div>
                        </div>
                    </li>

                </ul>
            </div>
            
            <script>
                $(document).ready(function () {
                    $('.mh_show_btn').click(function () {
                        $('.mh_hidden').addClass('mh_show');
                        $(this).hide();
                        $('.mh_hide_btn').show();
                    });
                    $('.mh_hide_btn').click(function () {
                        $('.mh_hidden').removeClass('mh_show');
                        $(this).hide();
                        $('.mh_show_btn').show();
                    });
                });
                $('.toggle2').click(function (e) {
                    e.preventDefault();

                    var $this = $(this);

                    if ($this.next().hasClass('show')) {
                        $this.next().removeClass('show');
                        $(".research").attr("src", "/assets/img/arr-down.png");
                        $this.next().slideUp(350);
                    } else {
                        //$this.parent().parent().find('li .inner').removeClass('show');
                        //$this.parent().parent().find('li .inner').slideUp(350);
                        $this.next().toggleClass('show');
                        $(".research").attr("src", "/assets/img/arr-up.png");
                        $this.next().slideToggle(350);
                    }
                });
            </script>
            <style>
                .mh_hidden {
                    max-height: 275px;
                    overflow: hidden;
                    padding-bottom: 50px;
                    border-bottom: 35px solid #fae0e7;
                    position: relative;
                    transition: ease all .5s;
                }

                .mh_show {
                    max-height: initial;
                    transition: ease all .5s;
                }

                .mh_show_btn {
                    position: absolute;
                    bottom: 5px;
                    text-decoration: underline;
                    right: 5px;
                }

                .mh_hide_btn {
                    position: absolute;
                    bottom: 5px;
                    text-decoration: underline;
                    right: 5px;
                    right: 5px;
                }

                .inner {
                    padding-left: 1em;
                    overflow: hidden;
                    /*display: none;*/
                }

                .hide-temp {
                    display: none;
                }
            </style>
            <div class="col-1">
                <div style="position: relative; width: 33%; display: inline-block; margin-left: .66%">
                </div>

            </div>

        </div>




    </div>



</asp:Content>
