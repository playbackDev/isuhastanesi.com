﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class eventList : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var _eventList = db.news_lang.Where(q => q.is_active && q.lang == Lang && q.news.type == 2)
              .Select(s => new
              {
                  s.id,
                  s.news.news_image,
                  s.news_content,
                  s.news_title,
                  s.created_time,
                  s.news.seoURL
              }).ToList();

            if (_eventList.Count > 0)
            {
                rptEventList.DataSource = _eventList;
                rptEventList.DataBind();
            }

            //var _eventList2 = db.news.FirstOrDefault(q => q.type == 2);

            //if (_eventList2.img_path != null && _eventList2.img_path != "")
            //{
            //    topbanner.Style.Add("background-image", "../" + _eventList2.img_path);
            //    topbanner.Style.Add("background-position-y", "-31px");
            //}
            //else
            //    topbanner.Style.Add("height", "0");


        }

        protected void rptEventList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                Literal _img = (Literal)e.Item.FindControl("ltrImg");
                if (DataBinder.Eval(e.Item.DataItem, "news_image").ToString() != string.Empty)
                    _img.Text = "<img src='"+ DataBinder.Eval(e.Item.DataItem, "news_image") + "' width='240' style='min-height: 170px;' />";
            }
            catch { }
        }
    }
}