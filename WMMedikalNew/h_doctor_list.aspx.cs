﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class WebForm10 : cBase
    {
        int PageID;
        int HospitalID;
        int ParentID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["page_id"] != "")
            {
                PageID = Request.QueryString["page_id"].toint();

                var PageDetail = db.Hastane_Sayfalar.Select(s => new
                {
                    s.hastane_id,
                    s.id,
                    s.Hastane_Sayfalar_Dil.FirstOrDefault().dil
                    ,s.Hastaneler.HastaneAdi,
                     s.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == Lang).top_banner,
                     s.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == Lang).meta_description
   
                }).Where(q => q.id == PageID && q.dil == Lang).FirstOrDefault();

                if (PageDetail !=null)
                {
                    HospitalID = PageDetail.hastane_id.toint();
                    ltrHospitalName.Text = PageDetail.HastaneAdi;

                    if (PageDetail.top_banner != null && PageDetail.top_banner != "")
                    {
                        topbanner.Style.Add("background-image", PageDetail.top_banner);
                        topbanner.Style.Add("background-position-y", "-31px");
                    }
                    else
                        topbanner.Style.Add("height", "0");

                    pagesubmenu.HospitalID = PageDetail.hastane_id.toint();
                   
                  
                }

                DataLoad("");

                if (!IsPostBack)
                {
                    LoadLetters();

                    var _ourMedical = db.TibbiBolumler_Dil.Select(s => new
                    {
                        s.BolumAdi,
                        s.BolumID,
                        s.Dil
                    }).Where(q => q.Dil == Lang).ToList();

                    if (_ourMedical.Count > 0)
                    {
                        drpMedical.DataSource = _ourMedical;
                        drpMedical.DataTextField = "BolumAdi";
                        drpMedical.DataValueField = "BolumID";
                        drpMedical.DataBind();
                        drpMedical.Items.Insert(0, new ListItem(Resources.Lang.BolumSec, "0"));
                    }
                }
            }
        }

        private void DataLoad(string letter)
        {
            try
            {

                var all_doctors = db.Doktorlar_Dil.Select(s => new
                {
                    s.doktor_id,
                    DoktorServisID=s.Doktorlar.ServisID,
                    BolumServisID = s.Doktorlar.TibbiBolumler.ServisID,
                    HastaneServisID= s.Doktorlar.Hastaneler.ServisHastaneID,
                    s.Doktorlar.Unvan,
                    s.Doktorlar.DoktorAdi,
                     s.Doktorlar.ResimAdi,
                    s.dil,
                    s.Doktorlar.HastaneID,
                    s.Doktorlar.TibbiBolumID,
                    s.Doktorlar.is_active,
                    s.Doktorlar.Hastaneler.HastaneAdi,
                    s.Doktorlar.TibbiBolumler.TibbiBolumler_Dil.Where(q => q.Dil == Lang).FirstOrDefault().BolumAdi
                });


                if (letter != "")
                {
                    all_doctors = all_doctors.Where(w => w.DoktorAdi.StartsWith(letter));

                }
                else
                {
                    if (drpMedical.SelectedValue.toint() > 0)
                    {
                        int selected_medical = drpMedical.SelectedValue.toint();

                        all_doctors = all_doctors.Where(q => q.TibbiBolumID == selected_medical);
                    }


                    if (drpDoctors.SelectedValue.toint() > 0)
                    {
                        int selected_doctor = drpDoctors.SelectedValue.toint();
                        all_doctors = all_doctors.Where(q => q.doktor_id == selected_doctor);
                    }

                }

                var results = all_doctors.Where(q => q.dil == Lang && q.HastaneID == HospitalID).ToList();

                if (results.Count > 0)
                {
                    rptDoctorList.DataSource = results;
                    rptDoctorList.DataBind();
                }
                else
                {
                    ltrUyari.Text = "Sonuç Bulunamadı";
                    rptDoctorList.ClearData();
                }
            }
            catch(Exception ex)
            {
                Response.Write(ex);
            }
        }

        protected void btnLetters_Command(object sender, CommandEventArgs e)
        {
            DataLoad(e.CommandArgument.ToString());
        }
        protected void drpMedical_SelectedIndexChanged(object sender, EventArgs e)
        {
            int MedicalID = drpMedical.SelectedValue.toint();

            var _doctorList = db.Doktorlar_Dil.Select(s => new
            {
                s.doktor_id,
                s.Doktorlar.DoktorAdi,
                s.dil,
                s.Doktorlar.HastaneID,
                s.Doktorlar.TibbiBolumID,
                s.Doktorlar.Hastaneler.HastaneAdi,
                s.Doktorlar.TibbiBolumler.TibbiBolumler_Dil.Where(q => q.Dil == Lang).FirstOrDefault().BolumAdi
            }).Where(q => q.TibbiBolumID == MedicalID && q.HastaneID == HospitalID && q.dil == Lang).ToList();

            if (_doctorList.Count > 0)
            {
                drpDoctors.DataSource = _doctorList;
                drpDoctors.DataTextField = "DoktorAdi";
                drpDoctors.DataValueField = "doktor_id";
                drpDoctors.DataBind();
                drpDoctors.Items.Insert(0, new ListItem(Resources.Lang.DoktorSec, "0"));
            }

          }

        public void LoadLetters()
        {
            string[] lets = new string[] { "A", "B", "C", "Ç", "D", "E", "F", "G", "Ğ", "H", "I", "İ", "J", "K", "L", "M", "N", "O", "Ö", "P", "R", "S", "Ş", "T", "U", "Ü", "V", "Y", "Z" };
            var harf_listesi = new List<LETTERS>();
            foreach (var item in lets)
            {
                harf_listesi.Add(new LETTERS { letter = item });
            }

            rptLetters.DataSource = harf_listesi;
            rptLetters.DataBind();
        }

        protected class LETTERS
        {
            public string letter { get; set; }
        }

    

    }
}