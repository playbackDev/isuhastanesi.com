﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class contact :cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                //string captchaCode = GetRandomText();
                //imgCaptcha.ImageUrl = "/CreateCaptcha.aspx?Code=" + captchaCode + "&ImgWidth=120&ImgHeight=44";
                //hdnCaptcha.Text = captchaCode;
                //ltrCaptcha.Text = captchaCode;
                fillPage();
                bntSend.Text = Resources.Lang.Gonder;
            }
        }

        private void fillPage()
        {
            var ContactList = db.hl_contact.Select(s => new
            {
                s.id,
                s.hospital_id,
                s.addressEN,
                s.addressTR,
                s.call_center,
                s.eposta,
                s.latlng,
                s.telephone,
                s.Hastaneler.HastaneAdi,
                s.img_path
            }).FirstOrDefault();

            lblCallCenter.Text = ContactList.call_center;
            lbleposta.InnerText = ContactList.eposta;
            ltrAdres.Text = ContactList.addressTR;
            lblTelefon.Text = ContactList.telephone;
            ltrMetrics34Page.Text = ContactList.latlng;

            //lblHastaneAdi.Text = ContactList.HastaneAdi;

            if (ContactList != null)
            {
                //pagesubmenu.HospitalID = _HakkimizdDetay.hastane_id.toint();




                if (ContactList.img_path != null && ContactList.img_path != "")
                {
                    topbanner.Style.Add("background-image", "../" + ContactList.img_path);
                    topbanner.Style.Add("background-position-y", "-31px");
                }
                else
                    topbanner.Style.Add("height", "0");




            }

        }

        protected void btnYenile_Click(object sender, EventArgs e)
        {
            //string captchaCode = GetRandomText();
            //imgCaptcha.ImageUrl = "/CreateCaptcha.aspx?Code=" + captchaCode + "&ImgWidth=120&ImgHeight=44";
            //hdnCaptcha.Text = captchaCode;
            //ltrCaptcha.Text = captchaCode;
            //txtCaptcha.Text = "";
        }
        private string GetRandomText()
        {
            StringBuilder randomText = new StringBuilder();
            string alphabets = "012345679ACEFGHKLMNPRSWXZabcdefghijkhlmnopqrstuvwxyz";
            Random r = new Random();
            for (int j = 0; j <= 4; j++)
            {
                randomText.Append(alphabets[r.Next(alphabets.Length)]);
            }
            Session["CaptchaCode"] = randomText.ToString();
            return Session["CaptchaCode"] as String;
        }

        protected void rptContactList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
             Literal ltrAdres = (Literal)e.Item.FindControl("ltrAdres");
            if(Lang =="TR")
                ltrAdres.Text = DataBinder.Eval(e.Item.DataItem, "addressTR").ToString();
            else
                ltrAdres.Text = DataBinder.Eval(e.Item.DataItem, "addressEN").ToString();
               
        }

        protected void bntSend_Click(object sender, EventArgs e)
        {
            try
            {
               
               #region bize ulaşın formu gönder
           
               // string _hospitalName = hdnActiveTab.Value.ToString();
                    
               
               
                   
                 
                    MailMessage Mesaj = new MailMessage();
                    Mesaj.From = new MailAddress("web@iauvmmedicalpark.com");// mailleri gönderen adres
                    Mesaj.To.Add("gunel.ayse7@gmail.com");

                    //Mesaj.To.Add(_contact.eposta);

                    Mesaj.Subject = "Aydın Üniversitesi Hastanesi - Bize Ulaşın Formu";
                    Mesaj.IsBodyHtml = true;
                    Mesaj.Body = "<table><tr><th style=\"text-align:left;\">Ad Soyad</th><td> : " + txtNameSurname.Text + "</td></tr>" +
                                        "<tr><th style=\"text-align:left;\">E-Posta</th><td> : " + txtEMail.Text + "</td></tr>" +
                                        "<tr><th style=\"text-align:left;\">Telefon</th><td> : " + txtPhone.Text + "</td></tr>" +
                                        "<tr><th style=\"text-align:left;\">Meslek</th><td> : " + txtJob.Text + "</td></tr>" +
                                        "<tr><th style=\"text-align:left;\">Konu</th><td> : " + drpSubjectList.SelectedValue + "</td></tr>" +
                                        "<tr><th style=\"text-align:left;\">Mesaj</th><td> : " + txtMessage.Text + "</td></tr>" +
                                 "</table>";



                    SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                    //System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential("365derece@vmmedicalpark.com.tr", "365Drc");
                    System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential("gunel.ayse7@gmail.com", "Ra2011Downey<3");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = SMTPUserInfo;
                    smtp.EnableSsl = true;
                    smtp.Send(Mesaj);

                    ltrlThanks.Text = "Formu doldurduğunuz için teşekkürler.";
                    txtNameSurname.Text = "";
                    txtEMail.Text = "";
                    txtPhone.Text = "";
                    txtJob.Text = "";
                    drpSubjectList.SelectedValue = "";
                    txtMessage.Text = "";

                

               
                #endregion


            }
            catch (Exception ex)
            {
                ltrlThanks.Text = "Lütfen daha sonra tekrar deneyiniz.";
                //divMessageResult.Style.Add("display", "block");
                //divMessageResult.Style.Add("color", "red !important");
                //divMessageResult.InnerText = ex.Message;
                Response.Write(ex);
            }
        }
    }
}