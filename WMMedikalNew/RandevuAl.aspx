﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="RandevuAl.aspx.cs" Inherits="WMMedikalNew.RandevuAl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <link rel="stylesheet" href="/assets/fancybox/jquery.fancybox.css" />
    <link href="assets/css/screen.css" rel="stylesheet" />
         <script src="assets/bootstrap/bootstrap.min.js"></script>
      <link href="assets/bootstrap/bootstrap.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">




</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="container">
            <div class="row">
                <div class="col-md-12">
        
    <div class="pnlFormContainer">
        <h2>E-Randevu</h2>
        <ul class="randevuList">
            <li>Kişisel Bilgiler</li>
            <li class="rActive">E-Randevu</li>
            <li>E-Sonuç</li>
        </ul>
        <p class="text">
            Randevu almak istediğiniz hastane, tıbbi birim ve hekimi seçiniz. Seçimlerinizin ardından, hekimin uygun takvimi üzerinden istediğiniz saat aralığına ve “Randevu Al” butonuna tıklamanız yeterlidir.
        </p>
        <form class="form-horizontal animated" id="pnlFormContainer" method="post" action="" role="form" style="">
            <div class="form-group">
                <label for="drpHastane">Hastane Seçimi</label>
                <select class="form-control" name="drpHastane" id="drpHastane">
                    <option value="">Lütfen Seçiniz</option>
              <%--      @{
                        var hospitalList = ViewBag.Hospitals as List<HospitalDto>;
                        if (hospitalList != null && hospitalList.Count > 0)
                        { 
                            foreach (var item in hospitalList)
                            {
                                <option value="@item.Id">@item.Name</option>
                            }
                        }
                    }--%>
                </select>
            </div>
            <div class="form-group">
                <label for="drpTibbiBolum">Tıbbi Bölüm Seçimi</label>
                <select class="form-control" name="drpTibbiBolum" id="drpTibbiBolum" disabled="disabled">
                    <option value="">Lütfen Seçiniz</option>
                </select>
            </div>
            <div class="form-group">
                <label for="drpDoktor">Doktor Seçimi</label>
                <select class="form-control" name="drpDoktor" id="drpDoktor" disabled="disabled">
                    <option value="">Lütfen Seçiniz</option>
                </select>
            </div>
        </form>
        <!-- /.form one -->



        <div class="calendar animated" style="position: relative;">
            <div class="pnlLoading" style="text-align: center; position: absolute; background-color: rgba(255,255,255, 0.8) !important; display: none; right: 0; left: 0; top: 0; bottom: 0; z-index: 999;">
                <img src="/Contents/img/loading.gif" alt="" />
            </div>
            <a href="#" class="nextWeek hidden" id="lastWeek">Önceki Hafta</a>
            <a href="#" class="nextWeek hidden" id="nextWeek">Sonraki Hafta</a>

            <table class="table" id="doctorCalendar">
                <thead>
                    <tr id="trHeader">
              <%--          @{
                            var firstlyDateAndDayNameList = ViewBag.FirstlyDateAndDayNames as List<DateWithDayName>;
                            if (firstlyDateAndDayNameList != null && firstlyDateAndDayNameList.Count > 0)
                            {
                                foreach (var item in firstlyDateAndDayNameList)
                                {
                                    <th class="w96"><span class="day">@item.DayName</span><span class="daydate">@item.Date</span></th>
                                }
                            }
                        }--%>
                    </tr>
                </thead>
                <tbody>
                    <tr class="tableRow">
                        <td>
                            <ul id="level_1" class="level"></ul>
                        </td>
                        <td>
                            <ul id="level_2" class="level"></ul>
                        </td>
                        <td>
                            <ul id="level_3" class="level"></ul>
                        </td>
                        <td>
                            <ul id="level_4" class="level"></ul>
                        </td>
                        <td>
                            <ul id="level_5" class="level"></ul>
                        </td>
                        <td>
                            <ul id="level_6" class="level"></ul>
                        </td>
                        <td>
                            <ul id="level_7" class="level"></ul>
                        </td>
                    </tr>
                </tbody>
            </table>
            <button type="submit" class="btn btn-default" disabled="disabled" id="btnSendForm">Randevu Al</button>
            <br />
            <br />
            <div class="pnlMessage alert alert-info" role="alert" style="display: none; font-size: 16px; cursor: pointer; ">

            </div>
        </div>
    </div>

                    </div></div></div>

<!-- /.main -->

    <script type="text/javascript">
        $(function () {
            $.mask.definitions['~'] = "[+-]";
            $("#txtPhone").mask("0(999) 999 99 99");
            $('#txtDtarihi').mask("99/99/9999");
            $("input").blur(function () {
                $("#info").html("Unmasked value: " + $(this).mask());
            }).dblclick(function () {
                $(this).unmask();
            });
        });

        $(document).ready(function () {
            //Validation
            // add the rule here
            $.validator.addMethod("valueNotEquals", function (value, element, arg) {
                return arg != value;
            }, "Value must not equal arg.");
            $("#pnlFormContainer").validate({
                rules: {
                    txtForm: {
                        valueNotEquals: "0"
                    }
                },
                messages: {
                    txtForm: "Lütfen .... alanını doldurunuz.",
                }
            });
            $("#pnlFormContainerKontrol").validate({
                rules: {
                    txtForm: {
                        valueNotEquals: "0"
                    }
                },
                messages: {
                    txtForm: "Lütfen .... alanını doldurunuz.",
                }
            });

            //ready end
        });
    </script>
    <script src="Scripts/ino.randevu.randevual.js"></script>
</asp:Content>
