﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="newsList.aspx.cs" Inherits="WMMedikalNew.news1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style>
        h1{
            font-size: 16px !important;
        }
    </style>

    <%--<div class="sub-page-title" runat="server" id="topbanner">
        <div class="container">
        </div>
    </div>--%>
    <div class="section-line">
        <div class="breadcrumb2">
            <a href="/">anasayfa | </a>
            <asp:HyperLink ID="hypBreadCrumb" runat="server">Sağlık Köşesi</asp:HyperLink>

        </div>
    </div>
    <div class="container content">
                <div class="detay-section">
                    <div class="sekmeAlani">

                        <div class="top-part">
                            <h1><asp:Literal ID="ltrTitle" runat="server" /></h1>
                        </div>


                        <%--<div class="accordion-left">
                        <img src="<%#Eval("news_image") %>" alt="">
                    </div>--%>
                        <div class="detay-part">
                            <asp:Literal ID="ltrImgBig" runat="server" />
                            <asp:Literal ID="ltrContent" runat="server" />
                            <div class=" news-trg-container" style="display: none;">
                                <img src="/assets/img/site/ico-accordion-arrow.png" alt="" style="cursor: pointer;">
                            </div>

                        </div>

                        <div class="clear"></div>
                    </div>
                </div>

    </div>
</asp:Content>
