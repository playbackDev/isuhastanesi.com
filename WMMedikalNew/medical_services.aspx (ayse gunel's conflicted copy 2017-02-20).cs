﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class WebForm4 : cBase
    {
        List<string> lets = new List<string> { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "Ö", "P", "R", "S", "T", "U", "V", "Y", "Z" };
        protected void Page_Load(object sender, EventArgs e)
        {
             
            if (!IsPostBack)
            {
                DataLoad("");
                LoadLetters();
                var _Hospitals = db.Hastaneler.ToList();
                //if (_Hospitals.Count > 0)
                //{
                //    drpHospitals.DataSource = _Hospitals;
                //    drpHospitals.DataTextField = "HastaneAdi";
                //    drpHospitals.DataValueField = "id";
                //    drpHospitals.DataBind();

                //    drpHospitals.Items.Insert(0, new ListItem(Resources.Lang.HastaneSec, "0"));
                //}
            }

        }
        private void DataLoad(string letter)
        {
            try
            {
                var _MedicalServices = db.TibbiBolumler_Dil.Select(s => new
                {
                    BolumID = s.TibbiBolumler.id,
                    s.BolumAdi,
                    s.Dil,
                    HastaneID = s.TibbiBolumler.Hastaneler.id,
                    s.TibbiBolumler.Hastaneler.HastaneAdi

                });

       
                _MedicalServices = _MedicalServices.Where(q => q.Dil == "TR");

                if (letter != "")
                {
                    _MedicalServices = _MedicalServices.Where(w => w.BolumAdi.StartsWith(letter));

                    if (_MedicalServices != null)
                    {
                        lets.Clear();
                        lets.Add(letter);
                    }

                }
                else
                {
                    //if (drpHospitals.SelectedValue.toint() > 0)
                    //{
                    //    int hospital_id = drpHospitals.SelectedValue.toint();
                    //    _MedicalServices = _MedicalServices.Where(w => w.HastaneID == hospital_id);
                    //}

                    //if (txtSearch.Text != "")
                    //{
                    //    string word = txtSearch.Text;
                    //    lets.Clear();
                    //    lets.Add(letter);
                    //    _MedicalServices = _MedicalServices.Where(w => w.BolumAdi.Contains(word));
                    //}
                }



                List<LETTERS> cats = new List<LETTERS>();


                foreach (var item in lets)
                {
                    LETTERS L = new LETTERS();

                    var letter_branches = _MedicalServices.Select(s => new BRANCH { id =s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi });

                    if (item == "C")
                    {
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith("C") || w.BolumAdi.StartsWith("Ç")).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi });
                        L.letter = "C-Ç";
                    }
                    else if (item == "G")
                    {
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith("G") || w.BolumAdi.StartsWith("Ğ")).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi });
                        L.letter = "G-Ğ";
                    }
                    else if (item == "I")
                    {
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith("I") || w.BolumAdi.StartsWith("İ")).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi });
                        L.letter = "I-İ";
                    }
                    else if (item == "O")
                    {
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith("O") || w.BolumAdi.StartsWith("Ö")).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi });
                        L.letter = "O-Ö";
                    }
                    else if (item == "S")
                    {
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith("S") || w.BolumAdi.StartsWith("Ş")).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi });
                        L.letter = "S-Ş";
                    }
                    else if (item == "U")
                    {
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith("U") || w.BolumAdi.StartsWith("Ü")).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi });
                        L.letter = "U-Ü";
                    }
                    else
                    {
                        L.letter = item;
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith(item)).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi });
                    }

                    L.services_list = letter_branches.ToList();
                    if (L.services_list.Count > 0)
                    {
                        cats.Add(L);
                    }
                  
                    
                  

                }


                if (cats.Count > 0)
                {
                    ltrUyari.Text = "";
                    rptLettersCats.DataSource = cats;
                    rptLettersCats.DataBind();
                    //rptLettersCats.BindData(cats);
                }
                else
                {
                    ltrUyari.Text = "Sonuç Bulunamadı";
                    rptLettersCats.ClearData();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        public void LoadLetters()
        {

            var harf_listesi = new List<LETTERS>();
            foreach (var item in lets)
            {
                harf_listesi.Add(new LETTERS { letter = item });
            }

            //rptLetters.BindData(harf_listesi);
        }

        protected class LETTERS
        {
            public string letter { get; set; }
            public List<BRANCH> services_list { get; set; }
        }

        protected class BRANCH
        {
            public int id { get; set; }
            public string medical_services_name { get; set; }
            public string hospital_name { get; set; }
        }

        protected void btnSearchMedUnit_Click(object sender, EventArgs e)
        {
            try
            {
                DataLoad("");
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        protected void rptBrn_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                Literal ltrLink = (Literal)e.Item.FindControl("ltrLink");
                Literal ltrUyari = (Literal)e.Item.FindControl("ltrUyari");

                int id = DataBinder.Eval(e.Item.DataItem, "id").toint() ;
                string BolumAdi = DataBinder.Eval(e.Item.DataItem, "medical_services_name").ToString();
                string HastaneAdi = DataBinder.Eval(e.Item.DataItem, "hospital_name").ToString();

                if (BolumAdi != null || BolumAdi != "" || BolumAdi != " ")
                    ltrLink.Text = "<a href="+"/"+Lang+"/detay/tibbi-hizmetlerimiz/" + BolumAdi.ToURL() + "-" + id +">" + BolumAdi + "</a>";
                else
                    ltrLink.Text = "Sonuç Bulunamadı";
                
                

            }
            catch(Exception ex)
            {
                Response.Write(ex);
            }
        }


        protected void btnLetters_Command(object sender, CommandEventArgs e)
        {
            DataLoad(e.CommandArgument.ToString());
        }


    }
}