﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class our_services : cBase
    {
        short ServiceID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"].ToString() != null && Request.QueryString["id"].ToString() != "")
            {

                ServiceID = Request.QueryString["id"].toshort();
                var ServiceDetail = db.TibbiBolumler_Dil
                    .Where(q => q.BolumID == ServiceID)
                    .Select(s => new
                    {
                        s.BolumAdi,
                        s.Aciklama,
                        s.Dil,
                        s.BolumID,
                        s.TibbiBolumler.Hastaneler.ServisHastaneID,
                        BolumServisID = s.TibbiBolumler.ServisID,
                        s.TibbiBolumler.is_manuel,
                        s.top_banner,
                        s.TibbiBolumler.seoPageTitle,
                        s.TibbiBolumler.seoMetaDesc,
                        s.TibbiBolumler.seoKeywords
                    }).FirstOrDefault();

                if (ServiceDetail != null)
                {
                    SetMetaTags(this.Page, ServiceDetail.seoPageTitle, ServiceDetail.seoMetaDesc, ServiceDetail.seoKeywords);
                    //ltrServiceName.Text = ServiceDetail.BolumAdi;
                    ltrBreadCrumbServiceName.Text = ServiceDetail.BolumAdi;
                    ltrContentServiceName.Text = ServiceDetail.BolumAdi;
                    ltrServiceNameTitle.Text = ServiceDetail.BolumAdi;
                    ltrContent.Text = ServiceDetail.Aciklama;

                    if (ServiceDetail.top_banner != null && ServiceDetail.top_banner != "")
                    {
                        topbanner.Style.Add("background-image", ServiceDetail.top_banner);
                        topbanner.Style.Add("background-position-y", "-31px");
                    }
                    else
                        topbanner.Style.Add("height", "0");

                    //hypRandevu.NavigateUrl = Lang + "/e-services/hospitalId=" + ServiceDetail.ServisHastaneID + "&branchId=" + ServiceDetail.BolumServisID + "&tab=3&source=service";

                    //if (ServiceDetail.is_manuel)
                    //    hypRandevu.Visible = false;
                    //else
                    //    hypRandevu.Visible = true;

                    DataLoad();
                }
            }
        }

        private void DataLoad()
        {
            try
            {
                var all_doctors = db.Doktorlar_Dil
                    .Where(q => q.Doktorlar.is_active && (q.Doktorlar.TibbiBolumID == ServiceID || q.Doktorlar.doktorlar_ek_bolumler.Where(q2 => q2.bolumId == ServiceID).Count() > 0))
                    .Select(s => new
                    {
                        s.doktor_id,
                        DoktorServisID = s.Doktorlar.ServisID,
                        BolumServisID = s.Doktorlar.TibbiBolumler.ServisID,
                        HastaneServisID = s.Doktorlar.Hastaneler.ServisHastaneID,
                        s.Doktorlar.Unvan,
                        s.Doktorlar.DoktorAdi,
                        s.Doktorlar.ResimAdi,
                        s.dil,
                        s.Doktorlar.HastaneID,
                        s.Doktorlar.TibbiBolumID,
                        s.Doktorlar.Hastaneler.HastaneAdi,
                        s.Doktorlar.is_active,
                        s.Doktorlar.TibbiBolumler.TibbiBolumler_Dil.Where(q => q.Dil == "TR").FirstOrDefault().BolumAdi,
                        s.displayOrder,
                        ekbolumler = s.Doktorlar.doktorlar_ek_bolumler
                                .Select(s2 => new
                                {
                                    s2.TibbiBolumler.BolumAdi
                                })
                    });


                var results = all_doctors.Where(q => q.dil == "TR").OrderBy(o => o.BolumAdi).ThenBy(o => o.displayOrder).ToList();

                if (results.Count > 0)
                {
                    rptDoctorList.DataSource = results;
                    rptDoctorList.DataBind();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}