﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class all_videos :cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var VideoList = db.videos.Where(q => q.is_active).ToList();

            if(VideoList.Count > 0)
            {
                rptVideo.DataSource = VideoList;
                rptVideo.DataBind();
            }
        }
    }
}