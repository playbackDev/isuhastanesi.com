//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WMMedikalNew
{
    using System;
    using System.Collections.Generic;
    
    public partial class video_gallery
    {
        public int id { get; set; }
        public string video_link { get; set; }
        public bool is_active { get; set; }
        public string video_name { get; set; }
    }
}
