﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="organization_list_general.aspx.cs" Inherits="WMMedikalNew.organization_list_general" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="sub-page-title" runat="server" id="topbanner">
    </div>



    <div class="section-line">
        <div class="breadcrumb2">
            <a href="/"><%=Resources.Lang.Anasayfa %> </a> | <a href="#">Anlaşmalı Kurumlar</a>
        </div>

    </div>
    <style>
        .description {
            margin: 1em auto 2.25em;
        }


        h1 {
            font-family: 'Pacifico', cursive;
            font-weight: 400;
            font-size: 2.5em;
        }

        ul {
            /*list-style: none;
            padding: 0;
            width: 100%;
            float: left;*/
        }

        .inner {
            overflow: hidden;
            display: none;
            width: 100%;
            margin: -10px 0 -10px 0px !important;
            padding: 20px 0 20px;
            background: rgba(255,255,255,.5);
        }

        }

        .inner .show {
            /*display: block;*/
        }


        .accordion li {
            margin: .5em 0;
            list-style: none;
        }

        a.toggle {
                width: 90%;
    display: block;
    background: #eaeaea;
    color: #7b262a;
    padding: .75em 5%;
    border-radius: 0.15em;
    transition: background .5s ease;
    margin: 0 auto;
    float: right;
    margin: 5px 0;
    text-align: center;
    font-weight: 800;
    font-size: 17px;
    border: 1px solid #90032B;
        }

            a.toggle:hover {
                background: #7b262a;
                 transition: background .5s ease;
                 color:#f2f2f2;
            }

        .med-list {
            width: 90%;
        }

            .med-list li {
                width: 46%;
                float: left;
                text-align: left;
                list-style: circle;
                margin-right: 4%;
            }
    </style>
    <div class="container content">
        <div class="sub-content-wide">

            <div class="sub-container">
                <div class="med-title">
                    <h2><%=Resources.Lang.AnlasmaliKurumlar %></h2>
                </div>
                <div class="list-container">
                    <div class="left-col" id="divContent" runat="server">

                        <%--<ul class="accordion">
        <li runat="server" id="uzmanlikalanlari" visible="false">
            <a class="toggle" href="javascript:void(0);"><%=Resources.Lang.UzmanlikAlanlari %></a>
            <div class="inner">
                <div class="dr-descr">
                    <asp:Literal ID="ltrExpertise" runat="server"></asp:Literal>

                </div>
            </div>
        </li>
    </ul>--%>
                        <ul class="accordion">
                            <asp:Repeater ID="rptOrganizationtype" runat="server" OnItemDataBound="rptOrganizationtype_ItemDataBound">
                                <ItemTemplate>
                                    <li>
                                        <a class="toggle" href="javascript:void(0);">
                                            <asp:Literal ID="ltrOrganizationName" runat="server"></asp:Literal></a>
                                        <%--<asp:Literal ID="ltrOrganizationName" runat="server"></asp:Literal>--%><div class="inner">
                                            <ul class="med-list">


                                                <asp:Repeater ID="rptOrganizationList" runat="server" OnItemDataBound="rptOrganizationList_ItemDataBound">
                                                    <ItemTemplate>
                                                        <li>
                                                            <asp:Literal ID="ltrOrganizationName_" runat="server"></asp:Literal>
                                                        </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>


                                            </ul>
                                        </div>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script>

        $('.toggle').click(function (e) {
            e.preventDefault();

            var $this = $(this);

            if ($this.next().hasClass('show')) {
                $this.next().removeClass('show');
                $this.next().slideUp(350);
            } else {
                $this.parent().parent().find('li .inner').removeClass('show');
                $this.parent().parent().find('li .inner').slideUp(350);
                $this.next().toggleClass('show');
                $this.next().slideToggle(350);
            }
        });
    </script>


    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="/assets/js/plugins.js"></script>
</asp:Content>
