﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class hakkimizda : cBase
    {
        int PageID = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["page_id"] != null)
            {
                PageID = Request.QueryString["page_id"].toint();
                var SayfaDetayTR = db.Sayfa_Detay_Dil.Select(s => new
                {
                    s.id,
                    s.sayfa_id,
                    SayfaAdi = s.Sayfalar.Sayfalar_Dil.FirstOrDefault().sayfa_adi,
                    s.lang,
                    s.description,
                    s.Sayfalar.sayfa_tipi,
                }).Where(q => q.sayfa_tipi == 11 && q.lang == Lang && q.sayfa_id == PageID).FirstOrDefault();

                if (SayfaDetayTR != null)
                {
                    ltrDescription.Text = SayfaDetayTR.description;
                    ltrPageName.Text = SayfaDetayTR.SayfaAdi;
                }
                else
                {
                    ltrDescription.Text = "İçerik Eklenmemiş";
                    ltrPageName.Text = "";
                }
            }
            else
                Response.Redirect("/",false);
         
        }
    }
}