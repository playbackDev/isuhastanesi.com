﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WMMedikalNew.LivHospitalERandevu1;
using WMMedikalNew.LivHospitalESonuc1;

namespace WMMedikalNew
{
    public partial class _default : cBase
    {
        randevufiltre eRandevu = new randevufiltre();
        TestResultService eSonuc = new TestResultService();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) _fillPage();
        }

        private void _fillPage()
        {
            var SlideImage = db.slider.Where(q => q.is_active && q.lang == "TR").OrderBy(o => o.display_order).ToList();
            if (SlideImage != null) rptSlide.BindData(SlideImage);

            //haber için type 1 olmalı 
            var _news = db.news_lang.Where(q => q.is_active && q.lang == Lang && q.news.type == 1).Select(s => new
            {
                s.id,
                s.news_id,
                s.created_time,
                s.news_short,
                s.news_title,
                s.news.news_image,
                s.news.seoURL
            }).OrderByDescending(o => o.created_time).Take(3).ToList();

            if (_news != null) rptNews.BindData(_news);

            //duyuru için type 2 olmalı 
            var _duyuru = db.news_lang.Where(q => q.is_active && q.lang == Lang && q.news.type == 2).Select(s => new
            {

                s.id,
                s.news_id,
                s.created_time,
                s.news_title,
                s.news.seoURL
            }).OrderByDescending(o => o.created_time).Take(4).ToList();

            if (_news != null) rptDuyuru.BindData(_duyuru);
        }



        public class HospitalList
        {
            public string HospitalId { get; set; }
            public string HospitalName { get; set; }
        }

        public class DepartmentList
        {
            public string DepartmentId { get; set; }
            public string DepartmentName { get; set; }
        }

        public class DoctorList
        {
            public string DoctorId { get; set; }
            public string DepartmentId { get; set; }
            public string BranchId { get; set; }
            public string DoctorName { get; set; }
            public string Title { get; set; }
        }





    }
}