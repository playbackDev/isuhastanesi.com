﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class WebForm9 : cBase
    {
        List<string> lets = new List<string> { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "Ö", "P", "R", "S", "T", "U", "V", "Y", "Z" };
        //int PageID;
        int HospitalID; 
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Request.QueryString["page_id"] != "")
            //{
            //    PageID = Request.QueryString["page_id"].toint();

                var PageDetail = db.Hastane_Sayfalar_Dil.Select(s => new { 
                    s.Hastane_Sayfalar.hastane_id,
                    s.sayfa_id,
                     s.dil   ,
                     s.Hastane_Sayfalar.Hastaneler.HastaneAdi,
                    s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == "TR").top_banner,
                     s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == "TR").meta_description

                }).Where(q=>q.dil == "TR").FirstOrDefault();

                if (PageDetail != null)
                {
                    HospitalID = PageDetail.hastane_id.toint();
                    //ltrHospitalName.Text = PageDetail.HastaneAdi;
                    

                    pagesubmenu.HospitalID = PageDetail.hastane_id.toint();
                    Page.MetaDescription = PageDetail.meta_description;
                    if (PageDetail.top_banner != null && PageDetail.top_banner != "")
                    {
                        topbanner.Style.Add("background-image", PageDetail.top_banner);
                        topbanner.Style.Add("background-position-y", "-31px");
                    }
                    else
                        topbanner.Style.Add("height", "0");
                   
                //}

                DataLoad("");

                if (!IsPostBack)
                {
                    LoadLetters();
                }
            }
            else
            {
                Response.Redirect("/",false);
            }
        }
        private void DataLoad(string letter)
        {
            try
            {
                


                var _MedicalServices = db.TibbiBolumler_Dil.Select(s => new
                {
                    BolumID = s.TibbiBolumler.id,
                    s.BolumAdi,
                    s.Dil,
                    HastaneID = s.TibbiBolumler.Hastaneler.id,
                    s.TibbiBolumler.Hastaneler.HastaneAdi,
                });

              

                if (letter != "")
                {
                    _MedicalServices = _MedicalServices.Where(w => w.BolumAdi.StartsWith(letter));

                    lets.Clear();
                    lets.Add(letter);
                    _MedicalServices = _MedicalServices.Where(w => w.BolumAdi.StartsWith(letter));
                }
                else
                {

                    if (txtSearch.Text != "")
                    {
                        string word = txtSearch.Text;
                        lets.Clear();
                        lets.Add(letter);
                        _MedicalServices = _MedicalServices.Where(w => w.BolumAdi.Contains(word));
                    }
                }


                _MedicalServices = _MedicalServices.Where(q => q.Dil == "TR" && q.HastaneID == HospitalID);

                List<LETTERS> cats = new List<LETTERS>();
                foreach (var item in lets)
                {
                    LETTERS L = new LETTERS();

                    var letter_branches = _MedicalServices.Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi });

                    if (item == "C")
                    {
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith("C") || w.BolumAdi.StartsWith("Ç")).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi });
                        L.letter = "C-Ç";
                    }
                    else if (item == "G")
                    {
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith("G") || w.BolumAdi.StartsWith("Ğ")).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi });
                        L.letter = "G-Ğ";
                    }
                    else if (item == "I")
                    {
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith("I") || w.BolumAdi.StartsWith("İ")).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi });
                        L.letter = "I-İ";
                    }
                    else if (item == "O")
                    {
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith("O") || w.BolumAdi.StartsWith("Ö")).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi });
                        L.letter = "O-Ö";
                    }
                    else if (item == "S")
                    {
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith("S") || w.BolumAdi.StartsWith("Ş")).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi });
                        L.letter = "S-Ş";
                    }
                    else if (item == "U")
                    {
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith("U") || w.BolumAdi.StartsWith("Ü")).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi });
                        L.letter = "U-Ü";
                    }
                    else
                    {
                        L.letter = item;
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith(item)).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi });
                    }

                    L.services_list = letter_branches.ToList();
                    if (L.services_list.Count > 0)
                    {
                        cats.Add(L);
                    }

                  

                }

         
                if (cats.Count > 0)
                {
                    ltrUyari.Text = "";
                    rptLettersCats.BindData(cats);
                }
                else
                {
                    ltrUyari.Text = "Sonuç Bulunamadı";
                    rptLettersCats.ClearData();
                }
            }
            catch { }
        }
        public void LoadLetters()
        {

            var harf_listesi = new List<LETTERS>();
            foreach (var item in lets)
            {
                harf_listesi.Add(new LETTERS { letter = item });
            }

            rptLetters.BindData(harf_listesi);
        }

        protected class LETTERS
        {
            public string letter { get; set; }
            public List<BRANCH> services_list { get; set; }
        }

        protected class BRANCH
        {
            public int id { get; set; }
            public string medical_services_name { get; set; }
            public string hospital_name { get; set; }
        }

        protected void btnSearchMedUnit_Click(object sender, EventArgs e)
        {
            try
            {
                DataLoad("");
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        protected void rptBrn_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                Literal ltrLink = (Literal)e.Item.FindControl("ltrLink");
                long id = Convert.ToInt64(DataBinder.Eval(e.Item.DataItem, "id"));
                string name = DataBinder.Eval(e.Item.DataItem, "medical_services_name").ToString();
                string HospitalName = DataBinder.Eval(e.Item.DataItem, "hospital_name").ToString();
                ltrLink.Text = "<a href=/detay/tibbi-hizmetlerimiz/" + name.ToURL() + "-" + id + "/" + Lang + ">" + name + "</a>";
              
            }
            catch
            {

            }
        }


        protected void btnLetters_Command(object sender, CommandEventArgs e)
        {
            DataLoad(e.CommandArgument.ToString());
        }
    }
}