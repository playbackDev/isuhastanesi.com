﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class WebForm12 :cBase
    {
        int PageID;
        int HospitalID; 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["page_id"] != "")
            {
                PageID = Request.QueryString["page_id"].toint();

                var PageDetail = db.Hastane_Sayfalar_Dil.Select(s => new
                {
                    s.Hastane_Sayfalar.hastane_id,
                    s.sayfa_id,
                    s.dil,
                    s.Hastane_Sayfalar.Hastaneler.HastaneAdi,
                    s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == Lang).top_banner,
                    s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == Lang).meta_description
                }).Where(q => q.sayfa_id == PageID && q.dil == Lang).FirstOrDefault();


                if (PageDetail !=null)
                {
                    HospitalID = PageDetail.hastane_id.toint();
                    ltrHospitalName.Text = PageDetail.HastaneAdi;

                    pagesubmenu.HospitalID = PageDetail.hastane_id.toint();

                    Page.MetaDescription = PageDetail.meta_description;
                    if (PageDetail.top_banner != null && PageDetail.top_banner != "")
                    {
                        topbanner.Style.Add("background-image", PageDetail.top_banner);
                        topbanner.Style.Add("background-position-y", "-31px");
                    }
                    else
                        topbanner.Style.Add("height", "0");
                }
              
                //pagesubmenu.ParentID = PageDetail.parent_id.toint();

                if (!IsPostBack)
                {
                    fillPage();
                }
            }
            else
            {
                Response.Redirect("/", false);
            }
        }

        private void fillPage()
        {
            var OrganizationTypeList = db.organization_type.Where(q => q.hospital_id == HospitalID).ToList();
            rptOrganizationtype.DataSource = OrganizationTypeList;
            rptOrganizationtype.DataBind();
        }

        protected void rptOrganizationtype_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Literal ltrOrganizationName = (Literal)e.Item.FindControl("ltrOrganizationName");
            int type_id= DataBinder.Eval(e.Item.DataItem, "id").toint();

            if(Lang == "TR")
            {
                ltrOrganizationName.Text = DataBinder.Eval(e.Item.DataItem, "organization_name_tr").ToString();
            }
            else
                ltrOrganizationName.Text = DataBinder.Eval(e.Item.DataItem, "organization_name_en").ToString();

            Repeater rptOrganizationList = (Repeater)e.Item.FindControl("rptOrganizationList");

            var OrganizationList = db.organizations.Where(q => q.organization_type_id == type_id && q.hospital_id == HospitalID).ToList();

            rptOrganizationList.DataSource = OrganizationList;
            rptOrganizationList.DataBind();
            
        }

        protected void rptOrganizationList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Literal ltrOrganizationName_ = (Literal)e.Item.FindControl("ltrOrganizationName_");

            if (Lang == "TR")
            {
                ltrOrganizationName_.Text = DataBinder.Eval(e.Item.DataItem, "organization_name_tr").ToString();
            }
            else
                ltrOrganizationName_.Text = DataBinder.Eval(e.Item.DataItem, "organization_name_en").ToString();
        }
     
    
    }
}