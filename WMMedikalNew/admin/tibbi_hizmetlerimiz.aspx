﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="tibbi_hizmetlerimiz.aspx.cs" Inherits="WMMedikalNew.admin.WebForm2" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .dataTables_length {
            display: none;
        }

        .dataTables_filter {
            float: right;
        }

        .dataTables_info {
            display: none;
        }

        #sortable {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

            #sortable li {
                display: block;
                width: 23%;
                height: 100%;
                float: left;
                margin: 10px;
            }
    </style>
    <script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
    <script type="text/javascript">

        function BrowseServer() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField;
            finder.popup();
        }

        function BrowseServer1() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField1;
            finder.popup();
        }

        function BrowseServer2() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField2;
            finder.popup();
        }
        function SetFileField(fileUrl) {
            document.getElementById('xFilePath').value = fileUrl;
        }

        function SetFileField1(fileUrl) {
            document.getElementById('xFilePath1').value = fileUrl;
        }

        function SetFileField2(fileUrl) {
            document.getElementById('xFilePath1').value = fileUrl;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        $(function () {
            $("#BirimListe").sortable({
                opacity: 0.8,
                cursor: 'move',
                update: function () {
                    var ResimSatir = $(this).sortable("toArray");

                    $.ajax({
                        type: "POST",
                        url: "tibbi_hizmetlerimiz.aspx/resimSirala",
                        data: "{'ResimSatir': '" + ResimSatir + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) { }
                    });
                }
            });
        });
    </script>
    <div class="content-wrapper" style="min-height: 916px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Tıbbi Hizmetlerimiz &amp; Merkezlerimiz</h1>
        </section>


        <section class="content">

            <div class="row">
                <a href="/admin/tibbi_bolum_ekle.aspx" class="btn btn-info pull-right">Yeni Tıbbi Bölüm &amp; Merkez Ekle</a><br />
                <br />
                <div class="col-xs-12">

                    <div class="text-center">
                        <asp:Literal ID="ltrLetter" runat="server" />
                    </div>

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Tıbbi Hizmetlerimiz &amp; Merkezlerimiz</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Başlık</th>
                                        <th>Düzenle</th>
                                    </tr>
                                </thead>

                                <tbody id="BirimListe">
                                    <asp:Repeater ID="rptServiceList" runat="server">
                                        <ItemTemplate>
                                            <tr id="<%#Eval ("BolumID") %>">
                                                <td><%#Eval ("BolumAdi") %></td>
                                                <td>
                                                    <a class="btn btn-s btn-success" href="tibbi_hizmet_detay.aspx?BolumID=<%#Eval ("BolumID") %>&Lang=TR">Düzenle </a>
                                                    <asp:LinkButton Text="SİL" runat="server" ID="lnkSil" CssClass="btn btn-danger" CommandArgument='<%#Eval ("BolumID") %>' OnCommand="lnkSil_Command" OnClientClick="return confirm('Tibbi birim / merkez silinecek, onaylıyor musunuz?')" />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>



    <%--  <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>--%>

    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <script src="plugins/fastclick/fastclick.min.js"></script>

    <script src="dist/js/demo.js"></script>

    <script>
        $(function () {
            $("#example1").DataTable();
        });
    </script>
</asp:Content>
