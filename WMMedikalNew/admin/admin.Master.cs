﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WMMedikalNew.Core;

namespace WMMedikalNew.admin
{
    public partial class admin : System.Web.UI.MasterPage
    {
        cBase cb = new cBase();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Request.IsLocal)
            {
                if (MemberManager.IsLoggedIn)
                {

                    var SabitSayfalar = cb.db.page_langs.Where(q => q.lang == "TR" && q.parent_id == 999).ToList();

                }
                else
                    Response.Redirect("/admin/login.aspx", false);
            }


        }

     
        protected void rptHospitalPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            HyperLink hypPageLink = (HyperLink)e.Item.FindControl("hypPageLink");


            int PageID = DataBinder.Eval(e.Item.DataItem, "page_id").toshort();
            string pageName = DataBinder.Eval(e.Item.DataItem, "page_name").ToString();

            string str = "VM";


            switch (pageName)
            {
                case "Hakkımızda":
                    hypPageLink.NavigateUrl = "/admin/hastanemiz.aspx?page_id=" + PageID;
                    break;

                case "Tıbbi Bölümler":
                    hypPageLink.NavigateUrl = "/admin/tibbi_hizmetlerimiz.aspx";
                    break;

                case "Doktorlarımız":
                    hypPageLink.NavigateUrl = "/admin/doktorlarimiz.aspx";
                    break;

                case "Anlaşmalı Kurumlar":
                    hypPageLink.NavigateUrl = "/admin/h_anlasmali_kurumlar.aspx";
                    break;

                case "Galeri":
                    hypPageLink.NavigateUrl = "/admin/galeri.aspx";
                    break;

                case "E-Randevu":
                    hypPageLink.Visible = false;
                    break;

                case "Kalite Yönetimi":
                    hypPageLink.NavigateUrl = "/admin/h_kalite_yonetimi.aspx?page_id=" + PageID;
                    break;

                case "Hastanemiz":
                    hypPageLink.NavigateUrl = "/admin/hastanemiz.aspx?page_id=" + PageID;
                    hypPageLink.Visible = false;
                    break;

                default:
                    if (pageName.Contains("VM"))
                    {
                        hypPageLink.Visible = false;
                    }

                    break;


            }


        }

        protected void lnkLogout_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Remove("wm_login");
                Session.Remove("wm_id");
                Session.Remove("wm_username");
                Response.Redirect("/admin/login.aspx", false);
            }
            catch (Exception ex)
            {


            }
        }
    }
}