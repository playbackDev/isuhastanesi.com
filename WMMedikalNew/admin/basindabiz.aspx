﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="basindabiz.aspx.cs" Inherits="WMMedikalNew.admin.WebForm13" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .dataTables_length {
            display: none;
        }

        .dataTables_filter {
            float: right;
        }

        .dataTables_info {
            display: none;
        }
    </style>
    <script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
	<script type="text/javascript">

	    function BrowseServer() {
	        var finder = new CKFinder();
	        finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
	        finder.selectActionFunction = SetFileField;
	        finder.popup();
	    }

	    function SetFileField(fileUrl) {
	        document.getElementById('xFilePath').value = fileUrl;
	    }

	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content-header">
            <h1>Basında Medicalpark </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-3">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Yeni Yayın Ekle</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Hastane Seçiniz</label>
                                <asp:DropDownList ID="drpHospital" runat="server" class="form-control"></asp:DropDownList>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Basın Tip Seçiniz</label>
                                <asp:DropDownList ID="drpType" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0" Text="Tip" Selected="True" />
                                    <asp:ListItem Value="1" Text="Yazılı Basın" />
                                    <asp:ListItem Value="2" Text="Görsel Basın" />

                                </asp:DropDownList>
                            </div>
                             <div class="form-group">
                                <label for="exampleInputEmail1">Basın Başlığı (Sitede Görüntülenmeyecek)</label>
                                <asp:TextBox ID="txtPressTitle" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Doktor Adı</label>
                                <asp:TextBox ID="txtDoctorName" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>

                              <div class="form-group">
                                <label for="exampleInputEmail1">Basın Adı</label>
                                <asp:TextBox ID="txtPressName" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                         
                                  <div class="form-group">
                    <label>Yayın Tarihi</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                     
                    <asp:TextBox ID="txtDate" runat="server"  class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask></asp:TextBox>
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                           

                            <div class="form-group">
                                <label for="exampleInputEmail1">Resim</label>
                                       <input id="xFilePath" name="FilePath" type="text" size="60" />
		                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer();" />
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Durum (Aktif/Pasif)</label>
                                <asp:CheckBox ID="ckIsActive" runat="server" class="form-control" />
                            </div>


                        </div>
                        <div class="box-footer">

                            <asp:Button ID="btnEkle" runat="server" Text="Yayın Ekle" class="btn btn-primary" OnClick="btnEkle_Click"/><br />
                            <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>

                        </div>
                    </div>

                </div>
                <div class="col-xs-9">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Basında Medicalpark </h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Durum</th>
                                         <th>Başlık</th>
                                        <th>Doktor Adı</th>
                                         <th>Basın Adı</th>
                                        <th>Basın Tipi</th>
                                        <th>Resim</th>

                                        <th>İşlem</th>

                                    </tr>
                                </thead>

                                <tbody>
                                    <asp:Repeater ID="rptPress" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%#Eval ("active_status") %></td>
                                                 <td><%#Eval ("title") %></td>
                                                 <td><%#Eval ("doctor_name") %></td>
                                                 <td><%#Eval ("press_name") %></td>
                                                 <td><%#Eval ("type") %></td>
                                                <td>
                                                    <img src="<%#Eval ("image_name") %>" style="width: 100px" /></td>
                                                  
                                                </td>
                                                <td>

                                                    <asp:LinkButton runat="server" ID="LinkButton1" OnCommand="LinkButton1_Command" CommandArgument='<%#Eval ("id") %>'  OnClientClick="return confirm('silmek istediğinizden emin misiniz!');" CssClass="btn btn-s btn-danger">Sil
                                                    </asp:LinkButton>
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>



<%--    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>--%>

    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <script src="plugins/fastclick/fastclick.min.js"></script>

    <script src="dist/js/demo.js"></script>

        <script src="plugins/input-mask/jquery.inputmask.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
        <script src="plugins/select2/select2.full.min.js"></script>

 

    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
     <script>
         $(function () {
             //Initialize Select2 Elements
             $(".select2").select2();

             //Datemask dd/mm/yyyy
             $("#datemask").inputmask("dd/mm/yyyy", { "placeholder": "dd/mm/yyyy" });
             //Datemask2 mm/dd/yyyy
             $("#datemask2").inputmask("mm/dd/yyyy", { "placeholder": "mm/dd/yyyy" });
             //Money Euro
             $("[data-mask]").inputmask();

             //Date range picker
             $('#reservation').daterangepicker();
             //Date range picker with time picker
             $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' });
             //Date range as a button
             $('#daterange-btn').daterangepicker(
                 {
                     ranges: {
                         'Today': [moment(), moment()],
                         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                         'This Month': [moment().startOf('month'), moment().endOf('month')],
                         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                     },
                     startDate: moment().subtract(29, 'days'),
                     endDate: moment()
                 },
             function (start, end) {
                 $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
             }
             );

             //iCheck for checkbox and radio inputs
             $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                 checkboxClass: 'icheckbox_minimal-blue',
                 radioClass: 'iradio_minimal-blue'
             });
             //Red color scheme for iCheck
             $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                 checkboxClass: 'icheckbox_minimal-red',
                 radioClass: 'iradio_minimal-red'
             });
             //Flat red color scheme for iCheck
             $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                 checkboxClass: 'icheckbox_flat-green',
                 radioClass: 'iradio_flat-green'
             });

             //Colorpicker
             $(".my-colorpicker1").colorpicker();
             //color picker with addon
             $(".my-colorpicker2").colorpicker();

             //Timepicker
             $(".timepicker").timepicker({
                 showInputs: false
             });
         });
    </script>
</asp:Content>
