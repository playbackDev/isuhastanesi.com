﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="h_anlasmali_kurumlar.aspx.cs" Inherits="WMMedikalNew.admin.WebForm17" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .dataTables_length {
            display: none;
        }

        .dataTables_filter {
            float: right;
        }

        .dataTables_info {
            display: none;
        }
    </style>
    <script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
    <script type="text/javascript">

        function BrowseServer() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField;
            finder.popup();
        }

        function BrowseServer1() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField1;
            finder.popup();
        }

        function BrowseServer2() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField2;
            finder.popup();
        }
        function SetFileField(fileUrl) {
            document.getElementById('xFilePath').value = fileUrl;
        }

        function SetFileField1(fileUrl) {
            document.getElementById('xFilePath1').value = fileUrl;
        }

        function SetFileField2(fileUrl) {
            document.getElementById('xFilePath2').value = fileUrl;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="min-height: 916px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Anlaşmalı Kurumlar</h1>
        </section>

        <section class="content">
            <div class="row">

                <asp:Panel ID="pnlKurumEkle" runat="server">
                    <div class="col-xs-3">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Anlaşmalı Kurum Ekle</h3>
                            </div>
                            <div class="box-body">

                                <div class="form-group" style="display: none;">
                                    <label for="exampleInputEmail1">hastane Seçiniz</label>
                                    <asp:DropDownList ID="drpHospital" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="drpHospital_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kurum Tipi Seçiniz</label>
                                    <asp:DropDownList ID="drpKurumlar" runat="server" class="form-control"></asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kurum Adı</label>
                                    <asp:TextBox ID="txtKurumAdiTR" runat="server" class="form-control" placeholder="Kurum Adı"></asp:TextBox>
                                </div>
                                <%--  <div class="form-group">
                                    <label for="exampleInputEmail1">Kurum Adı (EN)</label>
                                    <asp:TextBox ID="txtKurumAdiEN" runat="server" class="form-control" placeholder="Kurum Adı (EN)"></asp:TextBox>
                                </div>--%>
                            </div>
                            <div class="box-footer">

                                <asp:Button ID="btnHizmetEkle" runat="server" Text="Kurum Ekle" class="btn btn-primary" OnClick="btnHizmetEkle_Click" /><br />
                                <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>

                            </div>
                        </div>

                    </div>
                </asp:Panel>
            </div>
            <div class="row">

                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Sayfa Üst Banner</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                    <%--    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa Linki</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtPageURLTR" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Meta Description </label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtDescriptionTR" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>

                                    </div>--%>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa  Banner</label>
                                        <div class="col-sm-10">
                                            <asp:Literal ID="ltrTopBannerTR" runat="server"></asp:Literal><br />
                                            <br />
                                            <input id="xFilePath1" name="FilePathTR" type="text" size="60" />
                                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer1();" />
                                        </div>
                                    </div>

                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdateTR" runat="server" Text="Güncelle" class="btn btn-info pull-right" OnClick="btnUpdateTR_Click" />
                                    <br />
                                    <asp:Label ID="lblUyariTR" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <%--<div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title"> Sayfa Üst Banner İngilizce</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                                   <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa Linki EN</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtPageURLEN" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                        <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Meta Description </label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtDescriptionEN" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>

                                    </div>

                                        <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa  Banner EN</label>
                                        <div class="col-sm-10">
                                            <asp:Literal ID="ltrTopBannerEN" runat="server"></asp:Literal><br /><br />
                                            <input id="xFilePath2" name="FilePathEN" type="text" size="60" />
                                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer1();" />
                                        </div>
                                    </div>

                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="Button1" runat="server" Text="Güncelle (EN)" class="btn btn-info pull-right"  OnClick="Button1_Click" />
                                        <br />
                                    <asp:Label ID="Label1" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>
                <div class="col-xs-12">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Anlaşmalı Kurumlar</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <%--<th>Hastane</th>--%>
                                        <th>Kurum Tipi</th>
                                        <th>Kurum Adı</th>
                                        <%--  <th>Kurum  Adı EN</th>--%>
                                        <th>Düzenle</th>
                                        <th>Sil</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <asp:Repeater ID="rptKurumList" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <%--<td><%#Eval ("HastaneAdi") %></td>--%>
                                                <td><%#Eval ("organization_type_name") %> </td>
                                                <td><%#Eval ("organization_name_tr") %> </td>
                                                <%--  <td><%#Eval ("organization_name_en") %> </td>--%>


                                                <td>
                                                    <a class="btn btn-s btn-info" href="h_anlasmali_kurum_detay.aspx?id=<%#Eval ("id") %>">Düzenle </a>
                                                </td>
                                                <td>
                                                    <asp:LinkButton runat="server" OnCommand="lnkRemove_Command" CommandArgument='<%#Eval ("id") %>' ID="lnkRemove" OnClientClick="return confirm('silmek istediğinizden emin misiniz!');" CssClass="btn btn-s btn-danger">Sil
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>



    <%--    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>--%>

    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <script src="plugins/fastclick/fastclick.min.js"></script>

    <script src="dist/js/demo.js"></script>

    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
</asp:Content>
