﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm4 : cBase
    {
        byte _type = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["t"] != null)
            {
                _type = Convert.ToByte(Request.QueryString["t"]);
                //var bannerss = db.news.Where(q => q.id == 3).FirstOrDefault();
                //ImageBanner.ImageUrl = bannerss.img_path;
                _fillPage();

                hlAddNew.NavigateUrl = "/admin/haber_ekle.aspx?t=" + _type;
            }
        }
        private void _fillPage()
        {
            var _NewsList = db.news_lang.Where(q => q.lang == "TR" && q.news.type == _type)
                .Select(s => new
                {
                    s.id,
                    active_status = (s.is_active) ? "Aktif" : "Pasif",
                    s.news_id,
                    s.news_title,
                    s.news.type,
                    NewsTitle = s.news.news_title
                })
                .OrderByDescending(o => o.id)
                .ToList();

            rptNewList.DataSource = _NewsList;
            rptNewList.DataBind();
        }

        protected void btnHaberEkle_Click(object sender, EventArgs e)
        {
            //string ımageURL = string.Empty;
            //ımageURL = Request.Form["FilePath"].ToString();

            //if (drpType.SelectedValue.toint() != 0)
            //{
            //    if (ımageURL != null || ımageURL !="")
            //    {
            //        if (txtHaberBaslik.Text.Trim() != "")
            //        {
            //            news _NewNews = new news();

            //            int uzunluk = ımageURL.Split('.').Length;
            //            string random = CreateRandomValue(10, true, true, true, false) + "." + ımageURL.Split('.')[uzunluk - 1];
            //            _NewNews.news_title = txtHaberBaslik.Text.Trim();
            //            _NewNews.news_image = ımageURL;
            //            _NewNews.type = Convert.ToByte(drpType.SelectedValue);


            //            news_lang _NewsLangTR = new news_lang();
            //            _NewsLangTR.news_title = txtHaberBaslik.Text.Trim();
            //            _NewsLangTR.is_active = ckIsActive.Checked;
            //            _NewsLangTR.news_id = _NewNews.id;
            //            _NewsLangTR.lang = "TR";
            //            _NewsLangTR.created_time = DateTime.Now;

            //            news_lang _NewsLangEN = new news_lang();
            //            _NewsLangEN.news_title = txtHaberBaslik.Text.Trim();
            //            _NewsLangEN.is_active = ckIsActive.Checked;
            //            _NewsLangEN.news_id = _NewNews.id;
            //            _NewsLangEN.lang = "EN";
            //            _NewsLangEN.created_time = DateTime.Now;


            //            db.news.Add(_NewNews);
            //            db.news_lang.Add(_NewsLangTR);
            //            db.news_lang.Add(_NewsLangEN);

            //            if (db.SaveChanges() > 0)
            //            {
            //                txtHaberBaslik.Text = "";
            //                lblUyari.Text = "Eklendi";
            //                Response.Redirect("haberler.aspx", false);
            //            }

            //        }
            //        else
            //            lblUyari.Text = "Haber Adını Giriniz";
            //    }
            //    else
            //        lblUyari.Text = "Resim Seçmediniz";
            //}
            //else
            //    lblUyari.Text = "Tür Seçmediniz";
        }

        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {
            int newsID = e.CommandArgument.toint();
            var DeleteNewsLang = db.news_lang.Where(q => q.news_id == newsID).ToList();

            foreach (var u in DeleteNewsLang)
            {
                db.news_lang.Remove(u);
            }

            if (db.SaveChanges() > 0)
                Response.Redirect("haberler.aspx?t=" + _type, false);
        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                if (Request.Form["FilePathTR"].ToString() != null || Request.Form["FilePathTR"].ToString() != "")
                {



                    var _bannerlar = db.news.FirstOrDefault(q => q.id == 3);
                    if (_bannerlar != null)
                    {

                        if (!string.IsNullOrEmpty(Request.Form["FilePathTR"].ToString()))
                        {
                            _bannerlar.img_path = Request.Form["FilePathTR"].ToString();
                        }
                        //_bannerlar.meta_description = txtDescriptionTR.Text.Trim();
                        //_bannerlar.page_url = txtPageURLTR.Text.Trim();

                        if (db.SaveChanges() > 0)
                        {
                            ResizeImage(_bannerlar.img_path, 1903, 381);
                            Page.Response.Redirect(Page.Request.Url.ToString(), false);
                        }

                    }
                }


            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}