﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm25 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var SliderList = db.slider.Select(s => new
                {
                    s.id,
                    s.image_url,
                    s.lang,
                    s.link,
                    s.is_popup,
                    s.display_order,
                    active_status = (s.is_active) ? "Aktif" : "Pasif",
                }).OrderBy(o => o.display_order).ToList();

                rptSlider.DataSource = SliderList;
                rptSlider.DataBind();

                if (SliderList.Count >= 5) btnEkle.Visible = false;
            }
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            string ımageURL = Request.Form["FilePath"].ToString();
            string imageURL2 = Request.Form["FilePath2"].ToString();

            if (ımageURL != null && ımageURL != "" && imageURL2 != null && imageURL2 != "")
            {
                slider _newSlider = new slider();
                _newSlider.is_active = ckIsActive.Checked;
                _newSlider.link = txtURL.Text.Trim();
                _newSlider.lang = "TR";
                _newSlider.image_url = ımageURL;
                _newSlider.image_url_mobile = imageURL2;

                db.slider.Add(_newSlider);

                if (db.SaveChanges() > 0)
                {
                    ResizeImage(_newSlider.image_url, 1350, 371);
                    Page.Response.Redirect(Page.Request.Url.ToString(), false);
                }
                else
                    lblUyari.Text = "Bir hata oluştu";
            }
            else
                lblUyari.Text = "Resim Seçmediniz";
        }

        protected void lnkisMain_Command(object sender, CommandEventArgs e)
        {
            //Tüm ana resimleri sıfırlıyoruz
            //int ID = e.CommandArgument.toint();
            //db.Database.ExecuteSqlCommand("Update slider set is_popup = 0");

            ////Seçilen Resim Ana Resim yapılıyor
            //var isMain = db.slider.Where(q => q.id == ID).FirstOrDefault();
            //isMain.is_popup = true;
            //if (db.SaveChanges() > 0)
            //    Alert("Popup Resim Olarak Belirlediniz.");
            //else
            //    Alert("Bir Hata Oluştu");
        }
        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            int ID = e.CommandArgument.toint();
            var _delete = db.slider.Where(q => q.id == ID).FirstOrDefault();

            db.slider.Remove(_delete);

            if (db.SaveChanges() > 0)
                Page.Response.Redirect(Page.Request.Url.ToString(), false);
            else
                lblUyari.Text = "Bir hata oluştu";

        }

        protected void rptSlider_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //LinkButton lnkisMain = (LinkButton)e.Item.FindControl("lnkisMain");

            //bool is_popup = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "is_popup"));
            //if (is_popup)
            //{
            //    lnkisMain.Visible = false;
            //}
        }

        [WebMethod] //resim sıralama methodu
        public static void resimSirala(String ResimSatir)
        {
            WMMedikalDBEntities db = new WMMedikalDBEntities();

            string gelen = ResimSatir.Replace("satir", "");
            char[] ayrac = new char[] { ',' };
            string[] gelenler = gelen.Split(ayrac);
            int i = 1;
            foreach (string veri in gelenler)
            {
                db.Database.ExecuteSqlCommand("Update slider set display_order = " + i + " where id=" + Convert.ToInt32(veri));
                i++;
            }
        }
    }
}