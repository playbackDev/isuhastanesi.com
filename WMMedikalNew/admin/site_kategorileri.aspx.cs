﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm6 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var _CategoryList = db.site_category_lang.Where(q=> q.lang=="TR").Select(s => new { 
                    s.category_id,
                    s.site_category_name
                }).ToList();
                rptCategoryList.DataSource = _CategoryList;
                rptCategoryList.DataBind();
                
            }
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            if (txtNameEN.Text.Trim() != "" && txtNameTR.Text.Trim() != "")
            {
                site_category _category = new site_category();
                _category.site_category_name = txtNameTR.Text.Trim();
                 db.site_category.Add(_category);

                site_category_lang _categoryTR = new site_category_lang();
                _categoryTR.site_category_name = txtNameTR.Text.Trim();
                _categoryTR.category_id = _category.id;
                _categoryTR.lang = "TR";

                site_category_lang _categoryEN = new site_category_lang();
                _categoryEN.site_category_name = txtNameEN.Text.Trim();
                _categoryEN.category_id = _category.id;
                _categoryEN.lang = "EN";
            


                db.site_category_lang.Add(_categoryEN);
                db.site_category_lang.Add(_categoryTR);

                if (db.SaveChanges() > 0)
                    Response.Redirect("site_kategorileri.aspx", false);
            }
            else
                lblUyari.Text = "Boş Alan Bırakmayınız";

        }

        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {
            int categoryID = e.CommandArgument.toint();
            var Delete = db.site_category_lang.Where(q => q.category_id == categoryID).ToList();

            foreach (var u in Delete)
            {
                db.site_category_lang.Remove(u);
            }


            if (db.SaveChanges() > 0)
                Response.Redirect("site_kategorileri.aspx", false);

        }

        protected void rptCategoryList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Literal ltrCategoryName = (Literal)e.Item.FindControl("ltrCategoryName");
           int ID =  DataBinder.Eval(e.Item.DataItem, "category_id").toint();
           var CategorName = db.site_category_lang.Where(q => q.category_id == ID && q.lang == "EN").FirstOrDefault();
           ltrCategoryName.Text = CategorName.site_category_name;
        }
    }
}