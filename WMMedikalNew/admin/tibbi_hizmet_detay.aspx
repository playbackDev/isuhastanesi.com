﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="tibbi_hizmet_detay.aspx.cs" Inherits="WMMedikalNew.admin.WebForm3" EnableEventValidation="false" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
    <script type="text/javascript">

        function BrowseServer1() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField1;
            finder.popup();
        }

        function SetFileField1(fileUrl) {
            document.getElementById('FilePath1').value = fileUrl;
        }
    </script>
    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content">
            <div class="row">

                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                <asp:Literal ID="ltrServiceName" runat="server"></asp:Literal>
                                <%--(<asp:Literal ID="ltrLang" runat="server"></asp:Literal>)--%></h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">

                                    <%--   <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Meta Description</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtMetaDescription" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>--%>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa Banner</label>
                                        <div class="col-sm-10">
                                            <asp:Literal ID="ltrTopBannerTR" runat="server"></asp:Literal>
                                            <asp:Literal ID="ltrFilePath" runat="server" />
                                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer1();" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">SEO Browser Başlığı</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtSEOBrowserTitle" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">SEO Meta Description</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtSEOMetaDesc" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">SEO Meta Keywords</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtSEOMetaKeywords" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">SEO Sayfa URL</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtSEOPageUrl" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa Adı</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtServiceName" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Merkez olarak göster</label>
                                        <div class="col-sm-10"><asp:CheckBox ID="ckIsMerkez" runat="server" /></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa İçeriği</label>
                                        <div class="col-sm-10">
                                            <CKEditor:CKEditorControl ID="textArea1" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>

                                    </div>



                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdate" runat="server" Text="Güncelle" class="btn btn-info pull-right" OnClick="btnUpdate_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--
                      <div class="col-md-3">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Resim</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                    <div class="form-group">
                                    
                                        <div class="col-sm-12">
                                            <asp:Literal ID="ltrImage" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <br />
                                      <div class="form-group">
                                    
                                        <div class="col-sm-12">
                                               <input id="FilePath" name="FilePath" type="text" size="60" />
		                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer();" />
                                        </div>
                                    </div>
                          
                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdateGuncelle" runat="server" Text="Güncelle" class="btn btn-info pull-right"  OnClick="btnUpdateGuncelle_Click"/>
                                    <br />
                                    <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
            </div>
        </section>
    </div>

</asp:Content>
