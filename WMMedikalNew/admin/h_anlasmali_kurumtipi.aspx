﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="h_anlasmali_kurumtipi.aspx.cs" Inherits="WMMedikalNew.admin.WebForm18" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .dataTables_length {
            display: none;
        }

        .dataTables_filter {
            float: right;
        }

        .dataTables_info {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="min-height: 916px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Kurum Tipleri</h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-3">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Yeni Kurum Tipi</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group" style="display: none;">
                                <label for="exampleInputEmail1">Hastane Seçiniz</label>
                                <asp:DropDownList ID="drpHospital" runat="server" class="form-control"></asp:DropDownList>
                            </div>


                            <div class="form-group">
                                <label for="exampleInputEmail1">Kurum Tipi</label>
                                <asp:TextBox ID="kurumTipiTR" runat="server" class="form-control" placeholder="Kurum Tipi"></asp:TextBox>
                            </div>

                            <%-- <div class="form-group">
                                <label for="exampleInputEmail1">Kurum Tipi (EN)</label>
                                <asp:TextBox ID="kurumTipiEN" runat="server" class="form-control" placeholder="Kurum Tipi (EN)"></asp:TextBox>
                            </div>--%>
                        </div>
                        <div class="box-footer">

                            <asp:Button ID="btnHizmetEkle" runat="server" Text="Kurum Tipi Ekle" class="btn btn-primary" OnClick="btnHizmetEkle_Click" /><br />
                            <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>

                        </div>
                    </div>

                </div>
                <div class="col-xs-9">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Kurum Tipleri</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>

                                        <%--<th>Hastane</th>--%>
                                        <th>Kurum Tipi Adı</th>
                                        <%--  <th>Kurum Tipi Adı EN</th>    --%>
                                        <th>İşlem</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <asp:Repeater ID="rptKurumList" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <%--<td><%#Eval ("HastaneAdi") %></td>--%>
                                                <td><%#Eval ("organization_name_tr") %> </td>
                                                <%--  <td><%#Eval ("organization_name_en") %> </td>--%>

                                                <td>
                                                    <a class="btn btn-s btn-info" href="h_anlasmali_kurumlar_detay.aspx?id=<%#Eval ("id") %>">Düzenle </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton runat="server" OnCommand="lnkRemove_Command" CommandArgument='<%#Eval ("id") %>' ID="lnkRemove" OnClientClick="return confirm('silmek istediğinizden emin misiniz!');" CssClass="btn btn-s btn-danger">Sil
                                                        </asp:LinkButton>
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>



    <%--    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>--%>

    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <script src="plugins/fastclick/fastclick.min.js"></script>

    <script src="dist/js/demo.js"></script>

    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
</asp:Content>
