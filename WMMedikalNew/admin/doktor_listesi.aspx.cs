﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace WMMedikalNew.admin
{
    public partial class WebForm14 : cBase
    {
        int PageID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["page_id"] != null)
                PageID = Request.QueryString["page_id"].toint();

            if (!IsPostBack)
            {
                if (Request.QueryString["hastane_id"] != null)
                {
                    int HastaneID = Request.QueryString["hastane_id"].toint();

                    var _doktorlarimiz = db.Doktorlar.Select(s => new
                    {
                        s.id,
                        s.DoktorAdi,
                        s.TibbiBolumler.TibbiBolumler_Dil.FirstOrDefault(f => f.Dil == "TR").BolumAdi,
                        s.Hastaneler.HastaneAdi,
                        HastaneID = s.Hastaneler.id,
                        s.is_active,
                        s.Unvan
                    })
                    .OrderByDescending(o => o.is_active)
                    .Where(q => q.HastaneID == HastaneID)
                    .ToList();

                    rptDoctor.DataSource = _doktorlarimiz;
                    rptDoctor.DataBind();
                }
            }

            //if (PageID != 0)
            //{
            //    if (!IsPostBack)
            //    {
            //        var _bannerlar = db.Hastane_Sayfa_Detay_Dil.Where(q => q.sayfa_id == PageID).ToList();

            //        if (_bannerlar != null) foreach (var item in _bannerlar) if (item.lang == "TR") ltrTopBannerTR.Text = "<img src='" + item.top_banner + "' style='max-width:300px'/>";
            //    }

            //}
        }

        protected void btnUpdateTR_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    if (Request.Form["FilePathTR"].ToString() != null || Request.Form["FilePathTR"].ToString() != "")
            //    {
            //        var _bannerlar = db.Hastane_Sayfa_Detay_Dil.Where(q => q.sayfa_id == PageID && q.lang == "TR").FirstOrDefault();
            //        if (_bannerlar != null)
            //        {
            //            if (!string.IsNullOrEmpty(Request.Form["FilePathTR"].ToString()))
            //            {
            //                _bannerlar.top_banner = Request.Form["FilePathTR"].ToString();

            //            }
            //            //_bannerlar.meta_description = txtDescriptionTR.Text.Trim();
            //            //_bannerlar.page_url = txtPageURLTR.Text.Trim();
            //            if (db.SaveChanges() > 0)
            //            {
            //                ResizeImage(_bannerlar.top_banner, 1903, 381);
            //                Page.Response.Redirect(Page.Request.Url.ToString(), false);
            //            }
            //            else
            //                lblUyariTR.Text = "Güncelleme Yapılamadı";
            //        }
            //    }
            //    else
            //        lblUyariTR.Text = "Resim Seçiniz";
            //}
            //catch (Exception ex)
            //{
            //    Response.Write(ex);
            //}
        }

        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (Request.Form["FilePathEN"].ToString() != null || Request.Form["FilePathEN"].ToString() != "")
        //        {
        //            var _bannerlar = db.Hastane_Sayfa_Detay_Dil.Where(q => q.sayfa_id == PageID && q.lang == "EN").FirstOrDefault();
        //            if (_bannerlar != null)
        //            {
        //                if (!string.IsNullOrEmpty(Request.Form["FilePathEN"].ToString()))
        //                {
        //                    _bannerlar.top_banner = Request.Form["FilePathEN"].ToString();

        //                }
        //                _bannerlar.meta_description = txtDescriptionEN.Text.Trim();
        //                _bannerlar.page_url = txtPageURLEN.Text.Trim();
        //                if (db.SaveChanges() > 0)
        //                    Page.Response.Redirect(Page.Request.Url.ToString(), false);
        //                else
        //                    Label1.Text = "Güncelleme Yapılamadı";
        //            }
        //        }
        //        else
        //            Label1.Text = "Resim Seçiniz";
        //    }
        //    catch (Exception ex)
        //    {
        //        Response.Write(ex);
        //    }
        //}

        protected void lnkActive_Command(object sender, CommandEventArgs e)
        {
            int ID = e.CommandArgument.toint();
            var _update = db.Doktorlar.Where(q => q.id == ID).FirstOrDefault();

            if (_update != null)
            {
                _update.is_active = true;

                if (db.SaveChanges() > 0)
                    Refresh();
                else
                    Alert("Bir Hata Oluştu");
            }
        }

        protected void lnkPasif_Command(object sender, CommandEventArgs e)
        {
            int ID = e.CommandArgument.toint();
            var _update = db.Doktorlar.Where(q => q.id == ID).FirstOrDefault();

            if (_update != null)
            {
                _update.is_active = false;

                if (db.SaveChanges() > 0)
                    Refresh();
                else
                    Alert("Bir Hata Oluştu");
            }
        }

        protected void rptDoctor_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkActive = (LinkButton)e.Item.FindControl("lnkActive");
            LinkButton lnkPasif = (LinkButton)e.Item.FindControl("lnkPasif");
            bool is_active = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "is_active"));

            if (is_active)
            {
                lnkActive.Visible = false;
                lnkPasif.Visible = true;
            }
            else
            {
                lnkActive.Visible = true;
                lnkPasif.Visible = false;
            }


        }


    }
}