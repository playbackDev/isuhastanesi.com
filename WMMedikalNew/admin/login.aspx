﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="WMMedikalNew.admin.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>İstinye Üniversitesi Hastanesi</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
</head>
  <body class="hold-transition login-page">
    <form id="form1" runat="server">
       <div class="login-box">
      <div class="login-logo">
        <a href="#"><b>İstinye Üniversitesi</b>Panel</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Giriş Yaparak Web Sitenizi Yönetin</p>
      
          <div class="form-group has-feedback">
            <asp:TextBox ID="txtKullaniciAdi" runat="server" class="form-control"></asp:TextBox>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
              <asp:TextBox ID="txtSifre" runat="server" TextMode="Password" class="form-control"></asp:TextBox>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
        <div class="col-xs-8">
           <asp:Literal ID="ltrError" runat="server" ></asp:Literal>
            </div>
            <div class="col-xs-4">
           <asp:Button ID="btnLogin" runat="server" Text="Giriş Yap"  class="btn btn-primary btn-block btn-flat"  OnClick="btnLogin_Click"/>
             
            </div><!-- /.col -->
          </div>
       

    
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    </form>
</body>
</html>
