﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="h_anlasmali_kurumlar_detay.aspx.cs" Inherits="WMMedikalNew.admin.WebForm19" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="min-height: 916px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Anlaşmalı Kurumlar  </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-3">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Kurum Tipi Düzenle</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group" style="display: none;">
                                <label for="exampleInputEmail1">Hastane Seçiniz</label>
                                <asp:DropDownList ID="drpHospital" runat="server" class="form-control"></asp:DropDownList>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Kurum Tipi (TR)</label>
                                <asp:TextBox ID="kurumTipiTR" runat="server" class="form-control" placeholder="Kurum Tipi (TR)"></asp:TextBox>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Kurum Tipi (EN)</label>
                                <asp:TextBox ID="kurumTipiEN" runat="server" class="form-control" placeholder="Kurum Tipi (EN)"></asp:TextBox>
                            </div>

                        </div>
                        <div class="box-footer">

                            <asp:Button ID="btnHizmetEkle" runat="server" Text="Güncelle" class="btn btn-primary" OnClick="btnHizmetEkle_Click" /><br />
                            <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>

                        </div>
                    </div>

                </div>

                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

</asp:Content>
