﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="doktor_listesi.aspx.cs" Inherits="WMMedikalNew.admin.WebForm14" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .dataTables_length {
            display: none;
        }

        .dataTables_filter {
            float: right;
        }

        .dataTables_info {
            display: none;
        }
    </style>
    <script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
    <script type="text/javascript">

        function BrowseServer() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField;
            finder.popup();
        }

        function BrowseServer1() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField1;
            finder.popup();
        }

        function BrowseServer2() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField2;
            finder.popup();
        }
        function SetFileField(fileUrl) {
            document.getElementById('xFilePath').value = fileUrl;
        }

        function SetFileField1(fileUrl) {
            document.getElementById('xFilePath1').value = fileUrl;
        }

        function SetFileField2(fileUrl) {
            document.getElementById('xFilePath2').value = fileUrl;
        }
    </script>
    <div class="content-wrapper" style="min-height: 916px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Doktorlarımız  </h1>

        </section>

        <!-- Main content -->
        <section class="content">

            <%--<div class="row">
                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Sayfa Üst Banner</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa  Banner</label>
                                        <div class="col-sm-10">
                                            <asp:Literal ID="ltrTopBannerTR" runat="server"></asp:Literal><br />
                                            <br />
                                            <input id="xFilePath1" name="FilePathTR" type="text" size="60" />
                                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer1();" />
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdateTR" runat="server" Text="Güncelle" class="btn btn-info pull-right" OnClick="btnUpdateTR_Click" />
                                    <br />
                                    <asp:Label ID="lblUyariTR" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--%>

            <div class="row">
                <div class="col-xs-12">
                    <a href="/admin/yeni_doktor_ekle.aspx" class="btn btn-info pull-right">Yeni Doktor Ekle</a><br />
                    <br />
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Doktorlarımız</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Ad Soyad</th>
                                        <th>Tıbbi Birimi</th>
                                        <th>Düzenle</th>
                                        <th>Aktif / Pasif</th>

                                    </tr>
                                </thead>

                                <tbody>
                                    <asp:Repeater ID="rptDoctor" runat="server" OnItemDataBound="rptDoctor_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>

                                                <td><%#Eval ("Unvan") %> <%#Eval ("DoktorAdi") %></td>
                                                <td><%#Eval ("BolumAdi") %></td>
                                                <td>
                                                    <a class="btn btn-s btn-success" href="doktor_detay.aspx?doktor_id=<%#Eval ("id") %>&Lang=TR">Düzenle </a>
                                                </td>
                                                <td>
                                                    <asp:LinkButton runat="server" ID="lnkActive" CssClass="btn btn-xs btn-success" OnCommand="lnkActive_Command" CommandArgument='<%#Eval("id") %>' Visible="false">
                                                 <span class="glyphicon glyphicon-ok"></span>Aktif Yap
                                                    </asp:LinkButton>
                                                    <asp:LinkButton runat="server" ID="lnkPasif" CssClass="btn btn-xs btn-danger" OnCommand="lnkPasif_Command" CommandArgument='<%#Eval("id") %>' Visible="false">
                                                 <span class="glyphicon glyphicon-remove"></span>Pasif Yap
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>



    <%--    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>--%>

    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <script src="plugins/fastclick/fastclick.min.js"></script>

    <script src="dist/js/demo.js"></script>

    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
</asp:Content>

