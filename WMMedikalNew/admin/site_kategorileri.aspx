﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="site_kategorileri.aspx.cs" Inherits="WMMedikalNew.admin.WebForm6" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        .dataTables_length
        {
            display:none;
        }
        .dataTables_filter
        {
            float:right;
        }
        .dataTables_info
        {
            display:none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="content-wrapper" style="min-height: 916px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Mikro Site Kategorileri </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-3">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Yeni Kategori Ekle</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Kategori Adı - TR</label>
                                <asp:TextBox ID="txtNameTR" runat="server" class="form-control" placeholder="Kategori Adı"></asp:TextBox>
                            </div>

                           <div class="form-group">
                                <label for="exampleInputEmail1">Kategori Adı - EN</label>
                                <asp:TextBox ID="txtNameEN" runat="server" class="form-control" placeholder="Kategori Adı"></asp:TextBox>
                            </div>

                        </div>
                        <div class="box-footer">

                            <asp:Button ID="btnEkle" runat="server" Text="Kategori Ekle" class="btn btn-primary" OnClick="btnEkle_Click" /><br />
                            <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>

                        </div>
                    </div>

                </div>
                <div class="col-xs-9">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Mikro Site Kategorileri</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Kategori Adı TR</th>
                                        <th>Kategori Adı EN</th>
                                        <th>İşlem</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <asp:Repeater ID="rptCategoryList" runat="server" OnItemDataBound="rptCategoryList_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%#Eval ("site_category_name") %></td>
                                                <td> <asp:Literal ID="ltrCategoryName" runat="server"></asp:Literal></td>
                                              
                                                <td>
                                                          <a href="siteler.aspx" class="btn btn-s btn-success">Siteleri Gör</a> 
                                                
                                                        <asp:LinkButton runat="server" OnCommand="lnkRemove_Command" CommandArgument='<%#Eval ("category_id") %>' ID="lnkRemove" OnClientClick="return confirm('silmek istediğinizden emin misiniz!');" CssClass="btn btn-s btn-danger" >Sil
                                                    </asp:LinkButton>
                                                </td>
                                                
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>



<%--    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>--%>

    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <script src="plugins/fastclick/fastclick.min.js"></script>

    <script src="dist/js/demo.js"></script>

    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
</asp:Content>
