﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WMMedikalNew.Core;

namespace WMMedikalNew.admin
{
    public partial class login :cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (MemberManager.IsLoggedIn)
            {
                Response.Redirect("/default.aspx", false);
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtKullaniciAdi.Text == null)
                {

                    ltrError.Text = "Kullanıcı Adı Boş Olamaz";
                }
                else if (txtSifre.Text != null)
                {
                    if (MemberManager.PanelLogin(txtKullaniciAdi.Text, txtSifre.Text, false, true))
                    {
                        Response.Redirect("/admin/default.aspx", false);
                    }
                    else
                    {
                        ltrError.Text = "Giriş Yapılamadı";
                    }
                }
            }
            catch (Exception)
            {

            }
        }
    }
}