﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm19 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != "")
            {
                if (!IsPostBack)
                {
                    int ID = Request.QueryString["id"].toint();
                    var KurumTipi = db.organization_type.Select(s => new
                    {
                        s.id,
                        s.organization_name_en,
                        s.organization_name_tr,
                        hospital_id = s.Hastaneler.id

                    }).Where(q => q.id == ID).FirstOrDefault();

                    //drpHospital.DataSource = db.Hastaneler.ToList();
                    //drpHospital.DataTextField = "HastaneAdi";
                    //drpHospital.DataValueField = "id";
                    //drpHospital.DataBind();
                    //drpHospital.SelectedValue = KurumTipi.hospital_id.ToString();

                    kurumTipiTR.Text = KurumTipi.organization_name_tr;
                    kurumTipiEN.Text = KurumTipi.organization_name_en;

                }
            }
            else
                Response.Redirect("/", false);
        }

        protected void btnHizmetEkle_Click(object sender, EventArgs e)
        {
            int ID = Request.QueryString["id"].toint();
            var KurumTipi = db.organization_type.Where(q => q.id == ID).FirstOrDefault();

            KurumTipi.organization_name_tr = kurumTipiTR.Text.Trim();
            KurumTipi.organization_name_en = kurumTipiEN.Text.Trim();
            //KurumTipi.hospital_id = drpHospital.SelectedValue.toint();

            if (db.SaveChanges() > 0)
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            else
                lblUyari.Text = "Bir hata oluştu tekrar deneyin";
        }
    }
}