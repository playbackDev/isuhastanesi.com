﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="haberler.aspx.cs" Inherits="WMMedikalNew.admin.WebForm4" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
          <style>
        .dataTables_length {
            display: none;
        }

        .dataTables_filter {
            float: right;
        }

        .dataTables_info {
            display: none;
        }
    </style>
      <script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
    <script type="text/javascript">

        function BrowseServer() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField;
            finder.popup();
        }

        function BrowseServer1() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField1;
            finder.popup();
        }

        function BrowseServer2() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField2;
            finder.popup();
        }
        function SetFileField(fileUrl) {
            document.getElementById('xFilePath').value = fileUrl;
        }

        function SetFileField1(fileUrl) {
            document.getElementById('xFilePath1').value = fileUrl;
        }

        function SetFileField2(fileUrl) {
            document.getElementById('xFilePath2').value = fileUrl;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="min-height: 916px;">
        <!-- Content Header (Page header) -->
        <%--<section class="content-header">
            <h1>İçerikler</h1>
        </section>--%>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                 <%--<div class="col-md-9">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title"> Sayfa Üst Banner</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                               <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa Duyuru Banner</label>
                                        <div class="col-sm-10">
                                            <asp:Image ID="ImageBanner"  runat="server" Width="500px" /><br /><br />
                                            <input id="xFilePath1" name="FilePathTR" type="text" size="60" />
                                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer1();" />
                                        </div>
                                    </div>

                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="Button1" runat="server" Text="Güncelle" class="btn btn-info pull-right" OnClick="btnUpdate_Click" />
                                        <br />
                                    <asp:Label ID="Label2" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>

                <div class="col-xs-12 col-md-12">
                       
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">İçerikler</h3>
                            <asp:HyperLink ID="hlAddNew" runat="server" CssClass="btn btn-primary pull-right"><span class="glyphicon glyphicon-plus"></span> Yeni İçerik Ekle</asp:HyperLink>
                        </div>
                       
                        <div class="box-body">
                           
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Durum</th>
                                        <th>Başlık</th>
                                        <th>Düzenle</th>
                                        <th>İşlem</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <asp:Repeater ID="rptNewList" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%#Eval ("active_status") %></td>
                                                <td><%#Eval ("news_title") %></td>
                                                <td>
                                                    <a class="btn btn-s btn-success" href="haber_detay.aspx?haber_id=<%#Eval ("news_id") %>">Düzenle </a>
                                                   <%-- <a class="btn btn-s btn-info" href="haber_detay.aspx?haber_id=<%#Eval ("news_id") %>&Lang=EN">EN</a>--%>
                                                </td>
                                                <td>
                                                    <asp:LinkButton runat="server" ID="lnkRemove" OnCommand="lnkRemove_Command" CommandArgument='<%#Eval ("news_id") %>' OnClientClick="return confirm('silmek istediğinizden emin misiniz!');" CssClass="btn btn-s btn-danger">Sil
                                                    </asp:LinkButton>
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>



<%--    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>--%>

    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <script src="plugins/fastclick/fastclick.min.js"></script>

    <script src="dist/js/demo.js"></script>

    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
</asp:Content>
