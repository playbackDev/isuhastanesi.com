﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="aboneler.aspx.cs" Inherits="WMMedikalNew.admin.WebForm9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        .dataTables_length
        {
            display:none;
        }
        .dataTables_filter
        {
            float:right;
        }
        .dataTables_info
        {
            display:none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="min-height: 916px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Tıbbi Hizmetlerimiz  </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-5">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title"> E-Posta Abnoeler</h3>
                        </div>
                      
                         <asp:LinkButton ID="lnkProductExport" runat="server"  OnClick="lnkProductExport_Click" Text="Excel Çıktısı Al" CssClass="btn btn-info" /><br />
                            <asp:Literal ID="ltrExcel" runat="server"></asp:Literal>
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Durum</th>
                                        <th>E-Posta</th>

                                    </tr>
                                </thead>

                                <tbody>
                                    <asp:Repeater ID="rptNewsLetter" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%#Eval ("active_status") %></td>
                                                <td><%#Eval ("email") %></td>
                                               
                                        
                                                
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>





    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <script src="plugins/fastclick/fastclick.min.js"></script>

    <script src="dist/js/demo.js"></script>

    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
</asp:Content>
