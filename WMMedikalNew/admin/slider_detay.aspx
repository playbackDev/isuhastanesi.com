﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="slider_detay.aspx.cs" Inherits="WMMedikalNew.admin.slider_detay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
    <script type="text/javascript">
        function BrowseServer() {
            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';
            finder.selectActionFunction = SetFileField;
            finder.popup();
        }
        function SetFileField(fileUrl) { document.getElementById('FilePath').value = fileUrl; }
        function BrowseServer2() {
            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';
            finder.selectActionFunction = SetFileField2;
            finder.popup();
        }
        function SetFileField2(fileUrl) { document.getElementById('FilePath2').value = fileUrl; }
    </script>
    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content-header">
            <h1>Slider Yönetimi </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-3">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Slider Düzenle</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Link   (http ile ekleyiniz örn : htttp://www.google.com) </label>
                                <asp:TextBox ID="txtURL" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Masaüstü Banner</label>
                                <input id="FilePath" name="FilePath" type="text" size="46" />
                                <input type="button" value="Sunucuyu Gez" onclick="BrowseServer();" />
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Mobil Banner</label>
                                <input id="FilePath2" name="FilePath2" type="text" size="46" />
                                <input type="button" value="Sunucuyu Gez" onclick="BrowseServer2();" />
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Durum (Aktif/Pasif)</label>
                                <asp:CheckBox ID="ckIsActive" runat="server" class="form-control" />
                            </div>
                        </div>
                        <div class="box-footer">

                            <asp:Button ID="btnGuncelle" runat="server" Text="Güncelle" class="btn btn-primary" OnClick="btnGuncelle_Click" /><br />
                            <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>

                        </div>
                    </div>

                </div>
                <div class="col-xs-9">

                    <asp:Image ID="Image1" runat="server" />
                    <asp:Image ID="Image2" runat="server" />
                </div>

            </div>
        </section>
    </div>

</asp:Content>
