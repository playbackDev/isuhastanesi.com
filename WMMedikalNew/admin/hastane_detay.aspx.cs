﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm11 : cBase
    {
        int HastaneID;
        string Lang;
        protected void Page_Load(object sender, EventArgs e)
        {
            HastaneID = Request.QueryString["hastane_id"].toint();
            Lang = Request.QueryString["Lang"].ToString();
            if(!IsPostBack)
            {
               
                var _HastaneDetail = db.our_hospital_lang.Select(s => new { s.id, s.hospital_name, s.our_hospital.is_active, s.hospital_id,s.lang }).Where(q => q.hospital_id == HastaneID && q.lang == Lang ).FirstOrDefault();
                ltrHastaneTitle.Text = _HastaneDetail.hospital_name;
                ltrLang.Text = _HastaneDetail.lang;
                txtHospitalName.Text = _HastaneDetail.hospital_name;
                ckIsActive.Checked = _HastaneDetail.is_active;
                hdnID.Value = _HastaneDetail.id.ToString();
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int ID = hdnID.Value.toint();
            if (txtHospitalName.Text.Trim() != "")
            {
                var _UpdateLang = db.our_hospital_lang.Where(q => q.id == ID).FirstOrDefault();
                _UpdateLang.hospital_name = txtHospitalName.Text.Trim();

                var _Update = db.our_hospital.Where(q => q.id == HastaneID).FirstOrDefault();
                _Update.is_active = ckIsActive.Checked;



                if(Lang == "TR")
                {
                    _Update.hospital_name = txtHospitalName.Text.Trim();
                }
                
                if (db.SaveChanges() > 0)
                    Page.Response.Redirect(Page.Request.Url.ToString(), false);
                
            }
            else
                lblUyari2.Text = "Hastane Adı Boş Bırakmayın";
        }
    }
}