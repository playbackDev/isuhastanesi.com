﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class yeni_doktor_ekle : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var _medicalService = db.TibbiBolumler_Dil.Where(q => q.TibbiBolumler.HastaneID == 27 && q.Dil == "TR").ToList();

                if (_medicalService != null)
                {

                    drpTibbiBolumler.DataSource = _medicalService;
                    drpTibbiBolumler.DataTextField = "BolumAdi";
                    drpTibbiBolumler.DataValueField = "BolumID";
                    drpTibbiBolumler.DataBind();

                    drpTibbiBolumler.Items.Insert(0, new ListItem("Bölüm Seçiniz", "0"));
                }
            }
        }

        protected void btnDoktorEkle_Click(object sender, EventArgs e)
        {
            string imageURL = string.Empty;
            imageURL = Request.Form["FilePath"].ToString();

            if (drpTibbiBolumler.SelectedValue.toint() > 0)
            {
                if (!string.IsNullOrEmpty(txtMedinServiceID.Text.Trim()))
                {
                    if (!string.IsNullOrEmpty(txtDoktorAdi.Text.Trim()))
                    {
                        if (!string.IsNullOrEmpty(txtUnvan.Text.Trim()))
                        {
                            int service_id = txtMedinServiceID.Text.Trim().toint();
                            int hastane_id = drpHospital.SelectedValue.toint();
                            int bolum_id = drpTibbiBolumler.SelectedValue.toint();
                            Doktorlar _yeniDoktor = new Doktorlar();
                            _yeniDoktor.DoktorAdi = txtDoktorAdi.Text.Trim();
                            _yeniDoktor.ServisID = service_id;
                            _yeniDoktor.HastaneID = 27;
                            _yeniDoktor.TibbiBolumID = bolum_id;
                            _yeniDoktor.ResimAdi = imageURL;
                            _yeniDoktor.is_active = ckIsActive.Checked;
                            _yeniDoktor.Unvan = txtUnvan.Text.Trim();

                            db.Doktorlar.Add(_yeniDoktor);

                            if (db.SaveChanges() > 0)
                            {
                                Doktorlar_Dil _yeniDoktorDilTR = new Doktorlar_Dil();
                                _yeniDoktorDilTR.doktor_id = _yeniDoktor.id;
                                _yeniDoktorDilTR.dil = "TR";

                                _yeniDoktor.Doktorlar_Dil.Add(_yeniDoktorDilTR);

                                Doktorlar_Dil _yeniDoktorDilEN = new Doktorlar_Dil();
                                _yeniDoktorDilEN.doktor_id = _yeniDoktor.id;
                                _yeniDoktorDilEN.dil = "EN";

                                _yeniDoktor.Doktorlar_Dil.Add(_yeniDoktorDilEN);

                                if (db.SaveChanges() > 0)
                                    Response.Redirect("doktor_detay.aspx?doktor_id=" + _yeniDoktor.id + "&Lang=TR");
                                else
                                    Alert("Dil Değerleri Eklenemedi.");
                            }
                        }
                        else
                            Alert("Doktor Ünvanı  Girmelisiniz.");
                    }
                    else
                        Alert("Doktor Adı Girmelisiniz.");
                }
                else
                    Alert("Servis ID Girmelisiniz -> Bilmiyorsanız 0 yazabilirsiniz.");
            }
            else
                Alert("Tıbbi Bölüm Seçmelisiniz.");
        }

        protected void drpHospital_SelectedIndexChanged(object sender, EventArgs e)
        {
            int hastane_id = drpHospital.SelectedValue.toint();

            if (hastane_id > 0)
            {
                var _medicalService = db.TibbiBolumler_Dil.Where(q => q.TibbiBolumler.HastaneID == hastane_id && q.Dil == "TR").ToList();

                if (_medicalService != null)
                {

                    drpTibbiBolumler.DataSource = _medicalService;
                    drpTibbiBolumler.DataTextField = "BolumAdi";
                    drpTibbiBolumler.DataValueField = "BolumID";
                    drpTibbiBolumler.DataBind();

                    drpTibbiBolumler.Items.Insert(0, new ListItem("Bölüm Seçiniz", "0"));
                }
            }
        }
    }
}