﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm10 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            fillPage();
        }

        private void fillPage()
        {
        

            var _hastaneler = db.Hastaneler.ToList();

            rptHospitalList.DataSource = _hastaneler;
            rptHospitalList.DataBind();
        }

        protected void rptHospitalList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater rptHastaneSayfalar = (Repeater)e.Item.FindControl("rptHastaneSayfalar");
            int HastaneID = DataBinder.Eval(e.Item.DataItem, "id").toint();
            var HastaneSayfalar = db.Hastane_Sayfalar.Where(q => q.hastane_id == HastaneID && q.id!=5 && q.id!=13 ).ToList();

            rptHastaneSayfalar.DataSource = HastaneSayfalar;
            rptHastaneSayfalar.DataBind();

        }

        protected void rptHastaneSayfalar_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            HyperLink hypHastaneSayfa = (HyperLink)e.Item.FindControl("hypHastaneSayfa");
            int SayfaID = DataBinder.Eval(e.Item.DataItem, "id").toint();
            int SayfaTipi = DataBinder.Eval(e.Item.DataItem, "sayfa_tipi").toint();
            int HastaneID = DataBinder.Eval(e.Item.DataItem, "hastane_id").toint();

            switch (SayfaTipi)
            {
                case 1:
                    hypHastaneSayfa.NavigateUrl = "/admin/hastanemiz.aspx?page_id=" + SayfaID;
                    break;
                case 2:
                    hypHastaneSayfa.NavigateUrl = "/admin/tibbi_hizmetlerimiz.aspx?page_id=" + SayfaID;
                    break;
                case 3:
                    hypHastaneSayfa.NavigateUrl = "/admin/doktor_listesi.aspx?hastane_id=" + HastaneID+"&page_id="+SayfaID;
                    break;

                case 4:
                    hypHastaneSayfa.NavigateUrl = "/admin/h_anlasmali_kurumlar.aspx?hastane_id=" + HastaneID+"&page_id="+SayfaID;
                    break;

                case 5:
                    hypHastaneSayfa.NavigateUrl = "/admin/galeri.aspx?page_id=" + SayfaID;
                    break;
                case 6:
                    hypHastaneSayfa.Visible = false;
                    break;

                case 7:
                    hypHastaneSayfa.NavigateUrl = "/admin/h_kalite_yonetimi.aspx?page_id="+SayfaID;
                    break;
            }

            
        }

        //protected void btnHastaneEkle_Click(object sender, EventArgs e)
        //{
        //    try
        //    {

        //        if (FileUpload1.HasFiles)
        //        {
        //            if (txtHastaneAdi.Text.Trim() != "")
        //            {
        //                int uzunluk = FileUpload1.FileName.Split('.').Length;
        //                string random = CreateRandomValue(10, true, true, true, false) + "." + FileUpload1.FileName.Split('.')[uzunluk - 1];
        //                FileUpload1.SaveAs(Server.MapPath("~/uploads/hospitals/" + random));

        //                our_hospital _newHospital = new our_hospital();
        //                _newHospital.hospital_name = txtHastaneAdi.Text.Trim();
        //                _newHospital.is_active = ckIsActive.Checked;
        //                _newHospital.hospital_image = random;


        //                db.our_hospital.Add(_newHospital);

        //                pages _newPageHospital = new pages();
        //                _newPageHospital.page_name = txtHastaneAdi.Text.Trim();
        //                _newPageHospital.parent_id = 2;
        //                _newPageHospital.hospital_id = _newHospital.id;

        //                db.pages.Add(_newPageHospital);

        //                if (db.SaveChanges() > 0)
        //                {
        //                    //hastaneler için alt sayfa ekleme

        //                    our_hospital_lang _newHospitalTR = new our_hospital_lang();
        //                    _newHospitalTR.hospital_name = txtHastaneAdi.Text.Trim();
        //                    _newHospitalTR.lang = "TR";
        //                    _newHospitalTR.hospital_id = _newHospital.id;

        //                    our_hospital_lang _newHospitalEN = new our_hospital_lang();
        //                    _newHospitalEN.hospital_name = txtHastaneAdi.Text.Trim();
        //                    _newHospitalEN.lang = "EN";
        //                    _newHospitalEN.hospital_id = _newHospital.id;


        //                    hl_contact _newContact = new hl_contact();
        //                    _newContact.hospital_id = _newHospital.id;
        //                    _newContact.addressEN = "Adres İngilizce İçerik";
        //                    _newContact.addressTR = "Adres Türkçe İçerik";



        //                    pages _newPageAboutUS = new pages();
        //                    _newPageAboutUS.page_name = "Hakkımızda";
        //                    _newPageAboutUS.parent_id = _newPageHospital.id.toshort();
        //                    _newPageAboutUS.hospital_id = _newHospital.id;

        //                    pages _newPageMedicalService = new pages();
        //                    _newPageMedicalService.page_name = "Tıbbi Bölümler";
        //                    _newPageMedicalService.parent_id = _newPageHospital.id.toshort();
        //                    _newPageMedicalService.hospital_id = _newHospital.id;

        //                    pages _newPageOurdoctors = new pages();
        //                    _newPageOurdoctors.page_name = "Doktorlarımız";
        //                    _newPageOurdoctors.parent_id = _newPageHospital.id.toshort();
        //                    _newPageOurdoctors.hospital_id = _newHospital.id;

        //                    pages _newPageKurumlar = new pages();
        //                    _newPageKurumlar.page_name = "Anlaşmalı Kurumlar";
        //                    _newPageKurumlar.parent_id = _newPageHospital.id.toshort();
        //                    _newPageKurumlar.hospital_id = _newHospital.id;

        //                    pages _newPageGallery = new pages();
        //                    _newPageGallery.page_name = "Galeri";
        //                    _newPageGallery.parent_id = _newPageHospital.id.toshort();
        //                    _newPageGallery.hospital_id = _newHospital.id;

        //                    pages _newPageEservices = new pages();
        //                    _newPageEservices.page_name = "E-Randevu";
        //                    _newPageEservices.parent_id = _newPageHospital.id.toshort();
        //                    _newPageEservices.hospital_id = _newHospital.id;

        //                    pages _newPageKaliteYonetimi = new pages();
        //                    _newPageKaliteYonetimi.page_name = "Kalite Yönetimi";
        //                    _newPageKaliteYonetimi.parent_id = _newPageHospital.id.toshort();
        //                    _newPageKaliteYonetimi.hospital_id = _newHospital.id;

        //                     pages _newPageHastanemiz = new pages();
        //                    _newPageHastanemiz.page_name = "Hastanemiz";
        //                    _newPageHastanemiz.parent_id = _newPageHospital.id.toshort();
        //                    _newPageHastanemiz.hospital_id = _newHospital.id;


        //                    db.pages.Add(_newPageAboutUS);
        //                    db.pages.Add(_newPageMedicalService);
        //                    db.pages.Add(_newPageOurdoctors);
        //                    db.pages.Add(_newPageKurumlar);
        //                    db.pages.Add(_newPageGallery);
        //                    db.pages.Add(_newPageEservices);
        //                    db.pages.Add(_newPageKaliteYonetimi);
        //                    db.pages.Add(_newPageHastanemiz);

        //                    db.our_hospital_lang.Add(_newHospitalTR);
        //                    db.our_hospital_lang.Add(_newHospitalEN);
        //                    db.hl_contact.Add(_newContact);

        //                    if (db.SaveChanges() > 0)
        //                    {
        //                        //sayfalar için dil tablosuna veri ekleme

        //                        page_langs _newPageLangHospitalNameTR = new page_langs();
        //                        _newPageLangHospitalNameTR.page_name = txtHastaneAdi.Text.Trim();
        //                        _newPageLangHospitalNameTR.page_id = _newPageHospital.id.toint();
        //                        _newPageLangHospitalNameTR.is_active = true;
        //                        _newPageLangHospitalNameTR.lang = "TR";
        //                        _newPageLangHospitalNameTR.parent_id = 2;

        //                        db.page_langs.Add(_newPageLangHospitalNameTR);

        //                        page_langs _newPageLangHospitalNameEN = new page_langs();
        //                        _newPageLangHospitalNameEN.page_name = txtHastaneAdi.Text.Trim();
        //                        _newPageLangHospitalNameEN.page_id = _newPageHospital.id.toint();
        //                        _newPageLangHospitalNameEN.is_active = true;
        //                        _newPageLangHospitalNameEN.lang = "EN";
        //                        _newPageLangHospitalNameEN.parent_id = 2;

        //                        db.page_langs.Add(_newPageLangHospitalNameEN);

        //                        page_langs _newPageLangAboutTR = new page_langs();
        //                        _newPageLangAboutTR.page_name = "Hakkımızda";
        //                        _newPageLangAboutTR.page_id = _newPageAboutUS.id.toint();
        //                        _newPageLangAboutTR.is_active = true;
        //                        _newPageLangAboutTR.lang = "TR";
        //                        _newPageLangAboutTR.parent_id = _newPageHospital.id.toshort();

        //                        db.page_langs.Add(_newPageLangAboutTR);

        //                        page_langs _newPageLangAboutEN = new page_langs();
        //                        _newPageLangAboutEN.page_name = "About Us";
        //                        _newPageLangAboutEN.page_id = _newPageAboutUS.id.toint();
        //                        _newPageLangAboutEN.is_active = true;
        //                        _newPageLangAboutEN.lang = "EN";
        //                        _newPageLangAboutEN.parent_id = _newPageHospital.id.toshort();

        //                        db.page_langs.Add(_newPageLangAboutEN);

        //                        page_langs _newPageLangMedicalServicesTR = new page_langs();
        //                        _newPageLangMedicalServicesTR.page_name = "Tıbbi Bölümler";
        //                        _newPageLangMedicalServicesTR.page_id = _newPageMedicalService.id.toint();
        //                        _newPageLangMedicalServicesTR.is_active = true;
        //                        _newPageLangMedicalServicesTR.lang = "TR";
        //                        _newPageLangMedicalServicesTR.parent_id = _newPageHospital.id.toshort();

        //                        db.page_langs.Add(_newPageLangMedicalServicesTR);

        //                        page_langs _newPageLangMedicalServicesEN = new page_langs();
        //                        _newPageLangMedicalServicesEN.page_name = "Medical Services";
        //                        _newPageLangMedicalServicesEN.page_id = _newPageMedicalService.id.toint();
        //                        _newPageLangMedicalServicesEN.is_active = true;
        //                        _newPageLangMedicalServicesEN.lang = "EN";
        //                        _newPageLangMedicalServicesEN.parent_id = _newPageHospital.id.toshort();

        //                        db.page_langs.Add(_newPageLangMedicalServicesEN);

        //                        page_langs _newPageLangOurDoctorsTR = new page_langs();
        //                        _newPageLangOurDoctorsTR.page_name = "Doktorlarımız";
        //                        _newPageLangOurDoctorsTR.page_id = _newPageOurdoctors.id.toint();
        //                        _newPageLangOurDoctorsTR.is_active = true;
        //                        _newPageLangOurDoctorsTR.lang = "TR";
        //                        _newPageLangOurDoctorsTR.parent_id = _newPageHospital.id.toshort();

        //                        db.page_langs.Add(_newPageLangOurDoctorsTR);

        //                        page_langs _newPageLangOurDoctorsEN = new page_langs();
        //                        _newPageLangOurDoctorsEN.page_name = "Our Doctors";
        //                        _newPageLangOurDoctorsEN.page_id = _newPageOurdoctors.id.toint();
        //                        _newPageLangOurDoctorsEN.is_active = true;
        //                        _newPageLangOurDoctorsEN.lang = "EN";
        //                        _newPageLangOurDoctorsEN.parent_id = _newPageHospital.id.toshort();

        //                        db.page_langs.Add(_newPageLangOurDoctorsEN);

        //                        page_langs _newPageLangAnlasmaliKurumlarTR = new page_langs();
        //                        _newPageLangAnlasmaliKurumlarTR.page_name = "Anlaşmalı Kurumlar";
        //                        _newPageLangAnlasmaliKurumlarTR.page_id = _newPageKurumlar.id.toint();
        //                        _newPageLangAnlasmaliKurumlarTR.is_active = true;
        //                        _newPageLangAnlasmaliKurumlarTR.lang = "TR";
        //                        _newPageLangAnlasmaliKurumlarTR.parent_id = _newPageHospital.id.toshort();

        //                        db.page_langs.Add(_newPageLangAnlasmaliKurumlarTR);

        //                        page_langs _newPageLangAnlasmaliKurumlarEN = new page_langs();
        //                        _newPageLangAnlasmaliKurumlarEN.page_name = "Anlaşmalı Kurumlar";
        //                        _newPageLangAnlasmaliKurumlarEN.page_id = _newPageKurumlar.id.toint();
        //                        _newPageLangAnlasmaliKurumlarEN.is_active = true;
        //                        _newPageLangAnlasmaliKurumlarEN.lang = "EN";
        //                        _newPageLangAnlasmaliKurumlarEN.parent_id = _newPageHospital.id.toshort();

        //                        db.page_langs.Add(_newPageLangAnlasmaliKurumlarEN);


        //                        page_langs _newPageLangGaleriTR = new page_langs();
        //                        _newPageLangGaleriTR.page_name = "Galeri";
        //                        _newPageLangGaleriTR.page_id = _newPageGallery.id.toint();
        //                        _newPageLangGaleriTR.is_active = true;
        //                        _newPageLangGaleriTR.lang = "TR";
        //                        _newPageLangGaleriTR.parent_id = _newPageHospital.id.toshort();

        //                        db.page_langs.Add(_newPageLangGaleriTR);

        //                        page_langs _newPageLangGaleriEN = new page_langs();
        //                        _newPageLangGaleriEN.page_name = "Gallery";
        //                        _newPageLangGaleriEN.page_id = _newPageGallery.id.toint();
        //                        _newPageLangGaleriEN.is_active = true;
        //                        _newPageLangGaleriEN.lang = "EN";
        //                        _newPageLangGaleriEN.parent_id = _newPageHospital.id.toshort();

        //                        db.page_langs.Add(_newPageLangGaleriEN);

        //                        page_langs _newPageLangServicesTR = new page_langs();
        //                        _newPageLangServicesTR.page_name = "E-Randevu";
        //                        _newPageLangServicesTR.page_id = _newPageEservices.id.toint();
        //                        _newPageLangServicesTR.is_active = true;
        //                        _newPageLangServicesTR.lang = "TR";
        //                        _newPageLangServicesTR.parent_id = _newPageHospital.id.toshort();

        //                        db.page_langs.Add(_newPageLangServicesTR);

        //                        page_langs _newPageLangServicesEN = new page_langs();
        //                        _newPageLangServicesEN.page_name = "E-Appointment";
        //                        _newPageLangServicesEN.page_id = _newPageEservices.id.toint();
        //                        _newPageLangServicesEN.is_active = true;
        //                        _newPageLangServicesEN.lang = "EN";
        //                        _newPageLangServicesEN.parent_id = _newPageHospital.id.toshort();

        //                        db.page_langs.Add(_newPageLangServicesEN);

        //                        page_langs _newPageLangKaliteYonetimiTR = new page_langs();
        //                        _newPageLangKaliteYonetimiTR.page_name = "Kalite Yönetimi";
        //                        _newPageLangKaliteYonetimiTR.page_id = _newPageKaliteYonetimi.id.toint();
        //                        _newPageLangKaliteYonetimiTR.is_active = true;
        //                        _newPageLangKaliteYonetimiTR.lang = "TR";
        //                        _newPageLangKaliteYonetimiTR.parent_id = _newPageHospital.id.toshort();

        //                        db.page_langs.Add(_newPageLangKaliteYonetimiTR);

        //                        page_langs _newPageLangKaliteYonetimiEN = new page_langs();
        //                        _newPageLangKaliteYonetimiEN.page_name = "Kalite Yönetimi";
        //                        _newPageLangKaliteYonetimiEN.page_id = _newPageKaliteYonetimi.id.toint();
        //                        _newPageLangKaliteYonetimiEN.is_active = true;
        //                        _newPageLangKaliteYonetimiEN.lang = "EN";
        //                        _newPageLangKaliteYonetimiEN.parent_id = _newPageHospital.id.toshort();

        //                        db.page_langs.Add(_newPageLangKaliteYonetimiEN);

        //                        page_langs _newPageLangHastanemizTR = new page_langs();
        //                        _newPageLangHastanemizTR.page_name = "Hastanemiz";
        //                        _newPageLangHastanemizTR.page_id = _newPageHastanemiz.id.toint();
        //                        _newPageLangHastanemizTR.is_active = true;
        //                        _newPageLangHastanemizTR.lang = "TR";
        //                        _newPageLangHastanemizTR.parent_id = _newPageHospital.id.toshort();

        //                        db.page_langs.Add(_newPageLangHastanemizTR);

        //                        page_langs _newPageLangHastanemizEN = new page_langs();
        //                        _newPageLangHastanemizEN.page_name = "Hastanemiz";
        //                        _newPageLangHastanemizEN.page_id = _newPageHastanemiz.id.toint();
        //                        _newPageLangHastanemizEN.is_active = true;
        //                        _newPageLangHastanemizEN.lang = "EN";
        //                        _newPageLangHastanemizEN.parent_id = _newPageHospital.id.toshort();

        //                        db.page_langs.Add(_newPageLangHastanemizEN);

        //                        if (db.SaveChanges() > 0)
        //                        {

        //                            //sayfa detay ve dilleri
        //                            page_detail_lang _pageDetailHastanemizTR = new page_detail_lang();
        //                            _pageDetailHastanemizTR.description="İçerik Eklenmedi";
        //                            _pageDetailHastanemizTR.lang="TR";
        //                            _pageDetailHastanemizTR.page_id = _newPageHastanemiz.id;

        //                            db.page_detail_lang.Add(_pageDetailHastanemizTR);

        //                            page_detail_lang _pageDetailHastanemizEN = new page_detail_lang();
        //                            _pageDetailHastanemizEN.description = "İçerik Eklenmedi";
        //                            _pageDetailHastanemizEN.lang = "EN";
        //                            _pageDetailHastanemizEN.page_id = _newPageHastanemiz.id;

        //                            db.page_detail_lang.Add(_pageDetailHastanemizEN);


        //                            page_detail_lang _pageDetailKaliteYonetimiTR = new page_detail_lang();
        //                            _pageDetailKaliteYonetimiTR.description = "İçerik Eklenmedi";
        //                            _pageDetailKaliteYonetimiTR.lang = "TR";
        //                            _pageDetailKaliteYonetimiTR.page_id = _newPageKaliteYonetimi.id;

        //                            db.page_detail_lang.Add(_pageDetailKaliteYonetimiTR);

        //                            page_detail_lang _pageDetailKaliteYonetimiEN = new page_detail_lang();
        //                            _pageDetailKaliteYonetimiEN.description = "İçerik Eklenmedi";
        //                            _pageDetailKaliteYonetimiEN.lang = "EN";
        //                            _pageDetailKaliteYonetimiEN.page_id = _newPageKaliteYonetimi.id;

        //                            db.page_detail_lang.Add(_pageDetailKaliteYonetimiEN);

        //                            page_detail_lang _pageDetailHakkimizdaTR = new page_detail_lang();
        //                            _pageDetailHakkimizdaTR.description = "İçerik Eklenmedi";
        //                            _pageDetailHakkimizdaTR.lang = "TR";
        //                            _pageDetailHakkimizdaTR.page_id = _newPageAboutUS.id;

        //                            db.page_detail_lang.Add(_pageDetailHakkimizdaTR);

        //                            page_detail_lang _pageDetailHakkimizdaEN = new page_detail_lang();
        //                            _pageDetailHakkimizdaEN.description = "İçerik Eklenmedi";
        //                            _pageDetailHakkimizdaEN.lang = "EN";
        //                            _pageDetailHakkimizdaEN.page_id = _newPageAboutUS.id;

        //                            db.page_detail_lang.Add(_pageDetailHakkimizdaEN);

        //                            if(db.SaveChanges()>0)
        //                            {

        //                                Response.Redirect("hastanelerimiz.aspx", false);
        //                            }

        //                        }
        //                    }

        //                    else
        //                        lblUyari.Text = "Bir hata oluştu.";
        //                }
        //                else
        //                    lblUyari.Text = "Hastane Adı Girmediniz";
        //            }
        //            else
        //                lblUyari.Text = "Resim Seçmediniz.";
        //        }
        //    }
        //    catch ( DbEntityValidationException hata)
        //    {
        //        foreach (var ht in hata.EntityValidationErrors)
        //        {
        //            Response.Write(ht.Entry.Entity.GetType().Name + " " + ht.Entry.State);

        //            foreach (var valerror in ht.ValidationErrors)
        //            { 
        //                Response.Write(valerror.PropertyName + " " + valerror.ErrorMessage);
        //            }
        //        }
        //    }





        //}

        //protected void lnkRemove_Command(object sender, CommandEventArgs e)
        //{
        //    int hospitalID = e.CommandArgument.toint();
        //    var _deleteContanct = db.hl_contact.Where(q => q.hospital_id == hospitalID).FirstOrDefault();
        //    var _deletelang = db.our_hospital_lang.Where(q => q.hospital_id == hospitalID).ToList();
        //    var _pagelang = db.page_langs.Where(q => q.pages.hospital_id == hospitalID).ToList();

        //    var _pagelangDetail = db.page_detail_lang.Where(q => q.pages.hospital_id == hospitalID).ToList();

        //    var _deleteHospital = db.our_hospital.Where(q => q.id == hospitalID).FirstOrDefault();
        //    var _deletePages = db.pages.Where(q => q.hospital_id == hospitalID).ToList();
        //    var _deleteGaleri = db.gallery.Where(q=>q.hospital_id == hospitalID).ToList();

        //    db.hl_contact.Remove(_deleteContanct);

        //    foreach (var u in _deletelang)
        //    {
        //        db.our_hospital_lang.Remove(u);
        //    }

        //    foreach (var y in _deletePages)
        //    {
        //        db.pages.Remove(y);
        //    }
        //    foreach (var z in _pagelang)
        //    {
        //        db.page_langs.Remove(z);
        //    }

        //    foreach (var x in _pagelangDetail)
        //    {
        //        db.page_detail_lang.Remove(x);
        //    }

        //    foreach (var _delete in _deleteGaleri)
        //    {
        //        db.gallery.Remove(_delete);
        //    }

        //    db.our_hospital.Remove(_deleteHospital);
        //    if (db.SaveChanges() > 0)
        //        Response.Redirect("hastanelerimiz.aspx", false);

        //}
    }
}