﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm22 : cBase
    {
        //int PageID;
        protected void Page_Load(object sender, EventArgs e)
        {
            trHakkimizda.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
            trHakkimizda.integrationPlugin();

            //enHakkimizda.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
            //enHakkimizda.integrationPlugin();

            //if (Request.QueryString["page_id"] != "" && Request.QueryString["page_id"] != null)
            //{

                if (!IsPostBack)
                {


                    //PageID = Request.QueryString["page_id"].toint();

                    var _hakkimizda = db.static_pages.Select(s => new
                    {
                        s.id,
                        s.type,
                        s.title,
                        s.text,
                        s.img_path
                    }).Where(q => q.type == 1).FirstOrDefault();

                    if (_hakkimizda != null)
                    {
                        hdnPageID.Value = _hakkimizda.id.ToString();
                        ltrPageName.Text = _hakkimizda.title.ToString();
                        //ltrPageName2.Text = SayfaDetayTR.SayfaAdı.ToString();
                        trHakkimizda.Text = _hakkimizda.text;
                        ImageBanner.ImageUrl = _hakkimizda.img_path;
                    }


                    //var SayfaDetayEN = db.Sayfa_Detay_Dil.Select(s => new
                    //{
                    //    s.id,
                    //    s.sayfa_id,
                    //    SayfaAdı = s.Sayfalar.sayfa_adi,
                    //    s.lang,
                    //    s.description
                    //}).Where(q => q.sayfa_id == PageID && q.lang == "EN").FirstOrDefault();

                    //if (SayfaDetayEN != null)
                    //{

                    //    enHakkimizda.Text = SayfaDetayEN.description;
                    //}

                    //}
                //}
            }
        }

        protected void btnUpdateTR_Click(object sender, EventArgs e)
        {
            int PageID = hdnPageID.Value.toint();
            var _UpdatePageTR = db.static_pages.Where(q => q.type == 1).FirstOrDefault();


            _UpdatePageTR.text = trHakkimizda.Text;

            if (db.SaveChanges() > 0)
                Page.Response.Redirect(Page.Request.Url.ToString(), false);
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.Form["FilePathTR"].ToString() != null || Request.Form["FilePathTR"].ToString() != "")
                {
                    var _bannerlar = db.static_pages.Where( q=>q.type == 1).FirstOrDefault();
                    if (_bannerlar != null)
                    {
                        if (!string.IsNullOrEmpty(Request.Form["FilePathTR"].ToString()))
                        {
                            _bannerlar.img_path = Request.Form["FilePathTR"].ToString();
                        }
                        //_bannerlar.meta_description = txtDescriptionTR.Text.Trim();
                        //_bannerlar.page_url = txtPageURLTR.Text.Trim();

                        if (db.SaveChanges() > 0)
                        {
                            ResizeImage(_bannerlar.img_path, 1903, 381);
                            Page.Response.Redirect(Page.Request.Url.ToString(), false);
                        }
                        else
                            lblUyariTR.Text = "Güncelleme Yapılamadı";
                    }
                }
                else
                    lblUyariTR.Text = "Resim Seçiniz";
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        //protected void btnUpdateEN_Click(object sender, EventArgs e)
        //{
        //    int PageID = hdnPageID.Value.toint();
        //    var _UpdatePageEN = db.Sayfa_Detay_Dil.Where(q => q.sayfa_id == PageID && q.lang == "EN").FirstOrDefault();
        //    _UpdatePageEN.description = enHakkimizda.Text;

        //    if (db.SaveChanges() > 0)
        //        Page.Response.Redirect(Page.Request.Url.ToString(), false);
        //}
    }
}

