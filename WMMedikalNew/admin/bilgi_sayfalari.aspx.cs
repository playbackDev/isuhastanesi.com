﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm26 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            trDescription.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
            trDescription.integrationPlugin();

            //enDescription.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
            //enDescription.integrationPlugin();

            if (Request.QueryString["page_id"] != null)
            {
                int PageID = Request.QueryString["page_id"].toint();

                if (!IsPostBack)
                {
                   

                    var SayfaDetayTR = db.Sayfa_Detay_Dil.Select(s => new
                    {
                        s.id,
                        s.sayfa_id,
                        SayfaAdı = s.Sayfalar.sayfa_adi,
                        s.lang,
                        s.description
                    }).Where(q => q.sayfa_id == PageID && q.lang == "TR").FirstOrDefault();

                    if (SayfaDetayTR != null)
                    {
                        hdnPageID.Value = SayfaDetayTR.sayfa_id.ToString();
                        ltrPageName.Text = SayfaDetayTR.SayfaAdı.ToString();
                        //ltrPageName2.Text = SayfaDetayTR.SayfaAdı.ToString();
                    trDescription.Text = SayfaDetayTR.description;
                    }


                    var SayfaDetayEN = db.Sayfa_Detay_Dil.Select(s => new
                    {
                        s.id,
                        s.sayfa_id,
                        SayfaAdı = s.Sayfalar.sayfa_adi,
                        s.lang,
                        s.description
                    }).Where(q => q.sayfa_id == PageID && q.lang == "EN").FirstOrDefault();

                    //if (SayfaDetayEN != null)
                    //{

                    //    enDescription.Text = SayfaDetayEN.description;
                    //}

                    //var _pageDetailTR = db.page_detail_lang.Select(s => new
                    //{
                    //    s.page_id,
                    //    s.id,
                    //    s.description,
                    //    s.pages.page_name,
                    //    s.lang
                    //}).Where(q => q.page_id == PageID && q.lang == "TR").FirstOrDefault();

                    //if (_pageDetailTR != null)
                    //{


                    //    trDescription.InnerText = _pageDetailTR.description;
                    //    hdnPageID.Value = _pageDetailTR.page_id.ToString();
                    //    ltrPageName.Text = _pageDetailTR.page_name;
                    //    ltrPageName2.Text = _pageDetailTR.page_name;
                    //}


                    //var _pageDetailEN = db.page_detail_lang.Select(s => new
                    //{
                    //    s.id,
                    //    s.page_id,
                    //    s.description,
                    //    s.pages.page_name,
                    //    s.lang

                    //}).Where(q => q.page_id == PageID && q.lang == "EN").FirstOrDefault();

                    //if (_pageDetailEN != null)
                    //{
                    //    enDescription.InnerText = _pageDetailEN.description;
                    //}
                }

            }

        }

        protected void btnUpdateTR_Click(object sender, EventArgs e)
        {
            int _PageID = hdnPageID.Value.toint();
            var _updateTR = db.Sayfa_Detay_Dil.Where(q => q.sayfa_id == _PageID && q.lang == "TR").FirstOrDefault();
            _updateTR.description = trDescription.Text;

            if (db.SaveChanges() > 0)
                Page.Response.Redirect(Page.Request.Url.ToString(), false);

        }

        //protected void btnUpdateEN_Click(object sender, EventArgs e)
        //{
        //    int _PageID = hdnPageID.Value.toint();
        //    var _updateEN = db.Sayfa_Detay_Dil.Where(q => q.sayfa_id == _PageID && q.lang == "EN").FirstOrDefault();
        //    _updateEN.description = enDescription.Text;

        //    if (db.SaveChanges() > 0)
        //        Page.Response.Redirect(Page.Request.Url.ToString(), false);

        //}
    }
}