﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="sosyal_medya.aspx.cs" Inherits="WMMedikalNew.admin.sosyal_medya" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <div class="content-wrapper" style="min-height: 916px;">
        <section class="content">
            <div class="row">
                <asp:HiddenField ID="hdnPageID" runat="server" />
                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Sosyal Medya Hesapları</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                              

                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Facebook Link</label>
                                        <div class="col-sm-10">

                                            <asp:TextBox ID="txtFacebook" CssClass="form-control" runat="server"></asp:TextBox>
                                         

                                                <%-- <CKEditor:CKEditorControl ID="trHakkimizda" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>--%>
                                        </div>

                                    </div>
                        
                                       <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Twitter Link</label>
                                        <div class="col-sm-10">

                                            <asp:TextBox ID="txtTwitter" CssClass="form-control" runat="server"></asp:TextBox>
                                         

                                                <%-- <CKEditor:CKEditorControl ID="trHakkimizda" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>--%>
                                        </div>

                                    </div>

                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Instagram Link</label>
                                        <div class="col-sm-10">

                                            <asp:TextBox ID="txtInstagram" CssClass="form-control" runat="server"></asp:TextBox>
                                         

                                                <%-- <CKEditor:CKEditorControl ID="trHakkimizda" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>--%>
                                        </div>

                                    </div>

                                     <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Youtube Link</label>
                                        <div class="col-sm-10">

                                            <asp:TextBox ID="txtYoutube" CssClass="form-control" runat="server"></asp:TextBox>
                                         

                                                <%-- <CKEditor:CKEditorControl ID="trHakkimizda" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>--%>
                                        </div>

                                    </div>

                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdateTR" runat="server" Text="Güncelle" class="btn btn-info pull-right" OnClick="btnUpdateTR_Click"/>
                                        <br />
                                    <asp:Label ID="lblUyariTR" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
</div>
            </section>
            </div>


</asp:Content>
