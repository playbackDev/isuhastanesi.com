﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm7 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            var SiteList = db.micro_sites_lang.Where(q=> q.lang =="TR").Select(s=>
                new{
                   s.micro_site_id,
                   s.site_name,
                   active_status = (s.micro_sites.is_active) ? "Aktif" : "Pasif",
                }).ToList();

            rptSiteList.DataSource = SiteList;
            rptSiteList.DataBind();
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            string ımageURL = string.Empty;
            ımageURL = Request.Form["FilePath"].ToString();
            if (ımageURL != "" || ımageURL !=null)
            {
                if (txtSiteName.Text != "" && txtSiteURL.Text != "")
                {
                    int uzunluk = ımageURL.Split('.').Length;
                    string random = CreateRandomValue(10, true, true, true, false) + "." + ımageURL.Split('.')[uzunluk - 1];
                    //FileUpload1.SaveAs(Server.MapPath("~/uploads/sites/" + random));

                    micro_sites _newSite = new micro_sites();
                    _newSite.site_name = txtSiteName.Text.Trim();
                    _newSite.site_url = txtSiteURL.Text.Trim();
                    _newSite.site_image = ımageURL;
                    _newSite.is_active = ckIsActive.Checked;
                    db.micro_sites.Add(_newSite);


                    micro_sites_lang _MicroTR = new micro_sites_lang();
                    _MicroTR.site_name = txtSiteName.Text.Trim();
                    _MicroTR.micro_site_id = _newSite.id;
                    _MicroTR.lang = "TR";

                    micro_sites_lang _MicroEN = new micro_sites_lang();
                    _MicroEN.site_name = txtSiteName.Text.Trim();
                    _MicroEN.micro_site_id = _newSite.id;
                    _MicroEN.lang = "EN";

                    db.micro_sites_lang.Add(_MicroTR);
                    db.micro_sites_lang.Add(_MicroEN);

                    if (db.SaveChanges() > 0)
                        Page.Response.Redirect("siteler.aspx", false);

                }
                else
                    lblUyari.Text = "Site Adı ve Linkini Girmediniz";
            }
            else
                lblUyari.Text = "Resim Seçmediniz.";
        }

        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int microSiteID = e.CommandArgument.toint();
                var _deleteLang = db.micro_sites_lang.Where(q => q.micro_site_id == microSiteID).ToList();

                if (_deleteLang.Count() > 0)
                {
                    foreach (var item in _deleteLang)
                    {
                        db.micro_sites_lang.Remove(item);
                    }

                    if (db.SaveChanges() > 0)
                    {
                        var _deleteSite = db.micro_sites.Where(q => q.id == microSiteID).FirstOrDefault();
                        db.micro_sites.Remove(_deleteSite);

                        if (db.SaveChanges() > 0)
                        {
                            Page.Response.Redirect(Page.Request.Url.ToString(), false);
                        }
                        else
                            Alert("Silinemedi #001");

                    }
                    else
                        Alert("Silinemedi #002");
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
           
        }
    }
}