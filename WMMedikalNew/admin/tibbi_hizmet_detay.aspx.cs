﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm3 : cBase
    {
        short BolumID;
        //string Dil;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["BolumID"] != null)
            {
                BolumID = Request.QueryString["BolumID"].toshort();
                textArea1.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
                textArea1.integrationPlugin();

                if (!IsPostBack)
                {
                    //Dil = Request.QueryString["Lang"].ToString();
                    var BolumDetay = db.TibbiBolumler_Dil.Select(s => new
                    {
                        s.id,
                        s.BolumID,
                        s.BolumAdi,
                        s.Dil,
                        s.TibbiBolumler.Resim,
                        s.Aciklama,
                        s.MetaDescription,
                        s.top_banner,
                        s.TibbiBolumler.seoPageTitle,
                        s.TibbiBolumler.seoMetaDesc,
                        s.TibbiBolumler.seoKeywords,
                        s.TibbiBolumler.seoUrl,
                        s.TibbiBolumler.isMerkez
                    }).Where(q => q.BolumID == BolumID && q.Dil == "TR").FirstOrDefault();

                    if (BolumDetay.Resim != null && BolumDetay.Resim != "")
                    {
                        ltrImage.Text = "<img src=" + BolumDetay.Resim.ToString() + ">";
                    }
                    else
                        ltrImage.Text = "<img src='/admin/resimyok.png'>";

                    txtServiceName.Text = BolumDetay.BolumAdi;
                    ltrServiceName.Text = BolumDetay.BolumAdi;
                    ckIsMerkez.Checked = BolumDetay.isMerkez;
                    textArea1.Text = BolumDetay.Aciklama;
                    ltrTopBannerTR.Text = "<img src='" + BolumDetay.top_banner + "' style='max-width:500px'/><br /><br />";
                    ltrFilePath.Text = "<input id='FilePath1' name='FilePath1' value='" + BolumDetay.top_banner + "' type='text' size='60' />";
                    txtSEOBrowserTitle.Text = BolumDetay.seoPageTitle;
                    txtSEOMetaDesc.Text = BolumDetay.seoMetaDesc;
                    txtSEOMetaKeywords.Text = BolumDetay.seoKeywords;
                    txtSEOPageUrl.Text = BolumDetay.seoUrl;
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            if (txtServiceName.Text.Trim() != "")
            {                
                var _UpdateService = db.TibbiBolumler_Dil.Where(q => q.BolumID == BolumID && q.Dil == "TR").FirstOrDefault();

                if (_UpdateService != null)
                {
                    if (Request.Form["FilePath1"].ToString() != null) _UpdateService.top_banner = Request.Form["FilePath1"].ToString();

                    if (textArea1.Text != null)
                        _UpdateService.Aciklama = textArea1.Text;

                    _UpdateService.BolumAdi = txtServiceName.Text;

                    var _bolum = db.TibbiBolumler.Where(q => q.id == BolumID).FirstOrDefault();

                    _bolum.BolumAdi = txtServiceName.Text;
                    _bolum.seoPageTitle = txtSEOBrowserTitle.Text.Trim();
                    _bolum.seoMetaDesc = txtSEOMetaDesc.Text.Trim();
                    _bolum.seoKeywords = txtSEOMetaKeywords.Text.Trim();
                    _bolum.seoUrl = txtSEOPageUrl.Text.Trim().ToURL();
                    _bolum.isMerkez = ckIsMerkez.Checked;
                    //_UpdateService.MetaDescription = txtMetaDescription.Text.Trim();
                    db.SaveChanges();
                }
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }
            else
                lblUyari.Text = "Hizmet Adı Boş Olamaz";
        }

        protected void btnUpdateGuncelle_Click(object sender, EventArgs e)
        {
            string ımageURL = string.Empty;
            ımageURL = Request.Form["FilePath"].ToString();

            if (ımageURL != null || ımageURL != "")
            {
                BolumID = Request.QueryString["BolumID"].toshort();

                var _UpdateImage = db.TibbiBolumler.Where(q => q.id == BolumID).FirstOrDefault();
                _UpdateImage.Resim = ımageURL;


                if (db.SaveChanges() > 0)
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                else
                    lblUyari.Text = "Bir hata oluştu tekrar deneyin";

            }
            else
                lblUyari.Text = "Resim Seçmediniz";

        }
    }
}