﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="hakkimizda_genel.aspx.cs" Inherits="WMMedikalNew.admin.hakkimizda_genel" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="content-wrapper" style="min-height: 916px;">
        <section class="content">
            <div class="row">
                <asp:HiddenField ID="hdnPageID" runat="server" />
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title"> <asp:Literal ID="ltrHospitalName" runat="server"></asp:Literal>  - <asp:Literal ID="ltrPageName" runat="server"></asp:Literal>  (TR) </h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                              

                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Açıklama</label>
                                        <div class="col-sm-10">
                                          

                                                 <CKEditor:CKEditorControl ID="trHakkimizda" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>

                                    </div>
                        


                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdateTR" runat="server" Text="Güncelle (TR)" class="btn btn-info pull-right" OnClick="btnUpdateTR_Click"/>
                                        <br />
                                    <asp:Label ID="lblUyariTR" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title"> <asp:Literal ID="ltrPageName2" runat="server"></asp:Literal> (EN) </h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                            

                                           <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Açıklama</label>
                                        <div class="col-sm-10">
                                           
                                                <CKEditor:CKEditorControl ID="enHakkimizda" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>

                                    </div>
                        


                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdateEN" runat="server" Text="Güncelle (EN)" class="btn btn-info pull-right" OnClick="btnUpdateEN_Click" />
                                        <br />
                                    <asp:Label ID="lbluyariEN" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

               
            </div>
        </section>
    </div>
</asp:Content>
