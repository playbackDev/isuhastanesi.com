﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm17 : cBase
    {
        int PageID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["page_id"] != null)
                PageID = Request.QueryString["page_id"].toint();

            if (Request.QueryString["hastane_id"] == null)
            {
                var KurumList = db.organizations.Select(s => new
                {
                    s.id,
                    s.organization_name_tr,
                    s.organization_name_en,
                    s.Hastaneler.HastaneAdi,
                    organization_type_name = s.organization_name_tr
                    //s.organization_type.our_hospital.our_hospital_lang.FirstOrDefault().hospital_name,
                    //organization_type_name = s.organization_type.organization_name_tr
                }).ToList();

                rptKurumList.DataSource = KurumList;
                rptKurumList.DataBind();

            }
            else
            {
                pnlKurumEkle.Visible = false;
                int hastane_id = Request.QueryString["hastane_id"].toint();
                var KurumList = db.organizations.Select(s => new
                {
                    s.id,
                    s.organization_name_tr,
                    s.organization_name_en,
                    s.Hastaneler.HastaneAdi,
                    organization_type_name = s.organization_name_tr,
                    HastaneID = s.Hastaneler.id
                }).Where(q => q.HastaneID == hastane_id).ToList();

                rptKurumList.DataSource = KurumList;
                rptKurumList.DataBind();
            }

            if (!IsPostBack)
            {
                drpKurumlar.DataSource = db.organization_type.Where(q => q.hospital_id == 27).ToList();
                drpKurumlar.DataTextField = "organization_name_tr";
                drpKurumlar.DataValueField = "id";
                drpKurumlar.DataBind();
                drpKurumlar.Items.Insert(0, new ListItem("Kurum Tipi Seçiniz", "0"));

                //drpHospital.DataSource = db.Hastaneler.ToList();
                //drpHospital.DataTextField = "HastaneAdi";
                //drpHospital.DataValueField = "id";
                //drpHospital.DataBind();
                //drpHospital.Items.Insert(0, new ListItem("Hastane Seçiniz", "0"));
            }

            if (PageID != 0)
            {
                if (!IsPostBack)
                {
                    var _bannerlar = db.Hastane_Sayfa_Detay_Dil.Where(q => q.sayfa_id == PageID).ToList();

                    if (_bannerlar != null)
                    {
                        foreach (var item in _bannerlar)
                        {
                            if (item.lang == "TR")
                            {
                                ltrTopBannerTR.Text = "<img src='" + item.top_banner + "' style='max-width:300px'/>";
                                //txtPageURLTR.Text = item.page_url;
                                //txtDescriptionTR.Text = item.meta_description;
                            }
                            //else
                            //{
                            //    ltrTopBannerEN.Text = "<img src='" + item.top_banner + "' style='max-width:300px'/>";
                            //    txtPageURLEN.Text = item.page_url;
                            //    txtDescriptionEN.Text = item.meta_description;
                            //}

                        }
                    }
                }
            }

        }

        protected void btnHizmetEkle_Click(object sender, EventArgs e)
        {
            if (drpKurumlar.SelectedValue.toint() > 0)
            {
                organizations _newOrganizations = new organizations();
                _newOrganizations.organization_type_id = drpKurumlar.SelectedValue.toint();
                _newOrganizations.organization_name_tr = txtKurumAdiTR.Text.Trim();
                //_newOrganizations.organization_name_en = txtKurumAdiEN.Text.Trim();
                //_newOrganizations.hospital_id = drpHospital.SelectedValue.toint();
                _newOrganizations.hospital_id = 27;
                db.organizations.Add(_newOrganizations);

                if (db.SaveChanges() > 0)
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                else
                    lblUyari.Text = "Bir hata oluştu tekrar deneyin";
            }
            else
                lblUyari.Text = "Kurum Tipi Seçmediniz";
        }

        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {
            int ID = e.CommandArgument.toint();
            var _delete = db.organizations.Where(q => q.id == ID).FirstOrDefault();

            db.organizations.Remove(_delete);


            if (db.SaveChanges() > 0)
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            else
                lblUyari.Text = "Bir hata oluştu tekrar deneyin";
        }

        protected void drpHospital_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drpHospital.SelectedValue.toint() > 0)
            {
                int hospital_id = drpHospital.SelectedValue.toint();

                drpKurumlar.DataSource = db.organization_type.Where(q => q.hospital_id == hospital_id).ToList();
                drpKurumlar.DataTextField = "organization_name_tr";
                drpKurumlar.DataValueField = "id";
                drpKurumlar.DataBind();
                drpKurumlar.Items.Insert(0, new ListItem("Kurum Tipi Seçiniz", "0"));
            }

        }

        protected void btnUpdateTR_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.Form["FilePathTR"].ToString() != null || Request.Form["FilePathTR"].ToString() != "")
                {
                    var _bannerlar = db.Hastane_Sayfa_Detay_Dil.Where(q => q.sayfa_id == PageID && q.lang == "TR").FirstOrDefault();
                    if (_bannerlar != null)
                    {
                        if (!string.IsNullOrEmpty(Request.Form["FilePathTR"].ToString()))
                        {
                            _bannerlar.top_banner = Request.Form["FilePathTR"].ToString();
                        }
                        //_bannerlar.meta_description = txtDescriptionTR.Text.Trim();
                        //_bannerlar.page_url = txtPageURLTR.Text.Trim();

                        if (db.SaveChanges() > 0)
                            Page.Response.Redirect(Page.Request.Url.ToString(), false);
                        else
                            lblUyariTR.Text = "Güncelleme Yapılamadı";
                    }
                }
                else
                    lblUyariTR.Text = "Resim Seçiniz";
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (Request.Form["FilePathEN"].ToString() != null || Request.Form["FilePathEN"].ToString() != "")
        //        {
        //            var _bannerlar = db.Hastane_Sayfa_Detay_Dil.Where(q => q.sayfa_id == PageID && q.lang == "EN").FirstOrDefault();
        //            if (_bannerlar != null)
        //            {
        //                if (!string.IsNullOrEmpty(Request.Form["FilePathEN"].ToString()))
        //                {
        //                    _bannerlar.top_banner = Request.Form["FilePathEN"].ToString();
        //                }
        //                _bannerlar.meta_description = txtDescriptionEN.Text.Trim();
        //                _bannerlar.page_url = txtPageURLEN.Text.Trim();
        //                if (db.SaveChanges() > 0)
        //                    Page.Response.Redirect(Page.Request.Url.ToString(), false);
        //                else
        //                    Label1.Text = "Güncelleme Yapılamadı";
        //            }
        //        }
        //        else
        //            Label1.Text = "Resim Seçiniz";
        //    }
        //    catch (Exception ex)
        //    {
        //        Response.Write(ex);
        //    }
        //}
    }
}