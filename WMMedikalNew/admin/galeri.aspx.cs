﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class galeri : cBase
    {
        int PageID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["page_id"] != null)
                PageID = Request.QueryString["page_id"].toint();

            if (!IsPostBack)
            {
                var _gallery = db.gallery.Select(s => new
                {
                    s.id,
                    s.image_name,
                    active_status = (s.is_active) ? "Aktif" : "Pasif",
                    s.Hastaneler.HastaneAdi

                }).ToList();

                if (_gallery.Count > 0)
                {
                    rptGaleri.DataSource = _gallery;
                    rptGaleri.DataBind();
                }

                _Fillpage();
            }

            if (PageID != 0)
            {
                if (!IsPostBack)
                {
                    var _bannerlar = db.Hastane_Sayfa_Detay_Dil.Where(q => q.sayfa_id == PageID).ToList();

                    if (_bannerlar != null)
                    {
                        foreach (var item in _bannerlar)
                        {
                            if (item.lang == "TR")
                            {
                                ltrTopBannerTR.Text = "<img src='" + item.top_banner + "' style='max-width:300px'/>";
                                txtPageURLTR.Text = item.page_url;
                                txtDescriptionTR.Text = item.meta_description;
                            }
                            //else
                            //{
                            //    ltrTopBannerEN.Text = "<img src='" + item.top_banner + "' style='max-width:300px'/>";
                            //    txtPageURLEN.Text = item.page_url;
                            //    txtDescriptionEN.Text = item.meta_description;
                            //}
                               
                        }
                    }
                }
            }

        }

        private void _Fillpage()
        {


            drpHospital.DataSource = db.Hastaneler.ToList();
            drpHospital.DataTextField = "HastaneAdi";
            drpHospital.DataValueField = "id";
            drpHospital.DataBind();

        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            if (drpHospital.SelectedValue.isNumara())
            {
                string ımageURL = string.Empty;
                ımageURL = Request.Form["FilePath"].ToString();

                if (ımageURL != null &&  ımageURL != "")
                {
                    //int uzunluk = ımageURL.Split('.').Length;
                    //string random = CreateRandomValue(10, true, true, true, false) + "." + ımageURL.Split('.')[uzunluk - 1];


                    gallery _newGaleri = new gallery();
                    _newGaleri.image_name = ımageURL;
                    _newGaleri.is_active = ckIsActive.Checked;
                    _newGaleri.hospital_id = drpHospital.SelectedValue.toint();

                    db.gallery.Add(_newGaleri);

                    if (db.SaveChanges() > 0)
                        Response.Redirect("galeri.aspx", false);

                }
                else
                    lblUyari.Text = "Resim Seçmediniz";

            }
            else
                lblUyari.Text = "Hastane Seçmediniz";

        }

        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            int ID = e.CommandArgument.toint();
            var _delete = db.gallery.Where(q => q.id == ID).FirstOrDefault();
            db.gallery.Remove(_delete);

            if (db.SaveChanges() > 0)
                Response.Redirect("galeri.aspx", false);

        }

        protected void btnUpdateTR_Click(object sender, EventArgs e)
        {
            try
            {
                var _bannerlar = db.Hastane_Sayfa_Detay_Dil.Where(q => q.sayfa_id == PageID && q.lang == "TR").FirstOrDefault();
                if (Request.Form["FilePathTR"].ToString() != null && Request.Form["FilePathTR"].ToString() != "")
                {
                    if (!string.IsNullOrEmpty(Request.Form["FilePathTR"].ToString()))
                    {
                        _bannerlar.top_banner = Request.Form["FilePathTR"].ToString();
                    }

                }

                _bannerlar.page_url = txtPageURLTR.Text.Trim();
                _bannerlar.meta_description = txtDescriptionTR.Text.Trim();

                if (db.SaveChanges() > 0)
                    Page.Response.Redirect(Page.Request.Url.ToString(), false);
                else
                    lblUyariTR.Text = "Güncelleme Yapılamadı";
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        var _bannerlar = db.Hastane_Sayfa_Detay_Dil.Where(q => q.sayfa_id == PageID && q.lang == "EN").FirstOrDefault();

        //        if (Request.Form["FilePathEN"].ToString() != null && Request.Form["FilePathEN"].ToString() != "")
        //        {

        //            if (!string.IsNullOrEmpty(Request.Form["FilePathEN"].ToString()))
        //            {
        //                _bannerlar.top_banner = Request.Form["FilePathEN"].ToString();
        //            }

                   
        //        }

        //        _bannerlar.meta_description = txtDescriptionEN.Text.Trim();
        //        _bannerlar.page_url = txtPageURLEN.Text.Trim();


        //        if (db.SaveChanges() > 0)
        //            Page.Response.Redirect(Page.Request.Url.ToString(), false);
        //        else
        //            Label1.Text = "Güncelleme Yapılamadı";
        //    }
        //    catch (Exception ex)
        //    {
        //        Response.Write(ex);
        //    }
        //}
    }
}