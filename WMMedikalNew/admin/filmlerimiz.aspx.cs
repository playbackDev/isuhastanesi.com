﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class filmlerimiz : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                var VideoList = db.videos.Select(s => new
                {
                    s.id,
                    s.video_link,
                    s.video_name,
                    active_status = (s.is_active) ? "Aktif" : "Pasif",

                }).ToList();

                rptVideoList.DataSource = VideoList;
                rptVideoList.DataBind();
            }
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtVideoLink.Text.Trim() != "" && txtVideoName.Text.Trim() != "")
                {
                    videos _newVideo = new videos();
                    _newVideo.video_link = txtVideoLink.Text.Trim();
                    _newVideo.video_name = txtVideoName.Text.Trim();
                    _newVideo.is_active = ckIsActive.Checked;

                    db.videos.Add(_newVideo);

                    if (db.SaveChanges() > 0)
                        Response.Redirect("filmlerimiz.aspx", false);
                    else
                        lblUyari.Text = "Bir hata oluştu";
                }
                else
                    lblUyari.Text = "Boş bırakmayın";
            }
            catch (DbEntityValidationException hata)
            {
                foreach (var ht in hata.EntityValidationErrors)
                {
                    string a = ht.Entry.Entity.GetType().Name + " " + ht.Entry.State;
                    //Response.Write(ht.Entry.Entity.GetType().Name + " " + ht.Entry.State);
            
                    foreach (var valerror in ht.ValidationErrors)
                    {
                        //Response.Write(valerror.PropertyName + " " + valerror.ErrorMessage);
                        string b = valerror.PropertyName + " " + valerror.ErrorMessage;
                    }
                }
            }

        }


        protected void LinkButton1_Command1(object sender, CommandEventArgs e)
        {
            int ID =e.CommandArgument.toint();
            var Delete = db.videos.Where(q=> q.id == ID).FirstOrDefault();
            db.videos.Remove(Delete);
            if(db.SaveChanges() > 0 )
                Response.Redirect("filmlerimiz.aspx", false);

        }
    }
}