﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="haber_ekle.aspx.cs" Inherits="WMMedikalNew.admin.haber_ekle" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style>
        .dataTables_length {
            display: none;
        }

        .dataTables_filter {
            float: right;
        }

        .dataTables_info {
            display: none;
        }
    </style>
    <script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
    <script type="text/javascript">

        function BrowseServer() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField;
            finder.popup();
        }

        function BrowseServer1() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField1;
            finder.popup();
        }

        function BrowseServer2() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField2;
            finder.popup();
        }
        function SetFileField(fileUrl) {
            document.getElementById('xFilePath').value = fileUrl;
        }

        function SetFileField1(fileUrl) {
            document.getElementById('xFilePath1').value = fileUrl;
        }

        function SetFileField2(fileUrl) {
            document.getElementById('xFilePath2').value = fileUrl;
        }
    </script>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
    <script type="text/javascript">

        function BrowseServer() {
            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField;
            finder.popup();
        }

        function SetFileField(fileUrl) {
            document.getElementById('FilePath').value = fileUrl;
        }

        function BrowseServer2() {
            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField2;
            finder.popup();
        }

        function SetFileField2(fileUrl) {
            document.getElementById('FilePath2').value = fileUrl;
        }

    </script>
    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content">
            <div class="row">


                <%--<div class="col-md-9">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Sayfa Üst Banner</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa Haber Banner</label>
                                        <div class="col-sm-10">
                                            <asp:Image ID="ImageBanner" runat="server" Width="500px" /><br />
                                            <br />
                                            <input id="xFilePath1" name="FilePathTR" type="text" size="60" />
                                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer1();" />
                                        </div>
                                    </div>

                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="Button1" runat="server" Text="Güncelle" class="btn btn-info pull-right" OnClick="btnUpdate_Click" />
                                    <br />
                                    <asp:Label ID="Label2" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>



                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">İçerik Ekle</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">

                                    <div class="form-group" style="display: none;">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Haber/Duyuru Türü</label>

                                        <div class="col-sm-10">
                                            <asp:DropDownList ID="drpType" runat="server" CssClass="form-control">
                                                <asp:ListItem Enabled="true" Text="Tür Seçiniz" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Haber" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Duyuru" Value="2"></asp:ListItem>

                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">SEO Browser Başlığı</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtSEOBrowserTitle" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">SEO Meta Description</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtSEOMetaDesc" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">SEO Meta Keywords</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtSEOMetaKeywords" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">SEO Sayfa URL</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtSEOPageUrl" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Başlık</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtHaberBaslikTR" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Özet</label>
                                        <div class="col-sm-10">
                                            <CKEditor:CKEditorControl ID="txtHaberOzetTR" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">İçerik</label>
                                        <div class="col-sm-10">
                                            <CKEditor:CKEditorControl ID="txtHaberTR" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1" class="col-sm-2 control-label">Durum (Aktif/Pasif)</label>
                                        <div class="col-sm-10">
                                            <asp:CheckBox ID="ckIsActiveTR" runat="server" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Resim Boyutları </label>
                                        <label for="exampleInputEmail1" style="color: red" class="col-sm-4 control-label">315*315 şeklinde yüklenmelidir. Thumb alanı için Resize işlemi yapıalacaktır.</label>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1" class="col-sm-2 control-label">Resim</label>

                                        <div class="col-sm-10">
                                            <input id="FilePath" name="FilePath" type="text" size="60" />
                                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer();" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1" class="col-sm-2 control-label">İç Sayfa Resim</label>

                                        <div class="col-sm-10">
                                            <input id="FilePath2" name="FilePath2" type="text" size="60" />
                                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer2();" />
                                        </div>

                                    </div>

                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnHaberEkle" runat="server" Text="Yeni İçerik Ekle" class="btn btn-info pull-right" OnClick="btnHaberEkle_Click" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <%--<div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title"> Haber Ekle İngilizce </h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Haber Başlığı</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtHaberBaslikEN" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Haber Özeti</label>
                                        <div class="col-sm-10">
                                               <CKEditor:CKEditorControl ID="txtHaberOzetEN" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Haber İçeriği</label>
                                        <div class="col-sm-10">
                                             <CKEditor:CKEditorControl ID="txtHaberEN" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"  class="col-sm-2 control-label">Durum (Aktif/Pasif)</label>
                                        <div class="col-sm-10">
                                            <asp:CheckBox ID="ckIsActiveEN" runat="server" class="form-control" />
                                        </div>
                                    </div>
                                   
                                </div>
                               
                            </div>
                        </div>
                    </div>--%>
                </div>

            </div>
        </section>
    </div>
</asp:Content>
