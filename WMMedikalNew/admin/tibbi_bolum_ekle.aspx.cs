﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class tibbi_bolum_ekle : cBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnHizmetEkle_Click(object sender, EventArgs e)
        {
            TibbiBolumler _newTibbi = new TibbiBolumler();
            _newTibbi.BolumAdi = txtHizmetAdi.Text.Trim();
            _newTibbi.HastaneID = 27;
            _newTibbi.ServisID = 0;
            _newTibbi.Resim = "";
            _newTibbi.is_manuel = true;
            _newTibbi.seoPageTitle = txtSEOBrowserTitle.Text.Trim();
            _newTibbi.seoMetaDesc = txtSEOMetaDesc.Text.Trim();
            _newTibbi.seoKeywords = txtSEOMetaKeywords.Text.Trim();
            _newTibbi.seoUrl = txtSEOPageUrl.Text.Trim().ToURL();
            _newTibbi.isMerkez = ckIsMerkez.Checked;

            db.TibbiBolumler.Add(_newTibbi);

            if (db.SaveChanges() > 0)
            {
                TibbiBolumler_Dil _newTibbiDilTR = new TibbiBolumler_Dil();
                _newTibbiDilTR.BolumAdi = txtHizmetAdi.Text.Trim();
                _newTibbiDilTR.BolumID = _newTibbi.id;
                _newTibbiDilTR.Dil = "TR";

                db.TibbiBolumler_Dil.Add(_newTibbiDilTR);

                TibbiBolumler_Dil _newTibbiDilEN = new TibbiBolumler_Dil();
                _newTibbiDilEN.BolumAdi = txtHizmetAdi.Text.Trim();
                _newTibbiDilEN.BolumID = _newTibbi.id;
                _newTibbiDilEN.Dil = "EN";

                db.TibbiBolumler_Dil.Add(_newTibbiDilEN);

                if (db.SaveChanges() > 0)
                    Response.Redirect("tibbi_hizmet_detay.aspx?BolumID=" + _newTibbi.id + "&Lang=TR");
                else
                    Alert("Dil Değerleri Eklenemedi.");
            }
            else
                Alert("Tıbbi Bölüm Kaydedilemedi.");
        }
    }
}