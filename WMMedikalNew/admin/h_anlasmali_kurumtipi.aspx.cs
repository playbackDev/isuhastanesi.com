﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm18 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var KurumList = db.organization_type.Select(s => new { 
            s.id,
            s.organization_name_en,
            s.organization_name_tr,
            s.Hastaneler.HastaneAdi
            
            }).ToList();

            rptKurumList.DataSource = KurumList;
            rptKurumList.DataBind();

            //if (!IsPostBack)
            //{
            //    drpHospital.DataSource = db.Hastaneler.ToList();
            //    drpHospital.DataTextField = "HastaneAdi";
            //    drpHospital.DataValueField = "id";
            //    drpHospital.DataBind();
            //    drpHospital.Items.Insert(0, new ListItem("Hastane Seçiniz..", "0"));
            //}
        }

        protected void btnHizmetEkle_Click(object sender, EventArgs e)
        {
            if (drpHospital.SelectedValue.toint() > 0)
            {
                organization_type _newType = new organization_type();
                _newType.organization_name_tr = kurumTipiTR.Text.Trim();
                //_newType.organization_name_en = kurumTipiEN.Text.Trim();
                //_newType.hospital_id = drpHospital.SelectedValue.toint();
                _newType.hospital_id = 27;

                db.organization_type.Add(_newType);

                if (db.SaveChanges() > 0)
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                else
                    lblUyari.Text = "Bir hata oluştu tekrar deneyin";
            }
            else
                lblUyari.Text = "Hastane Seçmediniz";
        }

        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {
            int ID = e.CommandArgument.toint();

            var _delete = db.organization_type.Where(q => q.id == ID).FirstOrDefault();

            db.organization_type.Remove(_delete);


            if (db.SaveChanges() > 0)
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            else
                lblUyari.Text = "Bir hata oluştu tekrar deneyin";
        }
    }
}