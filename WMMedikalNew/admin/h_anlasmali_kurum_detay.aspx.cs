﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm20 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != "")
            {
                if (!IsPostBack)
                {
                    int ID = Request.QueryString["id"].toint();

                    //drpHospital.DataSource = db.Hastaneler.ToList();
                    //drpHospital.DataTextField = "HastaneAdi";
                    //drpHospital.DataValueField = "id";
                    //drpHospital.DataBind();

                    var _detail = db.organizations.Where(q => q.id == ID).FirstOrDefault();

                    txtKurumAdiTR.Text = _detail.organization_name_tr;
                    //txtKurumAdiEN.Text = _detail.organization_name_en;

                    //txtMetaDescriptionTR.Text = _detail.meta_description_tr;
                    //txtMetaDescriptionEN.Text = _detail.meta_description_en;

                    //drpHospital.SelectedValue = _detail.Hastaneler.id.ToString();

                    drpKurumlar.DataSource = db.organization_type.Where(q => q.hospital_id == _detail.Hastaneler.id).ToList();
                    drpKurumlar.DataTextField = "organization_name_tr";
                    drpKurumlar.DataValueField = "id";
                    drpKurumlar.DataBind();
                    drpKurumlar.SelectedValue = _detail.organization_type_id.ToString();

                }
            }
            else
                Response.Redirect("/", false);
        }

        protected void btnHizmetEkle_Click(object sender, EventArgs e)
        {
            if (drpKurumlar.SelectedValue.toint() > 0)
            {
                int ID = Request.QueryString["id"].toint();
                var _update = db.organizations.Where(q => q.id == ID).FirstOrDefault();
                _update.organization_name_tr = txtKurumAdiTR.Text.Trim();
                //_update.organization_name_en = txtKurumAdiEN.Text.Trim();
                _update.organization_type_id = drpKurumlar.SelectedValue.toint();
                //_update.meta_description_en = txtMetaDescriptionEN.Text.Trim();
                //_update.meta_description_tr = txtMetaDescriptionTR.Text.Trim();

                if (db.SaveChanges() > 0)
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                else
                    lblUyari.Text = "Bir hata oluştu tekrar deneyin";
            }
            else
                lblUyari.Text = "Kurum Tipi Seçmediniz";
           
        }

        protected void drpHospital_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(drpHospital.SelectedValue.toint()>0)
            {
                int hospital_id = drpHospital.SelectedValue.toint();
                drpKurumlar.DataSource = db.organization_type.Where(q=> q.hospital_id ==hospital_id).ToList();
                drpKurumlar.DataTextField = "organization_name_tr";
                drpKurumlar.DataValueField = "id";
                drpKurumlar.DataBind();
            }
        }
    }
}