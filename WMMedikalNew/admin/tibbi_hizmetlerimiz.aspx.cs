﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm2 : cBase
    {
        int PageID = 0;
        List<string> _letters = new List<string>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["page_id"] != null)
                PageID = Request.QueryString["page_id"].toint();

            _fillPage();
        }

        private void _fillPage()
        {
            var _tibbiBolumler = db.TibbiBolumler_Dil.Select(s => new
            {
                s.BolumID,
                s.TibbiBolumler.Hastaneler.HastaneAdi,
                s.Dil,
                s.BolumAdi,
                s.TibbiBolumler.displayOrder,
                s.TibbiBolumler.isMerkez
            }).Where(q => q.Dil == "TR").ToList();

            if (Request.QueryString["m"] != null)
            {
                rptServiceList.DataSource = _tibbiBolumler.Where(q => q.isMerkez).OrderBy(o => o.displayOrder).ToList();
                rptServiceList.DataBind();
            }
            else
            {
                foreach (var item in _tibbiBolumler)
                {
                    if (!_letters.Contains(item.BolumAdi.Substring(0, 1)))
                        _letters.Add(item.BolumAdi.Substring(0, 1));
                }

                foreach (var item in _letters.OrderBy(o => o))
                {
                    ltrLetter.Text += "<a href='tibbi_hizmetlerimiz.aspx?page_id=" + PageID + "&l=" + item + "' class='btn btn-xs btn-info'>" + item + "</a> ";
                }

                if (Request.QueryString["l"] != null)
                {
                    string _l = Request.QueryString["l"].ToString();
                    rptServiceList.DataSource = _tibbiBolumler.Where(q => q.BolumAdi.StartsWith(_l) && !q.isMerkez).OrderBy(o => o.displayOrder).ToList();
                    rptServiceList.DataBind();
                }
            }
        }

        protected void btnUpdateTR_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    if (Request.Form["FilePathTR"].ToString() != null || Request.Form["FilePathTR"].ToString() != "")
            //    {
            //        var _bannerlar = db.Hastane_Sayfa_Detay_Dil.Where(q => q.sayfa_id == PageID && q.lang == "TR").FirstOrDefault();
            //        if (_bannerlar != null)
            //        {
            //            if (!string.IsNullOrEmpty(Request.Form["FilePathTR"].ToString()))
            //            {
            //                _bannerlar.top_banner = Request.Form["FilePathTR"].ToString();
            //            }
            //            //_bannerlar.page_url = txtPageURLTR.Text.Trim();
            //            //_bannerlar.meta_description = txtDescriptionTR.Text.Trim();

            //            if (db.SaveChanges() > 0)
            //            {
            //                ResizeImage(_bannerlar.top_banner, 1903, 381);
            //                Page.Response.Redirect(Page.Request.Url.ToString(), false);
            //            }
            //            else
            //                lblUyariTR.Text = "Güncelleme Yapılamadı";
            //        }
            //    }
            //    else
            //        lblUyariTR.Text = "Resim Seçiniz";
            //}
            //catch (Exception ex)
            //{
            //    Response.Write(ex);
            //}
        }

        protected void lnkSil_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int _bolumId = e.CommandArgument.toint();

                var _langs = db.TibbiBolumler_Dil.Where(q => q.BolumID == _bolumId);
                var _data = db.TibbiBolumler.Where(q => q.id == _bolumId).FirstOrDefault();

                if (_langs.Count() > 0 && _data != null)
                {
                    foreach (var item in _langs)
                        db.TibbiBolumler_Dil.Remove(item);

                    db.TibbiBolumler.Remove(_data);

                    db.SaveChanges();
                }
            }
            catch { }

            Refresh();
        }

        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (Request.Form["FilePathEN"].ToString() != null || Request.Form["FilePathEN"].ToString() != "")
        //        {
        //            var _bannerlar = db.Hastane_Sayfa_Detay_Dil.Where(q => q.sayfa_id == PageID && q.lang == "EN").FirstOrDefault();
        //            if (_bannerlar != null)
        //            {
        //                if (!string.IsNullOrEmpty(Request.Form["FilePathEN"].ToString()))
        //                {
        //                    _bannerlar.top_banner = Request.Form["FilePathEN"].ToString();
        //                }

        //                _bannerlar.page_url = txtPageURLEN.Text.Trim();
        //                _bannerlar.meta_description = txtDescriptionEN.Text.Trim();
        //                if (db.SaveChanges() > 0)
        //                    Page.Response.Redirect(Page.Request.Url.ToString(), false);
        //                else
        //                    Label1.Text = "Güncelleme Yapılamadı";
        //            }
        //        }
        //        else
        //            Label1.Text = "Resim Seçiniz";
        //    }
        //    catch (Exception ex)
        //    {
        //        Response.Write(ex);
        //    }
        //}
        [WebMethod] //resim sıralama methodu
        public static void resimSirala(String ResimSatir)
        {
            WMMedikalDBEntities db = new WMMedikalDBEntities();

            string gelen = ResimSatir.Replace("satir", "");
            char[] ayrac = new char[] { ',' };
            string[] gelenler = gelen.Split(ayrac);
            int i = 1;
            foreach (string veri in gelenler)
            {
                db.Database.ExecuteSqlCommand("Update TibbiBolumler set displayOrder = " + i + " where id=" + Convert.ToInt32(veri));
                i++;
            }
        }

    }
}