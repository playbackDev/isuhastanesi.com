﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="dosya_yonetim.aspx.cs" Inherits="WMMedikalNew.dosya_yonetim" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
                
    <style>
        a.cke_button {
            display: inline-block;
            height: 200px;
            padding: 6px 8px;
            outline: 0;
            cursor: default;
            float: left;
            border: 0;
            width: 1200px;
        }

        #cke_1_bottom {
            display: none;
        }

        .cke_button__image_icon {
            display: none;
        }

        #cke_13 {
            display: none;
        }

        .baslik {
            font-size: 32px;
            margin: 0 auto;
            text-align: center;
        }
    </style>
    <div class="content-wrapper" style="min-height: 916px;">

        <section class="content">
            <div class="row">
                <iframe src="/ckfinder/ckfinder.html?type=Images&CKEditor=ContentPlaceHolder1_enDescription&CKEditorFuncNum=1&langCode=tr" style="width:100%;height:600px">

                </iframe>
           

            </div>
        </section>
    </div>


        <script src="/ckeditor/ckeditor.js?t=C6HH5UF" type="text/javascript"></script>

</asp:Content>
