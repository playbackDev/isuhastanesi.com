﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm5 : cBase
    {
        short newsID;
        //string Lang;
        protected void Page_Load(object sender, EventArgs e)
        {

            textArea2.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
            textArea2.integrationPlugin();

            textArea1.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
            textArea1.integrationPlugin();


            if (Request.QueryString["haber_id"] != null)
            {
                newsID = Request.QueryString["haber_id"].toshort();
                //Lang = Request.QueryString["Lang"].ToString();
                if (!IsPostBack)
                {
                    var NewDetail = db.news_lang.Where(q => q.news_id == newsID && q.lang == "TR").Select(s => new
                    {
                        s.id,
                        s.news_content,
                        s.news_title,
                        Image = s.news.news_image,
                        s.news.bigImg,
                        s.lang,
                        s.is_active,
                        s.news_short,
                        s.news_id,
                        s.news.seoPageTitle,
                        s.news.seoMetaDesc,
                        s.news.seoKeywords,
                        s.news.seoURL
                    }).FirstOrDefault();


                    if (NewDetail != null)
                    {
                        hdnID.Value = NewDetail.news_id.ToString();
                        txtHaberBaslik.Text = NewDetail.news_title;
                        textArea1.Text = NewDetail.news_content;
                        textArea2.Text = NewDetail.news_short;
                        //ltrLang.Text = NewDetail.lang;
                        ckIsActive.Checked = NewDetail.is_active;
                        ltrNewTitle.Text = NewDetail.news_title;
                        txtSEOBrowserTitle.Text = NewDetail.seoPageTitle;
                        txtSEOMetaDesc.Text = NewDetail.seoMetaDesc;
                        txtSEOMetaKeywords.Text = NewDetail.seoKeywords;
                        txtSEOPageUrl.Text = NewDetail.seoURL;

                        ltrImage.Text = "<img src=" + NewDetail.Image + " style='max-width:350px' >";
                        ltrImageContent.Text = "<img src=" + NewDetail.bigImg + " style='max-width:350px' >";

                        ltrFP1.Text = "<input id='FilePath' name='FilePath' type='text' size='60' value='" + NewDetail.Image + "' />";
                        ltrFP2.Text = "<input id='FilePath2' name='FilePath2' type='text' size='60' value='" + NewDetail.bigImg + "' />";
                    }
                }
            }


        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                string imageURL = string.Empty;
                if (Request.Form["FilePath"] != null) imageURL = Request.Form["FilePath"].ToString();

                string imageURL2 = string.Empty;
                if (Request.Form["FilePath2"] != null) imageURL2 = Request.Form["FilePath2"].ToString();

                short ID = hdnID.Value.toshort();
                var _UpdateNews = db.news_lang.Where(q => q.news_id == ID && q.lang == "TR").FirstOrDefault();

                if (_UpdateNews != null)
                {
                    _UpdateNews.news_title = txtHaberBaslik.Text.Trim();
                    _UpdateNews.news_content = textArea1.Text;
                    _UpdateNews.news_short = textArea2.Text;
                    _UpdateNews.is_active = ckIsActive.Checked;
                    _UpdateNews.news.seoPageTitle = txtSEOBrowserTitle.Text.Trim();
                    _UpdateNews.news.seoMetaDesc = txtSEOMetaDesc.Text.Trim();
                    _UpdateNews.news.seoKeywords = txtSEOMetaKeywords.Text.Trim();
                    _UpdateNews.news.seoURL = txtSEOPageUrl.Text.Trim().ToURL();

                    _UpdateNews.news.news_image = imageURL;
                    _UpdateNews.news.bigImg = imageURL2;

                    db.SaveChanges();
                }

                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }
            catch { Alert("Güncelleme esnasında hata oluştu"); }
        }
    }
}