﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="tibbi_bolum_ekle.aspx.cs" Inherits="WMMedikalNew.admin.tibbi_bolum_ekle" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content-header">
            <h1>Tıbbi Hizmet Ekle</h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Yeni Hizmet Ekle</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Hizmet Adı</label>
                                <asp:TextBox ID="txtHizmetAdi" runat="server" class="form-control" placeholder="Hizmet Adı"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <asp:CheckBox ID="ckIsMerkez" runat="server" /> 
                                Merkez olarak göster
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3">SEO Browser Başlığı</label>
                                <asp:TextBox ID="txtSEOBrowserTitle" runat="server" class="form-control"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3">SEO Meta Description</label>
                                <asp:TextBox ID="txtSEOMetaDesc" runat="server" class="form-control"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3">SEO Meta Keywords</label>
                                <asp:TextBox ID="txtSEOMetaKeywords" runat="server" class="form-control"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3">SEO Sayfa URL</label>
                                <asp:TextBox ID="txtSEOPageUrl" runat="server" class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="box-footer">
                            <asp:Button ID="btnHizmetEkle" runat="server" Text="Hizmet Ekle" class="btn btn-primary" OnClick="btnHizmetEkle_Click" /><br />
                            <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>

</asp:Content>
