﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="yeni_doktor_ekle.aspx.cs" Inherits="WMMedikalNew.admin.yeni_doktor_ekle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
    <script type="text/javascript">

        function BrowseServer() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField;
            finder.popup();
        }

        function SetFileField(fileUrl) {
            document.getElementById('xFilePath').value = fileUrl;
        }

    </script>
    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content-header">
            <h1>Yeni Doktor Ekle  </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-5">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Yeni Doktor Ekle</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group" style="display: none;">
                                <label for="exampleInputEmail1">Hastane Seçiniz</label>
                                <asp:DropDownList ID="drpHospital" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="drpHospital_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Bölüm Seçiniz</label>
                                <asp:DropDownList ID="drpTibbiBolumler" runat="server" class="form-control"></asp:DropDownList>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Medin Servis ID (Bilmiyorsanız 0 Yazınız)</label>
                                <asp:TextBox ID="txtMedinServiceID" runat="server" class="form-control" placeholder="Medin Servis ID (Bilmiyorsanız 0 Yazınız)"></asp:TextBox>
                            </div>



                            <div class="form-group">
                                <label for="exampleInputEmail1">Doktor Adı</label>
                                <asp:TextBox ID="txtDoktorAdi" runat="server" class="form-control" placeholder="Doktor Adı"></asp:TextBox>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Ünvanı</label>
                                <asp:TextBox ID="txtUnvan" runat="server" class="form-control" placeholder="Ünvanı"></asp:TextBox>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Resim </label>
                                <input id="xFilePath" name="FilePath" type="text" size="60" />
                                <input type="button" value="Sunucuyu Gez" onclick="BrowseServer();" />
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Aktif / Pasif</label>
                                <asp:CheckBox ID="ckIsActive" runat="server" class="form-control" />
                            </div>



                        </div>
                        <div class="box-footer">
                            <asp:Button ID="btnDoktorEkle" runat="server" Text="Doktor Ekle" class="btn btn-primary" OnClick="btnDoktorEkle_Click" /><br />
                            <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>

                        </div>
                    </div>

                </div>
        </section>
    </div>
</asp:Content>
