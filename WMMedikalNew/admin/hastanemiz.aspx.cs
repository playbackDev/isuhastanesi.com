﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm24 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            trHakkimizda.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
            //trHakkimizda.config.extraPlugins = "myplugin,mytools14";
            trHakkimizda.integrationPlugin();

            //enHakkimizda.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
            //enHakkimizda.integrationPlugin();

            if (Request.QueryString["page_id"] != "" && Request.QueryString["page_id"] != null)
            {
                int PageID = Request.QueryString["page_id"].toint();
                if (!IsPostBack)
                {
                    

                    var _HakkimizdaTR = db.Hastane_Sayfalar_Dil.Select(s => new
                    {
                        s.sayfa_adi,
                        s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(d => d.lang == "TR").description,
                        s.sayfa_id,
                        s.Hastane_Sayfalar.Hastaneler.HastaneAdi,
                        hastane_id = s.Hastane_Sayfalar.Hastaneler.id,
                        GalleryImage = s.Hastane_Sayfalar.gallery.Select(s2 => new {
                            id = s2.id,
                            s2.image_name,
                            active_status = (s2.is_active) ? "Aktif" : "Pasif",
                            s2.Hastaneler.HastaneAdi
                        }),
                        s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(d => d.lang == "TR").top_banner,
                         s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(d => d.lang == "TR").meta_description,
                        s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(d => d.lang == "TR").page_url

                        
                    }).Where(q => q.sayfa_id == PageID).FirstOrDefault();

                    if(_HakkimizdaTR !=null)
                    {
                        hdnPageID.Value = _HakkimizdaTR.sayfa_id.ToString();
                        hdnHospitalID.Value = _HakkimizdaTR.hastane_id.ToString();
                        ltrHospitalName.Text = _HakkimizdaTR.HastaneAdi;
                        //ltrHospitalName2.Text = _HakkimizdaTR.HastaneAdi;
                        ltrPageName.Text = _HakkimizdaTR.sayfa_adi;
                        //ltrPageName2.Text = _HakkimizdaTR.sayfa_adi;
                        trHakkimizda.Text = _HakkimizdaTR.description;
                        ltrTopBannerTR.Text = "<img src='" + _HakkimizdaTR.top_banner + "'/>";
                        //txtDescriptionTR.Text = _HakkimizdaTR.meta_description;
                        //txtPageURLTR.Text = _HakkimizdaTR.page_url;
                    }
               

                    rptGaleri.DataSource = _HakkimizdaTR.GalleryImage;
                    rptGaleri.DataBind();

                    //var _HakkimizdaEN = db.Hastane_Sayfalar_Dil.Select(s => new
                    //{
                    //    s.sayfa_adi,
                    //    s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(d => d.lang == "EN").description,
                    //    s.sayfa_id,
                    //    s.Hastane_Sayfalar.Hastaneler.HastaneAdi,
                    //    hastane_id = s.Hastane_Sayfalar.Hastaneler.id,
                    //      s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(d => d.lang == "EN").top_banner,
                    //    s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(d => d.lang == "EN").meta_description,
                    //           s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(d => d.lang == "EN").page_url
                    //}).Where(q => q.sayfa_id == PageID).FirstOrDefault();

                    //if (_HakkimizdaEN !=null)
                    //{

                    //    txtPageURLEN.Text = _HakkimizdaEN.page_url;
                    //enHakkimizda.Text = _HakkimizdaEN.description;
                    //ltrTopBannerEN.Text = "<img src='" + _HakkimizdaEN.top_banner + "'/>";
                    //txtDescriptionEN.Text = _HakkimizdaEN.meta_description;
                        

                    //}
                }
            
              
            }
        }

        protected void btnUpdateTR_Click(object sender, EventArgs e)
        {
            int PageID = hdnPageID.Value.toint();
            var _UpdatePageTR = db.Hastane_Sayfa_Detay_Dil.Where(q => q.sayfa_id == PageID && q.lang == "TR").FirstOrDefault();

           
                if (!string.IsNullOrEmpty(Request.Form["FilePathTR"].ToString()))
                {
                    _UpdatePageTR.top_banner = Request.Form["FilePathTR"].ToString();
                }
                _UpdatePageTR.description = trHakkimizda.Text;
                //_UpdatePageTR.page_url = txtPageURLTR.Text.Trim();
                //_UpdatePageTR.meta_description = txtDescriptionTR.Text.Trim();


                if (db.SaveChanges() > 0)
                {
                    ResizeImage(_UpdatePageTR.top_banner, 1903, 381);
                    Page.Response.Redirect(Page.Request.Url.ToString(), false);
                }
        
        }

        //protected void btnUpdateEN_Click(object sender, EventArgs e)
        //{
        //    int PageID = hdnPageID.Value.toint();
        //    var _UpdatePageEN = db.Hastane_Sayfa_Detay_Dil.Where(q => q.sayfa_id == PageID && q.lang == "EN").FirstOrDefault();

            
        //         if (!string.IsNullOrEmpty(Request.Form["FilePathEN"].ToString()))
        //         {
        //             _UpdatePageEN.top_banner = Request.Form["FilePathEN"].ToString();
        //         }

        //         _UpdatePageEN.description = enHakkimizda.Text;
        //         _UpdatePageEN.top_banner = Request.Form["FilePathEN"].ToString();
        //         _UpdatePageEN.meta_description = txtDescriptionEN.Text.Trim();
        //         _UpdatePageEN.page_url = txtPageURLEN.Text.Trim();
      

        //         if (db.SaveChanges() > 0)
        //             Page.Response.Redirect(Page.Request.Url.ToString(), false);


        //}

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            string ımageURL = string.Empty;
            ımageURL = Request.Form["FilePath"].ToString();


            if (ımageURL != null || ımageURL!="")
            {
                int uzunluk = ımageURL.Split('.').Length;
                string random = CreateRandomValue(10, true, true, true, false) + "." + ımageURL.Split('.')[uzunluk - 1];
                //FileUpload1.SaveAs(Server.MapPath("~/uploads/pages/" + random));


                gallery _newGaleri = new gallery();
                _newGaleri.image_name = Request.Form["FilePath"].ToString();
                _newGaleri.is_active = ckIsActive.Checked;
                _newGaleri.page_id = hdnPageID.Value.toint();
                _newGaleri.hospital_id = hdnHospitalID.Value.toint();

                db.gallery.Add(_newGaleri);

                if (db.SaveChanges() > 0)
                    Page.Response.Redirect(Page.Request.Url.ToString(), false);

            }
            else
                lblUyari.Text = "Resim Seçmediniz";

        }

        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int id = e.CommandArgument.toint();

                var _delete = db.gallery.Where(q => q.id == id).FirstOrDefault();

                if (_delete != null)
                {
                    db.gallery.Remove(_delete);
                    if (db.SaveChanges() > 0)
                        Page.Response.Redirect(Page.Request.Url.ToString(), false);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
         
        }

    }
}
