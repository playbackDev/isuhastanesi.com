﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class slider_detay : cBase
    {
        int SlideID;
        //string Lang;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["slider_id"] != null)
            {
                SlideID = Request.QueryString["slider_id"].toint();

                if (!IsPostBack)
                {
                    var _slide = db.slider.Where(q => q.id == SlideID && q.lang == "TR").FirstOrDefault();

                    if (_slide != null)
                    {
                        txtURL.Text = _slide.link;
                        ckIsActive.Checked = _slide.is_active;
                        Image1.ImageUrl = _slide.image_url;
                        Image2.ImageUrl = _slide.image_url_mobile;
                    }
                }
            }
        }

        protected void btnGuncelle_Click(object sender, EventArgs e)
        {
            string imageURL = Request.Form["FilePath"].ToString();
            string imageURL2 = Request.Form["FilePath2"].ToString();

            var _slide = db.slider.Where(q => q.id == SlideID && q.lang == "TR").FirstOrDefault();

            if (_slide != null)
            {
                _slide.lang = "TR";
                _slide.link = txtURL.Text.Trim();
                _slide.is_active = ckIsActive.Checked;

                if (!string.IsNullOrEmpty(imageURL)) _slide.image_url = imageURL;
                if (!string.IsNullOrEmpty(imageURL2)) _slide.image_url_mobile = imageURL2;

                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }
            else
                Alert("Böyle bir data bulunamadı");
        }
    }
}