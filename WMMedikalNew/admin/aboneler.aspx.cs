﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm9 : cBase
    {
        int j = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            var _newsLetter = db.newsletter.Select(s => new { s.email, active_status = (s.is_active) ? "Aktif" : "Pasif", }).ToList();
            rptNewsLetter.DataSource = _newsLetter;
            rptNewsLetter.DataBind();
        }

        protected void lnkProductExport_Click(object sender, EventArgs e)
        {
            var _newsLetter = db.newsletter.Select(s => new NewsLetterModel { email = s.email, aktif=  (s.is_active) ? "Aktif" : "Pasif", }).ToList();

            ltrExcel.Text = makeExcel(_newsLetter);
        }
       

         string makeExcel(List<NewsLetterModel> sl)
         {
             string random = GetRandom(12, 1, 1, 1, 0) + ".xls";
             try
             {
                 
                 using (ExcelPackage pck = new ExcelPackage())
                 {

                     string emptyexcel = Server.MapPath("~/admin/excel/" + random);
                     var newFile = new FileInfo(emptyexcel);
                     ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Rapor");
                     List<string> headList = new List<string>() { "Mail Adresi", "Aktif Durumu"};

                     for (int j = 1; j < headList.Count + 1; j++)
                         ws.Cells[1, j].Value = headList[j - 1];


                     for (int i = 2; i < sl.Count + 2; i++)
                     {
                         j++;
                         ws.Cells[i, 1].Value = sl[i - 2].email;
                         ws.Cells[i, 2].Value = sl[i - 2].aktif;
               
                     }

                     pck.SaveAs(newFile);
                 }


             }
             catch (Exception ex)
             {
                 Response.Write(ex);
             }

             return "<tr><th colspan='3'><a href='/admin/excel/" + random + "' target='_blank' class='btn btn-success'>Excel İndir</a></th></tr>";

         }
        public class NewsLetterModel
         {
             public string email { get; set; }
             public string aktif { get; set; }
         }
    }
}