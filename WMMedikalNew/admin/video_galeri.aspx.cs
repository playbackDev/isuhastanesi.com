﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class video_galeri : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var VideoList = db.video_gallery.Select(s => new
                {
                    s.id,
                    s.video_link,
                    s.video_name,
                    active_status = (s.is_active) ? "Aktif" : "Pasif",

                }).ToList();

                rptVideoList.DataSource = VideoList;
                rptVideoList.DataBind();
            }
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            
            if (txtVideoLink.Text.Trim() != "" && txtVideoName.Text.Trim() != "")
            {
                video_gallery _newVideo = new video_gallery();
                _newVideo.video_link = txtVideoLink.Text.Trim();
                _newVideo.video_name = txtVideoName.Text.Trim();
                _newVideo.is_active = ckIsActive.Checked;

                db.video_gallery.Add(_newVideo);

                if (db.SaveChanges() > 0)
                    Response.Redirect("video_galeri.aspx", false);
                else
                    lblUyari.Text = "Bir hata oluştu";
            }
            else
                lblUyari.Text = "Boş bırakmayın";
        }

        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            int ID = e.CommandArgument.toint();
            var Delete = db.video_gallery.Where(q => q.id == ID).FirstOrDefault();
            db.video_gallery.Remove(Delete);
            if (db.SaveChanges() > 0)
                Response.Redirect("video_galeri.aspx", false);
        }
    }
}