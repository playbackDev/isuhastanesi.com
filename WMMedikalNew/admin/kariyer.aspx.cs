﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class kariyer : cBase
    {
        int PageID;
        protected void Page_Load(object sender, EventArgs e)
        {
            //trHakkimizda.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
            //trHakkimizda.integrationPlugin();

            //enHakkimizda.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
            //enHakkimizda.integrationPlugin();

            if (Request.QueryString["page_id"] != "" && Request.QueryString["page_id"] != null)
            {

                if (!IsPostBack)
                {


                    PageID = Request.QueryString["page_id"].toint();

                    var SayfaDetayTR = db.Sayfa_Detay_Dil.Select(s => new
                    {
                        s.id,
                        s.sayfa_id,
                        SayfaAdı = s.Sayfalar.sayfa_adi,
                        s.lang,
                        s.description
                    }).Where(q => q.sayfa_id == PageID && q.lang == "TR").FirstOrDefault();

                    if (SayfaDetayTR != null)
                    {
                        hdnPageID.Value = SayfaDetayTR.sayfa_id.ToString();
                        ltrPageName.Text = SayfaDetayTR.SayfaAdı.ToString();
                        //ltrPageName2.Text = SayfaDetayTR.SayfaAdı.ToString();
                        //trHakkimizda.Text = SayfaDetayTR.description;
                        trLinkKariyer.Text = SayfaDetayTR.description;
                    }


                    //var SayfaDetayEN = db.Sayfa_Detay_Dil.Select(s => new
                    //{
                    //    s.id,
                    //    s.sayfa_id,
                    //    SayfaAdı = s.Sayfalar.sayfa_adi,
                    //    s.lang,
                    //    s.description
                    //}).Where(q => q.sayfa_id == PageID && q.lang == "EN").FirstOrDefault();

                    //if (SayfaDetayEN != null)
                    //{

                    //    enHakkimizda.Text = SayfaDetayEN.description;
                    //}

                }
            }
        }

        protected void btnUpdateTR_Click(object sender, EventArgs e)
        {
            int PageID = hdnPageID.Value.toint();
            var _UpdatePageTR = db.Sayfa_Detay_Dil.Where(q => q.sayfa_id == PageID && q.lang == "TR").FirstOrDefault();

            _UpdatePageTR.description = trLinkKariyer.Text;

            //_UpdatePageTR.description = trHakkimizda.Text;

            if (db.SaveChanges() > 0)
           
                Page.Response.Redirect(Page.Request.Url.ToString(), false);
            
        }

        //protected void btnUpdateEN_Click(object sender, EventArgs e)
        //{
        //    int PageID = hdnPageID.Value.toint();
        //    var _UpdatePageEN = db.Sayfa_Detay_Dil.Where(q => q.sayfa_id == PageID && q.lang == "EN").FirstOrDefault();
        //    _UpdatePageEN.description = enHakkimizda.Text;

        //    if (db.SaveChanges() > 0)
        //        Page.Response.Redirect(Page.Request.Url.ToString(), false);
        //}
    }
}