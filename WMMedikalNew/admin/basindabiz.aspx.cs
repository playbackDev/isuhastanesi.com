﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm13 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var PressList = db.press
                .Select(s => new { 
                s.id,
                s.title,
                s.doctor_name,
                s.date,
                s.press_name,
                s.image_name,
                type = (s.type ==1) ? "Yazılı Basın":"Görsel Basın",
                active_status = (s.is_active) ? "Aktif" : "Pasif",
                s.hospital_id
                })
                .ToList();

            rptPress.DataSource = PressList;
            rptPress.DataBind();

            if (!IsPostBack)
            {
                _Fillpage();
            }
        }

        private void _Fillpage()
        {


            drpHospital.DataSource = db.our_hospital.Where(q => q.is_active).ToList();
            drpHospital.DataTextField = "hospital_name";
            drpHospital.DataValueField = "id";
            drpHospital.DataBind();

        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            if (drpType.SelectedValue.toint() > 0)
            {
                if (txtPressName.Text.Trim() != "" && txtDoctorName.Text.Trim() != "" && txtDate.Text.Trim() != "" && txtPressTitle.Text.Trim() !="")
                {
                    string ımageURL = string.Empty;
                    ımageURL = Request.Form["FilePath"].ToString();

                    if (ımageURL != null || ımageURL != "")
                    {

                        //int uzunluk = FileUpload1.FileName.Split('.').Length;
                        //string random = CreateRandomValue(10, true, true, true, false) + "." + FileUpload1.FileName.Split('.')[uzunluk - 1];
    


                        //FileUpload1.SaveAs(Server.MapPath("~/uploads/press/kucuk/" + random));
                        //FileUpload1.SaveAs(Server.MapPath("~/uploads/press/" + random));

                        //ResizeImage("~/uploads/press/kucuk/" + random, 428, 283);
                        //ResizeImage("~/uploads/press/" + random, 1280, 1834);

                     
            
                        press _newPress = new press();
                        _newPress.title = txtPressTitle.Text.Trim();
                        _newPress.doctor_name = txtDoctorName.Text.Trim();
                        _newPress.date = txtDate.Text.Trim();
                        _newPress.press_name = txtPressName.Text.Trim();
                        _newPress.type = drpType.SelectedValue.toint();
                        _newPress.hospital_id = drpHospital.SelectedValue.toint();
                        _newPress.is_active = ckIsActive.Checked;
                        _newPress.image_name = ımageURL;

                        db.press.Add(_newPress);

                        if (db.SaveChanges() > 0)
                            Response.Redirect("basindabiz.aspx", false);
                    }
                    else
                        lblUyari.Text = "Resim seçmediniz.";
                }
                else
                    lblUyari.Text = "Boş alan bırakmayınız.";
            }
            else
                lblUyari.Text = "Yayın Türü Seçmediniz";
        }

        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            int ID = e.CommandArgument.toint();
            var _delete = db.press.Where(q=> q.id == ID).FirstOrDefault();
            db.press.Remove(_delete);
            if (db.SaveChanges() > 0)
                Response.Redirect("basindabiz.aspx", false);

        }
    }
}