﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="siteler.aspx.cs" Inherits="WMMedikalNew.admin.WebForm7" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
         <style>
        .dataTables_length
        {
            display:none;
        }
        .dataTables_filter
        {
            float:right;
        }
        .dataTables_info
        {
            display:none;
        }
    </style>
            	<script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
	<script type="text/javascript">

	    function BrowseServer() {

	        var finder = new CKFinder();
	        finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
	        finder.selectActionFunction = SetFileField;
	        finder.popup();


	    }

	    function SetFileField(fileUrl) {
	        document.getElementById('xFilePath').value = fileUrl;
	    }

	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content-header">
            <h1>Siteler </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-3">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Yeni Site Ekle</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Site Adı </label>
                                <asp:TextBox ID="txtSiteName" runat="server" class="form-control" placeholder="Site Adı"></asp:TextBox>
                            </div>

                              <div class="form-group">
                                <label for="exampleInputEmail1">Site Linki </label>
                                <asp:TextBox ID="txtSiteURL" runat="server" class="form-control" placeholder="Site Adı"></asp:TextBox>
                            </div>

                               <div class="form-group">
                                <label for="exampleInputEmail1">Durum (Aktif/Pasif)</label>
                                <asp:CheckBox ID="ckIsActive" runat="server" class="form-control" />
                            </div>

                               <div class="form-group">
                                <label for="exampleInputEmail1">Resim </label>
                                 	<input id="xFilePath" name="FilePath" type="text" size="60" />
		                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer();" />
                            </div>


                        </div>
                        <div class="box-footer">

                            <asp:Button ID="btnEkle" runat="server" Text="Site Ekle" class="btn btn-primary" OnClick="btnEkle_Click"/><br />
                            <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>

                        </div>
                    </div>

                </div>
                <div class="col-xs-9">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Siteler</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Site Adı</th>
                                        <th>Durum</th>
                                        <th>İşlem</th>
                                        <td>Diller</td>
                                    </tr>
                                </thead>

                                <tbody>
                                    <asp:Repeater ID="rptSiteList" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%#Eval ("site_name") %></td>
                                                <td> <%#Eval ("active_status") %></td>
                                                  <td>
                                                     <a class="btn btn-s btn-success" href="site_detay.aspx?site_id=<%#Eval ("micro_site_id") %>&Lang=TR"> TR </a>
                                                     <a class="btn btn-s btn-info" href="site_detay.aspx?site_id=<%#Eval ("micro_site_id") %>&Lang=EN">  EN</a>
                                                </td>
                                              
                                                <td>
                                                        <asp:LinkButton runat="server" OnCommand="lnkRemove_Command" CommandArgument='<%#Eval ("micro_site_id") %>' ID="lnkRemove" OnClientClick="return confirm('silmek istediğinizden emin misiniz!');" CssClass="btn btn-s btn-danger" >Sil
                                                    </asp:LinkButton>
                                                </td>
                                                
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>



<%--    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>--%>

    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <script src="plugins/fastclick/fastclick.min.js"></script>

    <script src="dist/js/demo.js"></script>

    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
</asp:Content>
