﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm23 : cBase
    {
        int HospitalID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["hospital_id"] != "" && Request.QueryString["hospital_id"] != null)
            {
                HospitalID = Request.QueryString["hospital_id"].toint();
                if (!IsPostBack)
                {

                    var AddressTR = db.hl_contact.Select(s => new
                    {
                        s.id,
                        s.addressTR,
                        s.call_center,
                        s.eposta,
                        s.Hastaneler.HastaneAdi,
                        s.hospital_id,
                        s.telephone,
                        s.latlng,
                        s.img_path
                        
                    }).Where(q => q.hospital_id == HospitalID).FirstOrDefault();

                    if (AddressTR != null)
                    {
                        hdnID.Value = AddressTR.id.ToString();
                        ltrHospitalName.Text = AddressTR.HastaneAdi;
                        //ltrHospitalName2.Text = AddressTR.HastaneAdi;
                        ltrHospitalName3.Text = AddressTR.HastaneAdi;
                        txtAddressTR.Text = AddressTR.addressTR;
                        txtEposta.Text = AddressTR.eposta;
                        txtCallCenter.Text = AddressTR.call_center;
                        txtTelephone.Text = AddressTR.telephone;
                        txtEnlemBoylam.Text = AddressTR.latlng;
                        ImageBanner.ImageUrl = AddressTR.img_path;
                     

                    }

                    //var AddressEN = db.hl_contact.Select(s => new
                    //{
                    //    s.id,
                    //    s.addressEN,
                    //    s.call_center,
                    //    s.eposta,
                    //    s.Hastaneler.HastaneAdi,
                    //    s.hospital_id
                    //}).Where(q => q.hospital_id == HospitalID).FirstOrDefault();

                    //if (AddressEN != null)
                    //{
                    //    txtAddressEN.Text = AddressEN.addressEN;
                    //}

                }
            }
        }

        protected void btnUpdateTR_Click(object sender, EventArgs e)
        {
            int ID = hdnID.Value.toint();
            var _UpdateTR = db.hl_contact.Where(q => q.id == ID).FirstOrDefault();
            _UpdateTR.addressTR = txtAddressTR.Text.Trim();

            if (db.SaveChanges() > 0)
                Page.Response.Redirect(Page.Request.Url.ToString(), false);

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.Form["FilePathTR"].ToString() != null || Request.Form["FilePathTR"].ToString() != "")
                {
                    var _bannerlar = db.hl_contact.FirstOrDefault();
                    if (_bannerlar != null)
                    {
                        if (!string.IsNullOrEmpty(Request.Form["FilePathTR"].ToString()))
                        {
                            _bannerlar.img_path = Request.Form["FilePathTR"].ToString();
                        }
                        //_bannerlar.meta_description = txtDescriptionTR.Text.Trim();
                        //_bannerlar.page_url = txtPageURLTR.Text.Trim();

                        if (db.SaveChanges() > 0)
                        {
                            ResizeImage(_bannerlar.img_path, 1903, 381);
                            Page.Response.Redirect(Page.Request.Url.ToString(), false);
                        }
                        else
                            lblUyariTR.Text = "Güncelleme Yapılamadı";
                    }
                }
                else
                    lblUyariTR.Text = "Resim Seçiniz";
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }


        //protected void btnUpdateEN_Click(object sender, EventArgs e)
        //{
        //    int ID = hdnID.Value.toint();
        //    var _UpdateEN = db.hl_contact.Where(q => q.hospital_id == HospitalID).FirstOrDefault();
        //    _UpdateEN.addressEN = txtAddressEN.Text.Trim();

        //    if (db.SaveChanges() > 0)
        //        Page.Response.Redirect(Page.Request.Url.ToString(), false);
        //}

        protected void btnContact_Click(object sender, EventArgs e)
        {
            int ID = hdnID.Value.toint();
            var _Update = db.hl_contact.Where(q => q.hospital_id == HospitalID).FirstOrDefault();

            _Update.eposta = txtEposta.Text.Trim();
            _Update.call_center = txtCallCenter.Text.Trim();
            _Update.telephone = txtTelephone.Text.Trim();
            _Update.latlng = txtEnlemBoylam.Text.Trim();
          
            if (db.SaveChanges() > 0)
                Page.Response.Redirect(Page.Request.Url.ToString(), false);
        }

    }
}