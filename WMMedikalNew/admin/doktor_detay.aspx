﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="doktor_detay.aspx.cs" Inherits="WMMedikalNew.admin.WebForm15" EnableEventValidation="false" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
    <script type="text/javascript">

        function BrowseServer() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField;
            finder.popup();


        }

        function SetFileField(fileUrl) {
            document.getElementById('FilePath').value = fileUrl;
        }
    </script>


    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content">
            <div class="row">

                <div class="col-md-9">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            Ek Tibbi Bölümler
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Bölüm seç</label>
                                <div class="col-sm-10">
                                    <asp:DropDownList runat="server" ID="dlEkBolumler" CssClass="form-control">
                                        <asp:ListItem Text="text1" />
                                        <asp:ListItem Text="text2" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <br /><br />
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <asp:LinkButton Text="Kaydet" runat="server" ID="lnkSaveEkBolum" CssClass="btn btn-success" OnClick="lnkSaveEkBolum_Click" />
                                </div>
                            </div>
                            <br />
                            <br />
                            <br />
                            <div class="form-group">
                                <asp:Repeater runat="server" ID="rptEkBolumler">
                                    <ItemTemplate>
                                        <span class="alert alert">
                                            <asp:LinkButton ID="lnkRemove" runat="server" CssClass="btn btn-danger btn-xs" CommandArgument='<%# Eval("id") %>' OnCommand="lnkRemove_Command" OnClientClick="return confirm('Silme işlemini onaylıyor musunuz?')"><span class="glyphicon glyphicon-remove"></span></asp:LinkButton>
                                            <%# Eval("BolumAdi") %>
                                        </span>
                                        <br /><br />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                <asp:HiddenField ID="hdnID" runat="server" />
                                <asp:Literal ID="ltrDoktorName" runat="server" />
                                <%--(<asp:Literal ID="ltrLang" runat="server"></asp:Literal>)--%></h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Aktif / Pasif</label>
                                        <div class="col-sm-10">
                                            <asp:CheckBox ID="ckIsActive" runat="server" class="form-control" />
                                        </div>
                                    </div>
                                    <%--  <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Meta Description</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtMetaDescription" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>--%>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1" class="col-sm-2 control-label">Bölüm Seçiniz</label>
                                        <asp:DropDownList ID="drpTibbiBolumler" runat="server" class="form-control"></asp:DropDownList>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Eposta Adresi</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Tıbbi Ünvan</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtUnvan" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Ad Soyad</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtNameSurname" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Açıklama</label>
                                        <div class="col-sm-10">

                                            <CKEditor:CKEditorControl ID="areaDescription" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Uzmanlık Alanları</label>
                                        <div class="col-sm-10">

                                            <CKEditor:CKEditorControl ID="areaExpertise" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Yayınlar</label>
                                        <div class="col-sm-10">

                                            <CKEditor:CKEditorControl ID="areaPublications" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Eğitim</label>
                                        <div class="col-sm-10">

                                            <CKEditor:CKEditorControl ID="areaEducation" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Üyelikler</label>
                                        <div class="col-sm-10">

                                            <CKEditor:CKEditorControl ID="areaMembership" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Deneyimler</label>
                                        <div class="col-sm-10">

                                            <CKEditor:CKEditorControl ID="areaExprience" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Ödüller</label>
                                        <div class="col-sm-10">


                                            <CKEditor:CKEditorControl ID="areaAwards" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sertifikalar</label>
                                        <div class="col-sm-10">


                                            <CKEditor:CKEditorControl ID="areaCertificates" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>
                                    </div>

                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdate" runat="server" Text="Güncelle" class="btn btn-info pull-right" OnClick="btnUpdate_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Resim</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                    <div class="form-group">

                                        <div class="col-sm-12">
                                            <asp:Literal ID="ltrImage" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="form-group">

                                        <div class="col-sm-12">
                                            <asp:Literal ID="ltrFilePath" runat="server" />
                                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer();" />
                                        </div>
                                    </div>

                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdateGuncelle" runat="server" Text="Resim Güncelle" class="btn btn-info pull-right" OnClick="btnUpdateGuncelle_Click" />
                                    <br />
                                    <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

</asp:Content>
