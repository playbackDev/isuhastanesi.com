﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class sosyal_medya : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


              

                var SayfaDetayTR = db.sosyal_ikon.Select(s => new
                {
                    s.id,
                    s.facebook,
                    s.twitter,
                    s.instagram,
                    s.youtube
                }).FirstOrDefault();

                if (SayfaDetayTR != null)
                {

                    txtFacebook.Text = SayfaDetayTR.facebook;
                    txtInstagram.Text = SayfaDetayTR.instagram;
                    txtTwitter.Text = SayfaDetayTR.twitter;
                    txtYoutube.Text = SayfaDetayTR.youtube;
                    //ltrPageName2.Text = SayfaDetayTR.SayfaAdı.ToString();
                    //trHakkimizda.Text = SayfaDetayTR.description;
                   
                }
            }
        }


        protected void btnUpdateTR_Click(object sender, EventArgs e)
        {
            
            var _UpdatePageTR = db.sosyal_ikon.FirstOrDefault();

            _UpdatePageTR.facebook = txtFacebook.Text;
            _UpdatePageTR.twitter = txtTwitter.Text;
            _UpdatePageTR.instagram = txtInstagram.Text;
            _UpdatePageTR.youtube = txtYoutube.Text;
            //_UpdatePageTR.description = trHakkimizda.Text;

            if (db.SaveChanges() > 0)
                Page.Response.Redirect(Page.Request.Url.ToString(), false);
        }
    }
}