﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class popup_yukle :cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            var studentList = db.Database.SqlQuery<pageModel>("SELECT sayfa_adi,id from sayfalar union all select sayfa_adi,id from hastane_sayfalar").ToList();

            Repeater1.DataSource = studentList;
            Repeater1.DataBind();
        }

        public class pageModel
        {
            public int id { get; set; }
            public string sayfa_adi { get; set; }
        }
    }
}