﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm12 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var _gallery = db.logo.Select(s => new
                {
                    s.id,
                    s.image_name,
                    active_status = (s.is_active) ? "Aktif" : "Pasif",
                   s.download_file

                }).ToList();

                if (_gallery.Count > 0)
                {
                    rptGaleri.DataSource = _gallery;
                    rptGaleri.DataBind();
                }

                _Fillpage();
            }
        }

        private void _Fillpage()
        {
          
            drpHospital.DataSource = db.Hastaneler.ToList();
            drpHospital.DataTextField = "HastaneAdi";
            drpHospital.DataValueField = "id";
            drpHospital.DataBind();
            drpHospital.Items.Insert(0, new ListItem("Hastane Seçiniz..", "0"));
        }

        protected void btnEkle_Click(object sender, EventArgs e)
        {
            if (drpHospital.SelectedValue.isNumara())
            {
                string ımageURL = string.Empty;
                ımageURL = Request.Form["FilePath"].ToString();

                string ımageURL1 = string.Empty;
                ımageURL1 = Request.Form["FilePath"].ToString();

                if (ımageURL != "" || ımageURL != null)
                {
                    if (ımageURL1 != "" || ımageURL1 != null)
                    {

                    //    int uzunluk = ımageURL.Split('.').Length;
                    //string random = CreateRandomValue(10, true, true, true, false) + "." + ımageURL.Split('.')[uzunluk - 1];

                    //int uzunluk2 = ımageURL1.Split('.').Length;
                    //string random2 = CreateRandomValue(10, true, true, true, false) + "." + ımageURL1.Split('.')[uzunluk2 - 1];


                    //FileUpload1.SaveAs(Server.MapPath("~/uploads/download/" + random));
                    //FileUpload2.SaveAs(Server.MapPath("~/uploads/download/" + random2));

                    logo _newLogo = new logo();
                    _newLogo.image_name = ımageURL;
                    _newLogo.is_active = ckIsActive.Checked;
                    _newLogo.hospital_id = drpHospital.SelectedValue.toint();
                    _newLogo.download_file = ımageURL1;

                    db.logo.Add(_newLogo);

                    if (db.SaveChanges() > 0)
                        Response.Redirect("kurumsal_kimlik.aspx", false);
                    }
                    else
                        lblUyari.Text = "İndirme Dosyası Seçmediniz";
                }
                else
                    lblUyari.Text = "Logo Seçmediniz";

            }
            else
                lblUyari.Text = "Hastane Seçmediniz";
        }

        protected void LinkButton1_Command(object sender, CommandEventArgs e)
        {
            int ID = e.CommandArgument.toint();
            var Delete = db.logo.Where(q => q.id == ID).FirstOrDefault();
            db.logo.Remove(Delete);
            if (db.SaveChanges() > 0)
                Response.Redirect("kurumsal_kimlik.aspx", false);
        }
    }
}