﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm21 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            trKalite.config.toolbar = WebTools.CKToolBar(CkTool.Simple);

            trKalite.integrationPlugin();

            //enKalite.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
            //enKalite.integrationPlugin();


            if (Request.QueryString["page_id"] != "" && Request.QueryString["page_id"] != null)
            {
                if (!IsPostBack)
                {
                    int PageID = Request.QueryString["page_id"].toint();
                    var _PageDetailTR = db.Hastane_Sayfalar_Dil.Select(s => new
                    {
                        s.sayfa_adi,
                        s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(d => d.lang == "TR").description,
                        s.sayfa_id,
                        s.Hastane_Sayfalar.Hastaneler.HastaneAdi,
                        hastane_id = s.Hastane_Sayfalar.Hastaneler.id,
                        s.dil,
                        s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(d => d.lang == "TR").top_banner,
                        s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(d => d.lang == "TR").meta_description,
                        s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(d => d.lang == "TR").page_url
                    }).Where(q => q.sayfa_id == PageID && q.dil == "TR").FirstOrDefault();

                    hdnPageID.Value = _PageDetailTR.sayfa_id.ToString();
                    ltrHospitalName.Text = _PageDetailTR.HastaneAdi;
                    //ltrHospitalName2.Text = _PageDetailTR.HastaneAdi;
                    ltrPageName.Text = _PageDetailTR.sayfa_adi;
                    //ltrPageName2.Text = _PageDetailTR.sayfa_adi;
                    ltrTopBannerTR.Text = "<img src='" + _PageDetailTR.top_banner + "'/>";
                    //txtMetaDescriptionTR.Text = _PageDetailTR.meta_description;
                    //txtPageURLTR.Text = _PageDetailTR.page_url;

                    trKalite.Text = _PageDetailTR.description;

                    //var _PageDetailEN = db.Hastane_Sayfalar_Dil.Select(s => new
                    //{
                    //    s.sayfa_adi,
                    //    s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(d => d.lang == "EN").description,
                    //    s.sayfa_id,
                    //    s.Hastane_Sayfalar.Hastaneler.HastaneAdi,
                    //    hastane_id = s.Hastane_Sayfalar.Hastaneler.id,
                    //    s.dil,
                    //    s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(d => d.lang == "EN").top_banner,
                    //    s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(d => d.lang == "EN").meta_description,
                    //    s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(d => d.lang == "EN").page_url
                    //}).Where(q => q.sayfa_id == PageID && q.dil == "EN").FirstOrDefault();

                    //enKalite.Text = _PageDetailEN.description;
                    //ltrTopBannerEN.Text = "<img src='" + _PageDetailEN.top_banner + "'/>";
                    //txtMetaDescriptionEN.Text = _PageDetailEN.meta_description;
                    //txtPageURLEN.Text = _PageDetailEN.page_url;
                }
            }
        }

        protected void btnUpdateTR_Click(object sender, EventArgs e)
        {
            try
            {
                int PageID = hdnPageID.Value.toint();
                var _UpdatePageTR = db.Hastane_Sayfa_Detay_Dil.Where(q => q.sayfa_id == PageID && q.lang == "TR").FirstOrDefault();

                if (Request.Form["FilePathTR"].ToString() != null && Request.Form["FilePathTR"].ToString() != "")
                {

                    if (!string.IsNullOrEmpty(Request.Form["FilePathTR"].ToString()))
                    {
                        _UpdatePageTR.top_banner = Request.Form["FilePathTR"].ToString();
                    }

                }
                    _UpdatePageTR.description = trKalite.Text;
                    //_UpdatePageTR.meta_description = txtMetaDescriptionTR.Text.Trim();
                    //_UpdatePageTR.page_url = txtPageURLTR.Text.Trim();



                    if (db.SaveChanges() > 0)
                    {
                        ResizeImage(_UpdatePageTR.top_banner, 1903, 381);
                        Page.Response.Redirect(Page.Request.Url.ToString(), false);
                    }

            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }

        }

    //    protected void btnUpdateEN_Click(object sender, EventArgs e)
    //    {
    //        try
    //        {
    //            int PageID = hdnPageID.Value.toint();
    //            var _UpdatePageEN = db.Hastane_Sayfa_Detay_Dil.Where(q => q.sayfa_id == PageID && q.lang == "EN").FirstOrDefault();

    //            if (!string.IsNullOrEmpty(Request.Form["FilePathEN"].ToString()))
    //            {
    //                if (!string.IsNullOrEmpty(Request.Form["FilePathEN"].ToString()))
    //                {
    //                    _UpdatePageEN.top_banner = Request.Form["FilePathEN"].ToString();
    //                }
                   
    //            }
    //            _UpdatePageEN.description = enKalite.Text;
    //            _UpdatePageEN.meta_description = txtMetaDescriptionEN.Text.Trim();
    //            _UpdatePageEN.page_url = txtPageURLEN.Text.Trim();


    //            if (db.SaveChanges() > 0)
    //                Page.Response.Redirect(Page.Request.Url.ToString(), false);
    //        }
    //        catch (Exception ex)
    //        {
    //            Response.Write(ex);
    //        }

    //    }
    }
}