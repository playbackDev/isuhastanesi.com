﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm15 : cBase
    {
        int DoctorID;
        //string Lang;
        protected void Page_Load(object sender, EventArgs e)
        {

            areaDescription.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
            areaDescription.integrationPlugin();

            areaExpertise.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
            areaExpertise.integrationPlugin();

            areaPublications.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
            areaPublications.integrationPlugin();

            areaEducation.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
            areaEducation.integrationPlugin();

            areaMembership.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
            areaMembership.integrationPlugin();

            areaExprience.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
            areaExprience.integrationPlugin();

            areaAwards.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
            areaAwards.integrationPlugin();

            areaCertificates.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
            areaCertificates.integrationPlugin();



            //Lang = Request.QueryString["Lang"].ToString();
            DoctorID = Request.QueryString["doktor_id"].toshort();

            if (!IsPostBack)
            {
                var _doktorDetay = db.Doktorlar_Dil.Select(s => new
                {
                    s.id,
                    s.Doktorlar.ResimAdi,
                    s.Doktorlar.DoktorAdi,
                    s.Doktorlar.HastaneID,
                    s.Doktorlar.TibbiBolumID,
                    s.doktor_id,
                    s.dil,
                    s.description,
                    s.expertise,
                    s.publications,
                    s.education,
                    s.membership,
                    s.experience,
                    s.awards,
                    s.certificates,
                    s.meta_description,
                    s.Doktorlar.is_active,
                    s.email,
                    s.Doktorlar.Unvan
                }).Where(q => q.doktor_id == DoctorID && q.dil == "TR").FirstOrDefault();


                var _medicalService = db.TibbiBolumler_Dil.Where(q => q.TibbiBolumler.HastaneID == 27 && q.Dil == "TR").ToList();

                if (_medicalService != null)
                {
                    drpTibbiBolumler.DataSource = _medicalService;
                    drpTibbiBolumler.DataTextField = "BolumAdi";
                    drpTibbiBolumler.DataValueField = "BolumID";
                    drpTibbiBolumler.DataBind();

                    drpTibbiBolumler.SelectedValue = _doktorDetay.TibbiBolumID.ToString();

                    dlEkBolumler.DataSource = _medicalService;
                    dlEkBolumler.DataTextField = "BolumAdi";
                    dlEkBolumler.DataValueField = "BolumID";
                    dlEkBolumler.DataBind();
                }

                hdnID.Value = _doktorDetay.id.ToString();
                ltrDoktorName.Text = _doktorDetay.DoktorAdi;
                //ltrLang.Text = _doktorDetay.dil;
                txtUnvan.Text = _doktorDetay.Unvan;
                txtNameSurname.Text = _doktorDetay.DoktorAdi;
                //txtTitle.Text = _doktorDetay.title;
                areaDescription.Text = _doktorDetay.description;
                areaExpertise.Text = _doktorDetay.expertise;
                areaPublications.Text = _doktorDetay.publications;
                areaEducation.Text = _doktorDetay.education;
                areaMembership.Text = _doktorDetay.membership;
                areaExprience.Text = _doktorDetay.experience;
                areaAwards.Text = _doktorDetay.awards;
                areaCertificates.Text = _doktorDetay.certificates;
                //txtMetaDescription.Text = _doktorDetay.meta_description;
                ckIsActive.Checked = (bool)_doktorDetay.is_active;
                txtEmail.Text = _doktorDetay.email;

                if (_doktorDetay.ResimAdi != null && _doktorDetay.ResimAdi != "")
                {
                    ltrImage.Text = "<img src=" + _doktorDetay.ResimAdi.ToString() + ">";
                    ltrFilePath.Text = "<input id='FilePath' name='FilePath' type='text' value='" + _doktorDetay.ResimAdi.ToString() + "' size='20' />";
                }
                else
                {
                    ltrImage.Text = "<img src='/admin/resimyok.png'>";
                    ltrFilePath.Text = "<input id='FilePath' name='FilePath' type='text' size='20' />";
                }

                rptEkBolumler.DataSource = db.doktorlar_ek_bolumler.Where(q => q.doktorId == DoctorID).Select(s => new
                {
                    s.id,
                    s.TibbiBolumler.BolumAdi
                }).ToList();
                rptEkBolumler.DataBind();
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int ID = hdnID.Value.toint();
            //string Lang = Request.QueryString["Lang"].ToString();
            var _updateDoctor = db.Doktorlar_Dil.Where(q => q.id == ID && q.dil == "TR").FirstOrDefault();

            if (txtNameSurname.Text.Trim() != "")
            {
                _updateDoctor.Doktorlar.DoktorAdi = txtNameSurname.Text.Trim();
                _updateDoctor.description = areaDescription.Text;
                _updateDoctor.expertise = areaExpertise.Text;
                _updateDoctor.publications = areaPublications.Text;
                _updateDoctor.education = areaEducation.Text;
                _updateDoctor.membership = areaMembership.Text;
                _updateDoctor.experience = areaExprience.Text;
                _updateDoctor.awards = areaAwards.Text;
                _updateDoctor.certificates = areaCertificates.Text;
                //_updateDoctor.meta_description = txtMetaDescription.Text.Trim();
                _updateDoctor.Doktorlar.is_active = ckIsActive.Checked;
                _updateDoctor.email = txtEmail.Text.Trim();
                _updateDoctor.Doktorlar.Unvan = txtUnvan.Text.Trim();
                _updateDoctor.Doktorlar.TibbiBolumID = drpTibbiBolumler.SelectedValue.toint();

                if (db.SaveChanges() > 0)
                {
                    Page.Response.Redirect(Page.Request.Url.ToString(), false);
                }
            }
            else
                lblUyari.Text = "Doktor İsmi Boş Olamaz";

        }

        protected void btnUpdateGuncelle_Click(object sender, EventArgs e)
        {
            string ımageURL = string.Empty;
            ımageURL = Request.Form["FilePath"].ToString();

            if (ımageURL != null || ımageURL != "")
            {
                var _updateImage = db.Doktorlar.Where(q => q.id == DoctorID).FirstOrDefault();
                _updateImage.ResimAdi = ımageURL;

                if (db.SaveChanges() > 0)
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                else
                    lblUyari.Text = "Bir hata oluştu tekrar deneyin";
            }
            else
                lblUyari.Text = "Resim Seçmediniz.";
        }

        protected void lnkSaveEkBolum_Click(object sender, EventArgs e)
        {
            try
            {
                int _bolumId = dlEkBolumler.SelectedValue.toint();

                if (db.doktorlar_ek_bolumler.Where(q => q.doktorId == DoctorID && q.bolumId == _bolumId).FirstOrDefault() == null)
                {
                    db.doktorlar_ek_bolumler.Add(new doktorlar_ek_bolumler
                    {
                        doktorId = DoctorID,
                        bolumId = _bolumId
                    });

                    db.SaveChanges();
                }
            }
            catch { }

            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }

        protected void lnkRemove_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int _id = e.CommandArgument.toint();

                db.doktorlar_ek_bolumler.Remove(db.doktorlar_ek_bolumler.Where(q => q.id == _id).FirstOrDefault());

                db.SaveChanges();
            }
            catch { }

            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
    }
}