﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="h_iletisim.aspx.cs" Inherits="WMMedikalNew.admin.WebForm23" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

        <style>
        .dataTables_length {
            display: none;
        }

        .dataTables_filter {
            float: right;
        }

        .dataTables_info {
            display: none;
        }
    </style>
      <script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
    <script type="text/javascript">

        function BrowseServer() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField;
            finder.popup();
        }

        function BrowseServer1() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField1;
            finder.popup();
        }

        function BrowseServer2() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField2;
            finder.popup();
        }
        function SetFileField(fileUrl) {
            document.getElementById('xFilePath').value = fileUrl;
        }

        function SetFileField1(fileUrl) {
            document.getElementById('xFilePath1').value = fileUrl;
        }

        function SetFileField2(fileUrl) {
            document.getElementById('xFilePath2').value = fileUrl;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        
    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content">
                        <div class="row">
                <asp:HiddenField ID="hdnID" runat="server" />


                  <div class="col-md-9">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title"> Sayfa Üst Banner</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                       <%--    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa Linki</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtPageURLTR" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Meta Description </label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtDescriptionTR" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>

                                    </div>--%>

                                        <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa  Banner</label>
                                        <div class="col-sm-10">
                                            <asp:Image ID="ImageBanner"  runat="server" Width="500px" /><br /><br />
                                            <%--<asp:Literal ID="ltrTopBannerTR" runat="server"></asp:Literal><br /><br />--%>
                                            <input id="xFilePath1" name="FilePathTR" type="text" size="60" />
                                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer1();" />
                                        </div>
                                    </div>

                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="Button1" runat="server" Text="Güncelle" class="btn btn-info pull-right" OnClick="btnUpdate_Click" />
                                        <br />
                                    <asp:Label ID="Label2" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title"> <asp:Literal ID="ltrHospitalName" runat="server"></asp:Literal> </h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Adres</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtAddressTR" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdateTR" runat="server" Text="Adres Güncelle" class="btn btn-info pull-right"  OnClick="btnUpdateTR_Click"/>
                                        <br />
                                    <asp:Label ID="lblUyariTR" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <%--<div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title"> <asp:Literal ID="ltrHospitalName2" runat="server"></asp:Literal>  (EN) </h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Adres</label>
                                        <div class="col-sm-10">
                                                  <asp:TextBox ID="txtAddressEN" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdateEN" runat="server" Text="Güncelle (EN)" class="btn btn-info pull-right" OnClick="btnUpdateEN_Click"/>
                                        <br />
                                    <asp:Label ID="lbluyariEN" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>

                       <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title"> <asp:Literal ID="ltrHospitalName3" runat="server"></asp:Literal>  </h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                       <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">EPosta</label>
                                        <div class="col-sm-10">
                                                  <asp:TextBox ID="txtEposta" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                              <div class="form-group">
                                        <%--<label for="inputEmail3" class="col-sm-2 control-label">Hasta İletişim</label>--%>
                                        <div class="col-sm-10">
                                                <%--  <asp:TextBox ID="txtHastailetisim" runat="server" class="form-control"></asp:TextBox>--%>
                                        </div>
                                    </div>

                                           </div>
                                           <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Çağrı Merkezi</label>
                                        <div class="col-sm-10">
                                                  <asp:TextBox ID="txtCallCenter" runat="server" class="form-control" ></asp:TextBox>
                                        </div>
                                    </div>
                                           </div>
                                           <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Telefon</label>
                                        <div class="col-sm-10">
                                                  <asp:TextBox ID="txtTelephone" runat="server" class="form-control" ></asp:TextBox>
                                        </div>
                                    </div>
                                           </div>
                                           <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Harita Enlem Boylam</label>
                                        <div class="col-sm-10">
                                                  <asp:TextBox ID="txtEnlemBoylam" runat="server" class="form-control"></asp:TextBox>
                                            <span> (40.188999, 29.061413)</span>
                                        </div>
                                    </div>
                                           </div>
                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnContact" runat="server" Text="Bilgileri Güncelle" class="btn btn-info pull-right" OnClick="btnContact_Click"/>
                                        <br />
                                    <asp:Label ID="Label1" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
        </section>
    </div>
</asp:Content>
