﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class haber_ekle : cBase
    {
        byte _type = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["t"] != null)
            {
                _type = Convert.ToByte(Request.QueryString["t"]);

                txtHaberOzetTR.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
                txtHaberOzetTR.integrationPlugin();

                txtHaberTR.config.toolbar = WebTools.CKToolBar(CkTool.Simple);
                txtHaberTR.integrationPlugin();

                //var bannerss = db.news.Where(q => q.id == 1).FirstOrDefault();
                //ImageBanner.ImageUrl = bannerss.img_path;
            }
        }

        protected void btnHaberEkle_Click(object sender, EventArgs e)
        {
            string ımageURL = string.Empty;
            ımageURL = Request.Form["FilePath"].ToString();

            string imageURL2 = string.Empty;
            imageURL2 = Request.Form["FilePath2"].ToString();

            if (ımageURL != null || ımageURL != "")
            {
                news _NewNews = new news();
                _NewNews.news_title = txtHaberBaslikTR.Text.Trim();
                _NewNews.news_image = ımageURL;
                _NewNews.type = _type;
                _NewNews.seoPageTitle = txtSEOBrowserTitle.Text.Trim();
                _NewNews.seoMetaDesc = txtSEOMetaDesc.Text.Trim();
                _NewNews.seoKeywords = txtSEOMetaKeywords.Text.Trim();
                _NewNews.seoURL = txtSEOPageUrl.Text.Trim().ToURL();

                if (imageURL2 != null || imageURL2 != "") _NewNews.bigImg = imageURL2;

                db.news.Add(_NewNews);
                if (db.SaveChanges() > 0)
                {
                    news_lang _NewsLangTR = new news_lang();
                    _NewsLangTR.news_title = txtHaberBaslikTR.Text.Trim();
                    _NewsLangTR.news_short = txtHaberOzetTR.Text.Trim();
                    _NewsLangTR.news_content = txtHaberTR.Text;
                    _NewsLangTR.is_active = ckIsActiveTR.Checked;
                    _NewsLangTR.lang = "TR";
                    _NewsLangTR.news_id = _NewNews.id;
                    _NewsLangTR.created_time = DateTime.Now;
                    db.news_lang.Add(_NewsLangTR);
                    //db.SaveChanges();

                    //news_lang _NewsLangEN = new news_lang();
                    //_NewsLangEN.news_title = txtHaberBaslikEN.Text.Trim();
                    //_NewsLangEN.news_short = txtHaberOzetEN.Text.Trim();
                    //_NewsLangEN.news_content = txtHaberEN.Text;
                    //_NewsLangEN.is_active = ckIsActiveEN.Checked;
                    //_NewsLangEN.lang = "EN";
                    //_NewsLangEN.news_id = _NewNews.id;
                    //_NewsLangEN.created_time = DateTime.Now;
                    //db.news_lang.Add(_NewsLangEN);

                    if (db.SaveChanges() > 0)
                    {
                        Response.Redirect("haberler.aspx?t=" + _type, false);
                    }

                }
            }
        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                if (Request.Form["FilePathTR"].ToString() != null || Request.Form["FilePathTR"].ToString() != "")
                {



                    var _bannerlar = db.news.FirstOrDefault(q => q.id == 1);
                    if (_bannerlar != null)
                    {

                        if (!string.IsNullOrEmpty(Request.Form["FilePathTR"].ToString()))
                        {
                            _bannerlar.img_path = Request.Form["FilePathTR"].ToString();
                        }
                        //_bannerlar.meta_description = txtDescriptionTR.Text.Trim();
                        //_bannerlar.page_url = txtPageURLTR.Text.Trim();

                        if (db.SaveChanges() > 0)
                        {
                            ResizeImage(_bannerlar.img_path, 1903, 381);
                            Page.Response.Redirect(Page.Request.Url.ToString(), false);
                        }

                    }
                }


            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }


    }
}