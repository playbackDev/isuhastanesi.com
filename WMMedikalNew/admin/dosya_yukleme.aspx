﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="dosya_yukleme.aspx.cs" Inherits="WMMedikalNew.admin.WebForm27" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        a.cke_button {
            display: inline-block;
            height: 200px;
            padding: 6px 8px;
            outline: 0;
            cursor: default;
            float: left;
            border: 0;
            width: 1200px;
        }

        #cke_1_bottom {
            display: none;
        }

        .cke_button__image_icon {
            display: none;
        }

        #cke_13 {
            display: none;
        }

        .baslik {
            font-size: 32px;
            margin: 0 auto;
            text-align: center;
        }
    </style>
    <script>
        setTimeout(function () { $('#cke_12').html("<h1 class='baslik'>Resim Yüklemek İçin Tıklayın </h1>") }, 1500);

    </script>
    <div class="content-wrapper" style="min-height: 916px;">

        <section class="content">
            <div class="row">

                <CKEditor:CKEditorControl ID="trDescription" Width="100%" Height="0" runat="server"></CKEditor:CKEditorControl>

            </div>
        </section>
    </div>

</asp:Content>
