﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="hastane_detay.aspx.cs" Inherits="WMMedikalNew.admin.WebForm11" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content">
            <div class="row">

                <div class="col-md-9">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                <asp:HiddenField ID="hdnID" runat="server" />
                                <asp:Literal ID="ltrHastaneTitle" runat="server"></asp:Literal>
                                (<asp:Literal ID="ltrLang" runat="server"></asp:Literal>)</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Hastane Adı</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtHospitalName" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1"  class="col-sm-2 control-label">Durum (Aktif/Pasif)</label>
                                        <div class="col-sm-10">
                                            <asp:CheckBox ID="ckIsActive" runat="server" class="form-control" />
                                        </div>
                                    </div>


                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdate" runat="server" Text="Güncelle" class="btn btn-info pull-right"  OnClick="btnUpdate_Click"/>
                                        <br />
                                    <asp:Label ID="lblUyari2" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

              
            </div>
        </section>
    </div>
</asp:Content>
