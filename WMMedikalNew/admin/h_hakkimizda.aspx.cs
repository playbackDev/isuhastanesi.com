﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm16 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["page_id"] != "" && Request.QueryString["page_id"] != null)
            {
                if (!IsPostBack)
                {
                    int PageID = Request.QueryString["page_id"].toint();
                    var _PageDetailTR = db.page_detail_lang.Select(s => new
                    {
                        s.page_id,
                        s.lang,
                        PageName = s.pages.page_name,
                        s.pages.our_hospital.hospital_name,
                        s.pages.page_langs.FirstOrDefault().page_name,
                        s.video_url,
                        s.description
                    }).Where(q => q.page_id == PageID && q.lang == "TR").FirstOrDefault();

                    hdnPageID.Value = _PageDetailTR.page_id.ToString();
                    ltrHospitalName.Text = _PageDetailTR.hospital_name;
                    ltrHospitalName2.Text = _PageDetailTR.hospital_name;
                    ltrPageName.Text = _PageDetailTR.PageName;
                    ltrPageName2.Text = _PageDetailTR.PageName;

                    txtVideoURLTR.Text = _PageDetailTR.video_url;
                    trHakkimizda.InnerText = _PageDetailTR.description;

                    var _PageDetailEN = db.page_detail_lang.Select(s => new
                    {
                        s.page_id,
                        s.lang,
                        PageName = s.pages.page_name,
                        s.pages.our_hospital.hospital_name,
                        s.pages.page_langs.FirstOrDefault().page_name,
                        s.video_url,
                        s.description
                    }).Where(q => q.page_id == PageID && q.lang == "EN").FirstOrDefault();

                    txtVideoURLEN.Text = _PageDetailEN.video_url;
                    enHakkimizda.InnerText = _PageDetailEN.description;
                }
            }
        }

        protected void btnUpdateTR_Click(object sender, EventArgs e)
        {
            
            int PageID = hdnPageID.Value.toint();
            var _UpdatePageTR = db.page_detail_lang.Where(q => q.page_id == PageID && q.lang == "TR").FirstOrDefault();
            _UpdatePageTR.video_url = txtVideoURLTR.Text.Trim();
            _UpdatePageTR.description = trHakkimizda.InnerText;

            if (db.SaveChanges() > 0)
                Page.Response.Redirect(Page.Request.Url.ToString(), false);
        }

        protected void btnUpdateEN_Click(object sender, EventArgs e)
        {
            int PageID = hdnPageID.Value.toint();
            var _UpdatePageEN = db.page_detail_lang.Where(q => q.page_id == PageID && q.lang == "EN").FirstOrDefault();
            _UpdatePageEN.video_url = txtVideoURLEN.Text.Trim();
            _UpdatePageEN.description = enHakkimizda.InnerText;

            if (db.SaveChanges() > 0)
                Page.Response.Redirect(Page.Request.Url.ToString(), false);
        }
    }
}