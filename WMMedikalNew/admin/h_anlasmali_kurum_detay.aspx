﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="h_anlasmali_kurum_detay.aspx.cs" Inherits="WMMedikalNew.admin.WebForm20" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="min-height: 916px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Kurum Tipleri</h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-3">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Anlaşmalı Kurum Ekle</h3>
                        </div>
                        <div class="box-body">

                            <%-- <div class="form-group">
                                <label for="exampleInputEmail1">Meta Description</label>
                                <asp:TextBox ID="txtMetaDescriptionTR" runat="server" class="form-control" placeholder="Kurum Adı"></asp:TextBox>
                            </div>--%>


                            <%--   <div class="form-group">
                                <label for="exampleInputEmail1">Meta Description (EN)</label>
                                <asp:TextBox ID="txtMetaDescriptionEN" runat="server" class="form-control" placeholder="Kurum Adı (TR)"></asp:TextBox>
                            </div>--%>


                            <div class="form-group" style="display: none;">
                                <label for="exampleInputEmail1">Kurum Tipi Seçiniz</label>
                                <asp:DropDownList ID="drpHospital" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="drpHospital_SelectedIndexChanged"></asp:DropDownList>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Kurum Tipi Seçiniz</label>
                                <asp:DropDownList ID="drpKurumlar" runat="server" class="form-control"></asp:DropDownList>
                            </div>


                            <div class="form-group">
                                <label for="exampleInputEmail1">Kurum Adı</label>
                                <asp:TextBox ID="txtKurumAdiTR" runat="server" class="form-control" placeholder="Kurum Adı"></asp:TextBox>
                            </div>

                            <%--    <div class="form-group">
                                <label for="exampleInputEmail1">Kurum Adı (EN)</label>
                                <asp:TextBox ID="txtKurumAdiEN" runat="server" class="form-control" placeholder="Kurum Adı (EN)"></asp:TextBox>
                            </div>--%>
                        </div>
                        <div class="box-footer">

                            <asp:Button ID="btnHizmetEkle" runat="server" Text="Güncelle" class="btn btn-primary" OnClick="btnHizmetEkle_Click" /><br />
                            <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>

                        </div>
                    </div>

                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>


</asp:Content>
