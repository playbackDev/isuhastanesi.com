﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.admin
{
    public partial class WebForm8 : cBase
    {
        int SiteID;
        string Lang;
        protected void Page_Load(object sender, EventArgs e)
        {
            SiteID = Request.QueryString["site_id"].toint();
            if (!IsPostBack)
            {
                if (Request.QueryString["site_id"] != null)
                {
                  
                    Lang = Request.QueryString["Lang"].ToString();
                    var _SiteDetail = db.micro_sites_lang.Select( s => new { 
                        s.id,
                        s.micro_site_id,
                        s.micro_sites.is_active,
                        s.micro_sites.site_image,
                        s.lang,
                        s.site_name,
                        s.micro_sites.site_url
                        
                        }).Where(q => q.micro_site_id == SiteID && q.lang == Lang).FirstOrDefault();

                    hdnID.Value = _SiteDetail.id.ToString();
                    ltrSiteTitle.Text = _SiteDetail.site_name;
                    ltrLang.Text = _SiteDetail.lang;
                    ckIsActive.Checked = _SiteDetail.is_active;
                    txtSiteName.Text = _SiteDetail.site_name;
                    txtSiteURL.Text = _SiteDetail.site_url;
   

                    ltrImage.Text = "<img src=" + _SiteDetail.site_image + " style='max-width:350px'>";
                }
                else
                    Response.Redirect("/",false);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (txtSiteName.Text.Trim() != "" && txtSiteURL.Text.Trim() != "")
            {
                short ID = hdnID.Value.toshort();
                var _Update = db.micro_sites.Where(q => q.id == SiteID).FirstOrDefault();
                _Update.is_active = ckIsActive.Checked;
                _Update.site_url = txtSiteURL.Text;
                _Update.site_name = txtSiteName.Text;

                var _UpdateLang = db.micro_sites_lang.Where(q => q.id == ID).FirstOrDefault();
                _UpdateLang.site_name = txtSiteName.Text;

                if (db.SaveChanges() > 0)
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);

            }
            else
                lblUyari2.Text = "Boş Alan Bırakmayın";

        }

        protected void btnUpdateGuncelle_Click(object sender, EventArgs e)
        {
            string ımageURL = string.Empty;
            ımageURL = Request.Form["FilePath"].ToString();
            if (ımageURL != null && ımageURL != "")
            {
             
                //int uzunluk = FileUpload1.FileName.Split('.').Length;
                //string random = CreateRandomValue(10, true, true, true, false) + "." + FileUpload1.FileName.Split('.')[uzunluk - 1];
                //FileUpload1.SaveAs(Server.MapPath("~/uploads/sites/" + random));

                var _UpdateImage = db.micro_sites.Where(q => q.id == SiteID).FirstOrDefault();
                _UpdateImage.site_image = ımageURL;


                if (db.SaveChanges() > 0)
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                else
                    lblUyari.Text = "Bir hata oluştu tekrar deneyin";

            }
            else
                lblUyari.Text = "Resim Seçmediniz";
        }
    }
}