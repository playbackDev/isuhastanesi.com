﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class WebForm11 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
              
                var PageDetail = db.Hastane_Sayfa_Detay_Dil.Select(s => new { 
                    s.sayfa_id,
                    s.Hastane_Sayfalar.Hastane_Sayfalar_Dil.FirstOrDefault().dil,
                    s.description,
                    s.Hastane_Sayfalar.hastane_id,
                    s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == "TR").top_banner,
                    s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == "TR").meta_description,
                 
                }).FirstOrDefault(q => q.sayfa_id ==7);

                if(PageDetail != null)
                {
                    ltrDescription.Text = PageDetail.description;

                  
                    if (PageDetail.top_banner != null && PageDetail.top_banner != "")
                    {
                        topbanner.Style.Add("background-image","../" + PageDetail.top_banner);
                        topbanner.Style.Add("background-position-y", "-31px");
                    }
                    else
                        topbanner.Style.Add("height", "0");

                   
                }
             
        }
    }
}