﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class gizlilik : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var SayfaDetayTR = db.static_pages.Select(s => new
            {
                s.id,
                s.title,
                s.text,
                s.img_path,
                s.type,
            }).Where(q => q.type == 2).FirstOrDefault();

            if (SayfaDetayTR != null)
            {
                ltrDescription.Text = SayfaDetayTR.text;
                //ltrPageName.Text = SayfaDetayTR.SayfaAdı;

            }
            else
            {
                ltrDescription.Text = "İçerik Eklenmemiş";
                //ltrPageName.Text = "";
            }

            if (SayfaDetayTR.img_path != null && SayfaDetayTR.img_path != "")
            {
                topbanner.Style.Add("background-image", "../" + SayfaDetayTR.img_path);
                topbanner.Style.Add("background-position-y", "-31px");
            }
            else
                topbanner.Style.Add("height", "0");

        }
    }
}