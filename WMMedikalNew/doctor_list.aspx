﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="doctor_list.aspx.cs" Inherits="WMMedikalNew.WebForm5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <style>
        .customselect1 {
            font-size: 13px;
            color: #364f60;
            border: solid 1px #e1e1e1;
            background: #fff;
            padding: 10px 10px 6px;
            border-radius: 5px;
        }

        .filter {
            padding: 10px;
            text-align: center;
        }

            .filter span {
                font-size: 14px;
                font-weight: bold;
            }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="section-line">
        <div class="breadcrumb2">
            <a href="/">Anasayfa | </a><a href="/doktorlarimiz">Doktorlarımız</a>
        </div>
    </div>
    <div class="container content">
        <div class="filter">
            <span>Tıbbi birimlere göre filtrele: </span>
            <asp:DropDownList runat="server" ID="dlFilters" CssClass="customselect1" AutoPostBack="true" OnSelectedIndexChanged="dlFilters_SelectedIndexChanged"></asp:DropDownList>
        </div>
        <div class="sub-content-wide">

            <%-- <div class="filter_list">

                <div class="filters">
                    <div class="filters-select">
                        <h4 style="font: bold">Filtreleme</h4>

                        <ul>
                            <asp:Repeater ID="rptMedicalServiceList" runat="server">
                                <ItemTemplate>
                                    <li>
                                        <asp:LinkButton Style="color: #fff" ID="lnkFiltrele" OnCommand="lnkFiltrele_Command" CommandArgument='<%#Eval ("BolumID") %>' runat="server"><%#Eval("BolumAdi") %></asp:LinkButton>

                                    </li>
                                    <br />
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
            </div>--%>
            <center><span style="color:#90032B;font-size:16px;margin-top:20px;"><asp:Literal ID="ltrUyari" runat="server"></asp:Literal></span></center>
            <ul class="doktor-list-sub">
                <asp:Repeater runat="server" ID="rptDoctorList">
                    <ItemTemplate>
                        <li>
                            <div class="photo">
                                <a href="/<%#Eval("Dil") %>/doktorlarimiz/<%#WMMedikalNew.cExtensions.ToURL(Eval("DoktorAdi").ToString())%>-<%#Eval("doktor_id") %>">
                                    <img src="<%# Eval("ResimAdi").ToString() != string.Empty ? "http://www.isuhastanesi.com/" + Eval("ResimAdi") :  "/admin/resimyok.png" %> " />
                                </a>
                            </div>
                            <div class="doktor-info">
                                <a href="/<%#Eval("Dil") %>/doktorlarimiz/<%#WMMedikalNew.cExtensions.ToURL(Eval("DoktorAdi").ToString())%>-<%#Eval("doktor_id") %>">
                                    <h5><%#Eval("Unvan") %>   <%#Eval("DoktorAdi") %></h5>
                                </a>
                                <hr />
                                <h6>
                                    <%#Eval("BolumAdi") %>
                                    <asp:Repeater runat="server" ID="rptEkBolumler" DataSource='<%# Eval("ekbolumler") %>'>
                                        <ItemTemplate>
                                            <br />
                                            <%# Eval("BolumAdi") %>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </h6>
                            </div>
                            <div class="uzmanlik"></div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
    </div>
</asp:Content>
