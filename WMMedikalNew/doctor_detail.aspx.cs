﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class WebForm6 : cBase
    {
        int DoctorID;
        protected void Page_Load(object sender, EventArgs e)
        {

            var _HakkimizdDetay = db.Hastane_Sayfalar_Dil.Select(s => new
            {
                s.sayfa_adi,
                s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == "TR").description,
                s.sayfa_id,
                s.Hastane_Sayfalar.Hastaneler.HastaneAdi,
                hastane_id = s.Hastane_Sayfalar.Hastaneler.id,
                s.dil,
                s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == "TR").top_banner,
                s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == "TR").meta_description,
                GalleryImage = s.Hastane_Sayfalar.gallery.Select(s2 => new
                {
                    ImageID = s2.id,
                    s2.image_name,
                    active_status = (s2.is_active) ? "Aktif" : "Pasif",
                    s2.Hastaneler.HastaneAdi
                })
            }).FirstOrDefault(q => q.sayfa_id == 3);

            //if (_HakkimizdDetay != null)
            //{
            //    //pagesubmenu.HospitalID = _HakkimizdDetay.hastane_id.toint();

            //    if (_HakkimizdDetay.top_banner != null && _HakkimizdDetay.top_banner != "")
            //    {
            //        topbanner.Style.Add("background-image", _HakkimizdDetay.top_banner);
            //        topbanner.Style.Add("background-position-y", "-31px");
            //    }
            //    else
            //        topbanner.Style.Add("height", "0");



            //}

            if (Request.QueryString["id"].ToString() != null && Request.QueryString["id"].ToString() != "")
            {
                DoctorID = Request.QueryString["id"].toint();


                var DoctorDetail = db.Doktorlar_Dil.Select(s => new
                {
                    s.id,
                    s.Doktorlar.ResimAdi,
                    s.Doktorlar.DoktorAdi,
                    s.Doktorlar.Unvan,
                    s.Doktorlar.HastaneID,
                    s.Doktorlar.TibbiBolumID,
                    s.doktor_id,
                    s.dil,
                    s.description,
                    s.expertise,
                    s.publications,
                    s.education,
                    s.membership,
                    s.experience,
                    s.email,
                    s.awards,
                    s.certificates,
                    s.Doktorlar.Hastaneler.HastaneAdi,
                    s.Doktorlar.TibbiBolumler.BolumAdi

                }).Where(q => q.doktor_id == DoctorID).FirstOrDefault();

                //Image1.ImageUrl = "/uploads/our_doctors/" + DoctorDetail.ResimAdi;

                if (DoctorDetail.ResimAdi != null && DoctorDetail.ResimAdi != "")
                {
                    ltrImage.Text = "<img src='http://www.isuhastanesi.com/" + DoctorDetail.ResimAdi.ToString() + "'>";
                }
                else
                    ltrImage.Text = "<img src='http://www.isuhastanesi.com/uploads/resimyok.png'>";

                ltrNameSurname.Text = DoctorDetail.Unvan + " " + DoctorDetail.DoktorAdi;
                hlDoctorBC.Text = DoctorDetail.Unvan + " " + DoctorDetail.DoktorAdi;
                hlDoctorBC.NavigateUrl = "/TR/doktorlarimiz/" + DoctorDetail.DoktorAdi.ToURL() + "-" + DoctorID;
                //ltrHospitalName.Text = "Aydın Üni";
                ltrMedical.Text = DoctorDetail.BolumAdi;

                var _ekBolumler = db.doktorlar_ek_bolumler.Where(q => q.doktorId == DoctorID).Select(s => new
                {
                    s.TibbiBolumler.BolumAdi
                })
                .ToList();

                if (_ekBolumler.Count > 0)
                {
                    foreach (var item in _ekBolumler)
                        ltrMedical.Text += " | " + item.BolumAdi;
                }

                ltrEmail.Text = DoctorDetail.email;
                if (DoctorDetail.expertise != null && DoctorDetail.expertise != "")
                {
                    ltrExpertise.Text = DoctorDetail.expertise;
                    uzmanlikalanlari.Visible = true;
                }

                if (DoctorDetail.description != null && DoctorDetail.description != "")
                {
                    ltrDescription.Text = DoctorDetail.description;

                }
                if (DoctorDetail.education != null && DoctorDetail.education != "")
                {
                    ltrEducation.Text = DoctorDetail.education;
                    egitim.Visible = true;
                }


                if (DoctorDetail.experience != null && DoctorDetail.experience != "")
                {
                    ltrExperience.Text = DoctorDetail.experience;
                    deneyim.Visible = true;
                }


                if (DoctorDetail.publications != null && DoctorDetail.publications != "")
                {
                    ltrResource.Text = DoctorDetail.publications;
                    yayinlar.Visible = true;
                }

                if (DoctorDetail.membership != null && DoctorDetail.membership != "")
                {
                    ltrMemberShip.Text = DoctorDetail.membership;
                    uyelikler.Visible = true;
                }

                if (DoctorDetail.awards != null && DoctorDetail.awards != "")
                {
                    ltrAwards.Text = DoctorDetail.awards;
                    oduller.Visible = true;
                }
                if (DoctorDetail.certificates != null && DoctorDetail.certificates != "")
                {
                    ltrCert.Text = DoctorDetail.certificates;
                    sertifikalar.Visible = true;
                }



            }
            else
                Response.Redirect("/", false);
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            Response.Redirect("/e-randevu-al");
        }
    }
}