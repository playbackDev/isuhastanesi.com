﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="hospitalSlider.ascx.cs" Inherits="WMMedikalNew.usercontrol.hospitalSlider" %>
        <section id="promo" class="home-slider">
            <div id="slider" class="sl-slider-wrapper">
                    <asp:Repeater ID="rptSlide" runat="server">
                        <ItemTemplate>
                            
                          <div class="sl-slider">
                        <div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15"  data-slice1-scale="0" data-slice2-scale="0">
                            <div class="sl-slide-inner">

                                <a href="<%#Eval("link") %>">
                                    <div class="bg-img item fadein" style='background-image: url(/uploads/slider/<%#Eval("image_url") %>)'></div>
                                </a>

                           
                            </div>
                        </div>
                    </div>
                        </ItemTemplate>
                    </asp:Repeater>

                </div>
        </section>