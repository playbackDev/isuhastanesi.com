﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.usercontrol
{
    public partial class WebUserControl1 : System.Web.UI.UserControl
    {
        public int HospitalID = 0;
        public int ParentID = 0;
        cBase cb = new cBase();
        string new_page_url = string.Empty;
        string lang;
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = cBase.Lang;

            if (HospitalID != 0)
            {
                var SubSubmenuPage = cb.db.Hastane_Sayfalar_Dil.Select(s => new
                {
                    s.Hastane_Sayfalar.hastane_id,
                    s.sayfa_id,
                    s.dil,
                    s.Hastane_Sayfalar.Hastaneler.HastaneAdi,
                    s.sayfa_adi,
                    link = s.sayfa_adi,
                    s.Hastane_Sayfalar.sayfa_tipi,
                    s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q2 => q2.lang == lang).page_url


                }).Where(q => q.hastane_id == HospitalID && q.dil == lang).ToList();

                rptSubSubMenu.DataSource = SubSubmenuPage;
                rptSubSubMenu.DataBind();

                
            }
          
        }

        protected void rptSubSubMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {

                HyperLink hypPageLink = (HyperLink)e.Item.FindControl("hypPageLink");
                int sayfa_id = DataBinder.Eval(e.Item.DataItem, "sayfa_id").toshort();
                string PageName = DataBinder.Eval(e.Item.DataItem, "sayfa_adi").ToString();
                string Link = DataBinder.Eval(e.Item.DataItem, "link").ToString();
                int SayfaTipi = DataBinder.Eval(e.Item.DataItem, "sayfa_tipi").toint();
                string hastane_adi = DataBinder.Eval(e.Item.DataItem, "HastaneAdi").ToString();
               

                switch (SayfaTipi)
                {
                    case 1:
                        new_page_url = DataBinder.Eval(e.Item.DataItem, "page_url").ToString();
                        hypPageLink.NavigateUrl = "/" + lang + "/hastane-detay/" + new_page_url + "-" + sayfa_id;
                        break;

                    case 2:
                        new_page_url = DataBinder.Eval(e.Item.DataItem, "page_url").ToString();
                        hypPageLink.NavigateUrl = "/" + lang + "/m-" + new_page_url + "-" + sayfa_id;
                        break;

                    case 3:
                        new_page_url = DataBinder.Eval(e.Item.DataItem, "page_url").ToString();
                        hypPageLink.NavigateUrl = "/" + lang + "/" + new_page_url + "-" + sayfa_id;
                        break;

                    case 4:
                        new_page_url = DataBinder.Eval(e.Item.DataItem, "page_url").ToString();
                        hypPageLink.NavigateUrl = "/" + lang + "/krm-" + new_page_url + "-" + sayfa_id;
                        break;
                    case 5:
                        new_page_url = DataBinder.Eval(e.Item.DataItem, "page_url").ToString();
                        hypPageLink.NavigateUrl = "/" + lang + "/fotograf/" + new_page_url + "-" + sayfa_id;
                        break;

                    case 6:
                        hypPageLink.NavigateUrl = "/" + lang + "/e-services";
                        break;

                    case 7:
                        new_page_url = DataBinder.Eval(e.Item.DataItem, "page_url").ToString();
                        hypPageLink.NavigateUrl = "/" + lang + "/vmkl-" + new_page_url + "-" + sayfa_id;
                        break;

                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }


        }

    }
}