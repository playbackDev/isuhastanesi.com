﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew.usercontrol
{
    public partial class hospitalSlider : System.Web.UI.UserControl
    {
        public int HospitalID = 0;
        cBase cb = new cBase();
        string Lang;
        protected void Page_Load(object sender, EventArgs e)
        {
            Lang = cBase.Lang;
            if(HospitalID>0)
            {
                var SlideImage = cb.db.slider.Where(q => q.is_active && q.lang == Lang && q.is_popup == false && q.hastane_id == HospitalID).OrderBy(o => o.display_order).ToList();

                rptSlide.BindData(SlideImage);
            }
           
        }
    }
}