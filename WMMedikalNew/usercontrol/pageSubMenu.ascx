﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="pageSubMenu.ascx.cs" Inherits="WMMedikalNew.usercontrol.WebUserControl1" %>

     <div class="sublinks">
            <div class="container">
                <ul class="clearfix">
                    <asp:Repeater ID="rptSubSubMenu" runat="server" OnItemDataBound="rptSubSubMenu_ItemDataBound">
                        <ItemTemplate>
                            <li> <asp:HyperLink ID="hypPageLink" runat="server"> <%#Eval("sayfa_adi") %> </asp:HyperLink>  </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
        </div>
