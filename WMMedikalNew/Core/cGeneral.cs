﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace WMMedikalNew
{

    public class Genel
    {
        //istinyedbEntities db = new istinyedbEntities();
        public void AddSystemLog(Exception ex) 
        {
            try
            {

                //system_logs log = new system_logs();
                //log.add_date = DateTime.Now;
                //log.message = ex.Message;
                //log.page = HttpContext.Current.Request.RawUrl;
                //log.source = ex.Source;
                //log.stack_trace = ex.StackTrace;
                //db.system_logs.Add(log);
                //db.SaveChanges();
            }
            catch
            {
            }
        }

        public void AddSystemLog(string message)
        {
            try
            {

                //system_logs log = new system_logs();
                //log.add_date = DateTime.Now;
                //log.message = message;
                //log.page = HttpContext.Current.Request.RawUrl;
                //log.source = "";
                //log.stack_trace = "";
                //db.system_logs.Add(log);
                //db.SaveChanges();
            }
            catch
            {
            }
        }

    }

    public enum LinkString
    {
        Sabit_Linkler_1 = 1,
        Sabit_Linkler_2 = 2,
        Sabit_Linkler_3 = 3
    }

    public class ToBasket
    {
        public int pd { get; set; }
        public int qt { get; set; }
    }

    public class uploadResult
    {
        public int result { get; set; }
        public string path { get; set; }
    }

 

    public class Bread 
    {
        public string page_title { get; set; }
        public string add_link { get; set; }
        public string add_link_text { get; set; }
        public List<breadcrumbs> crumb { get; set; }
    }

    public class breadcrumbs 
    {
        public string text { get; set; }
        public string link { get; set; }
        public bool is_active { get; set; }
    }

    public class Cats
    {
        public int id { get; set; }
        public bool is_main { get; set; }
    }

}