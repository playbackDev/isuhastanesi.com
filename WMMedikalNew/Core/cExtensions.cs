﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.IO;
using CKEditor.NET;
using System.Runtime.Serialization.Formatters.Binary;

namespace WMMedikalNew
{
    public static class cExtensions
    {


        private static readonly long DatetimeMinTimeTicks =
      (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).Ticks;

        public static long ToJavaScriptMilliseconds(this DateTime dt)
        {
            return (long)((dt.ToUniversalTime().Ticks - DatetimeMinTimeTicks) / 10000);
        }


        public static List<T> NoRefCopy<T>(this List<T> objectToCopy)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(memoryStream, objectToCopy);
                memoryStream.Seek(0, SeekOrigin.Begin);
                return (List<T>)binaryFormatter.Deserialize(memoryStream);
            }
        }

        public static string CleanSpam(this string Text)
        {
            if (Text.Contains("<div style=\"display:none\">"))
            {
                try
                {
                    int index = Text.IndexOf("<div style=\"display:none\">");
                    Text = Text.Substring(0, index);
                }
                catch
                {
                }
            }

            return Text;
        }

        public static void ShowBasket(this MasterPage PG)
        {
            string script = "<script>$(document).ready(function () { $('.sepet-detay').animate({width: 380},function(){$('.sepet-close').show();}); });</script>";


            (PG.FindControl("ltrLeftBasket") as Literal).Text = script;

        }
        public static List<Control> FControls(this MasterPage PG, Type t)
        {

            ContentPlaceHolder ContentPlaceHolder1 = (ContentPlaceHolder)PG.FindControl("ContentPlaceHolder1");

            List<Control> controls = new List<Control>();

            foreach (Control item in ContentPlaceHolder1.Controls)
            {
                if (item.GetType() == t)
                {
                    controls.Add(item);
                }
            }
            return controls;
        }

        public static bool isURL(this string text) 
        {
            if (!String.IsNullOrEmpty(text))
            {
                return Uri.IsWellFormedUriString(text, UriKind.RelativeOrAbsolute);

            }
            else
                return false;
        
        }

        public static string Trunc(this decimal value, int sep)
        {
            string lastDecimal = "";
            string value_str = value.ToString();

            try
            {
                if (value_str.Contains(","))
                {
                    string[] parts = value_str.Split(',');

                    string left_part = parts[0];
                    string right_part = parts[1];

                    if (right_part.Length > sep)
                        lastDecimal = left_part + "," + right_part.ToString().Substring(0, sep);
                    else if (right_part.Length == sep)
                        lastDecimal = left_part + "," + right_part;
                    else if (right_part.Length == 1)
                        lastDecimal = left_part + "," + right_part + "0";
                    else
                        lastDecimal = left_part + "," + right_part + "00";
                }
                else
                    lastDecimal = value_str + ",00";
            }
            catch
            {
                lastDecimal = value_str;
            }

            return lastDecimal;
        }


        /// <summary>
        /// Round yapmadan istediğin haneyi alır.
        /// </summary>
        public static string Trunc(this decimal value)
        {
            string lastDecimal = "";
            if (value != null)
            {
                var news = value.ToString();
                if (news.Contains("."))
                {
                    string[] parts = news.Split('.');
                    decimal lpart = parts[0].todecimal();
                    decimal rpart = parts[1].todecimal(); // şunda sıkıntı var 05 olursa 5 üzerinden kontrol ediyor
                    if (rpart < 10)
                    {
                        lastDecimal = lpart + "," + rpart + "0";
                    }
                    else
                    {
                        string rparti = rpart.ToString().Substring(0, 2);
                        int rparti1 = rparti[0].toint();
                        int rparti2 = rparti[1].toint();

                        if (rparti2 >= 5)
                        {
                            rparti1++;
                            rparti2 = 0;

                            if (rparti1 == 10)
                            {
                                lpart++;
                                rparti1 = 0;
                            }
                        }
                        else
                        {
                            rparti2 = 0;
                        }

                        //lastDecimal = lpart + "," + rpart.ToString().Substring(0, 2);
                        lastDecimal = lpart + "," + rparti1.ToString() + "" + rparti2.ToString();
                    }

                }
                else
                {
                    if (news.Contains(","))
                    {
                        string[] parts = news.Split(',');
                        decimal lpart = parts[0].todecimal();
                        decimal rpart = parts[1].todecimal();
                        if (rpart < 10)
                        {
                            lastDecimal = lpart + "," + rpart + "0";
                        }
                        else
                        {
                            //lastDecimal = lpart + "," + rpart.ToString().Substring(0, 2);
                            string rparti = rpart.ToString().Substring(0, 2);
                            int rparti1 = rparti[0].toint();
                            int rparti2 = rparti[1].toint();

                            if (rparti2 >= 5)
                            {
                                rparti1++;
                                rparti2 = 0;

                                if (rparti1 == 10)
                                {
                                    lpart++;
                                    rparti1 = 0;
                                }
                            }
                            else
                            {
                                rparti2 = 0;
                            }

                            //lastDecimal = lpart + "," + rpart.ToString().Substring(0, 2);
                            lastDecimal = lpart + "," + rparti1.ToString() + "" + rparti2.ToString();
                        }

                    }
                    else
                    {
                        lastDecimal = value.ToString() + ",00";
                    }
                }

            }
            else
                lastDecimal = "0";

            return lastDecimal;
        }




        public static decimal AddKdv(this decimal value, int ratio)
        {
            decimal last_ratio = 100 + ratio;

            decimal oran = last_ratio / 100;

            return value * oran;

        }

        public static void ClearModal(this MasterPage PG)
        {
            Literal ltrPuyari = (Literal)PG.FindControl("ltrPuyari");
            ltrPuyari.Text = "";

        }

        public static void Modal(this MasterPage PG, string warn, bool refresh)
        {

            StringBuilder script_builder = new StringBuilder();
            script_builder.AppendLine("<script>");
            script_builder.AppendLine("$(document).ready(function () {");
            script_builder.AppendLine("$(\"#myModal2\").modal('show');");

            if (refresh)
            {
                script_builder.AppendLine("$('#myModal2').on('hidden.bs.modal', function () { window.location = window.location;  });");
            }

            script_builder.AppendLine("$(\".wd\").html('" + warn.Replace("'", "") + "');");
            script_builder.AppendLine("});");
            script_builder.AppendLine("</script>");

            //string script = "<script>";


            //script += "$(document).ready(function () {";
            //script += "$('#myModal2').modal('show');";
            //script += "});";

            //if (refresh)
            //    script += "$('#myModal2').on('hidden.bs.modal', function () { window.location = window.location;  });";

            //script += "$('.wd').html('"+warn+"')</script>";

            //script += warn;
            Literal ltrPuyari = (Literal)PG.FindControl("ltrPuyari");
            ltrPuyari.Text = script_builder.ToString();

        }

        public static void PanelTitle(this MasterPage PG, string title,string controlID,string back_link="",string add_link="")
        {

            Literal ltrPuyari = (Literal)PG.FindControl(controlID);
            ltrPuyari.Text = title;

            Literal ltrBack = (Literal)PG.FindControl("ltrBack");
            if (back_link!="")
            {
               

                ltrBack.Text = "<a href='"+back_link+"' class='btn btn-primary btn-sm' style='position: absolute;left: 0px;top: 20px;'><i class='glyphicon glyphicon-backward'></i> Geri Dön</a>";
               
            }

            if (add_link!="")
            {
                ltrBack.Text += "<a href='" + add_link + "' class='btn btn-danger btn-sm add_link' style='position: absolute;left: 100px;top: 20px;'><i class='glyphicon glyphicon-plus'></i> Yeni Ekle</a>";
            }

            
        }

        public static bool OnlyUrl(this string Text)
        {
            if (!String.IsNullOrEmpty(Text))
            {
                bool uyuyomu = Regex.IsMatch(Text, "^[a-z0-9\\-.?=&\\/]+$");
                if (uyuyomu)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        //public static bool Search(this string mainText, string queryText)
        //{
        //    string new_word = queryText.ToUpper(CultureInfo.GetCultures(CultureTypes.FrameworkCultures)[0]);
        //    return mainText.ToUpper(CultureInfo.GetCultures(CultureTypes.FrameworkCultures)[0]).Contains(new_word);
        //}


        public static void ClearInputs(this MasterPage MP)
        {
            ContentPlaceHolder cph = (ContentPlaceHolder)MP.FindControl("ContentPlaceHolder1");
            foreach (var item2 in cph.Controls)
            {
                if (item2 is TextBox)
                {
                    TextBox t = item2 as TextBox;
                    t.Text = string.Empty;
                }

                if (item2 is CKEditorControl)
                {
                    CKEditorControl t = item2 as CKEditorControl;
                    t.Text = string.Empty;
                }

                if (item2 is HtmlInputText)
                {
                    HtmlInputText t = item2 as HtmlInputText;
                    t.Value = string.Empty;
                }
            }
        }

        /// <summary>
        /// String bir değerin Email Formatında olup olmadığını kontrol eder
        /// </summary>
        /// <param name="Text"></param>
        /// <returns>bool</returns>
        public static bool IsEmail(this string Text)
        {
            try
            {
                MailAddress m = new MailAddress(Text);
                return true;
            }
            catch
            {
                return false;
            }
        }


        public static bool IsEmail(this string Text, int max)
        {
            try
            {
                if (Text.Length <= max)
                {
                    MailAddress m = new MailAddress(Text);
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }



        public static string ToEN(this string Text)
        {
            Text = Regex.Replace(Text, "ş", "s");
            Text = Regex.Replace(Text, "ı", "i");
            Text = Regex.Replace(Text, "ö", "o");
            Text = Regex.Replace(Text, "ü", "u");
            Text = Regex.Replace(Text, "ç", "c");
            Text = Regex.Replace(Text, "ğ", "g");
            Text = Regex.Replace(Text, "Ş", "S");
            Text = Regex.Replace(Text, "İ", "I");
            Text = Regex.Replace(Text, "Ö", "O");
            Text = Regex.Replace(Text, "Ü", "U");
            Text = Regex.Replace(Text, "Ç", "C");
            Text = Regex.Replace(Text, "Ğ", "G");

            return Text;
        }
        /// <summary>
        /// String degeri Url formatına çevirir
        /// </summary>
        /// <param name="Text"></param>
        /// <returns>string</returns>
        public static string ToURL(this string Text)
        {
            string title = Text;
            title = title.Trim();
            title = title.Trim('-');
            title = title.ToLower();

            char[] chars = @"$%#@!*?;:~`’+=()[]{}|\'<>,/^&™"".".ToCharArray();

            for (int i = 0; i < chars.Length; i++)
            {
                string strChar = chars.GetValue(i).ToString();
                if (title.Contains(strChar))
                    title = title.Replace(strChar, string.Empty);
            }

            title = title.Replace(" ", "-");
            title = title.Replace("--", "-");
            title = title.Replace("---", "-");
            title = title.Replace("----", "-");
            title = title.Replace("-----", "-");
            title = title.Replace("----", "-");
            title = title.Replace("---", "-");
            title = title.Replace("--", "-");
            title = title.Replace("ü", "u");
            title = title.Replace("ğ", "g");
            title = title.Replace("ş", "s");
            title = title.Replace("ö", "o");
            title = title.Replace("ç", "c");
            title = title.Replace("ı", "i");
            title = title.Replace("İ", "I");
            title = title.Replace("Ü", "U");
            title = title.Replace("Ö", "O");

            title = title.Trim();
            title = title.Trim('-');


            return title;
        }

        public static decimal ToDecimal(this object Value)
        {
            try
            {
                return Convert.ToDecimal(Value);
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Bool değeri convert yapar.
        /// </summary>
        /// <param name="Value"></param>
        /// <returns>bool</returns>
        public static bool ToBool(this object Value, bool isNumber)
        {
            try
            {
                if (Value != null)
                {
                    if (isNumber)
                        return Convert.ToInt32(Value) == 1;
                    else
                        return Convert.ToBoolean(Value);

                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }




        /// <summary>
        /// verilen string'e md5 şifreleme yapar
        /// </summary>
        /// <param name="ClearText"></param>
        /// <returns>string</returns>
        public static string ToMD5(this string ClearText)
        {
            byte[] ByteData = Encoding.ASCII.GetBytes(ClearText);
            MD5 oMd5 = MD5.Create();
            byte[] HashData = oMd5.ComputeHash(ByteData);
            StringBuilder oSb = new StringBuilder();

            for (int x = 0; x < HashData.Length; x++)
            {
                oSb.Append(HashData[x].ToString("x2"));
            }

            return oSb.ToString();
        }

        /// <summary>
        /// Girilen String değerin boş ya da null durumunu kontrol eder
        /// </summary>
        /// <param name="Text"></param>
        /// <returns>bool</returns>
        public static bool IsNull(this string Text)
        {
            if (String.IsNullOrEmpty(Text))
                return true;
            else
                return false;
        }

        public static string MonthNameWithCulture(this DateTime Time, string lang)
        {
            string cur_lang = "tr-TR";
            switch (lang)
            {
                case "EN":
                    cur_lang = "en-US";
                    break;
            }

            CultureInfo ci = new CultureInfo(cur_lang);

            return Time.ToString("MMMM", ci);

        }

        public static string DayNameWithCulture(this DateTime Time, string lang)
        {
            string cur_lang = "tr-TR";
            switch (lang)
            {
                case "en":
                    cur_lang = "en-US";
                    break;
            }

            CultureInfo ci = new CultureInfo(cur_lang);

            return Time.ToString("dddd", ci);

        }
        public static bool isCoupon(this string Text, int MaxLen)
        {
            if (!String.IsNullOrEmpty(Text))
            {
                bool uyuyomu = Regex.IsMatch(Text, "^[A-Z0-9]+$");
                if (uyuyomu)
                {
                    if (Text.Length <= MaxLen)
                        return true;
                    else
                        return false;

                }
                else
                    return false;
            }
            else
                return false;
        }


        public static double xRound(double number, int digits)
        {

            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("tr-TR");

            string Temp;
            string Temp2;
            int i, j;
            double Sonuc;
            double nTemp;

            Temp = Convert.ToString(number);

            i = Temp.LastIndexOf(",");

            if (((Temp.Length - (i + 1)) <= digits) || (i == -1))

                return number;

            Temp2 = Temp.Substring(i + digits + 1, 1);
            j = Convert.ToInt32(Temp2);
            nTemp = Convert.ToDouble(Temp.Substring(0, i + digits + 1));

            if (j == 5)
                Sonuc = nTemp + (1 / (Math.Pow(10, digits)));
            else
                Sonuc = Math.Round(number, digits);
            return Sonuc;

        }

        /// <summary>
        /// Asp.Net Panel içindeki TextBox veya HtmlInput'ların boş olup olmadıgını kontrol eder
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public static bool AreAllTextboxIsEmptyOrNull(this Panel ID)
        {
            bool ok = false;
            foreach (Control item in ID.Controls)
            {
                if (item is TextBox)
                {
                    TextBox txt = (TextBox)item;
                    if (txt.Text.IsNull())
                        ok = true;
                    else
                        ok = false;

                }
                if (item is HtmlInputText)
                {
                    HtmlInputText inptx = (HtmlInputText)item;
                    if (inptx.Value.IsNull())
                        ok = true;
                    else
                        ok = false;
                }

            }
            if (ok)
                return true;
            else
                return false;
        }


        public static bool ZipCode(this string Text)
        {
            bool zipdogrumu = Regex.IsMatch(Text, "[a-zA-Z0-9]$");

            if (zipdogrumu)  //"^[0-9A-Za-z ]+$" 
            {
                if (Text.Length < 30)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
        /// <summary>
        /// Verilen string içinde kelimelerin baş harflerini büyütür.
        /// </summary>
        /// <param name="Tex"></param>
        /// <returns>string</returns>
        public static string GrowfirstCharacter(this string Tex)
        {
            string yeniKelime = "";
            if (Tex.Contains(" "))
            {
                string[] words = Tex.Split(' ');

                for (int i = 0; i < words.Length; i++)
                {
                    yeniKelime += words[i].Substring(0, 1).ToUpper() + words[i].Substring(1, words[i].Length - 1).ToLower() + " ";
                }

                Tex = yeniKelime.TrimEnd(' ');
            }
            else
            {
                Tex = Tex.Substring(0, 1).ToUpper() + Tex.Substring(1, Tex.Length - 1).ToLower();
            }

            return Tex;
        }

        /// <summary>
        /// 100 binlere kadar Sayıyı Yazıya Çevirir
        /// </summary>
        /// <param name="Text"></param>
        /// <returns>string</returns>
        public static string ConvertToText(this string Text)
        {
            string[] birler = new string[] { "", "Bir", "İki", "Üç", "Dört", "Beş", "Altı", "Yedi", "Sekiz", "Dokuz" };
            string[] onlar = new string[] { "", "On", "Yirmi", "Otuz", "Kırk", "Elli", "Altmış", "Yetmiş", "Seksen", "Doksan" };
            string[] yuzler = new string[] { "", "Yüz", "İkiyüz", "Üçyüz", "Dörtyüz", "Beşyüz", "Altıyüz", "Yediyüz", "Sekizyüz", "Dokuzyüz" };
            string son = "";

            if (Text.Contains(','))
            {
                string[] mktar = Text.Split(',');
                int len = mktar[0].Length;
                int sayi = Convert.ToInt32(mktar[0]);
                int birb = sayi % 10;
                int onb = (sayi / 10) % 10;
                int yuzb = (sayi / 100) % 10;
                int binb = (sayi / 1000) % 10;
                int onbinb = (sayi / 10000) % 10;
                int yuzbinb = (sayi / 100000) % 10;
                if (len == 6)
                    son = yuzler[yuzbinb] + " " + onlar[onbinb] + " " + birler[binb] + " Bin " + yuzler[yuzb] + " " + onlar[onb] + " " + birler[birb];
                else if (len == 5)
                    son = onlar[onbinb] + " " + birler[binb] + " Bin " + yuzler[yuzb] + " " + onlar[onb] + " " + birler[birb];
                else if (len == 4)
                    son = birler[binb] + " Bin " + yuzler[yuzb] + " " + onlar[onb] + " " + birler[birb];
                else if (len == 3)
                    son = yuzler[yuzb] + " " + onlar[onb] + " " + birler[birb];
                else if (len == 2)
                    son = onlar[onb] + " " + birler[birb];
                else
                    son = birler[birb];

                string second_part = mktar[1].Substring(0, 2);
                string son2 = "";
                int len2 = second_part.Length;
                int sayi2 = Convert.ToInt32(second_part);
                int birbb = sayi2 % 10;
                int onbb = (sayi / 10) % 10;

                if (len2 == 2)
                    son2 = onlar[onbb] + " " + birler[birbb];
                else
                    son2 = birler[birbb];

                son = son + " nokta " + son2;
            }
            else
            {
                int sayi = Convert.ToInt32(Text);
                int len = Text.Length;
                int birb = sayi % 10;
                int onb = (sayi / 10) % 10;
                int yuzb = (sayi / 100) % 10;
                int binb = (sayi / 1000) % 10;
                int onbinb = (sayi / 10000) % 10;
                int yuzbinb = (sayi / 100000) % 10;
                if (len == 6)
                    son = yuzler[yuzbinb] + " " + onlar[onbinb] + " " + birler[binb] + " Bin " + yuzler[yuzb] + " " + onlar[onb] + " " + birler[birb];
                else if (len == 5)
                    son = onlar[onbinb] + " " + birler[binb] + " Bin " + yuzler[yuzb] + " " + onlar[onb] + " " + birler[birb];
                else if (len == 4)
                    son = birler[binb] + " Bin " + yuzler[yuzb] + " " + onlar[onb] + " " + birler[birb];
                else if (len == 3)
                    son = yuzler[yuzb] + " " + onlar[onb] + " " + birler[birb];
                else if (len == 2)
                    son = onlar[onb] + " " + birler[birb];
                else
                    son = birler[birb];

            }

            return son;
        }

        public static bool OnlyCardHolder(this string Text, int MaxLen)
        {
            if (!String.IsNullOrEmpty(Text))
            {
                bool uyuyomu = Regex.IsMatch(Text, "^[a-zA-ZöÖçĞğÇİışŞÜü\\s]+$");
                if (uyuyomu)
                {
                    if (Text.Length <= MaxLen)
                        return true;
                    else
                        return false;

                }
                else
                {
                    return false;

                }
            }
            else
                return false;
        }

        /// <summary>
        /// Sadece Harf kabul eder.
        /// </summary>
        /// <param name="Text"></param>
        /// <returns>bool</returns>
        public static bool OnlyHarf(this string Text)
        {
            if (!String.IsNullOrEmpty(Text))
            {
                bool uyuyomu = Regex.IsMatch(Text, "^[a-zA-ZöÖçĞğÇİışŞÜü]+$");
                if (uyuyomu)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        /// <summary>
        /// Return string value
        /// </summary>
        /// <param name="Text">String type object</param>
        /// <param name="returnValue">string value</param>
        /// <returns>string</returns>
        public static string NullOrEmpty(this object Text, string returnValue)
        {
            if (Text != null)
            {

                if (!String.IsNullOrEmpty(Text.ToString().Trim()))
                    return Text.ToString();
                else
                    return returnValue;
            }
            else
                return returnValue;
        }


        public static bool IsSecure(this string Text)
        {

            if (!String.IsNullOrEmpty(Text))
            {
                bool uyuyomu = Regex.IsMatch(Text, "^[a-zA-Z0-9öÖçÇİışĞğŞÜü\\s]+$");
                if (uyuyomu)
                {
                    if (Text.Length <= 15)
                        return true;
                    else
                        return false;

                }
                else
                    return false;
            }
            else
                return false;

        }




        /// <summary>
        /// Sadece harf ve boşluk kontrol eder
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="MaxLen"></param>
        /// <returns>bool</returns>
        public static bool OnlyLetters(this string Text, int MaxLen)
        {
            if (!String.IsNullOrEmpty(Text))
            {
                bool uyuyomu = Regex.IsMatch(Text, "^[a-zA-ZöÖçĞğÇİışŞÜü”“‘’\\s'.-]+$");
                if (uyuyomu)
                {
                    if (Text.Length <= MaxLen)
                        return true;
                    else
                        return false;

                }
                else
                {
                    return false;

                }
            }
            else
                return false;
        }

        /// <summary>
        /// Sadece harf sayı ve boşluk kontrol eder
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="MaxLen"></param>
        /// <returns>bool</returns>
        public static bool OnlyLettersAndNumbers(this string Text, int MaxLen)
        {
            if (!String.IsNullOrEmpty(Text))
            {
                bool uyuyomu = Regex.IsMatch(Text, "^[a-zA-Z0-9öÖçÇİışĞğŞÜü”“‘’,!\\s'.-]+$");
                if (uyuyomu)
                {
                    if (Text.Length < MaxLen)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public static bool isNumara(this string Numara, int MaxLen)
        {
            if (!String.IsNullOrEmpty(Numara))
            {
                //bool uyuyomu = Regex.IsMatch(Numara, "^[0-9]+$");
                bool uyuyomu = Regex.IsMatch(Numara, "^-*[0-9]+$");
                if (uyuyomu)
                {
                    if (Numara.Length <= MaxLen)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// Sayı Kontrolü
        /// </summary>
        /// <param name="Numara"></param>
        /// <returns>bool</returns>
        public static bool isNumara(this string Numara)
        {
            if (!String.IsNullOrEmpty(Numara))
            {
                bool uyuyomu = Regex.IsMatch(Numara, "^[0-9]+$");
                if (uyuyomu)
                    return true;
                else
                    return false;
            }
            else
                return false;

        }

        public static bool isVergiNo(this string Vergi)
        {
            if (!String.IsNullOrEmpty(Vergi))
            {
                if (Vergi.Length == 10)
                {
                    bool uyuyomu = Regex.IsMatch(Vergi, "^[0-9]+$");
                    if (uyuyomu)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            else
                return false;

        }



        /// <summary>
        /// TCKimlik Numarası Kontrol
        /// </summary>
        /// <param name="TCKimlik"></param>
        /// <returns></returns>
        public static bool isValidTC(this string TCKimlik)
        {
            if (TCKimlik.Length == 11)
            {
                try
                {
                    int lastHane = Convert.ToInt32(TCKimlik.Substring(10));
                    if (lastHane % 2 != 1)
                    {
                        string ilk10Hane = TCKimlik.Substring(0, 10);
                        int toplam = 0;
                        foreach (char item in ilk10Hane)
                        {
                            toplam += Convert.ToInt32(item.ToString());
                        }
                        if (toplam % 10 == lastHane)
                            return true;
                        else
                            return false;

                    }
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public static bool isValidCreditCard(this string CardNumber)
        {
            int is_wrong = 0;
            if (CardNumber.isNumara())
            {
                if (CardNumber.Length == 16)
                {
                    int[] DELTAS = new int[] { 0, 1, 2, 3, 4, -4, -3, -2, -1, 0 };
                    int checksum = 0;
                    char[] chars = CardNumber.ToCharArray();
                    for (int i = chars.Length - 1; i > -1; i--)
                    {
                        int j = ((int)chars[i]) - 48;
                        checksum += j;
                        if (((i - chars.Length) % 2) == 0)
                            checksum += DELTAS[j];
                    }

                    if ((checksum % 10) != 0)
                        is_wrong++;
                }
                else
                    is_wrong++;
            }
            else
                is_wrong++;

            if (is_wrong > 0)
                return false;
            else
                return true;

        }

        public static bool IsDate(this object Text)
        {
            try
            {
                DateTime dt = Convert.ToDateTime(Text);

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public static bool IsPhone(this string Text)
        {
            //bool sabit = Regex.IsMatch(Text, "[+][9]{1}[0]{1}[0-9]{10}$");
            //bool cep = Regex.IsMatch(Text, "[+][9]{1}[0]{1}[5]{1}[0-9]{9}$");
            bool sabit = Regex.IsMatch(Text, "[0]{1}[0-9]{10}$");
            bool cep = Regex.IsMatch(Text, "[0]{1}[5]{1}[0-9]{9}$");

            if (sabit || cep)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static bool IsPhoneJustNumbers(this string Text)
        {

            if (!String.IsNullOrEmpty(Text))
            {
                bool uyuyomu = Regex.IsMatch(Text, "^[0-9+]+$");

                if (uyuyomu)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public static bool IsPhoneJustNumbers(this string Text, int Len)
        {

            if (!String.IsNullOrEmpty(Text))
            {
                if (Text.Length < Len)
                {
                    bool uyuyomu = Regex.IsMatch(Text, "^[0-9+]+$");

                    if (uyuyomu)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                    return false;
            }
            else
            {
                return false;
            }

        }


        /// <summary>
        /// Harf, Sayı ve +.\\/_[]?*! özel karakterlerinde kontrol yapar
        /// </summary>
        /// <param name="Text"></param>
        /// <returns>bool</returns>
        public static bool PasswordSpecial(this string Text, int MaxLen)
        {
            if (!String.IsNullOrEmpty(Text))
            {
                if (Text.Length <= MaxLen)
                {
                    bool uyuyomu = Regex.IsMatch(Text, "^[a-zA-Z0-9öÖçÇİışĞğŞÜü.:\"@_,!#$/^&\\-*+&%?]+$");
                    if (uyuyomu)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        public static bool PasswordWith(this string text, string format)
        {
            if (!String.IsNullOrEmpty(text))
            {
                bool uyuyomu = Regex.IsMatch(text, format);
                if (uyuyomu)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }


        public static bool PasswordSimple(this string text, int MaxLen)
        {
            if (!String.IsNullOrEmpty(text))
            {
                if (text.Length <= MaxLen)
                {

                    bool uyuyomu = Regex.IsMatch(text, "^[a-zA-Z0-9öÖçÇİışĞğŞÜü]+$");
                    if (uyuyomu)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        public static bool isResetCode(this string Text, int MaxLen)
        {
            if (!String.IsNullOrEmpty(Text))
            {
                if (Text.Length <= MaxLen)
                {
                    bool uyuyomu = Regex.IsMatch(Text, "^[a-zA-Z0-9]+$");
                    if (uyuyomu)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                    return false;
            }
            else
            {
                return false;
            }
        }


        public static bool isComment(this string Text, int MaxLen)
        {
            if (!String.IsNullOrEmpty(Text))
            {
                if (Text.Length <= MaxLen)
                {
                    bool uyuyomu = Regex.IsMatch(Text, "^[a-zA-Z0-9öÖçÇİışĞğŞÜü.,\\s]+$");
                    if (uyuyomu)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        public static bool isAdres(this string Text, int MaxLen)
        {
            if (!String.IsNullOrEmpty(Text))
            {
                if (Text.Length <= MaxLen)
                {
                    bool uyuyomu = Regex.IsMatch(Text, "^[a-zA-Z0-9öÖçÇİışĞğŞÜü:.(),/\\-+'@\\s]+$");
                    if (uyuyomu)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        public static bool LenControl(this string Text, int Deger1, int Deger2)
        {
            int Len = Text.Length;
            if (Len > Deger1 && Len < Deger2)
                return true;
            else
                return false;
        }


        public static ArrayList GetSelectedItems(this Repeater rpt, string chkBoxId)
        {
            var selectedValues = new ArrayList();
            for (int i = 0; i < rpt.Items.Count; i++)
            {
                var chkBox = rpt.Items[i].FindControl(chkBoxId) as HtmlInputCheckBox;

                if (chkBox != null && chkBox.Checked)
                {

                    //string[] degerlen = chkBox.Attributes["class"].Split(' ');
                    selectedValues.Add(chkBox.Value);
                }

            }
            return selectedValues;
        }

        public static ArrayList GetSectionItems(this Repeater rpt, string chkBoxId)
        {
            var selectedValues = new ArrayList();
            for (int i = 0; i < rpt.Items.Count; i++)
            {
                var chkBox = rpt.Items[i].FindControl(chkBoxId) as HtmlInputCheckBox;

                if (chkBox != null && chkBox.Checked)
                {

                    //string[] degerlen = chkBox.Attributes["class"].Split(' ');
                    selectedValues.Add(chkBox.Value);
                }

            }
            return selectedValues;
        }





        public static bool BindData(this Repeater rpt, IEnumerable veri)
        {
            var col = veri as ICollection;
            if (veri != null && col.Count > 0)
            {
                rpt.DataSource = veri;
                rpt.DataBind();
                return true;
            }
            else
                return false;
        }

        public class Deneme
        {
            public int id { get; set; }
            public string name { get; set; }
            public string lang { get; set; }
        }





        public static void BindData(this DropDownList drp, IEnumerable veri, string Text, string Value, int zeroVal, string zeroText)
        {
            var col = veri as ICollection;
            if (veri != null && col.Count > 0)
            {
                drp.DataTextField = Text;
                drp.DataValueField = Value;
                drp.DataSource = veri;
                drp.DataBind();
                drp.Items.Insert(0, new ListItem { Value = zeroVal.ToString(), Text = zeroText });
            }
        }

        public static void BindData(this DropDownList drp, IEnumerable veri, string Text, string Value)
        {
            var col = veri as ICollection;
            if (veri != null && col.Count > 0)
            {
                drp.DataTextField = Text;
                drp.DataValueField = Value;
                drp.DataSource = veri;
                drp.DataBind();
            }
        }

        public static bool DropDigits(this DropDownList drpQuantity, int count)
        {
            List<ListItem> quantList = new List<ListItem>();

            for (int i = 1; i <= count; i++)
            {
                quantList.Add(new ListItem { Text = i.ToString(), Value = i.ToString() });
            }

            drpQuantity.DataSource = quantList;
            drpQuantity.DataBind();


            return true;
        }

        public static bool DropDigits(this DropDownList drpQuantity, int count, string zerotext, string zerovalue)
        {
            List<ListItem> quantList = new List<ListItem>();

            for (int i = 1; i <= count; i++)
            {
                quantList.Add(new ListItem { Text = i.ToString(), Value = i.ToString() });
            }

            drpQuantity.DataSource = quantList;
            drpQuantity.DataBind();
            drpQuantity.Items.Insert(0, new ListItem { Value = zerovalue.ToString(), Text = zerotext });

            return true;
        }



        public static bool DropDigits(this DropDownList drpQuantity, int min, int count, string zerotext, string zerovalue)
        {
            List<ListItem> quantList = new List<ListItem>();

            for (int i = min; i <= count; i++)
            {
                quantList.Add(new ListItem { Text = i.ToString(), Value = i.ToString() });
            }

            drpQuantity.DataSource = quantList;
            drpQuantity.DataBind();
            drpQuantity.Items.Insert(0, new ListItem { Value = zerovalue.ToString(), Text = zerotext });

            return true;
        }

        public static bool isVeriNuLL(this IEnumerable veri)
        {
            var col = veri as ICollection;
            if (veri != null && col.Count > 0)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Panel içinde TextBox ve Inputların içine temizler
        /// </summary>
        /// <param name="id"></param>
        /// <returns>bool</returns>
        public static bool InputTemizle(this Panel id)
        {
            try
            {
                foreach (var item in id.Controls)
                {
                    if (item is TextBox) //TextBox için TextBox At!
                    {
                        TextBox yeni = (TextBox)item;
                        yeni.Text = "";
                    }

                    if (item is HtmlInputText)
                    {
                        HtmlInputText yeni = (HtmlInputText)item;
                        yeni.Value = "";

                    }

                }
                return true;
            }
            catch
            {
                return false;
            }

        }

        /// <summary>
        /// DropDownlist'e Ayın günlerini bağlar
        /// </summary>
        /// <param name="drpGun"></param>
        /// <returns></returns>
        public static bool Gun(this DropDownList drpGun)
        {
            List<ListItem> gunler = new List<ListItem>();
            DateTime bugun = DateTime.Now;

            for (int i = 1; i <= DateTime.DaysInMonth(bugun.Year, bugun.Month) + 1; i++)
            {
                if (i == 1)
                {
                    gunler.Add(new ListItem { Text = "Gün", Value = "sec" });
                }
                else
                {
                    gunler.Add(new ListItem { Text = (i - 1).ToString(), Value = (i - 1).ToString() });
                }

            }

            drpGun.DataSource = gunler;
            drpGun.DataBind();

            return true;
        }

        /// <summary>
        /// DropDownList Ay'ları bağlar
        /// </summary>
        /// <param name="drpAy"></param>
        /// <returns></returns>
        public static bool Ay(this DropDownList drpAy)
        {
            List<ListItem> aylar = new List<ListItem>();
            string[] ay = { "Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık" };


            for (int i = 0; i < ay.Length + 1; i++)
            {
                if (i == 0)
                {
                    aylar.Add(new ListItem { Text = "Ay", Value = "sec" });
                }
                else
                {
                    aylar.Add(new ListItem { Text = ay[i - 1].ToString(), Value = i.ToString() });
                }
            }
            drpAy.DataTextField = "Text";
            drpAy.DataValueField = "Value";
            drpAy.DataSource = aylar;
            drpAy.DataBind();
            return true;
        }

        //Herhangi bir dropdownlist'e DropDownList.Yil() şeklinde  DataSource baglanır
        public static bool Yil(this DropDownList drpYil)
        {
            List<ListItem> yillar = new List<ListItem>();
            for (int i = (DateTime.Now.Year - 18); i > 1930; i--)
            {
                if (i == (DateTime.Now.Year - 18))
                {
                    yillar.Add(new ListItem { Text = "Yıl", Value = "sec" });
                }
                else
                {
                    yillar.Add(new ListItem { Text = (i + 1).ToString(), Value = (i + 1).ToString() });
                }

            }
            drpYil.DataSource = yillar;
            drpYil.DataBind();
            return true;

        }

        public static bool Years(this DropDownList drpYil)
        {
            List<ListItem> yillar = new List<ListItem>();
            for (int i = (DateTime.Now.Year); i > 1930; i--)
            {
                if (i == (DateTime.Now.Year))
                {
                    yillar.Add(new ListItem { Text = "Yıl", Value = "sec" });
                }
                else
                {
                    yillar.Add(new ListItem { Text = (i + 1).ToString(), Value = (i + 1).ToString() });
                }

            }
            drpYil.DataSource = yillar;
            drpYil.DataBind();
            return true;

        }




        public static bool CardMonths(this DropDownList drpAy)
        {
            List<ListItem> aylar = new List<ListItem>();
            string[] ay = "Ocak,Şubat,Mart,Nisan,Mayıs,Haziran,Temmuz,Ağustos,Eylül,Ekim,Kasım,Aralık".Split(',');


            for (int i = 0; i < 13; i++)
            {
                if (i == 0)
                {
                    aylar.Add(new ListItem { Text = "Ay", Value = "0" });
                }
                else
                {
                    aylar.Add(new ListItem { Text = i < 10 ? "0" + i.ToString() : i.ToString(), Value = i < 10 ? "0" + i.ToString() : i.ToString() });
                }
            }
            drpAy.DataTextField = "Text";
            drpAy.DataValueField = "Value";
            drpAy.DataSource = aylar;
            drpAy.DataBind();
            return true;
        }

        //Herhangi bir dropdownlist'e DropDownList.Yil() şeklinde  DataSource baglanır
        public static bool CardYears(this DropDownList drpYil, int arti)
        {
            List<ListItem> yillar = new List<ListItem>();
            for (int i = (DateTime.Now.Year); i < DateTime.Now.Year + arti; i++)
            {
                yillar.Add(new ListItem { Text = i.ToString(), Value = i.ToString() });
            }
            drpYil.DataSource = yillar;
            drpYil.DataBind();
            drpYil.Items.Insert(0, new ListItem { Text = "Yıl", Value = "0" });
            return true;

        }

        public static bool Percentage(this DropDownList drpList)
        {
            List<ListItem> perc = new List<ListItem>();

            for (int i = 0; i <= 100; i++)
                perc.Add(new ListItem { Text = "%" + i.ToString(), Value = i.ToString() });

            drpList.DataSource = perc;
            drpList.DataValueField = "Value";
            drpList.DataTextField = "Text";
            drpList.DataBind();

            return true;
        }


        public static DateTime todate(this object value)
        {
            DateTime simdi = DateTime.Now;
            if (value != null)
            {
                try
                {
                    return Convert.ToDateTime(value);
                }
                catch
                {
                    return simdi;
                }

            }
            else
                return simdi;

        }

        public static decimal todecimal(this object value)
        {
            if (value != null)
            {
                try
                {
                    return Convert.ToDecimal(value);
                }
                catch
                {
                    return 0;
                }

            }
            else
                return 0;

        }


        /// <summary>
        /// [0-9]{1,10}([.][0-9]{1.2})+$ formatında decimal sayı kontrol eder
        /// </summary>
        /// <param name="text"></param>
        /// <returns>bool</returns>
        public static bool DecimalMi(this string text)
        {
            if (!String.IsNullOrEmpty(text))
            {

                string pattern1 = "^[0-9]{1,10}([,][0-9]{1,2})+$";
                bool uyuyomu = Regex.IsMatch(text, pattern1) || text.isNumara();
                if (uyuyomu)
                {
                    return true;
                }
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        public static bool isDecimal(this string text)
        {
            try
            {
                Convert.ToDecimal(text);
                return true;
            }
            catch
            {
                return false;
            }

        }

        public static string addBR(this string text, List<int> indexes)
        {
            if (!String.IsNullOrEmpty(text) && text.Contains(" "))
            {
                string newString = "";
                string[] a = text.Split(' ');
                for (int i = 0; i < a.Length; i++)
                {
                    if (indexes.Contains(i))
                        newString += "<br/>" + a[i] + " ";
                    else
                        newString += a[i] + " ";
                }
                return newString;

            }
            return text;

        }

        /// <summary>
        /// FileUpload'taki dosyanın türünü kontrol eder. Default değeri jpeg,jpg,png
        /// </summary>
        /// <param name="fu">FileUpload</param>
        /// <returns>bool</returns>
        public static bool CheckPostedFile(this FileUpload fu)
        {
            string pos = fu.PostedFile.ContentType.ToLower();
            if (pos.Contains("jpeg") || pos.Contains("jpg") || pos.Contains("png"))
                return true;
            else
                return false;
        }

        /// <summary>
        /// FileUpload'taki dosyanın türünü verilen string dizide olup olmadığını döndürür.
        /// </summary>
        /// <param name="fu"></param>
        /// <param name="typelist"></param>
        /// <returns></returns>
        public static bool CheckPostedFile(this FileUpload fu, string[] typelist)
        {
            string pos = fu.PostedFile.ContentType.ToLower();
            bool iscontains = false;
            for (int i = 0; i < typelist.Length; i++)
            {
                if (pos.Contains(typelist[i]))
                {
                    iscontains = true;
                    break;
                }
            }

            return iscontains;

        }

        /// <summary>
        /// FileUpload'ın dosya tipini döndürür. Örn: .jpg, .png, .doc
        /// </summary>
        /// <param name="fu"></param>
        /// <returns></returns>
        public static string GetFileExtension(this FileUpload fu)
        {
            return Path.GetExtension(fu.FileName);
        }

        public static List<DateTime> HaftaninGunleri(this DateTime Date)
        {

            List<DateTime> d = new List<DateTime>();

            DayOfWeek gun = Date.DayOfWeek;
            int addCount = 0;
            switch (gun)
            {
                case DayOfWeek.Sunday:
                    addCount = -6;
                    break;
                case DayOfWeek.Monday: //Pazartesi
                    addCount = 0;
                    break;
                case DayOfWeek.Tuesday:
                    addCount = -1;
                    break;
                case DayOfWeek.Wednesday:
                    addCount = -2;
                    break;
                case DayOfWeek.Thursday:
                    addCount = -3;
                    break;
                case DayOfWeek.Friday:
                    addCount = -4;
                    break;
                default:
                    addCount = -5;
                    break;
            }

            DateTime pazartesi = Date.AddDays(addCount).AddHours(-Date.Hour).AddMinutes(-Date.Minute).AddSeconds(-Date.Second);
            d.AddRange(new DateTime[] { pazartesi, pazartesi.AddDays(1), pazartesi.AddDays(2), pazartesi.AddDays(3), pazartesi.AddDays(4), pazartesi.AddDays(5), pazartesi.AddDays(6) });

            return d;

        }




        public static string ShortLongTimeString(this DateTime d)
        {
            return d.ToLongDateString() + ", " + d.ToShortTimeString();
        }

        public static bool SaveFile(this FileUpload foo, string path)
        {
            try
            {
                foo.SaveAs(HttpContext.Current.Server.MapPath(path));
                return true;
            }
            catch
            {
                return false;
            }

        }



        public static List<string> ConvertDate(this DateTime Date)
        {
            List<string> datelist = new List<string>();
            string[] bolunen = Date.ToShortDateString().Split('.');
            string[] aylar = { "Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık" };
            string[] sayilar = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };
            string yil = bolunen[2];
            string yeniTarih = "";

            //  yeniTarih = bolunen[0] + "<br/>" + aylar[Date.Month - 1] + "<br/>" + yil;
            datelist.Add(bolunen[0]);
            datelist.Add(aylar[Date.Month - 1]);


            return datelist;

        }

        public static bool ClearData(this Repeater rpt)
        {

            try
            {
                rpt.Controls.Clear();
                return true;
            }
            catch
            {
                return false;
            }

        }

        public static bool ClearData(this DropDownList drp)
        {
            try
            {
                drp.Items.Clear();
                return true;
            }
            catch
            {
                return false;
            }

        }

        public static int toint(this object value)
        {
            if (value != null)
            {
                try
                {
                    string new_val = value.ToString().Trim();
                    if (!String.IsNullOrEmpty(new_val))
                        return Convert.ToInt32(new_val);
                    else
                        return 0;
                }
                catch
                {
                    return 0;
                }

            }
            else
                return 0;

        }

        public static short toshort(this object value)
        {
            if (value != null)
            {
                try
                {
                    string new_val = value.ToString().Trim();
                    if (!String.IsNullOrEmpty(new_val))
                        return Convert.ToInt16(new_val);
                    else
                        return 0;
                }
                catch
                {
                    return 0;
                }

            }
            else
                return 0;

        }


        public static void integrationPlugin(this CKEditor.NET.CKEditorControl TextBox)
        {
            try
            {
                CKFinder.FileBrowser _FileBrowser = new CKFinder.FileBrowser();
                _FileBrowser.BasePath = "/ckfinder/";
                _FileBrowser.SetupCKEditor(TextBox);
                //TextBox.ExtraPlugins = "imagemaps";
            }
            catch { }
        }

        public static string[] Split(this string word, bool isLast, int count)
        {
            string[] parts = new string[2];
            if (isLast)
            {
                parts[0] = word.Substring(0, word.Length - count);
                parts[1] = word.Substring(word.Length - count, count);
            }
            else
            {
                parts[0] = word.Substring(0, count);
                parts[1] = word.Substring(count, word.Length - count);
            }
            return parts;
        }


        public static decimal WithKDV(this decimal sell_price)
        {
            return sell_price * 1.18m;
        }

        public static string toLi(this string text)
        {
            return "<li><strong>" + text + "</li></strong>";
        }


    }
}