﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Linq;
using System.IO;
using System.Web;
//using System.Data.Objects;

namespace WMMedikalNew
{

    public class cMail
    {



        public static bool Contact(string adsoyad, string email,string subject, string mesaj)
        {

            string sb = "<table style='width:100%;'>";
            try
            {
                sb += "<tr>";
                sb += "<td>Ad Soyad</td><td>" + adsoyad + "</td>";
                sb += "</tr>";
                sb += "<tr>";
                sb += "<td>E-Mail</td><td>" + email + "</td>";
                sb += "</tr>";
                sb += "<tr>";
                sb += "<td>Konu</td><td>" + subject + "</td>";
                sb += "</tr>";
                sb += "<tr>";
                sb += "<td colspan='2'>Mesaj</td>";
                sb += "</tr>";
                sb += "<tr>";
                sb += "<td colspan='2'>" + mesaj + "</td>";
                sb += "</tr></table>";


                return SendEmail("İstinye Üniversitesi İletişim Paneli", sb, "eozturk2009@gmail.com", false);

            }
            catch
            {
                //Genel.AddErrorLog(1, ex);
                return false;
            }
        }

        public static bool BilgiForm(string adsoyad, string email, string telefon, string mesaj,string title,string email2)
        {

            string sb = "<table style='width:100%;'>";
            try
            {
                sb += "<tr>";
                sb += "<td>Ad Soyad</td><td>" + adsoyad + "</td>";
                sb += "</tr>";
                sb += "<tr>";
                sb += "<td>E-Mail</td><td>" + email + "</td>";
                sb += "</tr>";
                sb += "<tr>";
                sb += "<td>Telefon</td><td>" + telefon + "</td>";
                sb += "</tr>";
                sb += "<tr>";
                sb += "<td colspan='2'>Mesaj</td>";
                sb += "</tr>";
                sb += "<tr>";
                sb += "<td colspan='2'>" + mesaj + "</td>";
                sb += "</tr></table>";

                return SendEmail(title, sb, email2, false);

            }
            catch
            {
                //Genel.AddErrorLog(1, ex);
                return false;
            }
        }

        public static bool CV(string adsoyad, string email, string phone, HttpPostedFile posted)
        {

            string sb = "<table style='width:100%;'>";
            try
            {
                sb += "<tr>";
                sb += "<td>Ad Soyad</td><td>" + adsoyad + "</td>";
                sb += "</tr>";
                sb += "<tr>";
                sb += "<td>E-Mail</td><td>" + email + "</td>";
                sb += "</tr>";
                sb += "<tr>";
                sb += "<td>Telefon</td><td>" + phone + "</td>";
                sb += "</tr>";
                sb += "</table>";


                return SendEmail("Alnal Kariyer Yeni Başvuru", sb, "eozturk2009@gmail.com", false, posted);
            }
            catch
            {
                //Genel.AddErrorLog(1, ex);
                return false;
            }
        }


        public static bool ErrorMailToMe(string mesaj, string source, string stack_trace, string page, DateTime time)
        {
            string sb = "<table style='width:100%;'>";
            try
            {
                sb += "<tr><td>" + mesaj + "</td></tr>";
                sb += "<tr><td>" + source + "</td></tr>";
                sb += "<tr><td>" + stack_trace + "</td></tr>";
                sb += "<tr><td>" + page + "</td></tr>";
                sb += "<tr><td>" + time + "</td></tr>";
                sb += "</table>";

                SendEmail("İkiSepet Error!!", sb, "eozturk2009@gmail.com", false);

                return true;
            }
            catch
            {
                return false;
            }
        }

        //public static bool forgotPassword(string mail, string code)
        //{
        //    duyursanaEntities db = new duyursanaEntities();
        //    var m = db.DFirms.FirstOrDefault(f => f.email == mail);
        //    m.reset_code = code;
        //    if (db.SaveChanges() > 0)
        //    {
        //        string Mail = File.ReadAllText(HttpContext.Current.Server.MapPath("/uploads/webmail/sifremiunuttum.html"));
        //        Mail = Mail.Replace("%name%", m.name + " " + m.lastname);
        //        Mail = Mail.Replace("%baglanti%", "<a href='" + "http://www.ikisepet.com/sifre-degistir/" + code + "' target='_blank'>" + "http://www.ikisepet.com/sifre-degistir/" + code + "</a>");
        //        return SendEmail("İki Sepet Şifrenizi Sıfırlayın!", Mail, m.email, false);
        //    }
        //    else
        //        return false;

        //}


        public static bool SendEmail(string subject, string Mesaj, string mail, bool isbcc)
        {
            try
            {
                MailMessage mailMsg = new MailMessage();
                MailAddress kimden = new MailAddress("test@istinye.edu.tr", "İstinye Üniversitesi");
                MailAddress kime = new MailAddress(mail);
                mailMsg.From = kimden;
                mailMsg.Subject = subject;
                mailMsg.Body = Mesaj;
                mailMsg.IsBodyHtml = true;
                mailMsg.To.Add(kime);

                if (isbcc)
                {
                    mailMsg.Bcc.Add(new MailAddress("yavuz@playback.com.tr"));
                    mailMsg.Bcc.Add(new MailAddress("eozturk2009@gmail.com"));
                }

                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587); //
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("eozturk2009@gmail.com", "59686313416");
                smtpClient.Credentials = credentials;
                smtpClient.EnableSsl = true;

                smtpClient.Send(mailMsg);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool SendEmail(string subject, string Mesaj, string mail, bool isbcc, HttpPostedFile fileAttac)
        {
            try
            {
                MailMessage mailMsg = new MailMessage();
                MailAddress kimden = new MailAddress("info@alnal.net", "ALNAL");
                MailAddress kime = new MailAddress(mail);
                mailMsg.From = kimden;
                mailMsg.Subject = subject;
                mailMsg.Body = Mesaj;
                mailMsg.IsBodyHtml = true;
                mailMsg.To.Add(kime);


                string strFileName = Path.GetFileName(fileAttac.FileName);
                Attachment attachFile = new Attachment(fileAttac.InputStream, strFileName);
                mailMsg.Attachments.Add(attachFile);


                if (isbcc)
                {
                    mailMsg.Bcc.Add(new MailAddress("yavuz@playback.com.tr"));
                    mailMsg.Bcc.Add(new MailAddress("eozturk2009@gmail.com"));
                }

                SmtpClient smtpClient = new SmtpClient("mail.alnal.net", 587); //
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("info@alnal.net", "al_Nal*54"); //ay123456
                smtpClient.Credentials = credentials;
                //smtpClient.EnableSsl = true;

                smtpClient.Send(mailMsg);

                return true;
            }
            catch
            {
                return false;
            }
        }

        //public static bool HosGeldin(members uye)
        //{
        //    string Mail = File.ReadAllText(HttpContext.Current.Server.MapPath("/uploads/webmail/hosgeldiniz.html"));
        //    Mail = Mail.Replace("%name%", uye.name + " " + uye.lastname);
        //    //Mail = Mail.Replace("%email%", uye.email);
        //    return SendEmail("İkiSepet.Com'a Hoş Geldiniz.", Mail, uye.email, false);
        //}



    }
}