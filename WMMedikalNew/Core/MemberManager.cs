﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace WMMedikalNew.Core
{
    public class MemberManager
    {
        public static bool IsLoggedIn
        {
            get
            {
                try
                {
                    if (HttpContext.Current.Session["wm_login"] == "")
                    {
                        if (HttpContext.Current.Request.Cookies["wm_panel"] != null)
                        {
                            string cookie_content = HttpContext.Current.Request.Cookies["wm_panel"].Value;

                            if (cookie_content != "")
                            {
                                cookie_content = cEncrytion.DecryptString(cookie_content);

                                if (cookie_content.Contains("-wm-"))
                                    return PanelLogin(Regex.Split(cookie_content, "-wm-")[0], Regex.Split(cookie_content, "-wm-")[1], false);
                                else
                                    return false;
                            }
                            else
                                return false;
                        }
                        else
                            return false;
                    }
                    else
                        return Convert.ToBoolean(HttpContext.Current.Session["wm_login"]);
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool PanelLogin(string Username, string Password, bool ClearText = false, bool RememberMe = true)
        {
            if (IsLoggedIn)
                return true;
            else
            {
                if (Username != "" && Password != "")
                {
                    WMMedikalDBEntities db = new WMMedikalDBEntities();


                    var _loginControl = db.panel_users
                        .Where(q => q.username == Username
                        && q.password == Password).FirstOrDefault();

                    if (_loginControl == null)
                        return false;
                    else
                    {
                        HttpContext.Current.Session["wm_login"] = true;
                        HttpContext.Current.Session["wm_id"] = _loginControl.id;
                        HttpContext.Current.Session["wm_username"] = _loginControl.username;

                        if (RememberMe)
                        {
                            HttpContext.Current.Response.Cookies["wm_panel"].Value = cEncrytion.EncryptString(Username + "-wm-" + Password);
                            HttpContext.Current.Response.Cookies["wm_panel"].Expires = DateTime.Now.AddDays(30);
                        }
                        return true;
                    }
                }
                else
                    return false;
            }
        }
    }
}