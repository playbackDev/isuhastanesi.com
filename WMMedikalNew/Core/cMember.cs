﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace WMMedikalNew
{
    public class cMember : cBase
    {
       
        public int FirmID;
        /// <summary>
        /// If client has Logged in to account
        /// </summary>
        /// <returns>bool</returns>
        public bool IsLoggedIn()
        {
            try
            {
                if (HttpContext.Current.Session["admin_login"] == null)
                {
                    if (HttpContext.Current.Request.Cookies["admin"] != null)
                    {
                        string cookie_content = HttpContext.Current.Request.Cookies["admin"].Value;

                        if (cookie_content != "")
                        {
                            cookie_content = cEncrytion.DecryptString(cookie_content);

                            if (cookie_content.Contains("-ls-"))
                                if (Login(Regex.Split(cookie_content, "-ls-")[0], Regex.Split(cookie_content, "-ls-")[1]))
                                    return true;
                                else
                                    return false;
                            else
                                return false;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    if (Convert.ToBoolean(HttpContext.Current.Session["admin_login"]))
                        return true;
                    else
                        return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Login(string Email, string Password)
        {

            try
            {
                //Password = Password.ToMD5();
                //var login = (from c in db.users
                //             where c.username == Email && c.password == Password
                //             select new
                //             {
                //                 c.id,
                //                 c.username
                //             }).FirstOrDefault();

                //if (login == null)
                //    return false;
                //else
                //{
                //    HttpContext.Current.Session["admin_login"] = true;
                //    HttpContext.Current.Session["admin_id"] = login.id;
                //    HttpContext.Current.Session["admin_name"] = login.username;
                //    return true;
                //}
                return true;
            }
            catch
            {
                return false;
            }

        }




        private string getIpAddres()
        {
            try
            {
                if (!string.IsNullOrEmpty(HttpContext.Current.Request.ServerVariables["HTTP_CLIENT_IP"]))
                    return HttpContext.Current.Request.ServerVariables["HTTP_CLIENT_IP"];
                else
                    return HttpContext.Current.Request.UserHostAddress;
            }
            catch { return HttpContext.Current.Request.UserHostAddress; }
        }





        //public members FBUser(string facebookid)
        //{
        //    return db.members.FirstOrDefault(f => f.facebook_user_id == facebookid && f.is_banned == false);

        //}
    }
}