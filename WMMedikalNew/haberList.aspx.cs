﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class haberList : cBase
    {
        int CurrentPage = 1;
        int ItemsPerPage = 10;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["pg"] != null) CurrentPage = Request.QueryString["pg"].toint();

            var _eventList = db.news_lang
                    .Where(q => q.is_active && q.news.type == 1)
                    .OrderByDescending(o => o.id)
                  .Select(s => new
                  {
                      s.id,
                      s.news_id,
                      s.news_short,
                      s.news_title,
                      s.news.news_image,
                      s.created_time,
                      s.news.seoURL
                  }).ToList();

            if (_eventList.Count > 0)
            {
                rptEventList.DataSource = _eventList.Skip((CurrentPage - 1) * ItemsPerPage).Take(ItemsPerPage).ToList();
                rptEventList.DataBind();

                for (int i = 1; i <= (_eventList.Count / ItemsPerPage); i++)
                {
                    ltrPagination.Text += "<li><a href='/saglik-kosesi?pg=" + i + "'>" + i + "</a></li>";
                }
            }
            //var _eventList2 = db.news.FirstOrDefault(q => q.type == 1);

            //if (_eventList2.img_path != null && _eventList2.img_path != "")
            //{
            //    topbanner.Style.Add("background-image", "../" + _eventList2.img_path);
            //    topbanner.Style.Add("background-position-y", "-31px");
            //}
            //else
            //    topbanner.Style.Add("height", "0");
        }
    }
}