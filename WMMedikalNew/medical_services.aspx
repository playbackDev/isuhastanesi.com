﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="medical_services.aspx.cs" Inherits="WMMedikalNew.WebForm4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        <div class="sub-page-title" runat="server" id="topbanner">
  
        <div class="container">
         
        </div>
    </div>

    <div class="section-line">
        <div class="breadcrumb2">
            <a href="/"> Anasayfa | </a>  <a href="#">Tıbbi Bölümler</a> 
        </div>
    </div>
    <style>
        .med-list li{margin-left:5%;}
        .med-list li a{font-size:16px}
        .med-list {    width: 30%;
    margin-top: 40px;
    margin-left: 3.33%;}


    </style>
     <div class="container content">
        <div class="sub-content-wide">
        <%--    <div class="filters">
                <div class="filters-select">
                    <h4><%=Resources.Lang.BolumAra %></h4>
           
                        <ul>
                        
                            <li>
                             
                                <asp:DropDownList ID="drpHospitals" runat="server" CssClass="customselect select-alt" AutoPostBack="true"></asp:DropDownList>
                            </li>
                            <li>
                                <asp:TextBox ID="txtSearch" runat="server" CssClass="itext" placeholder="ara" Text="" />


                            </li>
                            <li>
                                <asp:LinkButton ID="btnSearchMedUnit" Text="Ara" runat="server" CssClass="btn-search" OnClick="btnSearchMedUnit_Click" />
                            </li>
                        </ul>
                </div>

                <div  class="filters-alphabetic">
                      <h5><%=Resources.Lang.Alfabetik %></h5>
                    <ul>
                         <asp:Repeater ID="rptLetters" runat="server">
                            <ItemTemplate>
                                <li>
                             <asp:LinkButton Text='<%#Eval("letter") %>' ID="btnLetters" runat="server" CommandArgument='<%#Eval("letter") %>' OnCommand="btnLetters_Command" />
                                 
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
            </div>--%>
        
            <div class="sub-container">
			    <div class="med-title">
				    <h2 style="text-align:center">Tıbbi Bölümler</h2>
			    </div>
			    <div class="list-container">
				    <div class="left-col" id="divContent" runat="server">
                        <center>
                        <span style="color:#90032B;font-size:16px;margin-top:20px;"><asp:Literal ID="ltrUyari" runat="server"></asp:Literal></span>
                            </center>
                       <asp:Repeater ID="rptLettersCats" runat="server">
                            <ItemTemplate>
                                <asp:Literal Text="" ID="ltrCounter1"  runat="server" />

                                <ul class="med-list">
                                    <li class="span"><%#Eval("letter") %></li>
                                    <asp:Repeater ID="rptBrn" DataSource='<%#Eval("services_list") %>' runat="server" OnItemDataBound="rptBrn_ItemDataBound">
                                        <ItemTemplate>
                                            <li>
                                                
                                                <asp:Literal Text="" ID="ltrLink" runat="server" />
                                               
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                
                                </ul>

                                 <asp:Literal Text="" ID="ltrCounter2"  runat="server" />
                                              
                            </ItemTemplate>
                        </asp:Repeater>
					    
				    </div>
				
			    </div>
		    </div>
          </div>
     </div>



        <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="/assets/js/plugins.js"></script>
</asp:Content>
