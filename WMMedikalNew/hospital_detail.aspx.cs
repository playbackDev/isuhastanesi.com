﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class hospital_detail : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            var _HakkimizdDetay = db.Hastane_Sayfalar_Dil.Select(s => new
            {
                s.sayfa_adi,
                s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == "TR").description,
                s.sayfa_id,
                s.Hastane_Sayfalar.Hastaneler.HastaneAdi,
                hastane_id = s.Hastane_Sayfalar.Hastaneler.id,
                s.dil,
                s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == "TR").top_banner,
                s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == "TR").meta_description,
                GalleryImage = s.Hastane_Sayfalar.gallery.Select(s2 => new
                {
                    ImageID = s2.id,
                    s2.image_name,
                    active_status = (s2.is_active) ? "Aktif" : "Pasif",
                    s2.Hastaneler.HastaneAdi
                })
            }).FirstOrDefault(q=> q.sayfa_id == 1);

            if (_HakkimizdDetay != null)
            {
                //pagesubmenu.HospitalID = _HakkimizdDetay.hastane_id.toint();

                Page.MetaDescription = _HakkimizdDetay.meta_description.ToString();
                ltrDescription.Text = _HakkimizdDetay.description;
                ltrHospitalName.Text = _HakkimizdDetay.sayfa_adi;

                if (_HakkimizdDetay.top_banner != null && _HakkimizdDetay.top_banner != "")
                {
                    topbanner.Style.Add("background-image", "../" + _HakkimizdDetay.top_banner);
                    topbanner.Style.Add("background-position-y", "-31px");
                }
                else
                    topbanner.Style.Add("height", "0");


                if (_HakkimizdDetay.GalleryImage.ToList().Count > 0)
                {
                    PageGallery.DataSource = _HakkimizdDetay.GalleryImage;
                    PageGallery.DataBind();
                }
            }


            //var PageDetail = db.page_detail_lang.Select(s => new
            //{
            //    s.page_id,
            //    s.description,
            //    s.pages.page_langs.FirstOrDefault().page_name,
            //    s.lang,
            //    s.pages.parent_id,
            //    GalleryImage = s.pages.gallery

            //}).Where(q => q.page_id == PageID && q.lang == Lang).FirstOrDefault();


            //if (PageDetail != null)
            //{
            //    ltrDescription.Text = PageDetail.description;
            //    ltrHospitalName.Text = PageDetail.page_name;
            //    ltrHospitalNameTitle.Text = PageDetail.page_name;

            //    if (PageDetail.GalleryImage.ToList().Count > 0)
            //    {
            //        PageGallery.DataSource = PageDetail.GalleryImage;
            //        PageGallery.DataBind();
            //    }
            //}
            //else
            //{
            //    ltrDescription.Text = "İçerik Eklenmemiş";
            //    ltrHospitalName.Text = "";
            //}





        }
    }
}