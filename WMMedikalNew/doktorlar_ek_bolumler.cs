//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WMMedikalNew
{
    using System;
    using System.Collections.Generic;
    
    public partial class doktorlar_ek_bolumler
    {
        public int id { get; set; }
        public int doktorId { get; set; }
        public int bolumId { get; set; }
    
        public virtual Doktorlar Doktorlar { get; set; }
        public virtual TibbiBolumler TibbiBolumler { get; set; }
    }
}
