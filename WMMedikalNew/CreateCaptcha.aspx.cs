﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class CreateCaptcha : System.Web.UI.Page
    {
        private Random rand = new Random();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CreateCaptchaImage();
            }

        }
        private void CreateCaptchaImage()
        {
            string code = string.Empty;
            int imgwidth = 0;
            int imgheight = 0;
            int imgFontRNDNextValue1 = 0;
            int imgFontRNDNextValue2 = 0;
            int imgPintF = 0;
            int CounterEnc = 0;
            //if (Request.QueryString["New"].ToString() == "1")
            //{
            //    code = GetRandomText();
            //}
            //else
            //{
            //    code = Session["CaptchaCode"].ToString();
            //}

            code = Request.QueryString["Code"].ToString();
            if ((Request.QueryString["ImgWidth"] != null) && (Request.QueryString["ImgHeight"] != null))
            {
                imgwidth = int.Parse(Request.QueryString["ImgWidth"].ToString());
                imgheight = int.Parse(Request.QueryString["ImgHeight"].ToString());

                imgFontRNDNextValue1 = 10;
                imgFontRNDNextValue2 = 12;

                imgPintF = 2;
                CounterEnc = 23;
            }
            else
            {
                imgwidth = 165;
                imgheight = 55;

                imgFontRNDNextValue1 = 15;
                imgFontRNDNextValue2 = 20;

                imgPintF = 10;
                CounterEnc = 28;
            }

            Bitmap bitmap = new Bitmap(imgwidth, imgheight, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            Graphics g = Graphics.FromImage(bitmap);
            Pen pen = new Pen(Color.Yellow);
            Rectangle rect = new Rectangle(0, 0, 200, 60);

            SolidBrush blue = new SolidBrush(Color.White);
            SolidBrush black = new SolidBrush(Color.Black);

            int counter = 0;

            g.DrawRectangle(pen, rect);
            g.FillRectangle(blue, rect);

            for (int i = 0; i < code.Length; i++)
            {
                g.DrawString(code[i].ToString(), new Font("Tahoma", 6 + rand.Next(imgFontRNDNextValue1, imgFontRNDNextValue2), FontStyle.Italic), black, new PointF(imgPintF + counter, 10));
                counter += CounterEnc;
            }

            DrawRandomLines(g);
            bitmap.Save(Response.OutputStream, ImageFormat.Gif);

            g.Dispose();
            bitmap.Dispose();
        }
        private void DrawRandomLines(Graphics g)
        {
            SolidBrush yellow = new SolidBrush(Color.Blue);
            for (int i = 0; i < 20; i++)
            {
                g.DrawLines(new Pen(yellow, 1), GetRandomPoints());
            }
        }
        private Point[] GetRandomPoints()
        {
            Point[] points = { new Point(rand.Next(0, 150), rand.Next(1, 150)), new Point(rand.Next(0, 200), rand.Next(1, 190)) };
            return points;
        }
        /// <summary>
        /// Method for generating random text of 5 cahrecters as captcha code
        /// </summary>
        /// <returns></returns>
        private string GetRandomText()
        {
            StringBuilder randomText = new StringBuilder();
            string alphabets = "012345679ACEFGHKLMNPRSWXZabcdefghijkhlmnopqrstuvwxyz";
            Random r = new Random();
            for (int j = 0; j <= 4; j++)
            {
                randomText.Append(alphabets[r.Next(alphabets.Length)]);
            }
            Session["CaptchaCode"] = randomText.ToString();
            return Session["CaptchaCode"] as String;
        }
    }
}