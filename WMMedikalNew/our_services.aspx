﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="our_services.aspx.cs" Inherits="WMMedikalNew.our_services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="sub-page-title" runat="server" id="topbanner">
        <div class="container">
        </div>
    </div>

    <div class="section-line">
        <div class="breadcrumb2">
            <a href="/">Anasayfa | </a><a id="Content_hpTitle">
                <asp:Literal ID="ltrBreadCrumbServiceName" runat="server"></asp:Literal></a>
        </div>
    </div>

    <div class="container content">
        <div class="detay-section">
            <div class="sekmeAlani">
                <ul class="sekmeler">
                    <li class="sekmeSecili"><a href="javascript:void(0);" class="s0 s1">
                        <asp:Literal ID="ltrContentServiceName" runat="server"></asp:Literal></a>
                        <br />

                    </li>
                </ul>
                <div id="opener-part" class="s0" style="display: block;">
                    <div class="top-part">
                        <h1>
                            <asp:Literal ID="ltrServiceNameTitle" runat="server"></asp:Literal>
                        </h1>
                    </div>
                    <div class="detay-part" style="width: 100%;">
                        <asp:Literal ID="ltrContent" runat="server"></asp:Literal>
                    </div>

                    <ul class="doktor-list-sub" style="clear: both;">
                <asp:Repeater runat="server" ID="rptDoctorList">
                    <ItemTemplate>
                        <li>
                            <div class="photo">
                                <a href="/<%#Eval("Dil") %>/doktorlarimiz/<%#WMMedikalNew.cExtensions.ToURL(Eval("DoktorAdi").ToString())%>-<%#Eval("doktor_id") %>">
                                    <img src="<%# Eval("ResimAdi") != null ? "http://www.isuhastanesi.com/"+Eval("ResimAdi") :  "/admin/resimyok.png" %> " />
                                </a>
                            </div>
                            <div class="doktor-info">
                                <a href="/<%#Eval("Dil") %>/doktorlarimiz/<%#WMMedikalNew.cExtensions.ToURL(Eval("DoktorAdi").ToString())%>-<%#Eval("doktor_id") %>">
                                    <h5><%#Eval("Unvan") %>   <%#Eval("DoktorAdi") %></h5>
                                </a>
                                <hr />
                                <h6>
                                    <%#Eval("BolumAdi") %>
                                    <asp:Repeater runat="server" ID="rptEkBolumler" DataSource='<%# Eval("ekbolumler") %>'>
                                        <ItemTemplate>
                                            <br />
                                            <%# Eval("BolumAdi") %>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </h6>
                            </div>
                            <div class="uzmanlik"></div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
                </div>

            </div>
            <!--[if !IE]>sekmeAlani sonu <![endif]-->
            

        </div>
    </div>
</asp:Content>
