﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class organization_list_general : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            var _HakkimizdDetay = db.Hastane_Sayfalar_Dil.Select(s => new
            {
                s.sayfa_adi,
                s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == "TR").description,
                s.sayfa_id,
                s.Hastane_Sayfalar.Hastaneler.HastaneAdi,
                hastane_id = s.Hastane_Sayfalar.Hastaneler.id,
                s.dil,
                s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == "TR").top_banner,
                s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == "TR").meta_description,
                GalleryImage = s.Hastane_Sayfalar.gallery.Select(s2 => new
                {
                    ImageID = s2.id,
                    s2.image_name,
                    active_status = (s2.is_active) ? "Aktif" : "Pasif",
                    s2.Hastaneler.HastaneAdi
                })
            }).FirstOrDefault( q=> q.sayfa_id == 4);

            if (_HakkimizdDetay != null)
            {
                //pagesubmenu.HospitalID = _HakkimizdDetay.hastane_id.toint();

                Page.MetaDescription = _HakkimizdDetay.meta_description.ToString();


                if (_HakkimizdDetay.top_banner != null && _HakkimizdDetay.top_banner != "")
                {
                    topbanner.Style.Add("background-image", "../" + _HakkimizdDetay.top_banner);
                    topbanner.Style.Add("background-position-y", "-31px");
                }
                else
                    topbanner.Style.Add("height", "0");



            }

            if (!IsPostBack)
            {
                fillPage();
            }
        }
        private void fillPage()
        {
            var OrganizationTypeList = db.organization_type.ToList();
            rptOrganizationtype.DataSource = OrganizationTypeList;
            rptOrganizationtype.DataBind();
        }


        protected void rptOrganizationtype_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Literal ltrOrganizationName = (Literal)e.Item.FindControl("ltrOrganizationName");
            int type_id = DataBinder.Eval(e.Item.DataItem, "id").toint();

            if (Lang == "TR")
            {
                ltrOrganizationName.Text = DataBinder.Eval(e.Item.DataItem, "organization_name_tr").ToString();
            }
            else
                ltrOrganizationName.Text = DataBinder.Eval(e.Item.DataItem, "organization_name_en").ToString();

            Repeater rptOrganizationList = (Repeater)e.Item.FindControl("rptOrganizationList");

            var OrganizationList = db.organizations.Where(q => q.organization_type_id == type_id).ToList();

            rptOrganizationList.DataSource = OrganizationList;
            rptOrganizationList.DataBind();

        }

        protected void rptOrganizationList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Literal ltrOrganizationName_ = (Literal)e.Item.FindControl("ltrOrganizationName_");

            if (Lang == "TR")
            {
                ltrOrganizationName_.Text = DataBinder.Eval(e.Item.DataItem, "organization_name_tr").ToString();
            }
            else
                ltrOrganizationName_.Text = DataBinder.Eval(e.Item.DataItem, "organization_name_en").ToString();
        }
     
    }
}