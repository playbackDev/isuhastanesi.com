/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */



CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:


    config.language = 'tr';
    config.filebrowserBrowseUrl = "/ckeditor/plugins/imageuploader/imgbrowser.php";
    //config.filebrowserUploadUrl = '/uploader/upload.php'
    // config.uiColor = '#AADC6E';
    // config.extraPlugins = 'myplugin,mytools14';
    config.contentsCss = ['https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', 'http://localhost:3558/Private/assets/css/kodstrap.css'];
    config.allowedContent = true;

    CKEDITOR.dtd.$removeEmpty['i'] = false; //font-awesome ve bootstrap iconlarının kaldırılmasını engelliyor.
};

