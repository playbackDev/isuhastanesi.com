﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="haberList.aspx.cs" Inherits="WMMedikalNew.haberList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="section-line">
        <div class="breadcrumb2">
            <a href="/">anasayfa |</a><a href="/saglik-kosesi">Sağlık Köşesi</a>
        </div>
    </div>


    <style>
        .accordion-title-new {
            display: block;
            background-color: #fff;
        }

            .accordion-title-new h2 {
                text-align: center;
                padding: 20px 0;
                background-color: #fff;
                cursor: pointer;
                font-size: 16px;
                color: #90032B;
                border-bottom: 1px solid #90032B;
            }

        .pagination li {
            float: left;
            border: 0px;
            margin: 0 5px 0 5px;
            padding: 0px;
        }

            .pagination li a {
                background: #a83338;
                border: 0;
                padding: 8px;
                margin: 0 5px 0 5px;
                color: #fff;
                font-size: 14px;
                text-decoration: none;
            }
    </style>
    <div class="container">



        <div class="page-wrapper">
            <div class="accordion-title-new">
                <h2>Sağlık Köşesi</h2>
            </div>

            <div class="etkinlik white">

                <asp:Repeater ID="rptEventList" runat="server">
                    <ItemTemplate>
                        <a href="/s-<%# Eval("seoURL") %>/<%#Eval("news_id") %>">
                            <div>
                                <img src="<%#Eval("news_image") %>" width="204" style="min-height: 170px;" />
                                <div class="etkinlik-title">
                                    <p><%#Eval("news_title") %> </p>
                                </div>

                                <div class="etkinlik-date">
                                    <%--<p><%#Eval("created_time", "{0:d}") %> </p>--%>
                                    <p><%# Eval("news_short") %></p>
                                </div>
                            </div>
                        </a>
                        <hr style="background-color: #90032B; border: 0; height: 1px; background-image: -webkit-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0); background-image: -moz-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0); background-image: -ms-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0); background-image: -o-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0); clear: left; width: 80%; display: block; position: relative; top: 16px;" />

                    </ItemTemplate>
                </asp:Repeater>
                <ul class="pagination">
                    <asp:Literal ID="ltrPagination" runat="server" />
                </ul>

            </div>
        </div>

    </div>


</asp:Content>
