﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="eventList.aspx.cs" Inherits="WMMedikalNew.eventList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="sub-page-title bg-1">
        <div class="container">
            <h1 class="middle-content" style="margin-top: 128px;"><%=Resources.Lang.Etkinlikler %></h1>
        </div>

     </div>
    <div class="section-line">
        <div class="breadcrumb2">
            <a href="/">anasayfa &gt; </a>
            <asp:HyperLink ID="hypBreadCrumb" runat="server"><%=Resources.Lang.Etkinlikler %></asp:HyperLink>
       
        </div>
    </div>

    <div class="container">

        <div class="page-wrapper">
            <asp:Repeater ID="rptEventList" runat="server">
                <ItemTemplate>
                    <div class="etkinlik white">
                        <div class="etkinlik-title">
                         <p><%#Eval("news_title") %> </p>  
                        </div>
                        <div class="etkinlik-date">
                            <p><%#Eval("created_time", "{0:d}") %> </p>
                            </div>
                            <div class="etkinlik-desc">
                                   <p><%#Eval("news_content") %> </p> 

                            </div>
                            <div class="etkinlik-image">
                                <img src="/uploads/news/<%#Eval("news_image") %> ">
                            </div>
                        </div>
                    </div>
                    </ItemTemplate>
                </asp:Repeater>
          </div>
     </div>
</asp:Content>
