﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="WMMedikalNew._default" ValidateRequest="false" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/assets/css/jquery.bxslider.css" />
    <link href="/assets/plugins/flexslider.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="">
        <section id="promo" class="home-slider1">

            <div class="flexslider" style="overflow: hidden; width: 100%; border: 0 !important;">
                <ul class="slides">
                    <asp:Repeater ID="rptSlide" runat="server">
                        <ItemTemplate>
                            <li><a href="<%#Eval("link") %>">
                                <img src="<%#Eval("image_url") %>" draggable="false"/></a>

                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
             
                </ul>
            </div>
        </section>

    </div>


    <div id="widget">
        <ul class="tabs">
            <li><a href="#" id="lnkTab1" runat="server" data-tab="tab1" class="tab1"><i class="arrow"></i><%=Resources.Lang.DoktorAra %>  </a></li>
            <li><a href="#" id="lnkTab2" runat="server" data-tab="tab2" class="tab2"><i class="arrow"></i><%=Resources.Lang.RandevuAl %> </a></li>
            <li><a href="#" id="lnkTab3" runat="server" data-tab="tab3" class="tab3"><i class="arrow"></i><%=Resources.Lang.SonuclariGor %></a></li>
        </ul>
        <div class="tabcontainers">

            <div id="tab1" class="tabcontent">

                <div class="filters-select">
                    <ul>
                        <li>
                            <asp:DropDownList ID="drpHospital" runat="server" class="customselect1" AutoPostBack="true" OnSelectedIndexChanged="drpHospital_SelectedIndexChanged"></asp:DropDownList>
                        </li>
                        <li><span class="selectbox size3 alt">
                            <asp:DropDownList ID="drpMedical" runat="server" CssClass="customselect1" AutoPostBack="true" OnSelectedIndexChanged="drpMedical_SelectedIndexChanged">
                            </asp:DropDownList>
                        </span>
                        </li>
                        <li><span class="selectbox size3 alt" id="spn">
                            <asp:DropDownList ID="drpDoctors" runat="server" CssClass="customselect1" AutoPostBack="true" OnSelectedIndexChanged="drpDoctors_SelectedIndexChanged">
                            </asp:DropDownList>

                        </span>
                        </li>
                    </ul>
                </div>

                <input type="hidden" id="hdChar" runat="server" />
                <div id="" class="filters-alphabetic">
                    <h5><%=Resources.Lang.Alfabetik %></h5>
                    <ul>

                        <asp:Repeater ID="rptLetters" runat="server">
                            <ItemTemplate>
                                <li>

                                    <asp:LinkButton Text='<%#Eval("letter") %>' ID="btnLetters" runat="server" CommandArgument='<%#Eval("letter") %>' OnCommand="btnLetters_Command" />
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>

            </div>

            <div id="tab2" class="tabcontent">
                <div class="filters-select">
                    <asp:HiddenField runat="server" ID="hdTabId" />
                    <ul>
                        <li><span class="selectbox size4 alt">

                            <asp:DropDownList ID="drpAppointmentHospitalList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpAppointmentHospitalList_SelectedIndexChanged" CssClass="drpSelect">
                            </asp:DropDownList>
                        </span>
                        </li>

                        <li><span class="selectbox size4 alt">

                            <asp:DropDownList ID="drpAppointmentBranchList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpAppointmentBranchList_SelectedIndexChanged" CssClass="drpSelect">
                            </asp:DropDownList>
                        </span>
                        </li>

                        <li><span class="selectbox size4 alt">

                            <asp:DropDownList ID="drpAppointmentDoctorList" CssClass="drpSelectDoctor" runat="server">
                            </asp:DropDownList>
                        </span>
                        </li>

                        <li>
                            <asp:Button ID="btnAppointment" runat="server" CssClass="btn btn-type2" Text="Randevu Al" OnClick="btnAppointment_Click" />
                        </li>
                    </ul>
                    <style>
                        .drpSelect {
                            width: 100%;
                            height: 43px;
                            background: url("/assets/img/site/btn-select.png") white no-repeat 100% 0;
                            padding: 7px;
                            background-position: 97%;
                            -webkit-appearance: none;
                            border: solid 1px #c5d0db;
                            border-radius: 5px;
                        }

                        .drpSelectDoctor {
                            width: 100%;
                            height: 43px;
                            background: url("/assets/img/site/btn-select.png") white no-repeat 100% 0;
                            padding: 7px;
                            background-position: 97%;
                            -webkit-appearance: none;
                            border: solid 1px #c5d0db;
                            border-radius: 5px;
                        }
                    </style>
                    <div class="clear"></div>
                    <p>&nbsp; </p>

                </div>
            </div>

            <div id="tab3" class="tabcontent">
                <div class="filters-select middle">
                
                    <ul>
                        <li><span class="selectbox size5 alt">
                     
                   
                            <asp:DropDownList ID="drpSonuclariGorHastaneListesi" runat="server" CssClass="customselect1"></asp:DropDownList>
                        </span>
                        </li>

                        <li>
      
                            <asp:TextBox ID="txtIdentityNo" CssClass="itext" placeholder="T.C. Kimlik Numaranız" MaxLength="11"
                                Style="padding: 13px !important;" runat="server"></asp:TextBox>
                        </li>

                        <li>
                            
                            <asp:Button ID="btnFirstSMS" CssClass="btn btn-type2" Text="ŞİFRE AL" runat="server" OnClick="btnFirstSMS_Click" />
                        </li>
                    </ul>
             
                    <div class="clear"></div>
                    <div id="divFirstErrorResult" style="color: red; width: 100%; float: left; margin-left: 8px; display: none;" runat="server"></div>
                    <p>&nbsp;</p>
                    <p>
                        Tahlilinizi hangi hastanemizde yaptırdıysanız işaretleyiniz.<br>
                        Sisteme kayıtlı olduğunuz 11 haneli TC kimlik numaranızı girmeniz gerekmektedir.
                    </p>
                </div>
            </div>

        </div>

    </div>

    <div class="medikalteknoloji-slider full-width">
        <h1 class="full-width align-center"><a class="med-tec" href="/tibbi-hizmetlerimiz/TR"><%=Resources.Lang.TibbiHizmetlerimizLink %> </a></h1>

        <p class="full-width align-center"></p>
        <div class="container">
            <div class="bx-wrapper" style="max-width: 100%; margin: 0px auto;">
                <div class="bx-viewport" style="position: relative; height: 186px;">


                    <div class="bx-wrapper" style="max-width: 100%; margin: 0px auto;">
                        <div class="bx-viewport" style="width: 100%; position: relative; height: 204px;">
                            <div id="owl-demo" class="owl-carousel bx-clone">
                                <asp:Repeater ID="rptServices" runat="server" OnItemDataBound="rptServices_ItemDataBound">
                                    <ItemTemplate>

                                        <div class="item">
                                            <li>
                                               <a href="/detay/tibbi-hizmetlerimiz/<%#WMMedikalNew.cExtensions.ToURL(Eval("BolumAdi").ToString())%>-<%#Eval("BolumID") %>/<%#Eval("Dil") %>">
                                                 
                                                   <asp:Image ID="MedicalServiceImage" runat="server" />
                                                    <span class="title"><%#Eval ("BolumAdi") %></span>
                                                </a>
                                            </li>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>


                            </div>
                        </div>
                        <div class="bx-controls bx-has-controls-direction">
                            <div class="bx-controls-direction"><a class="bx-prev" href="">Prev</a><a class="bx-next" href="">Next</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="full-width">


      <!--  <div class="home-events">
            <h1 class="h1-home"><%=Resources.Lang.Etkinlikler %></h1>

            <div class="pull-right full-width">


                <div class="home-news-item-description">
                    <div class="date-label">
                        <strong>
                            <asp:Literal ID="ltrEventDate" runat="server"></asp:Literal></strong>
                        <span>
                            <asp:Literal ID="ltrEventMonth" runat="server"></asp:Literal></span>
                        <em>
                            <asp:Literal ID="ltrEventYear" runat="server"></asp:Literal></em>
                    </div>
                    <div class="clear"></div>
                    <h2>
                        <asp:Literal ID="ltrEventTitle" runat="server"></asp:Literal>
                    </h2>

                    <div>
                        <asp:Literal ID="ltrEventContent" runat="server"></asp:Literal>

                    </div>

                    <div class="btn-container">
                        <asp:HyperLink ID="hypEventReadMore" runat="server" class="btn-white"><%=Resources.Lang.DevamınıOku %></asp:HyperLink>
                        <asp:HyperLink ID="hypAllEvent" runat="server" class="btn-gray"><%=Resources.Lang.Etkinlik %></asp:HyperLink>

                    </div>
                </div>


            </div>
        </div>
        <div class="clear"></div>
    </div>-->

    <div class="full-width" id="home-bottom-ctas">
        <div class="home-bottom-cta-item mikro-siteler" >
            <h1><%=Resources.Lang.Mikro %><br>
                <%=Resources.Lang.Sitelerimiz %></h1>
            <ul>

                <li><a href="/mikro-siteler/<%=WMMedikalNew.cBase.Lang%>"><%=Resources.Lang.KanserMerkezi  %></a> </li>
                <li><a href="/mikro-siteler/<%=WMMedikalNew.cBase.Lang%>"><%=Resources.Lang.KokHucreMerkezi  %></a> </li>
                <li><a href="/mikro-siteler/<%=WMMedikalNew.cBase.Lang%>"><%=Resources.Lang.HemsirelikHizmetleri  %></a> </li>

            </ul>
            <style>
                .mikro-siteler {background:url(assets/img/content/bg-home-cta-1.jpg)no-repeat; background-size:cover;  background-position: top center;}
            </style>
            <%--<img src="assets/img/content/bg-home-cta-1.jpg" alt="">--%>
        </div>

        <div class="home-bottom-cta-item tibbi-faaliyet-raporu" >
            <h1><%=Resources.Lang.Tanıtım %>
                <br>
                <%=Resources.Lang.Filmlerimiz %></h1>
            <asp:HyperLink ID="hypTanitim" runat="server" class="download">  <%=Resources.Lang.TanitimFilmlerimiz %> <br>
                         <%=Resources.Lang.Buradanİzleyebilirsiniz %> 
            </asp:HyperLink>
            <%--<img src="assets/img/content/bg-home-cta-2.jpg" alt="">--%>
            <style>
                .tibbi-faaliyet-raporu {background:url(assets/img/content/bg-home-cta-2.jpg)no-repeat; background-size:cover;  background-position: top center;}
            </style>
        </div>

        <!--<div class="home-bottom-cta-item ebulten eb1" >
            <div class="blue-form ebulten">

                <asp:TextBox ID="txtEposta" runat="server" placeholder="e-posta" Style="margin: 58% auto; width: 63%; margin-bottom: 4px;"></asp:TextBox>

                <center>
                    <input type="checkbox" name="checkbox" value="value">
                 
                     <a href="javascript:" id="btn-kullanim-kosullari"><%=Resources.Lang.EbultenDescription %></a><br />
            
                    <asp:LinkButton ID="lnkGonder" runat="server" class="btn btn-type2" style="margin-top:10px;"  OnClick="lnkGonder_Click"><%=Resources.Lang.Gonder %></asp:LinkButton>
                    
                </center>
                <div class="clear"></div>
            </div>

            <h1><%=Resources.Lang.EBulten %></h1>
            <p><%=Resources.Lang.EbultenTitle %></p>
            <style>
                .eb1 {background:url(assets/img/content/bg-home-cta-3.jpg)no-repeat; background-size:cover;    background-position: top center;}
            </style>
            <%--<img src="assets/img/content/bg-home-cta-3.jpg" alt="">--%>
        </div>
        -->
             <div class="home-bottom-cta-item ebulten" >
                  <div id="home-news" style="    width: 100%;
    padding: 0;
    min-height: 550px !important;">
            

            <div class="home-news-item" style="margin: 0 auto;" >
                

                <div class="home-news-item-description">
                    <div class="date-label">
                        <strong>
                            <asp:Literal ID="ltrDate" runat="server"></asp:Literal></strong>
                        <span>
                            <asp:Literal ID="ltrMonth" runat="server"></asp:Literal></span>
                        <em>
                            <asp:Literal ID="ltrYear" runat="server"></asp:Literal></em>
                    </div>
                    <div class="clear"></div>
                    <h1 class="h1-home" style="position:relative;    padding-bottom: 0;
    margin-bottom: 0;"><%=Resources.Lang.Haberler %>
            </h1>
                    <h2>
                        <asp:Literal ID="ltrNewsTitle" runat="server"></asp:Literal>
                    </h2>

                    <div>
                        <asp:Literal ID="ltrNewsContent" runat="server"></asp:Literal>
                    </div>

                    <div class="btn-container">

                        <asp:HyperLink ID="newsLink" runat="server" class="btn-white"><%=Resources.Lang.DevamınıOku %></asp:HyperLink>
                        <asp:HyperLink ID="hypEtkinlik" runat="server" class="btn-gray"><%=Resources.Lang.TumHaberler %></asp:HyperLink>

                    </div>
                </div>





            </div>
            <div class="clear"></div>
        </div>
        </div>
        </div>



        <div class="clear"></div>
  
    <script src="/assets/js/jquery-1.9.1.min.js"></script>
    <script src="/assets/js/plugins.js"></script>
    <script src="/assets/js/jquery.bxslider.min.js"></script>
    <script src="/assets/js/modernizr.custom.79639.js"></script>
    <script src="/assets/js/jquery.slitslider.js"></script>

    <script src="/assets/js/liv_global.js"></script>
    <script src="/carousel/owl.carousel.js"></script>

    <script type="text/javascript">

        $(window).load(function () {
            SetClass2(document.getElementById('ContentPlaceHolder1_hdTabId').value);
        });

        function SetClass2(id) {
            if (id != "") {
                for (var i = 1; i <= 3; i++) {
                    var current_tab = "ContentPlaceHolder1_lnkTab" + id;
                    var tab = "ContentPlaceHolder1_lnkTab" + i;
                    if (current_tab == tab) {
                        var clss = "tab" + id;
                        //document.getElementById('lnkTab' + id).className = clss;
                        document.getElementById('ContentPlaceHolder1_lnkTab' + id).click();
                    } else {
                        //document.getElementById('lnkTab' + i).className = "";
                    }
                }
            }
        }

        //function SetAppointment() {
        //    window.location = "/online-hizmetlerimiz/e-randevu?hospitalId=" + document.getElementById('drpAppointmentHospitalList').value +
        //                                            "&branchId=" + document.getElementById('drpAppointmentBranchList').value +
        //                                            "&doctorId=" + document.getElementById('drpAppointmentDoctorList').value;
        //}

    </script>

    <script type="text/javascript" src="/assets/mask/jquery.maskedinput.js"></script>
    <script src="/assets/plugins/jquery.flexslider.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $('#txtIdentityNo').mask('11111111111');
            $('#Content_txtIdentityNo').mask('11111111111');
            $('#Content_txtCancelIdentityNo').mask('11111111111');

        });
    </script>
    <script>
        $(document).ready(function (e) {

            if (checkCookie() == false) {
                setCookie("cookiePopup", 1, 0.001);
                $('#popup').animate({ "top": "10%", "marginTop": "30px" }, 500);
            }
            else { $('#popup,#ContentPlaceHolder1_overlayPop').css('display', 'none'); }

            getCookie("cookiePopup");
            $('#closePop,#ContentPlaceHolder1_overlayPop').click(function () {
                $('#popup,#ContentPlaceHolder1_overlayPop').css('display', 'none');
            });
            $("#owl-demo").owlCarousel({
                autoPlay: 3000,
                items: 3,



            });

        });

        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        }
        function checkCookie() {
            var cookieName = getCookie("cookiePopup");
            if (cookieName != "") {
                return true;
            } else {
                return false;

            }
        }
        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
            }
            return "";
        }
    </script>
    <script>
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                directionNav: true,
                controlNav: false,
                slideshow: true
            });
        });</script>
    <div id="overlayPop" runat="server" visible="false">
        <div id="popup">
            <a id="closePop" href="#">Kapat</a>
            <asp:Literal ID="ltrPopup" runat="server"></asp:Literal>

        </div>
    </div>

    <style>
        .flex-viewport {
            width: 100%;
        }

        #ContentPlaceHolder1_overlayPop {
            position: fixed;
            width: 100%;
            height: 100vh;
            background: #000000;
            opacity: 0.9;
            top: 0;
            margin: 0;
            z-index: 9999;
        }

        .bx-controls-direction {
            display: none;
        }

        #popup {
            width: 60%;
            height: auto;
            background: transparent;
            color: #fff;
            /* padding: 20px; */
            position: relative;
            top: 0%;
            /* left: 25%; */
            margin-top: -500px;
            /* margin-left: -250px; */
            /* z-index: 91; */
            margin: 10% auto;
        }

            #popup h1 {
                margin: 0px;
                padding: 0px;
            }

        .pop-img {
            display: block;
            width: 100%;
        }

        #owl-demo .item li {
            height: 204px;
            width: 267px !important;
            float: left;
            padding: 16px;
            box-sizing: border-box;
            list-style: none;
            background: url("/assets/img/site/medikalteknoloji.png") no-repeat 100% 0;
        }

        #owl-demo .item img {
            display: block;
            width: 107px;
            height: auto;
            margin: 0 auto;
        }
    </style>

    <style>
        .customselect1 {
            font-size: 13px;
            color: #364f60;
            border: solid 1px #e1e1e1;
            background: #fff;
            padding: 10px 10px 6px;
            border-radius: 5px;
        }

        .flex-next {
            font-size: 0;
            background: url(/assets/plugins/next.png)no-repeat 100% 100%;
            background-size: 100%;
        }

        .flex-prev {
            font-size: 0;
            background: url(/assets/plugins/prev.png)no-repeat 100% 100%;
            background-size: 100%;
        }

        .flex-next:before {
            display: none !important;
        }

        .flex-prev:before {
            display: none !important;
        }
    </style>
</asp:Content>
