﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="our_services.aspx.cs" Inherits="WMMedikalNew.our_services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="sub-page-title bg-2">
        <div class="container">
            <h1 id="Content_hTitle" class="middle-content" style="margin-top:128px;">
                <asp:Literal ID="ltrServiceName" runat="server"></asp:Literal>
            </h1>
        </div>
    </div>
    <div class="section-line">
        <div class="breadcrumb2">
            <a href="/">anasayfa &gt;</a><a href="/tibbi-hizmetlerimiz/<%#WMMedikalNew.cBase.Lang%>">tıbbi hizmetler &gt;</a><a id="Content_hpTitle">
                <asp:Literal ID="ltrBreadCrumbServiceName" runat="server"></asp:Literal></a>
        </div>
    </div>

    <div class="container content">
        <div class="detay-section">
            <div class="sekmeAlani">
                <ul class="sekmeler">
                    <li class="sekmeSecili"><a href="javascript:void(0);" class="s0 s1">
                        <asp:Literal ID="ltrContentServiceName" runat="server"></asp:Literal></a>
                        <br />
                           
                    </li>
                </ul>

                  
        
                <div id="opener-part" class="s0" style="display: block;">
                    <div class="top-part">
                        <h1>
                            <asp:Literal ID="ltrServiceNameTitle" runat="server"></asp:Literal>
                        </h1>
                    </div>

                         <div class="top-part">
                        <h1>
                                     <asp:HyperLink ID="hypRandevu" runat="server" Style="color:White"><%=Resources.Lang.RandevuAl2 %></asp:HyperLink>
                        </h1>
                    </div>

                    <div class="detay-part">
                        <asp:Literal ID="ltrContent" runat="server"></asp:Literal>
                   

                        
                    </div>
                </div>


            </div>
            <!--[if !IE]>sekmeAlani sonu <![endif]-->

        </div>
    </div>
</asp:Content>
