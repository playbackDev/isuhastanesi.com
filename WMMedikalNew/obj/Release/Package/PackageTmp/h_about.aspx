﻿hh<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="h_about.aspx.cs" Inherits="WMMedikalNew.WebForm8" %>
<%@ Register Src="~/usercontrol/pageSubMenu.ascx" TagPrefix="uc1" TagName="pagesubmenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="sub-page-title bg-1">
        <div class="container">
            <h1 class="middle-content" style="margin-top: 128px;"><%=Resources.Lang.Hakkimizda %> </h1>
        </div>
           <uc1:pagesubmenu runat="server" ID="pagesubmenu" />
  
    </div>

    <div class="container content pad4050">

        <div id="video_div" runat="server">
            <iframe class="video-full" frameborder="0" allowfullscreen="" id="iframe_video" runat="server"></iframe>
        </div>

        <asp:Literal ID="ltrDescription" runat="server"></asp:Literal>

    </div>
</asp:Content>
