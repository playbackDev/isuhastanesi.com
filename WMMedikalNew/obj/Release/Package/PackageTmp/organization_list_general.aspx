﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="organization_list_general.aspx.cs" Inherits="WMMedikalNew.organization_list_general" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="sub-page-title bg-1" runat="server" id="topbanner">

    </div>



    <div class="section-line">
        <div class="breadcrumb2">
            <a href="/"><%=Resources.Lang.Anasayfa %>> </a>  <a href="#"><%=Resources.Lang.AnlasmaliKurumlar %></a> 
        </div>

    </div>
     
     <div class="container content">
        <div class="sub-content-wide">
        
            <div class="sub-container">
                <div class="med-title">
				    <h2> <%=Resources.Lang.AnlasmaliKurumlar %></h2>
			    </div>
			    <div class="list-container">
				    <div class="left-col" id="divContent" runat="server">
                        <asp:Repeater ID="rptOrganizationtype" runat="server" OnItemDataBound="rptOrganizationtype_ItemDataBound">
                            <ItemTemplate>
                                <ul class="med-list">
                                    <li class="span">
                                        <asp:Literal ID="ltrOrganizationName" runat="server"></asp:Literal>
                                        <asp:Repeater ID="rptOrganizationList" runat="server" OnItemDataBound="rptOrganizationList_ItemDataBound">
                                            <ItemTemplate>
                                                 <li>
                                                       <asp:Literal ID="ltrOrganizationName_" runat="server"></asp:Literal>
                                                     </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </li>

                                </ul>
                            </ItemTemplate>
                        </asp:Repeater>
					    
				    </div>
				
			    </div>
		    </div>
          </div>
     </div>



        <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="/assets/js/plugins.js"></script>
</asp:Content>
