﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="slider_detay.aspx.cs" Inherits="WMMedikalNew.admin.slider_detay" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
    <script type="text/javascript">

        function BrowseServer() {
            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField;
            finder.popup();
        }

        function SetFileField(fileUrl) {
            document.getElementById('xFilePath').value = fileUrl;
        }

    </script>
        <div class="content-wrapper" style="min-height: 916px;">
        <section class="content-header">
            <h1>Slider Yönetimi </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-3">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Slider Düzenle</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Dil Seçiniz</label>
                                <div class="form-group">
                                    <asp:DropDownList ID="drpLanguage" runat="server" CssClass="form-control">

                                        <asp:ListItem Value="TR"> TR </asp:ListItem>
                                        <asp:ListItem Value="EN"> EN</asp:ListItem>

                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Link   (http ile ekleyiniz örn : htttp://www.google.com) </label>
                                <asp:TextBox ID="txtURL" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Resim Yükle   </label>

                                <input id="xFilePath" name="FilePath" type="text" size="60" />
                                <input type="button" value="Sunucuyu Gez" onclick="BrowseServer();" />


                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Durum (Aktif/Pasif)</label>
                                <asp:CheckBox ID="ckIsActive" runat="server" class="form-control" />
                            </div>


                        </div>
                        <div class="box-footer">

                            <asp:Button ID="btnGuncelle" runat="server" Text="Güncelle" class="btn btn-primary" OnClick="btnGuncelle_Click"/><br />
                            <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>

                        </div>
                    </div>

                </div>
                <div class="col-xs-9">
               
                            <asp:Image ID="Image1" runat="server"  />
                            </div>
                     
                </div>
            </section>
            </div>
    
</asp:Content>
