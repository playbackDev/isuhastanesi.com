﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="hastanemiz.aspx.cs" Inherits="WMMedikalNew.admin.WebForm24" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%--    <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>--%>
    <%--    <script>
        $(function () {
            CKEDITOR.replace('<%=trHakkimizda.ClientID %>');
            CKEDITOR.replace('<%=enHakkimizda.ClientID %>');

        });
    </script>--%>
    <style>
        .dataTables_length {
            display: none;
        }

        .dataTables_filter {
            float: right;
        }

        .dataTables_info {
            display: none;
        }
    </style>
    <script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
    <script type="text/javascript">

        function BrowseServer() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField;
            finder.popup();
        }

        function BrowseServer1() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField1;
            finder.popup();
        }

        function BrowseServer2() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField2;
            finder.popup();
        }
        function SetFileField(fileUrl) {
            document.getElementById('xFilePath').value = fileUrl;
        }

        function SetFileField1(fileUrl) {
            document.getElementById('xFilePath1').value = fileUrl;
        }

        function SetFileField2(fileUrl) {
            document.getElementById('xFilePath2').value = fileUrl;
        }
    </script>
    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content">
            <div class="row">
                <asp:HiddenField ID="hdnPageID" runat="server" />
                <asp:HiddenField ID="hdnHospitalID" runat="server" />
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                <asp:Literal ID="ltrHospitalName" runat="server"></asp:Literal>
                                -
                                <asp:Literal ID="ltrPageName" runat="server"></asp:Literal>
                                (TR) </h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                             <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa Link </label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtPageURLTR" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>

                                    </div>

                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Meta Description </label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtDescriptionTR" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa  Banner</label>
                                        <div class="col-sm-10">
                                            <asp:Literal ID="ltrTopBannerTR" runat="server"></asp:Literal><br /><br />
                                            <input id="xFilePath1" name="FilePathTR" type="text" size="60" />
                                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer1();" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Açıklama</label>
                                        <div class="col-sm-10">
                                            <CKEditor:CKEditorControl ID="trHakkimizda" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>

                                    </div>

                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdateTR" runat="server" Text="Güncelle (TR)" class="btn btn-info pull-right" OnClick="btnUpdateTR_Click" />
                                    <br />
                                    <asp:Label ID="lblUyariTR" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                <asp:Literal ID="ltrHospitalName2" runat="server"></asp:Literal>
                                -
                                <asp:Literal ID="ltrPageName2" runat="server"></asp:Literal>
                                (EN) </h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                                  <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa Link </label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtPageURLEN" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>

                                    </div>
                                           <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Meta Description </label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtDescriptionEN" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>

                                    </div>
                                          <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa  Banner</label>
                                        <div class="col-sm-10">
                                            <asp:Literal ID="ltrTopBannerEN" runat="server"></asp:Literal><br /><br />
                                            <input id="xFilePath2" name="FilePathEN" type="text" size="60" />
                                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer1();" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Açıklama</label>
                                        <div class="col-sm-10">
                                            <CKEditor:CKEditorControl ID="enHakkimizda" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>

                                    </div>
                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdateEN" runat="server" Text="Güncelle (EN)" class="btn btn-info pull-right" OnClick="btnUpdateEN_Click" />
                                    <br />
                                    <asp:Label ID="lbluyariEN" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Sayfaya Resim Ekle</h3>
                        </div>
                        <div class="box-body">

                            <div class="form-group">
                                <label for="exampleInputEmail1">Resim   </label>
                                <input id="xFilePath" name="FilePath" type="text" size="60" />
                                <input type="button" value="Sunucuyu Gez" onclick="BrowseServer();" />
                                318*206 px
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Durum (Aktif/Pasif)</label>
                                <asp:CheckBox ID="ckIsActive" runat="server" class="form-control" />
                            </div>


                        </div>
                        <div class="box-footer">

                            <asp:Button ID="btnEkle" runat="server" Text="Resim Ekle" class="btn btn-primary" OnClick="btnEkle_Click" /><br />
                            <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>

                        </div>
                    </div>

                </div>

                <div class="col-xs-9">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Resimler</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Durum</th>
                                        <th>Hastane</th>
                                        <th>Resim</th>
                                        <th>İşlem</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <asp:Repeater ID="rptGaleri" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%#Eval ("active_status") %></td>
                                                <td><%#Eval ("HastaneAdi") %></td>
                                                <td>
                                                    <img src="../<%#Eval ("image_name") %>" style="width: 150px" />

                                                </td>
                                                <td>
                                                    <asp:LinkButton runat="server" ID="lnkRemove" OnCommand="lnkRemove_Command" CommandArgument='<%#Eval("id") %>' OnClientClick="return confirm('silmek istediğinizden emin misiniz!');" CssClass="btn btn-s btn-danger">Sil
                                                    </asp:LinkButton>
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
    </div>
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
</asp:Content>
