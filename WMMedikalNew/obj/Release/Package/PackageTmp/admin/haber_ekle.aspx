﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="haber_ekle.aspx.cs" Inherits="WMMedikalNew.admin.haber_ekle" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        	<script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
	<script type="text/javascript">

	    function BrowseServer() {
	        var finder = new CKFinder();
	        finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
	        finder.selectActionFunction = SetFileField;
	        finder.popup();
	    }

	    function SetFileField(fileUrl) {
	        document.getElementById('xFilePath').value = fileUrl;
	    }

	</script>
        <div class="content-wrapper" style="min-height: 916px;">
        <section class="content">
            <div class="row">

                <div class="col-md-12">
                           <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title"> Haber Ekle Türkçe </h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                   <div class="box-body">
                            
                                         <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Haber Türü</label>
                                       
                                                    <div class="col-sm-10">
                                <asp:DropDownList ID="drpType" runat="server" CssClass="form-control">
                                    <asp:ListItem Enabled="true" Text="Tür Seçiniz" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Haber" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Etkinlik" Value="2"></asp:ListItem>

                                </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Haber Başlığı</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtHaberBaslikTR" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Haber Özeti</label>
                                        <div class="col-sm-10">
                                         <CKEditor:CKEditorControl ID="txtHaberOzetTR" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Haber İçeriği</label>
                                        <div class="col-sm-10">
                                             <CKEditor:CKEditorControl ID="txtHaberTR" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"  class="col-sm-2 control-label">Durum (Aktif/Pasif)</label>
                                        <div class="col-sm-10">
                                            <asp:CheckBox ID="ckIsActiveTR" runat="server" class="form-control" />
                                        </div>
                                    </div>


                                </div>
                       
                            </div>
                        </div>
                    </div>

                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title"> Haber Ekle İngilizce </h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Haber Başlığı</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtHaberBaslikEN" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Haber Özeti</label>
                                        <div class="col-sm-10">
                                               <CKEditor:CKEditorControl ID="txtHaberOzetEN" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Haber İçeriği</label>
                                        <div class="col-sm-10">
                                             <CKEditor:CKEditorControl ID="txtHaberEN" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"  class="col-sm-2 control-label">Durum (Aktif/Pasif)</label>
                                        <div class="col-sm-10">
                                            <asp:CheckBox ID="ckIsActiveEN" runat="server" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                         <label for="exampleInputEmail1"  class="col-sm-2 control-label">Resim</label>
                                        <div class="col-sm-10">
                                          <input id="xFilePath" name="FilePath" type="text" size="60" />
		                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer();" />
                                            </div>

                                    </div>
                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnHaberEkle" runat="server" Text="Yeni Haber Ekle" class="btn btn-info pull-right" OnClick="btnHaberEkle_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>
</asp:Content>
