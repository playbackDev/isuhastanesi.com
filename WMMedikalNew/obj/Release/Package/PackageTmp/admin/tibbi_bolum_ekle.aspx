﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="tibbi_bolum_ekle.aspx.cs" Inherits="WMMedikalNew.admin.tibbi_bolum_ekle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content-header">
            <h1>Tıbbi Hizmet Ekle  </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-5">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Yeni Hizmet Ekle</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Hastane Seçiniz</label>
                                <asp:DropDownList ID="drpHospital" runat="server" class="form-control"></asp:DropDownList>
                            </div>


                            <div class="form-group">
                                <label for="exampleInputEmail1">Hizmet Adı</label>
                                <asp:TextBox ID="txtHizmetAdi" runat="server" class="form-control" placeholder="Hizmet Adı"></asp:TextBox>
                            </div>


                        </div>
                        <div class="box-footer">
                            <asp:Button ID="btnHizmetEkle" runat="server" Text="Hizmet Ekle" class="btn btn-primary" OnClick="btnHizmetEkle_Click" /><br />
                            <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>

                        </div>
                    </div>

                </div>
        </section>
    </div>

</asp:Content>
