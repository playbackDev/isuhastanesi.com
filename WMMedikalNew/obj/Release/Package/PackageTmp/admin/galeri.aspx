﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="galeri.aspx.cs" Inherits="WMMedikalNew.admin.galeri" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <style>
        .dataTables_length
        {
            display:none;
        }
        .dataTables_filter
        {
            float:right;
        }
        .dataTables_info
        {
            display:none;
        }
    </style>
    <script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
    <script type="text/javascript">

        function BrowseServer() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField;
            finder.popup();
        }

        function BrowseServer1() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField1;
            finder.popup();
        }

        function BrowseServer2() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField2;
            finder.popup();
        }
        function SetFileField(fileUrl) {
            document.getElementById('xFilePath').value = fileUrl;
        }

        function SetFileField1(fileUrl) {
            document.getElementById('xFilePath1').value = fileUrl;
        }

        function SetFileField2(fileUrl) {
            document.getElementById('xFilePath2').value = fileUrl;
        }
    </script>
    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content-header">
            <h1>Galeri </h1>
        </section>
        <section class="content">
               <div class="row">

                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title"> Sayfa Üst Banner Türkçe</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                              <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa Linki TR</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtPageURLTR" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                        <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Meta Description </label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtDescriptionTR" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>

                                    </div>

                                        <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa  Banner TR</label>
                                        <div class="col-sm-10">
                                            <asp:Literal ID="ltrTopBannerTR" runat="server"></asp:Literal><br /><br />
                                            <input id="xFilePath1" name="FilePathTR" type="text" size="60" />
                                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer1();" />
                                        </div>
                                    </div>

                                      

                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdateTR" runat="server" Text="Güncelle (TR)" class="btn btn-info pull-right" OnClick="btnUpdateTR_Click" />
                                        <br />
                                    <asp:Label ID="lblUyariTR" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                            <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title"> Sayfa Üst Banner İngilizce</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">

                                              <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa Linki EN</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtPageURLEN" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                        <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Meta Description </label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtDescriptionEN" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>

                                    </div>

                                        <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Sayfa  Banner EN</label>
                                        <div class="col-sm-10">
                                            <asp:Literal ID="ltrTopBannerEN" runat="server"></asp:Literal><br /><br />
                                            <input id="xFilePath2" name="FilePathEN" type="text" size="60" />
                                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer1();" />
                                        </div>
                                    </div>
                                          

                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="Button1" runat="server" Text="Güncelle (EN)" class="btn btn-info pull-right" OnClick="Button1_Click"  />
                                        <br />
                                    <asp:Label ID="Label1" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                  </div>
            <div class="row">
                <div class="col-xs-3">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Yeni Resim Ekle</h3>
                        </div>
                        <div class="box-body">
                             <div class="form-group">
                                <label for="exampleInputEmail1">Hastane Seçiniz</label>
                                 <asp:DropDownList ID="drpHospital" runat="server" class="form-control"></asp:DropDownList>
                            </div>

                              <div class="form-group">
                                <label for="exampleInputEmail1">Resim   </label>
                                  <input id="xFilePath" name="FilePath" type="text" size="60" />
		                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer();" />
                            </div>

                               <div class="form-group">
                                <label for="exampleInputEmail1">Durum (Aktif/Pasif)</label>
                                <asp:CheckBox ID="ckIsActive" runat="server" class="form-control" />
                            </div>


                        </div>
                        <div class="box-footer">

                            <asp:Button ID="btnEkle" runat="server" Text="Resim Ekle" class="btn btn-primary" OnClick="btnEkle_Click"/><br />
                            <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>

                        </div>
                    </div>

                </div>
                <div class="col-xs-9">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Resim Galerisi</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                         <th>Durum</th>
                                             <th>Hastane</th>
                                        <th>Resim</th>
                                    
                                        <th>İşlem</th>
                                    
                                    </tr>
                                </thead>

                                <tbody>
                                    <asp:Repeater ID="rptGaleri" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td> <%#Eval ("active_status") %></td>
                                                   <td><%#Eval ("HastaneAdi") %></td>
                                                <td><img src="<%#Eval ("image_name") %>" style="width:150px" />

                                                </td>
                                                <td>
                                               
                                                       <asp:LinkButton runat="server" ID="LinkButton1" OnCommand="LinkButton1_Command" CommandArgument='<%#Eval ("id") %>' OnClientClick="return confirm('silmek istediğinizden emin misiniz!');" CssClass="btn btn-s btn-danger">Sil
                                                    </asp:LinkButton>
                                                </td>
                                                
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>



<%--    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>--%>

    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <script src="plugins/fastclick/fastclick.min.js"></script>

    <script src="dist/js/demo.js"></script>

    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
</asp:Content>
