﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="doktor_detay.aspx.cs" Inherits="WMMedikalNew.admin.WebForm15" EnableEventValidation="false" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
              	<script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
	<script type="text/javascript">

	    function BrowseServer() {

	        var finder = new CKFinder();
	        finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
	        finder.selectActionFunction = SetFileField;
	        finder.popup();


	    }

	    function SetFileField(fileUrl) {
	        document.getElementById('xFilePath').value = fileUrl;
	    }
</script>


    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content">
            <div class="row">

                <div class="col-md-9">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                <asp:HiddenField ID="hdnID" runat="server" />
                                <asp:Literal ID="ltrDoktorName" runat="server"></asp:Literal>
                                (<asp:Literal ID="ltrLang" runat="server"></asp:Literal>)</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                     <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Aktif / Pasif</label>
                                        <div class="col-sm-10">
                                            <asp:CheckBox ID="ckIsActive" runat="server"  class="form-control"/>
                                        </div>
                                    </div>
                                          <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Meta Description</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtMetaDescription" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                            <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Eposta Adresi</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                        <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Tıbbi Ünvan</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtUnvan" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Ad Soyad</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtNameSurname" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Açıklama</label>
                                        <div class="col-sm-10">
                                          
                                              <CKEditor:CKEditorControl ID="areaDescription" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Uzmanlık Alanları</label>
                                        <div class="col-sm-10">
                                           
                                             <CKEditor:CKEditorControl ID="areaExpertise" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Yayınlar</label>
                                        <div class="col-sm-10">
                                          
                                               <CKEditor:CKEditorControl ID="areaPublications" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Eğitim</label>
                                        <div class="col-sm-10">
                                         
                                            <CKEditor:CKEditorControl ID="areaEducation" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Üyelikler</label>
                                        <div class="col-sm-10">
                                       
                                                <CKEditor:CKEditorControl ID="areaMembership" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Deneyimler</label>
                                        <div class="col-sm-10">
                                          
                                               <CKEditor:CKEditorControl ID="areaExprience" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Ödüller</label>
                                        <div class="col-sm-10">
                                          

                                            <CKEditor:CKEditorControl ID="areaAwards" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Ödüller</label>
                                        <div class="col-sm-10">
                                          

                                             <CKEditor:CKEditorControl ID="areaCertificates" Width="100%" Height="500" runat="server" placeholder="" Text=''></CKEditor:CKEditorControl>
                                        </div>
                                    </div>

                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdate" runat="server" Text="Güncelle" class="btn btn-info pull-right" OnClick="btnUpdate_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Resim</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                    <div class="form-group">

                                        <div class="col-sm-12">
                                            <asp:Literal ID="ltrImage" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="form-group">

                                        <div class="col-sm-12">
                                              <input id="xFilePath" name="FilePath" type="text" size="60" />
		                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer();" />
                                        </div>
                                    </div>

                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdateGuncelle" runat="server" Text="Resim Güncelle" class="btn btn-info pull-right" OnClick="btnUpdateGuncelle_Click" />
                                    <br />
                                    <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

</asp:Content>
