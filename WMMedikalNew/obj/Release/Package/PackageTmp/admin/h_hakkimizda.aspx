﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="h_hakkimizda.aspx.cs" Inherits="WMMedikalNew.admin.WebForm16"  EnableEventValidation="false"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script>
        $(function () {
            CKEDITOR.replace('<%=trHakkimizda.ClientID %>');
            CKEDITOR.replace('<%=enHakkimizda.ClientID %>');

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content">
            <div class="row">
                <asp:HiddenField ID="hdnPageID" runat="server" />
                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title"> <asp:Literal ID="ltrHospitalName" runat="server"></asp:Literal>  - <asp:Literal ID="ltrPageName" runat="server"></asp:Literal>  (TR) </h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Video Link</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtVideoURLTR" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Açıklama</label>
                                        <div class="col-sm-10">
                                            <textarea id="trHakkimizda" rows="10" cols="80" runat="server">
                           
                                            </textarea>
                                        </div>

                                    </div>
                        


                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdateTR" runat="server" Text="Güncelle (TR)" class="btn btn-info pull-right"  OnClick="btnUpdateTR_Click"/>
                                        <br />
                                    <asp:Label ID="lblUyariTR" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title"> <asp:Literal ID="ltrHospitalName2" runat="server"></asp:Literal> - <asp:Literal ID="ltrPageName2" runat="server"></asp:Literal> (EN) </h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Video Link</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtVideoURLEN" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                           <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Açıklama</label>
                                        <div class="col-sm-10">
                                            <textarea id="enHakkimizda" rows="10" cols="80" runat="server">
                           
                                            </textarea>
                                        </div>

                                    </div>
                        


                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdateEN" runat="server" Text="Güncelle (EN)" class="btn btn-info pull-right" OnClick="btnUpdateEN_Click" />
                                        <br />
                                    <asp:Label ID="lbluyariEN" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

               
            </div>
        </section>
    </div>
</asp:Content>
