﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="slider_ekle.aspx.cs" Inherits="WMMedikalNew.admin.WebForm25" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .dataTables_length {
            display: none;
        }

        .dataTables_filter {
            float: right;
        }

        .dataTables_info {
            display: none;
        }

        #sortable {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

            #sortable li {
                display: block;
                width: 23%;
                height: 100%;
                float: left;
                margin: 10px;
            }
    </style>

    <script type="text/javascript">
        $(function () {
            $("#SliderResim").sortable({
                opacity: 0.8,
                cursor: 'move',
                update: function () {
                    var ResimSatir = $(this).sortable("toArray");

                    $.ajax({
                        type: "POST",
                        url: "slider_ekle.aspx/resimSirala",
                        data: "{'ResimSatir': '" + ResimSatir + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) { }


                    });
                }
            });


        });

        function penAcKonum(adres, genislik, yukseklik, soldan, ustten) {
            var param = "width=" + genislik + "," +
                        "height=" + yukseklik + "," +
                        "left=" + soldan + "," +
                        "top=" + ustten;
            window.open(adres, "_blank", param, false);
        }
        function penAc() {
            penAcKonum("/ckfinder/ckfinder.html?type=Images&CKEditor=ContentPlaceHolder1_enDescription&CKEditorFuncNum=1&langCode=tr", 960, 600, 200, 100);
        }

    </script>


    <script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
    <script type="text/javascript">

        function BrowseServer() {

            var finder = new CKFinder();
            finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
            finder.selectActionFunction = SetFileField;
            finder.popup();


        }

        function SetFileField(fileUrl) {
            document.getElementById('xFilePath').value = fileUrl;
        }

    </script>
    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content-header">
            <h1>Slider Yönetimi </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-3">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Yeni Slider Ekle</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Dil Seçiniz</label>
                                <div class="form-group">
                                    <asp:DropDownList ID="drpLanguage" runat="server" CssClass="form-control">

                                        <asp:ListItem Value="TR"> TR </asp:ListItem>
                                        <asp:ListItem Value="EN"> EN</asp:ListItem>

                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Link   (http ile ekleyiniz örn : htttp://www.google.com) </label>
                                <asp:TextBox ID="txtURL" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Resim Yükle   </label>

                                <input id="xFilePath" name="FilePath" type="text" size="60" />
                                <input type="button" value="Sunucuyu Gez" onclick="BrowseServer();" />


                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Durum (Aktif/Pasif)</label>
                                <asp:CheckBox ID="ckIsActive" runat="server" class="form-control" />
                            </div>


                        </div>
                        <div class="box-footer">

                            <asp:Button ID="btnEkle" runat="server" Text="Resim Ekle" class="btn btn-primary" OnClick="btnEkle_Click" /><br />
                            <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>

                        </div>
                    </div>

                </div>
                <div class="col-xs-9">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Slider Resimleri (Sürükle Bırak ile Sıralayabilirsiniz)</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Durum</th>
                                        <th>Dil</th>
                                        <th>Resim</th>
                                        <th>Link</th>
                                         <th>Düzenle</th>
                                        <th>Sil</th>

                                    </tr>
                                </thead>

                                <tbody id="SliderResim">
                                    <asp:Repeater ID="rptSlider" runat="server" OnItemDataBound="rptSlider_ItemDataBound">
                                        <ItemTemplate>
                                            <tr id="satir<%#Eval("id")%>">
                                                <td><%#Eval ("active_status") %></td>
                                                <td><%#Eval("lang") %></td>
                                                <td>
                                                    <img src="<%#Eval ("image_url") %>" style="width: 150px" />
                                                    <td><%#Eval ("link") %></td>
                                                </td>
                                                 <td><a href="slider_detay.aspx?slider_id=<%#Eval("id")%>&lang=<%#Eval("lang") %>" class="btn btn-primary"> Düzenle</a></td>
                                                <td>
                             <%--                       <asp:LinkButton runat="server" ID="lnkisMain" CssClass="btn btn-xs btn-success" OnCommand="lnkisMain_Command" CommandArgument='<%#Eval("id") %>'>
                                      <span class="glyphicon glyphicon-ok"></span>Popup Resim Yap
                                                    </asp:LinkButton>--%>
                                                   
                                                    <asp:LinkButton runat="server" ID="LinkButton1" OnCommand="LinkButton1_Command" CommandArgument='<%#Eval ("id") %>' OnClientClick="return confirm('silmek istediğinizden emin misiniz!');" CssClass="btn btn-s btn-danger">Sil
                                                    </asp:LinkButton>
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>



    <%--   <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>--%>

    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <script src="plugins/fastclick/fastclick.min.js"></script>

    <script src="dist/js/demo.js"></script>

    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
</asp:Content>
