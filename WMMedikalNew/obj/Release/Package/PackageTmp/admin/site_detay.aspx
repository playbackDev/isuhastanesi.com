﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="site_detay.aspx.cs" Inherits="WMMedikalNew.admin.WebForm8" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       	<script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
	<script type="text/javascript">

	    function BrowseServer() {

	        var finder = new CKFinder();
	        finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
	        finder.selectActionFunction = SetFileField;
	        finder.popup();


	    }

	    function SetFileField(fileUrl) {
	        document.getElementById('xFilePath').value = fileUrl;
	    }

	</script>

    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content">
            <div class="row">

                <div class="col-md-9">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                <asp:HiddenField ID="hdnID" runat="server" />
                                <asp:Literal ID="ltrSiteTitle" runat="server"></asp:Literal>
                                (<asp:Literal ID="ltrLang" runat="server"></asp:Literal>)</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Site Adı</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtSiteName" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Site Linki</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtSiteURL" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                 
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"  class="col-sm-2 control-label">Durum (Aktif/Pasif)</label>
                                        <div class="col-sm-10">
                                            <asp:CheckBox ID="ckIsActive" runat="server" class="form-control" />
                                        </div>
                                    </div>


                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdate" runat="server" Text="Güncelle" class="btn btn-info pull-right"  OnClick="btnUpdate_Click"/>
                                        <br />
                                    <asp:Label ID="lblUyari2" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                      <div class="col-md-3">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Resim</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                    <div class="form-group">
                                    
                                        <div class="col-sm-12">
                                            <asp:Literal ID="ltrImage" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <br />
                                      <div class="form-group">
                                    
                                        <div class="col-sm-12">
                                      <input id="xFilePath" name="FilePath" type="text" size="60" />
		                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer();" />
                                        </div>
                                    </div>
                          
                                </div>
                                <div class="box-footer">
                                    <asp:Button ID="btnUpdateGuncelle" runat="server" Text="Güncelle" class="btn btn-info pull-right" OnClick="btnUpdateGuncelle_Click"/>
                                    <br />
                                    <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>
