﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="haberler.aspx.cs" Inherits="WMMedikalNew.admin.WebForm4" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .dataTables_length {
            display: none;
        }

        .dataTables_filter {
            float: right;
        }

        .dataTables_info {
            display: none;
        }
    </style>
    	<script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
	<script type="text/javascript">

	    function BrowseServer() {
	        var finder = new CKFinder();
	        finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
	        finder.selectActionFunction = SetFileField;
	        finder.popup();
	    }

	    function SetFileField(fileUrl) {
	        document.getElementById('xFilePath').value = fileUrl;
	    }

	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="min-height: 916px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Haberler</h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
         <%--       <div class="col-xs-3">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Yeni Haber Ekle</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Haber Adı</label>
                                <asp:TextBox ID="txtHaberBaslik" runat="server" class="form-control" placeholder="Haber Adı"></asp:TextBox>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Tür </label>
                                <asp:DropDownList ID="drpType" runat="server" CssClass="form-control">
                                    <asp:ListItem Enabled="true" Text="Tür Seçiniz" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Haber" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Etkinlik" Value="2"></asp:ListItem>

                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Durum (Aktif/Pasif)</label>
                                <asp:CheckBox ID="ckIsActive" runat="server" class="form-control" />
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Resim</label>
                              	<input id="xFilePath" name="FilePath" type="text" size="60" />
		                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer();" />
                            </div>

                        </div>
                        <div class="box-footer">

                            <asp:Button ID="btnHaberEkle" runat="server" Text="Hizmet Ekle" class="btn btn-primary" OnClick="btnHaberEkle_Click" /><br />
                            <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>

                        </div>
                    </div>

                </div>--%>
                <div class="col-xs-9">
                       
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Haberler     </h3>
                            <a href="/admin/haber_ekle.aspx" class="btn btn-primary" style="float:right;right:0">
                                <span class="glyphicon glyphicon-plus"></span>
                                Yeni Haber Ekle
                            </a>
                                
                       
                        </div>
                       
                        <div class="box-body">
                           
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Durum</th>
                                        <th>Haber Başlığı</th>
                                        <th>Diller</th>
                                        <th>İşlem</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <asp:Repeater ID="rptNewList" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%#Eval ("active_status") %></td>
                                                <td><%#Eval ("news_title") %></td>
                                                <td>
                                                    <a class="btn btn-s btn-success" href="haber_detay.aspx?haber_id=<%#Eval ("news_id") %>&Lang=TR">TR </a>
                                                    <a class="btn btn-s btn-info" href="haber_detay.aspx?haber_id=<%#Eval ("news_id") %>&Lang=EN">EN</a>
                                                </td>
                                                <td>
                                                    <asp:LinkButton runat="server" ID="lnkRemove" OnCommand="lnkRemove_Command" CommandArgument='<%#Eval ("news_id") %>' OnClientClick="return confirm('silmek istediğinizden emin misiniz!');" CssClass="btn btn-s btn-danger">Sil
                                                    </asp:LinkButton>
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>



<%--    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>--%>

    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <script src="plugins/fastclick/fastclick.min.js"></script>

    <script src="dist/js/demo.js"></script>

    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
</asp:Content>
