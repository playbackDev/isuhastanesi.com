﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="hastane_sayfalar.aspx.cs" Inherits="WMMedikalNew.admin.WebForm10"  EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <style>
        .dataTables_length
        {
            display:none;
        }
        .dataTables_filter
        {
            float:right;
        }
        .dataTables_info
        {
            display:none;
        }
    </style>
       <script>
           $(document).ready(function () {
               CKEDITOR.timestamp = 'something_random22';
           });
            </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="content-wrapper" style="min-height: 916px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Hastanelerimiz  </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
<%--                <div class="col-xs-3">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Yeni Hastane Ekle</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Hastane Adı</label>
                                <asp:TextBox ID="txtHastaneAdi" runat="server" class="form-control" placeholder="Hastane Adı"></asp:TextBox>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Durum (Aktif/Pasif)</label>
                                <asp:CheckBox ID="ckIsActive" runat="server" class="form-control" />
                            </div>

                               <div class="form-group">
                                <label for="exampleInputEmail1">Durum (Aktif/Pasif)</label>
                                   <asp:FileUpload ID="FileUpload1" runat="server" />
                            </div>

                        </div>
                        <div class="box-footer">

                            <asp:Button ID="btnHastaneEkle" runat="server" Text="Hastane Ekle" class="btn btn-primary" OnClick="btnHastaneEkle_Click" /><br />
                            <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>

                        </div>
                    </div>

                </div>--%>
                <div class="col-xs-9">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Hastanelerimiz</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Hastane Adı</th>
                                        <th>Slider</th>
                                        <th>İletişim</th>
                                        <th>Sayfalar</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <asp:Repeater ID="rptHospitalList" runat="server" OnItemDataBound="rptHospitalList_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%#Eval ("HastaneAdi") %></td>
                                                <td>
                                                             <a class="btn btn-s btn-success" href="slider_ekle.aspx?hospital_id=<%#Eval("id") %>">Slider Yönetimi </a>
                                                </td>
                                                <td>
                                                    <a class="btn btn-s btn-success" href="h_iletisim.aspx?hospital_id=<%#Eval("id") %>">İletişim Bilgileri </a>

                                             
                                                </td>
                                                <td>
                                                    <asp:Repeater ID="rptHastaneSayfalar" runat="server" OnItemDataBound="rptHastaneSayfalar_ItemDataBound">
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="hypHastaneSayfa" runat="server" class="btn btn-s btn-info"><%#Eval("sayfa_adi") %></asp:HyperLink>

                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <a href="video_galeri.aspx" class="btn btn-s btn-info">Video Galeri</a>
                                                    
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>



  <%--  <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>--%>

    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <script src="plugins/fastclick/fastclick.min.js"></script>

    <script src="dist/js/demo.js"></script>

    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>

</asp:Content>
