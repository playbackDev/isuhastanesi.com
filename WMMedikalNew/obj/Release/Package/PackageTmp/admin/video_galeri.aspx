﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="video_galeri.aspx.cs" Inherits="WMMedikalNew.admin.video_galeri" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <style>
        .dataTables_length
        {
            display:none;
        }
        .dataTables_filter
        {
            float:right;
        }
        .dataTables_info
        {
            display:none;
        }
    </style>

    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content-header">
            <h1>Video Galeri </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-3">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Yeni Video Ekle</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Video Adı (Sitede Görünmeyecektir)</label>
                                <asp:TextBox ID="txtVideoName" runat="server" class="form-control" placeholder="Video Adı"></asp:TextBox>
                            </div>

                              <div class="form-group">
                                <label for="exampleInputEmail1">Video Linki  </label>
                                <asp:TextBox ID="txtVideoLink" runat="server" class="form-control" placeholder="Video Linki"></asp:TextBox>
                            </div>

                               <div class="form-group">
                                <label for="exampleInputEmail1">Durum (Aktif/Pasif)</label>
                                <asp:CheckBox ID="ckIsActive" runat="server" class="form-control" />
                            </div>


                        </div>
                        <div class="box-footer">

                            <asp:Button ID="btnEkle" runat="server" Text="Video Ekle" class="btn btn-primary" OnClick="btnEkle_Click"/><br />
                            <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>

                        </div>
                    </div>

                </div>
                <div class="col-xs-9">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Videolar</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                         <th>Durum</th>
                                        <th>Video Adı</th>
                                         <th>Video Link</th>
                                        <th>İşlem</th>
                                    
                                    </tr>
                                </thead>

                                <tbody>
                                    <asp:Repeater ID="rptVideoList" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td> <%#Eval ("active_status") %></td>
                                                <td><%#Eval ("video_name") %></td>
                                                <td><%#Eval ("video_link") %></td>
                                                <td>
                                               
                                                       <asp:LinkButton runat="server" ID="LinkButton1"  OnCommand="LinkButton1_Command"  CommandArgument='<%#Eval ("id") %>' OnClientClick="return confirm('silmek istediğinizden emin misiniz!');" CssClass="btn btn-s btn-danger">Sil
                                                    </asp:LinkButton>
                                                </td>
                                                
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>



<%--    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>--%>

    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <script src="plugins/fastclick/fastclick.min.js"></script>

    <script src="dist/js/demo.js"></script>

    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
</asp:Content>
