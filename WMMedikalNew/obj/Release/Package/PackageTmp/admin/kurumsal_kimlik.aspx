﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="kurumsal_kimlik.aspx.cs" Inherits="WMMedikalNew.admin.WebForm12" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <style>
        .dataTables_length
        {
            display:none;
        }
        .dataTables_filter
        {
            float:right;
        }
        .dataTables_info
        {
            display:none;
        }
    </style>
                	<script type="text/javascript" src="/admin/ckfinder/ckfinder.js"></script>
	<script type="text/javascript">

	    function BrowseServer() {

	        var finder = new CKFinder();
	        finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
	        finder.selectActionFunction = SetFileField;
	        finder.popup();


	    }

	    function BrowseServe1() {

	        var finder = new CKFinder();
	        finder.basePath = '/admin/ckfinder/';	// The path for the installation of CKFinder (default = "/ckfinder/").
	        finder.selectActionFunction = SetFileField;
	        finder.popup();


	    }

	    function SetFileField(fileUrl) {
	        document.getElementById('xFilePath').value = fileUrl;

	    }
	    function SetFileField1(fileUrl) {
	        document.getElementById('xFilePath').value = fileUrl;

	    }

	</script>

    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content-header">
            <h1>Logo ve Kurumsal Kimlik </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-3">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Yeni Logo Ekle</h3>
                        </div>
                        <div class="box-body">
                             <div class="form-group">
                                <label for="exampleInputEmail1">Hastane Seçiniz</label>
                                 <asp:DropDownList ID="drpHospital" runat="server" class="form-control"></asp:DropDownList>
                            </div>

                              <div class="form-group">
                                <label for="exampleInputEmail1">Resim </label>
                                          <input id="xFilePath" name="FilePath" type="text" size="60" />
		                            <input type="button" value="Browse Server" onclick="BrowseServer();" />
                            </div>

                                <div class="form-group">
                                <label for="exampleInputEmail1">İndirilecek Dosya </label>
                                           <input id="xFilePath1" name="FilePath1" type="text" size="60" />
		                            <input type="button" value="Sunucuyu Gez" onclick="BrowseServer1();" />
                            </div>

                               <div class="form-group">
                                <label for="exampleInputEmail1">Durum (Aktif/Pasif)</label>
                                <asp:CheckBox ID="ckIsActive" runat="server" class="form-control" />
                            </div>


                        </div>
                        <div class="box-footer">

                            <asp:Button ID="btnEkle" runat="server" Text="Logo Ekle" class="btn btn-primary"  OnClick="btnEkle_Click"/><br />
                            <asp:Label ID="lblUyari" runat="server" Text="" ForeColor="Red"></asp:Label>

                        </div>
                    </div>

                </div>
                <div class="col-xs-9">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Logo ve Kurumsal Kimlik </h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                         <th>Durum</th>
                                       
                                        <th>Resim</th>
                                    
                                        <th>İşlem</th>
                                    
                                    </tr>
                                </thead>

                                <tbody>
                                    <asp:Repeater ID="rptGaleri" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td> <%#Eval ("active_status") %></td>
                                                 <td><img src="<%#Eval ("image_name") %>" style="width:150px" />
                                                 <td> <a href="<%#Eval ("download_file") %>"><%#Eval ("download_file") %></a> </td>
                                                </td>
                                                <td>
                                               
                                                       <asp:LinkButton runat="server" ID="LinkButton1" OnCommand="LinkButton1_Command" CommandArgument='<%#Eval ("id") %>'  OnClientClick="return confirm('silmek istediğinizden emin misiniz!');" CssClass="btn btn-s btn-danger">Sil
                                                    </asp:LinkButton>
                                                </td>
                                                
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>



<%--    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>--%>

    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <script src="plugins/fastclick/fastclick.min.js"></script>

    <script src="dist/js/demo.js"></script>

    <script>
        $(function () {
            $("#example1").DataTable();

        });
    </script>
</asp:Content>
