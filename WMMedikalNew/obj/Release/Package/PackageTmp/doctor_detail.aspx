﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="doctor_detail.aspx.cs" Inherits="WMMedikalNew.WebForm6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="sub-page-title bg-2">
        <div class="container">
            <h1 class="middle-content" style="margin-top: 128px;"><%=Resources.Lang.Doktorlarimiz %></h1>
        </div>
    </div>

    <div class="container content">


        <div class="profile-section" style="display: inline-table;">

            <div class="doktor-img">
                <asp:Literal ID="ltrImage" runat="server"></asp:Literal>
            </div>
            <div class="doctor-desc">
                <h1>
                    <asp:Literal ID="ltrTitle" runat="server"></asp:Literal>
                    <asp:Literal ID="ltrNameSurname" runat="server"></asp:Literal></h1>
                <h2>
                    <asp:Literal ID="ltrHospitalName" runat="server"></asp:Literal>
                    -
                    <asp:Literal ID="ltrMedical" runat="server"></asp:Literal></h2>
                <p>
                    <asp:Literal ID="ltrEmail" runat="server"></asp:Literal></p>


                <p>
                    <asp:Literal ID="ltrDescription" runat="server"></asp:Literal>
                </p>

            </div>

        </div>

        <div class="descr-section">
            <div class="col-1">
                <div class="box-1" runat="server" id="uzmanlikalanlari" visible="false">
                    <div class="dr-title">
                        <p><%=Resources.Lang.UzmanlikAlanlari %></p>
                    </div>
                    <div class="dr-descr">
                        <asp:Literal ID="ltrExpertise" runat="server"></asp:Literal>

                    </div>
                </div>
                <div class="education" runat="server" id="egitim" visible="false">
                    <div class="dr-title">
                        <p><%=Resources.Lang.Egitim %></p>
                    </div>
                    <asp:Literal ID="ltrEducation" runat="server"></asp:Literal>

                </div>



                <div class="education" runat="server" id="deneyim" visible="false">
                    <div class="dr-title">
                        <p><%=Resources.Lang.Deneyim %></p>
                    </div>
                    <asp:Literal ID="ltrExperience" runat="server"></asp:Literal>

                </div>




            </div>
            <style>
                .mh_hidden {
                    max-height: 275px;
                    overflow: hidden;
                    padding-bottom: 50px;
                    border-bottom: 35px solid #fae0e7;
                    position: relative;
                    transition: ease all .5s;
                }

                .mh_show {
                    max-height: initial;
                    transition: ease all .5s;
                }

                .mh_show_btn {
                    position: absolute;
                    bottom: 5px;
                    text-decoration: underline;
                    right: 5px;
                }

                .mh_hide_btn {
                    position: absolute;
                    bottom: 5px;
                    text-decoration: underline;
                    right: 5px;
                    right: 5px;
                }
            </style>
            <script>
                $(document).ready(function () {
                    $('.mh_show_btn').click(function () {
                        $('.mh_hidden').addClass('mh_show');
                        $(this).hide();
                        $('.mh_hide_btn').show();
                    });
                    $('.mh_hide_btn').click(function () {
                        $('.mh_hidden').removeClass('mh_show');
                        $(this).hide();
                        $('.mh_show_btn').show();
                    });
                });
            </script>
            <div class="col-1">
                <div style="position: relative; width: 33%; display: inline-block; margin-left: .66%">
                    <div class="box-1 mh_hidden" style="width: 100%" runat="server" id="yayinlar" visible="false">
                        <div class="dr-title">
                            <p><%=Resources.Lang.Yayinlar %></p>
                        </div>
                        <div class="dr-descr">
                            <asp:Literal ID="ltrResource" runat="server"></asp:Literal>
                        </div>

                    </div>
                    <a href="javascript:void(0);" class="mh_show_btn"><%=Resources.Lang.DevaminiOku %></a>
                    <a href="javascript:void(0);" class="mh_hide_btn" style="display: none"><%=Resources.Lang.Gizle %></a>
                </div>
                <div class="education" runat="server" id="uyelikler" visible="false">
                    <div class="dr-title">
                        <p><%=Resources.Lang.Uyelikler %></p>
                    </div>
                    <asp:Literal ID="ltrMemberShip" runat="server"></asp:Literal>
                </div>
                <div class="education" runat="server" id="sertifikalar" visible="false">
                    <div class="dr-title">
                        <p><%=Resources.Lang.Sertifikalar %></p>
                    </div>
                    <asp:Literal ID="ltrCert" runat="server"></asp:Literal>
                </div>
                <div class="education" runat="server" id="oduller" visible="false">
                    <div class="dr-title">
                        <p><%=Resources.Lang.Oduller %></p>
                    </div>
                    <asp:Literal ID="ltrAwards" runat="server"></asp:Literal>
                </div>
            </div>

        </div>




    </div>
</asp:Content>
