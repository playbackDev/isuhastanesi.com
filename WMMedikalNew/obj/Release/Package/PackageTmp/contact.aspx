﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="contact.aspx.cs" Inherits="WMMedikalNew.contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBbDKtGQoXfF_vI7O4wNMubMVwXBAGj86s"></script>
    <script type="text/javascript">
        function CloseMessage() {
            document.getElementById('divMessageResult').style.display = "none";
        }

        function CheckSecurity() {
            var hdValue = document.getElementById('Content_hdnCaptcha').value;
            var secValue = document.getElementById('Content_txtCaptcha').value;

            if (hdValue == secValue) {
                document.getElementById('Content_lblSecErrorr').style.display = "none";
                return true;
            }
            else {
                document.getElementById('Content_lblSecErrorr').style.display = "block";
                return false;
            }
        }
    </script>
    <style>
        #map_canvas {
            width: 100% !important;
            height: 320px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="sub-page-title bg-1">
        <div class="container">

            <h1 class="middle-content" style="margin-top: 128px;"><%=Resources.Lang.iletisimBilgileri %> </h1>
        </div>
    </div>
    <div class="section-line">
        <div class="breadcrumb2">
            <a href="/"><%=Resources.Lang.Anasayfa %> > </a><a href="#"><%=Resources.Lang.BizeUlasin %> </a>
        </div>
    </div>

    <div class="container content">
        <div class="accordion">
            <asp:Repeater ID="rptContactList" runat="server" OnItemDataBound="rptContactList_ItemDataBound">
                <ItemTemplate>
                    <div class="accordion-item secordion">
                        <div class="accordion-title">
                            <%#Eval("HastaneAdi") %>
                            <br />
                            <img src="/assets/img/site/ico-accordion-arrow.png" alt="" />
                        </div>
                        <div class="accordion-content">

                            <h5><%=Resources.Lang.Adres %> </h5>
                            <p>
                                <asp:Literal ID="ltrAdres" runat="server"></asp:Literal>
                            </p>

                            <div id="map<%#Eval("id") %>" style="width: 100%; height: 320px;"></div>
                            <ul class="addr-info">
                                <li>
                                    <h5><%=Resources.Lang.EPosta %></h5>
                                    <p>
                                        <a href="mailto:<%#Eval("eposta") %>">
                                            <%#Eval("eposta") %>
                                        </a>
                                    </p>
                                </li>
                                <li>
                                    <h5><%=Resources.Lang.CagriMerkezi %></h5>
                                    <p><%#Eval("call_center") %></p>
                                </li>
                                <li>
                                    <h5><%=Resources.Lang.Telefon %></h5>
                                    <p>
                                        <%#Eval("telephone") %>
                                    </p>
                                </li>
                            </ul>

                            <hr class="clear" />
                            <div class="send_message_acc"></div>
                        </div>
                    </div>
                    <script type="text/javascript">

                        var map;
                        var cen = new google.maps.LatLng(<%#Eval("latlng") %>);
                        function initMap() {
                            var mapProp = {
                                center: cen,
                                zoom: 12,
                                mapTypeId: google.maps.MapTypeId.ROADMAP,

                            };
                            map = new google.maps.Map(document.getElementById("map<%#Eval("id") %>"), mapProp);
                            placeMarker(map, cen);

                        }
                        function placeMarker(map, location) {
                            var marker = new google.maps.Marker({
                                position: location,
                                map: map,

                            });

                            var infowindow = new google.maps.InfoWindow({

                                content: "<%#Eval("HastaneAdi") %>"
                            });
                            infowindow.open(map, marker);
                        }

                        $(document).ready(function () {
                            $(".secordion").click(function () {
                                $("#map<%#Eval("id") %>").html("");
                                   initMap();
                                   //console.log($('.accordion-item.secordion.open').find('.accordion-title').text())
                                   $('#ContentPlaceHolder1_hdnActiveTab').val($('.accordion-item.secordion.open').find('.accordion-title').text().trim());
                               });
                               //$('.send_msg').hide();
                               //$('.send_message_acc').html($('.send_msg').html());


                           });
                    </script>
                </ItemTemplate>
            </asp:Repeater>
         <%--   <script>
                $(document).ready(function () {
                    var asd = $("#mesajSend").clone();
                    $(".send_msg").remove();
                    $(".send_message_acc").append(asd)
                })
            </script>--%>


            <div class="accordion-item send_msg">
                <div class="accordion-title">
                    Mesaj Gönder
                            <br />
                    <img src="/assets/img/site/ico-accordion-arrow.png" alt="" />
                </div>
                <div class="accordion-content">
                    <div class="form" id="mesajSend">
                        <asp:HiddenField ID="hdnActiveTab" runat="server" />
                        <div class="column">
                            <div class="form-group">
                                <label for="iname" class="input-label"><%=Resources.Lang.AdSoyad %> *</label>

                                <asp:TextBox runat="server" ID="txtNameSurname" CssClass="itext"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="req_txtNameSurname" ControlToValidate="txtNameSurname" Width="200px" Style="margin-left: 130px;"
                                    ErrorMessage="'ad soyad' alanı zorunludur." ForeColor="Red" runat="server"></asp:RequiredFieldValidator>
                            </div>
                            <div class="form-group">
                                <label for="imail" class="input-label"><%=Resources.Lang.EPosta %> *</label>

                                <asp:TextBox runat="server" ID="txtEMail" CssClass="itext"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="req_txtEmail" ControlToValidate="txtEmail" Width="200px" Style="margin-left: 130px;"
                                    ErrorMessage="'e-posta' alanı zorunludur." ForeColor="Red" runat="server"></asp:RequiredFieldValidator>
                            </div>


                            <div class="form-group">
                                <label for="iinterest" class="input-label"><%=Resources.Lang.Konu %> </label>

                                <asp:DropDownList runat="server" ID="drpSubjectList" CssClass=" select-alt" Style="width: 270px">
                                    <asp:ListItem Text="Seçiniz" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Şikayet" Value="Şikayet"></asp:ListItem>
                                    <asp:ListItem Text="İstek" Value="İstek"></asp:ListItem>
                                    <asp:ListItem Text="Teşekkür" Value="Teşekkür"></asp:ListItem>
                                </asp:DropDownList>
                            </div>

                        </div>

                        <div class="column">

                            <div class="form-group">
                                <label for="itel" class="input-label"><%=Resources.Lang.Telefon %></label>

                                <asp:TextBox runat="server" ID="txtPhone" CssClass="itext"></asp:TextBox>
                                <br />
                                <br />
                                <br />
                            </div>

                            <div class="form-group">
                                <label for="isurname" class="input-label"><%=Resources.Lang.Meslek %> </label>
                                <asp:TextBox runat="server" ID="txtJob" CssClass="itext"></asp:TextBox>
                                <br />
                                <br />
                            </div>
                        </div>

                        <div class="form-group clear">
                            <label for="imsg" class="input-label"><%=Resources.Lang.Mesajiniz %> </label>
                            <asp:TextBox ID="txtMessage" CssClass="itext" TextMode="MultiLine" Rows="3" runat="server"></asp:TextBox>
                            <br />
                            <br />
                        </div>

                        <div class="form-group clear">
                            <asp:ScriptManager runat="server"></asp:ScriptManager>
                            <label for="imsg" class="input-label"><%=Resources.Lang.GuvenlikKodu %>  </label>
                            <asp:UpdatePanel ID="upCaptcha" runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Image ID="imgCaptcha" BorderWidth="1px" CssClass="captcha-image" BorderColor="DarkBlue" runat="server" />
                                            </td>
                                            <td style="margin-left: 11px; margin-right: -15px;">
                                                <asp:ImageButton ID="btnYenile" ValidationGroup="2" ImageUrl="/assets/img/captcha-refresh.png" runat="server"
                                                    CssClass="captcha-button" OnClick="btnYenile_Click" />
                                            </td>
                                            <td colspan="2">
                                                <asp:TextBox ID="txtCaptcha" runat="server" CssClass="itext" Width="98px"></asp:TextBox></td>
                                            <td>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtCaptcha" Width="200px" Style="margin-left: 30px;"
                                                    ErrorMessage="'Güvenlik Kodu' alanı zorunludur." ForeColor="Red" runat="server"></asp:RequiredFieldValidator>
                                                <asp:CompareValidator ID="RequiredFieldValidator1" ControlToValidate="txtCaptcha" ControlToCompare="hdnCaptcha" Width="200px"
                                                    Style="margin-left: 30px;" ErrorMessage="Güvenlik Kodu doğrulanamadı!" runat="server"></asp:CompareValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:TextBox ID="hdnCaptcha" runat="server" Style="display: none;"></asp:TextBox>
                            <asp:Literal ID="ltrCaptcha" Visible="false" runat="server"></asp:Literal>
                            <br />
                            <br />
                        </div>

                        <div class="clear align-center">
                            <asp:Button runat="server" ID="bntSend" CssClass="btn btn-type2" OnClick="bntSend_Click" />
                        </div>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divMessageResult" runat="server" style="text-align: center; color: green; font-weight: bold; font-size: 15px; padding: 5px; display: none;"></div>
        </div>
    </div>

    <script src="/assets/js/plugins.js"></script>
    <script src="/assets/js/liv_global.js"></script>



</asp:Content>
