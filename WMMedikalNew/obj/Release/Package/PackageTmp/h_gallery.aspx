﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="h_gallery.aspx.cs" Inherits="WMMedikalNew.WebForm14" %>
<%@ Register Src="~/usercontrol/pageSubMenu.ascx" TagPrefix="uc1" TagName="pagesubmenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="sub-page-title bg-12" runat="server" id="topbanner" >
     <%--   <div class="container">
            <h1 class="middle-content" style="margin-top: 128px;"><%=Resources.Lang.MedyaBasin %> </h1>
        </div>--%>
        <uc1:pagesubmenu runat="server" ID="pagesubmenu" />
    </div>
    <div class="container content pad20">


        <ul class="gallery-items">
            <asp:Repeater ID="rptGaleri" runat="server">
                <ItemTemplate>
            <li class="">
                <a class="fancybox gallery-item" rel="gallery1" href="../<%#Eval ("image_name") %>" title="">
                    <img src="../<%#Eval ("image_name") %>" alt="">
                </a>
            </li>
                    </ItemTemplate>
            </asp:Repeater>

        </ul>

    </div>
</asp:Content>
