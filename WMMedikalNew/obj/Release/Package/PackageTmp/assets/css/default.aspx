﻿<%@ Page EnableEventValidation="false" Title="" Language="C#" MasterPageFile="~/medicalpark.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Web.UI._default" %>

<%@ Import Namespace="System.Globalization" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <%--<link rel="stylesheet" href="/assets/fancybox/jquery.fancybox.css"/>--%>
    <link rel="stylesheet" href="/assets/css/jquery.bxslider.css"/>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

    <div class="">

    
    <section id="promo" class="home-slider">
             

                  <div id="slider" class="sl-slider-wrapper">
            
            <asp:Repeater runat="server" ID="rptBannerList">
                <ItemTemplate>
                    <div class="sl-slider">
                        <div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15"
                             data-slice1-scale="0" data-slice2-scale="0">
                            <div class="sl-slide-inner">

                                <a href="<%# ( (!string.IsNullOrEmpty(Eval("Link_TR").ToString())) ? Eval("Link_TR") : ((Eval("Document_TR") != null) ? ("/admin" + Eval("Document_TR")) : "#") ) %>" 
                                    target="<%# ( (Convert.ToBoolean(Eval("Tab").ToString())) ? "_blank" : "_self" ) %>">
                                    <div class="bg-img item fadein" style='background-image: url(/admin<%#Eval("Image_TR") %>)'></div>
                                </a>

                                <%--<a href='<%# ((Container.ItemIndex == 1) ? "/medical_activity_reports/Liv_faliyet_Raporu_Baskı_Final_2013.pdf" : ((Container.ItemIndex == 2) ? "/pdf/oktar-robot.pdf" : "#") )  %>' target="_blank">
                                    <div class="bg-img item fadein" style='background-image: url(/admin<%#Eval("Image_TR") %>)'></div>
                                </a>--%>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <%--<nav id="nav-dots" class="nav-dots">
                <asp:Repeater ID="rptBannerSpanList" runat="server">
                    <ItemTemplate>
                        <span  class='<%# (Container.ItemIndex == 0) ? "nav-dot-current" : "" %>'></span>
                    </ItemTemplate>
                </asp:Repeater>
            </nav>--%>
            <nav id="nav-arrows" class="nav-arrows">
                <span class="nav-arrow-prev">Previous</span>
                <span class="nav-arrow-next">Next</span>
            </nav>
        </div>

            <nav id="nav-dots" class="nav-dots">
                <asp:Repeater ID="rptBannerSpanList" runat="server">
                    <ItemTemplate>
                        <span  class='<%# (Container.ItemIndex == 0) ? "nav-dot-current" : "" %>'></span>
                    </ItemTemplate>
                </asp:Repeater>
            </nav>
            
        </div>
        <div id="widget">
            <ul class="tabs">
                <li><a href="#" id="lnkTab1" runat="server" data-tab="tab1" class="tab1"><i class="arrow"></i> Doktor Ara</a></li>
                <li><a href="#" id="lnkTab2" runat="server" data-tab="tab2" class="tab2"><i class="arrow"></i> Randevu Al</a></li>
                <li><a href="#" id="lnkTab3" runat="server" data-tab="tab3" class="tab3"><i class="arrow"></i> Sonuçları Gör</a></li>
            </ul>
            <div class="tabcontainers">

                <div id="tab1" class="tabcontent">

                    <div class="filters-select">
                        <%--<form action="" id="filterDoktor" name="filterDoktor">--%>
                            <ul><!-- TODO Listede neler olacak -->
                                <li>
                                    <span class="selectbox size3 alt">
                                    <select class="customselect " name="ihastane" id="ihastane" runat="server"></select>
                                    </span>
                                </li>
                                <li> <span class="selectbox size3 alt">
                                    <select class="customselect " name="ibolum" id="ibolum" runat="server">
                                        <option value="">Bölüm Seçiniz</option>
                                    </select>
                                     </span>
                                </li>
                                <li> <span class="selectbox size3 alt" id="spn">
                                    <select class="customselect " name="iname" id="iname" runat="server">
                                        <option value="">Doktor Seçiniz</option>
                                    </select></span>
                                </li>
                                <%--<li> <span class="selectbox size3 alt">
                                    <select class="customselect  " name="ihastalik" id="ihastalik">
                                        <option value="">Hastalığa göre</option>
                                        <option value="1">Anemi</option>
                                    </select></span>
                                </li>--%>
                                <li>
                                    <%--<button id="btn-search" class="btn-search">Ara</button>--%>
                                    <button id="btn_search_doktor" class="btn-search" runat="server">Ara</button>
                                </li>
                            </ul>
                        <%--</form>--%>
                    </div>

                    <input type="hidden" id="hdChar" runat="server" />
                    <%--<div id="filterDoktor-alphabetic" class="filters-alphabetic">--%>
                    <div id="home-doktor-filters-alphabetic" class="filters-alphabetic">
                        <h5>Alfabetik Sıralama</h5>
                        <ul>
                            <li><a href="" data-val="A">A</a></li>
                            <li><a href="" data-val="B">B</a></li>
                            <li><a href="" data-val="C">C</a></li>
                            <li><a href="" data-val="Ç">Ç</a></li>
                            <li><a href="" data-val="D">D</a></li>
                            <li><a href="" data-val="E">E</a></li>
                            <li><a href="" data-val="F">F</a></li>
                            <li><a href="" data-val="G">G</a></li>
                            <li><a href="" data-val="H">H</a></li>
                            <li><a href="" data-val="I">I</a></li>
                            <li><a href="" data-val="İ">İ</a></li>
                            <li><a href="" data-val="J">J</a></li>
                            <li><a href="" data-val="K">K</a></li>
                            <li><a href="" data-val="L">L</a></li>
                            <li><a href="" data-val="M">M</a></li>
                            <li><a href="" data-val="N">N</a></li>
                            <li><a href="" data-val="O">O</a></li>
                            <li><a href="" data-val="Ö">Ö</a></li>
                            <li><a href="" data-val="P">P</a></li>
                            <li><a href="" data-val="Q">Q</a></li>
                            <li><a href="" data-val="R">R</a></li>
                            <li><a href="" data-val="S">S</a></li>
                            <li><a href="" data-val="Ş">Ş</a></li>
                            <li><a href="" data-val="T">T</a></li>
                            <li><a href="" data-val="U">U</a></li>
                            <li><a href="" data-val="Ü">Ü</a></li>
                            <li><a href="" data-val="V">V</a></li>
                            <li><a href="" data-val="W">W</a></li>
                            <li><a href="" data-val="X">X</a></li>
                            <li><a href="" data-val="Y">Y</a></li>
                            <li><a href="" data-val="Z">Z</a></li>
                        </ul>
                    </div>

                </div>

                <div id="tab2" class="tabcontent">
                    <div class="filters-select">
                        <asp:HiddenField runat="server" ID="hdTabId"/>
                        <%--<form action="" id="filterRandevu" name="filterRandevu">--%>
                        <ul>
                            <li> <span class="selectbox size4 alt">
                                <%--<select class="customselect " name="irhospital" id="irhospital">
                                    <option value="">Hastane</option>
                                    <option value="1">VM Medical Park</option>
                                </select>--%>
                                <asp:DropDownList ID="drpAppointmentHospitalList" AutoPostBack="true" OnTextChanged="drpAppointmentHospitalList_TextChanged"
                                    CssClass="customselect" runat="server"></asp:DropDownList>
                                </span>
                            </li>

                            <li> <span class="selectbox size4 alt">
                                <%--<select class="customselect " name="ibolum" id="ibolum2">
                                    <option value="">Bölüm</option>
                                    <option value="1">Ağız</option>
                                    <option value="2">Beyin</option>
                                </select>--%> 
                                <asp:DropDownList ID="drpAppointmentBranchList" AutoPostBack="true" OnTextChanged="drpAppointmentBranchList_TextChanged"
                                    CssClass="customselect" runat="server">
                                    <asp:ListItem Text="Bölüm Seçiniz" Value=""></asp:ListItem>
                                </asp:DropDownList>  
                                 </span>
                            </li>

                            <li> <span class="selectbox size4 alt">
                                <%--<select class="customselect " name="iuzman" id="iuzman">
                                    <option value="">Uzman</option>
                                    <option value="1">Lorem</option>
                                    <option value="2">Ipsum</option>
                                </select>--%>   
                                <asp:DropDownList ID="drpAppointmentDoctorList" CssClass="customselect" runat="server">
                                    <asp:ListItem Text="Doktor Seçiniz" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                 </span>
                            </li>

                            <li>
                                <%--<button id="btn-randevus" class="btn btn-type2">RANDEVU AL</button>--%>
                                <asp:Button ID="btnAppointment" runat="server" CssClass="btn btn-type2" Text="RANDEVU AL" OnClick="btnAppointment_Click" />
                            </li>
                        </ul>
                        <%--</form>--%>
                        <div class="clear"></div>
                        <p>&nbsp; </p>

                      <%--  <p>Tahlilinizi hangi hastanemizde yaptırdıysanız işaretleyiniz.<br>
                            Sisteme kayıtlı olduğunuz 11 haneli TC kimlik numaranızı girmeniz gerekmektedir. </p>--%>
                    </div>
                </div>

                <div id="tab3" class="tabcontent">
                    <div class="filters-select middle">
                        <%--<form action="" id="filterSonuc" name="filterSonuc">--%>
                        <ul>
                            <li> <span class="selectbox size5 alt">
                                <%--<select class="customselect med" name="ishospital" id="ishospital">
                                    <option value="">Hastane</option>
                                    <option value="1">VM Medical Park</option>
                                </select> --%>
                                <asp:DropDownList ID="drpEResultHospitalList" runat="server" CssClass="customselect med">
                                    <asp:ListItem Text="Hastane Seçiniz" Value=""></asp:ListItem>
                                    <asp:ListItem Text="VM Medical Park Kocaeli" Value="412"></asp:ListItem>
                                </asp:DropDownList>
                              </span>
                            </li>

                            <li>
                                <%--<input type="text" name="itck" id="itck" placeholder="T.C. Kimlik Numaranız"
                                        value="T.C. Kimlik Numaranız" class="itext"
                                        onkeypress="return numberKey(event)"/>--%>
                                <asp:TextBox ID="txtIdentityNo" CssClass="itext" placeholder="T.C. Kimlik Numaranız" MaxLength="11" 
                                    style="padding: 13px !important;" runat="server"></asp:TextBox>
                            </li>

                            <li>
                                <%--<button id="btn-rsearch" class="btn btn-type2">ŞİFRE AL</button>--%>
                                <asp:Button ID="btnFirstSMS" CssClass="btn btn-type2" Text="ŞİFRE AL" runat="server" OnClick="btnFirstSMS_OnClick" />
                            </li>
                        </ul>
                        <%--</form>--%>
                        <div class="clear"></div>
                        <div id="divFirstErrorResult" style="color: red;width: 100%;float: left;margin-left: 8px;display: none;" runat="server"></div>
                        <p>&nbsp;</p>
                        <p>Tahlilinizi hangi hastanemizde yaptırdıysanız işaretleyiniz.<br>
                            Sisteme kayıtlı olduğunuz 11 haneli TC kimlik numaranızı girmeniz gerekmektedir. </p>
                    </div>
                </div>

            </div>

        </div>
   
    </section>

   <div class="medikalteknoloji-slider full-width">
        <h1 class="full-width align-center"><a class="med-tec" href="#">Medikal Teknolojilerimiz</a></h1>

        <p class="full-width align-center">Lorem ipsum this come get in fat
            velit.</p>
        <div class="container">

            <asp:Repeater ID="rptMedUnitList" runat="server">
                <HeaderTemplate><ul class="medikalteknoloji"></HeaderTemplate>
                <ItemTemplate>
                    <li>
                        <div class="photo"><img src="/admin/<%#Eval("Icon") %>" alt="<%#Eval("Title_TR") %>"/></div>
                        <span class="title"> <%#Eval("Title_TR") %></span>
                    </li>
                </ItemTemplate>
                <FooterTemplate></ul></FooterTemplate>
            </asp:Repeater>
        </div>
    </div>

    <div class="full-width">
        <div id="home-news">
            <h1 class="h1-home">
                Haberler
            </h1>

            <div class="home-news-item">

                <asp:Repeater runat="server" ID="rptNewsList">
                    <ItemTemplate>
                        <div class="home-news-item-description">
                            <div class="date-label">
                                <strong><%# (!string.IsNullOrEmpty(Eval("Date").ToString())) ? Convert.ToDateTime(Eval("Date").ToString()).Date.Day.ToString() : "" %></strong>
                                <span><%# (!string.IsNullOrEmpty(Eval("Date").ToString())) ? Convert.ToDateTime(Eval("Date").ToString()).Date.ToString("MMMM", new CultureInfo("tr-TR")).ToUpper() : "" %></span>
                                <em><%# (!string.IsNullOrEmpty(Eval("Date").ToString())) ? Convert.ToDateTime(Eval("Date").ToString()).Date.Year.ToString() : "" %></em>
                            </div>
                            <div class="clear"></div>
                            <h2>
                                <%#Eval("Title_TR") %>
                            </h2>

                            <div>
                                <%# (!string.IsNullOrEmpty(Eval("ShortContent_TR").ToString())) ? ((Eval("ShortContent_TR").ToString().Length > 200) ? ((Eval("ShortContent_TR").ToString().Contains("<span style=")) ? Eval("ShortContent_TR").ToString().Substring(0, 200) + " ..." : Eval("ShortContent_TR").ToString().Substring(0, 100) + " ...") : Eval("ShortContent_TR").ToString()) : "" %>
                            </div>

                            <div class="btn-container">
                                <a class="btn-white" href="/haberler">Devamını Oku</a>
                                <a class="btn-gray" href="/haberler">Tüm Etkinlikler</a>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:Repeater>


                
            </div>
            <div class="clear"></div>
        </div>

        <div class="home-events">
            <h1 class="h1-home">Etkinlikler</h1>

            <div class="pull-right full-width">

                <asp:Repeater runat="server" ID="rptActivityList">
                    <ItemTemplate>
                        <div class="home-news-item-description">
                            <div class="date-label">
                                <strong><%# (!string.IsNullOrEmpty(Eval("Date").ToString())) ? Convert.ToDateTime(Eval("Date").ToString()).Date.Day.ToString() : "" %></strong>
                                <span><%# (!string.IsNullOrEmpty(Eval("Date").ToString())) ? Convert.ToDateTime(Eval("Date").ToString()).Date.ToString("MMMM", new CultureInfo("tr-TR")).ToUpper() : "" %></span>
                                <em><%# (!string.IsNullOrEmpty(Eval("Date").ToString())) ? Convert.ToDateTime(Eval("Date").ToString()).Date.Year.ToString() : "" %></em>
                            </div>
                            <div class="clear"></div>
                            <h2>
                                <%#Eval("Title_TR") %>
                            </h2>

                            <div>
                                <%# (!string.IsNullOrEmpty(Eval("ShortContent_TR").ToString())) ? ((Eval("ShortContent_TR").ToString().Length > 200) ? ((Eval("ShortContent_TR").ToString().Contains("<span style=")) ? Eval("ShortContent_TR").ToString().Substring(0, 200) + " ..." : Eval("ShortContent_TR").ToString().Substring(0, 100) + " ...") : Eval("ShortContent_TR").ToString()) : "" %>
                            </div>

                            <div class="btn-container">
                                <a class="btn-white" href="/etkinlikler.aspx">Devamını Oku</a>
                                <a class="btn-gray" href="/etkinlikler.aspx">Tüm Etkinlikler</a>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
        <div class="clear"></div>
    </div>


   <div class="full-width" id="home-bottom-ctas">
        <div class="home-bottom-cta-item mikro-siteler">
            <h1>Mikro<br> Sitelerimiz</h1>
            <ul>
                <li><a href="micro-siteler.aspx">Kanser Merkezi</a></li>
                <li><a href="micro-siteler.aspx">Kök Hücre Merkezi</a></li>
                <li><a href="micro-siteler.aspx">Hemşirelik Hizmetleri</a></li>
                <li><a href="micro-siteler.aspx">Devamı...</a></li>
            </ul>
            <img src="assets/img/content/bg-home-cta-1.jpg" alt=""/>
        </div>
        <div class="home-bottom-cta-item tibbi-faaliyet-raporu">          
			<h1>Tanıtım </br> Filmlerimiz</h1>			
			<a href="tanitim-filmlerimiz" class="download">Tanıtım Filmlerimizi<br> Buradan İzleyebilirsiniz</a>  
			<img src="assets/img/content/bg-home-cta-2.jpg" alt=""/>			
        </div>
        <div class="home-bottom-cta-item ebulten">
            <div class="blue-form ebulten">
               
                
              
                    <input type="text" placeholder="e-posta" style="       margin: 68% auto;
    width: 63%;
    margin-bottom: 4px;"/>
                
                       
                  <input type="checkbox" name="checkbox" value="value" style="    margin-left: 120px;">
                  <a href="javascript:" id="btn-kullanim-kosullari">Kullanım Koşulları</a>'nı okudum,
                 kabul ediyorum.

                    <button class="btn btn-type2 f-right" id="" style="  position: absolute;
    margin-top: 36px;
    margin-left: -85px;">Gönder</button>
                    <div class="clear"></div>
            

            </div>
            <a href="">
                <h1>E-Bülten</h1>
                <p>Bizden Haber Almak İçin Lütfen Üye Olunuz</p>                
            </a>
			<img src="assets/img/content/bg-home-cta-3.jpg" alt=""/>
        </div>
        <div class="clear"></div>
    </div>

</div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterBottomContent" runat="server">

    <script src="/assets/js/jquery-1.9.1.min.js"></script>
    <script src="/assets/js/plugins.js"></script>
    <script src="/assets/js/jquery.bxslider.min.js"></script>
    <script src="/assets/js/modernizr.custom.79639.js"></script>
    <script src="/assets/js/jquery.slitslider.js"></script>

    <script src="/assets/js/liv_global.js"></script>
   <%-- <script>

       $(function(){
            $('.searchbtn').on("click",function(){
                var $this = $(this);
                if($this.hasClass("open")){
                    $('.frmPageSearch').animate({
                        top:-100
                    },300,function(){
                        $('.frmPageSearch').removeClass("open");
                    });
                } else {
                    $('.frmPageSearch').animate({
                        top:28
                    },300,function(){
                        $this.addClass("open");
                    });
                }
            });
       })
    </script>

    <script type="text/javascript" src="/assets/mask/jquery.maskedinput.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#txtIdentityNo').mask('11111111111');
            $('#Content_txtIdentityNo').mask('11111111111');
            $('#Content_txtCancelIdentityNo').mask('11111111111');
            
        });
    </script>--%>


    <script type="text/javascript">

        $(window).load(function () {
            SetClass2(document.getElementById('Content_hdTabId').value);
        });

        function SetClass2(id) {
            if (id != "") {
                for (var i = 1; i <= 3; i++) {
                    var current_tab = "Content_lnkTab" + id;
                    var tab = "Content_lnkTab" + i;
                    if (current_tab == tab) {
                        var clss = "tab" + id;
                        //document.getElementById('lnkTab' + id).className = clss;
                        document.getElementById('Content_lnkTab' + id).click();
                    } else {
                        //document.getElementById('lnkTab' + i).className = "";
                    }
                }
            }
        }

        function SetAppointment() {
            window.location = "/online-hizmetlerimiz/e-randevu?hospitalId=" + document.getElementById('drpAppointmentHospitalList').value +
                                                    "&branchId=" + document.getElementById('drpAppointmentBranchList').value +
                                                    "&doctorId=" + document.getElementById('drpAppointmentDoctorList').value;
        }

    </script>

    <script type="text/javascript" src="/assets/mask/jquery.maskedinput.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#txtIdentityNo').mask('11111111111');
            $('#Content_txtIdentityNo').mask('11111111111');
            $('#Content_txtCancelIdentityNo').mask('11111111111');

        });
    </script>

</asp:Content>
