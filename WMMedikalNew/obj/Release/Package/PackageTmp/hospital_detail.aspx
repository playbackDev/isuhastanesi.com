﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="hospital_detail.aspx.cs" Inherits="WMMedikalNew.hospital_detail" %>
<%@ Register Src="~/usercontrol/pageSubMenu.ascx" TagPrefix="uc1" TagName="pagesubmenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  
        <div class="sub-page-title bg-hastanemiz1" runat="server" id="topbanner">

        
        <div class="container">
            
        </div>
            <uc1:pagesubmenu runat="server" ID="pagesubmenu" />
    </div>

    <div class="container content">
        <div class="sub-content">
            <h1 class="huge">
                <asp:Literal ID="ltrHospitalName" runat="server"></asp:Literal></h1>
            <div class="content-section">
                <div class="grid2">
                    <asp:Repeater ID="PageGallery" runat="server">
                        <itemtemplate>
                              <img src="../<%#Eval("image_name") %>" alt="">
                          </itemtemplate>
                    </asp:Repeater>
                </div>
                <div class="grid4 content">

                    <asp:Literal ID="ltrDescription" runat="server"></asp:Literal>

                </div>
            </div>

        </div>
    </div>
</asp:Content>
