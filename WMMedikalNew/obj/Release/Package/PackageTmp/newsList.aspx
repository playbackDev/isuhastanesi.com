﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="newsList.aspx.cs" Inherits="WMMedikalNew.news1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="sub-page-title bg-6">
        <div class="container">
            <h1 class="middle-content" style="margin-top: 128px;"><%=Resources.Lang.Haberler %></h1>
        </div>
    </div>
    <div class="section-line">
        <div class="breadcrumb2">
            <a href="/">anasayfa &gt; </a>
            <asp:HyperLink ID="hypBreadCrumb" runat="server"><%=Resources.Lang.Haberler %></asp:HyperLink>
    
        </div>
    </div>
    <div class="container">
        <asp:Repeater ID="rptNewList" runat="server">
            <ItemTemplate>
        <div class="accordion-content news-content">
            <h2 style="display: block;"><%#Eval("news_title") %></h2>


            <div class="accordion-left">
                <img src="<%#Eval("news_image") %>" alt="">
            </div>
            <div class="accordion-right">
         <%#Eval("news_content") %>
            
               


                    <div class=" news-trg-container" style="display: none;">
                    <img src="/assets/img/site/ico-accordion-arrow.png" alt="" style="cursor: pointer;">
                </div>

            </div>

            <div class="clear"></div>
        </div>
            </ItemTemplate>
            </asp:Repeater>
            
    </div>
</asp:Content>
