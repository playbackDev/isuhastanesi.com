﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="e-randevu.aspx.cs" Inherits="WMMedikalNew.WebForm7" ValidateRequest="false" EnableEventValidation="false"  %>
<%@ Register Src="~/usercontrol/pageSubMenu.ascx" TagPrefix="uc1" TagName="pagesubmenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <link rel="stylesheet" href="/assets/fancybox/jquery.fancybox.css" />


    <style type="text/css">
        .table2 td {
            margin-top: -6px !important;
        }

        .form-group .item-detail .customSelect, .form-group .item-detail .customSelectInner {
            width: 67px !important;
        }

        #divBirthDate span {
            width: 67px !important;
        }

        .sub-page-sub-nav a.four-col {
            width: calc(20% - 3px);
        }

        #ContentPlaceHolder1_drpDepartmentList,
        #ContentPlaceHolder1_drpDoctorList {
            width: 272px !important;
        }
      
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="sub-page-title bg-online-hiz">
        <div class="container" style="padding-top:150px;">
            <h1 class="middle-content1" style="margin-top: 168px !important; width: 50%"><%=Resources.Lang.ERandevu %></h1>
        </div>
         <uc1:pagesubmenu runat="server" ID="pagesubmenu" />
        <%--    <div class="sublinks">
            <div class="container">
                <ul class="clearfix">
                    <li><a href="/online-hizmetlerimiz/e-randevu">E-Randevu</a></li>
                    <li><a href="/online-hizmetlerimiz/e-sonuc">E-Sonuç</a></li>
                </ul>

            </div>
        </div>--%>
    </div>
    <asp:HiddenField runat="server" ID="hdTabId" />
    <asp:HiddenField runat="server" ID="hdHospitalName" />
    <asp:HiddenField runat="server" ID="hdHospitalId" />
    <asp:HiddenField runat="server" ID="hdDepartmentId" />
    <asp:HiddenField runat="server" ID="hdDoctorId" />
    <asp:HiddenField runat="server" ID="hdAppointmentStartEndDateTime" />
    <div class="container content pad20">
        <div class="">

            <div class="item-detail">
                <ul class="tab fiveitem">
                    <li>
                        <a href="" id="lnkTab1" runat="server" data-tab="tab1" class=""><i class="ico1 rimg"></i>Hastane</a>
                    </li>
                    <li>
                        <a href="" id="lnkTab2" runat="server" data-tab="tab2"><i class="ico2 rimg"></i>Bölüm</a>
                    </li>
                    <li>
                        <a href="" id="lnkTab3" runat="server" data-tab="tab3"><i class="ico3 rimg"></i>Uzman</a>
                    </li>

                    <li>
                        <a href="" id="lnkTab4" runat="server" data-tab="tab4"><i class="ico4 rimg"></i>Tarih & Saat</a>
                    </li>

                    <li>
                        <a href="" id="lnkTab5" runat="server" data-tab="tab5"><i class="ico5 rimg"></i>Kişisel Bilgiler</a>
                    </li>
                </ul>


                <div class="tabcontainer wide">
                    <div class="tabcontent select" id="tab1">
                        <p>Hastane seçiniz</p>
                        <div class="radio-buttons">
                            <asp:Repeater runat="server" ID="rptHospitalList">
                                <ItemTemplate>
                                    <span class="btn btn-default btn-radio" style="cursor: pointer;"
                                        onclick="javascript:SetHospital('<%#Eval("HospitalId") %>', '<%#Eval("HospitalName") %>');"><%#Eval("HospitalName") %></span>
                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:Literal ID="ltrUyari" runat="server"></asp:Literal>
                        </div>
                        <asp:Button runat="server" ID="btnHospitalNext" CssClass="btn btn-type2 btn-absolute" Text="İleri >>" OnClick="btnHospitalNext_Click" />

                    </div>

                    <div class="tabcontent" id="tab2">
                        <p>Bölüm seçiniz</p>

                        <asp:DropDownList runat="server" ID="drpDepartmentList" onchange="javascript:SetDepartment(this.value);" CssClass="drpSelect">
                            <asp:ListItem Text="Bölüm Seçiniz" Value=""></asp:ListItem>
                        </asp:DropDownList>
                        <a class="btn btn-type2 btn-absolute btn-back" data-tab="tab1" href="javascript:SetClass2('1');"><< Geri</a>

                        <asp:Button runat="server" CssClass="btn btn-type2 btn-absolute" ID="btnDepartmentNext" Text="İleri >>"  OnClick="btnDepartmentNext_Click"/>
                    </div>

                    <div class="tabcontent" id="tab3">
                        <p>Uzman seçiniz</p>
                        <asp:DropDownList runat="server" ID="drpDoctorList" onchange="javascript:SetDoctor(this.value);" CssClass="drpSelectDoctor">
                            <asp:ListItem Text="Doktor Seçiniz" Value=""></asp:ListItem>
                        </asp:DropDownList>
                        <a class="btn btn-type2 btn-absolute btn-back" data-tab="tab2" href="javascript:SetClass2('2');"><< Geri</a>
                        <asp:Button runat="server" CssClass="btn btn-type2 btn-absolute" ID="btnDoctorNewxt" Text="İleri >>" OnClick="btnDoctorNewxt_Click" />
                    </div>

                    <div class="tabcontent" id="tab4">
                        <p>Tarih ve saat seçiniz.</p>
                        <div class="calendar">
                            <%--<a href="javascript:;" class="rimg arrow-left"></a>
                                <a href="javascript:;" class="rimg arrow-right"></a>--%>
                            <asp:LinkButton CssClass="rimg arrow-left" runat="server" ID="lnkPrevDateTimeList" OnClick="lnkPrevDateTimeList_Click"></asp:LinkButton>
                            <asp:LinkButton CssClass="rimg arrow-right" runat="server" ID="lnkNextDateTimeList" OnClick="lnkNextDateTimeList_Click"></asp:LinkButton>

                            <table class="table2">
                                <thead>
                                    <tr>
                                        <asp:Repeater runat="server" ID="rptDateTimeTitleList">
                                            <ItemTemplate>
                                                <th>
                                                    <div class="day"><%#Eval("DayName") %></div>
                                                    <div class="date"><%# (!string.IsNullOrEmpty(Eval("DateFrom").ToString())) ? Convert.ToDateTime(Eval("DateFrom").ToString()).Date.ToString("dd.MM.yyyy") : "" %></div>
                                                </th>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tr>
                                </thead>
                            </table>
                            <div class="tablescroll" id="divDTContent" runat="server"></div>

                        </div>
                        <a class="btn btn-type2 btn-absolute btn-back" data-tab="tab3" href="javascript:SetClass2('3');"><< Geri</a>
                        <%--<a href="" class="btn btn-type2 btn-absolute">İleri >></a>--%>
                        <asp:Button runat="server" CssClass="btn btn-type2 btn-absolute" ID="btnDateTimeNext" Text="İleri >>"  OnClick="btnDateTimeNext_Click"/>
                    </div>

                    <div class="tabcontent" id="tab5">
                        <div class="form " id="divUserInfo" runat="server">
                            <%--<form action="" name="frmRegister" id="frmRegister">--%>

                            <div class="column">

                                <div class="form-group">
                                    <label for="iname" class="input-label">TC Kimlik No *</label>
                                    <%--<input type="text" class="itext " name="itck" id="itck" maxlength="11" onkeypress="return numberKey(event)" />--%>
                                    <asp:TextBox runat="server" ID="txtIdentityNo" CssClass="itext" MaxLength="11" placeholder="tc kimlik no"></asp:TextBox>
                                    <asp:Label runat="server" ID="lblIdentityNo" Style="color: red; display: none; margin-left: 130px; margin-top: 48px;">'TC Kimlik No' alanı zorunludur.</asp:Label>
                                </div>

                                <div class="form-group">
                                    <label for="iname" class="input-label">Adınız *</label>
                                    <%--<input type="text" class="itext " name="iname" id="iname" maxlength="50" />--%>
                                    <asp:TextBox runat="server" ID="txtName" CssClass="itext" placeholder="adınız"></asp:TextBox>
                                    <asp:Label runat="server" ID="lblName" Style="color: red; display: none; margin-left: 130px; margin-top: 48px;">'Adınız' alanı zorunludur.</asp:Label>
                                </div>


                                <div class="form-group">
                                    <label for="itel" class="input-label">Cep Telefonunuz *</label>
                                    <%--<input type="text" class="itext required" name="itel" id="itel" maxlength="15" onkeypress="return numberKey(event)" />--%>
                                    <asp:TextBox runat="server" ID="txtGSM" CssClass="itext" MaxLength="15" placeholder="5xxxxxxxxx"></asp:TextBox>
                                    <asp:Label runat="server" ID="lblGSM" Style="color: red; display: none; margin-left: 130px; margin-top: 48px;">'Cep Telefonunuz' alanı zorunludur.</asp:Label>
                                </div>

                                <div class="form-group">
                                    <label for="igender0" class="input-label">Cinsiyetiniz *</label>
                                    <%--<label class="formlabel"> <input type="radio" name="igender" id="igender0" checked="checked"/> Kadın</label>
                                        <label class="formlabel"> <input type="radio" name="igender" id="igender1"/> Erkek</label>--%>
                                    <asp:RadioButtonList runat="server" RepeatColumns="2" ID="rdGender">
                                        <asp:ListItem Text="Kadın" Value="F"></asp:ListItem>
                                        <asp:ListItem Text="Erkek" Value="M"></asp:ListItem>
                                    </asp:RadioButtonList>
                                    <asp:Label runat="server" ID="lblGender" Style="color: red; display: none; margin-left: 130px;">'Cinsiyetiniz' alanı zorunludur.</asp:Label>
                                </div>

                            </div>

                            <div class="column">
                                <div class="form-group">
                                    <label for="imail" class="input-label">E-posta *</label>
                                    <%--<input type="text" class="itext required" name="imail" id="imail" data-type="mail" maxlength="90" />--%>
                                    <asp:TextBox runat="server" ID="txtEmail" CssClass="itext" placeholder="e-posta"></asp:TextBox>
                                    <asp:Label runat="server" ID="lblEmail" Style="color: red; display: none; margin-left: 130px; margin-top: 48px;">'E-posta' alanı zorunludur.</asp:Label>
                                </div>



                                <div class="form-group">
                                    <label for="isurname" class="input-label">Soyadınız *</label>
                                    <%--<input type="text" class="itext " name="isurname" id="isurname" maxlength="50" />--%>
                                    <asp:TextBox runat="server" ID="txtSurname" CssClass="itext" placeholder="soyadınız"></asp:TextBox>
                                    <asp:Label runat="server" ID="lblSurname" Style="color: red; display: none; margin-left: 130px; margin-top: 48px;">'Soyadınız' alanı zorunludur.</asp:Label>
                                </div>

                                <div class="form-group" id="divBirthDate">
                                    <label for="iday" class="input-label">Doğum Tarihiniz *</label>

                                    <asp:DropDownList runat="server" ID="drpDayList" CssClass=" select-alt small">
                                        <asp:ListItem Value="">gün</asp:ListItem>
                                        <asp:ListItem Value="01">1</asp:ListItem>
                                        <asp:ListItem Value="02">2</asp:ListItem>
                                        <asp:ListItem Value="03">3</asp:ListItem>
                                        <asp:ListItem Value="04">4</asp:ListItem>
                                        <asp:ListItem Value="05">5</asp:ListItem>
                                        <asp:ListItem Value="06">6</asp:ListItem>
                                        <asp:ListItem Value="07">7</asp:ListItem>
                                        <asp:ListItem Value="08">8</asp:ListItem>
                                        <asp:ListItem Value="09">9</asp:ListItem>
                                        <asp:ListItem Value="10">10</asp:ListItem>
                                        <asp:ListItem Value="11">11</asp:ListItem>
                                        <asp:ListItem Value="12">12</asp:ListItem>
                                        <asp:ListItem Value="13">13</asp:ListItem>
                                        <asp:ListItem Value="14">14</asp:ListItem>
                                        <asp:ListItem Value="15">15</asp:ListItem>
                                        <asp:ListItem Value="16">16</asp:ListItem>
                                        <asp:ListItem Value="17">17</asp:ListItem>
                                        <asp:ListItem Value="18">18</asp:ListItem>
                                        <asp:ListItem Value="19">19</asp:ListItem>
                                        <asp:ListItem Value="20">20</asp:ListItem>
                                        <asp:ListItem Value="21">21</asp:ListItem>
                                        <asp:ListItem Value="22">22</asp:ListItem>
                                        <asp:ListItem Value="23">23</asp:ListItem>
                                        <asp:ListItem Value="24">24</asp:ListItem>
                                        <asp:ListItem Value="25">25</asp:ListItem>
                                        <asp:ListItem Value="26">26</asp:ListItem>
                                        <asp:ListItem Value="27">27</asp:ListItem>
                                        <asp:ListItem Value="28">28</asp:ListItem>
                                        <asp:ListItem Value="29">29</asp:ListItem>
                                        <asp:ListItem Value="30">30</asp:ListItem>
                                        <asp:ListItem Value="31">31</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList runat="server" ID="drpMonthList" CssClass=" select-alt small">
                                        <asp:ListItem Value="">ay</asp:ListItem>
                                        <asp:ListItem Value="01">1</asp:ListItem>
                                        <asp:ListItem Value="02">2</asp:ListItem>
                                        <asp:ListItem Value="03">3</asp:ListItem>
                                        <asp:ListItem Value="04">4</asp:ListItem>
                                        <asp:ListItem Value="05">5</asp:ListItem>
                                        <asp:ListItem Value="06">6</asp:ListItem>
                                        <asp:ListItem Value="07">7</asp:ListItem>
                                        <asp:ListItem Value="08">8</asp:ListItem>
                                        <asp:ListItem Value="09">9</asp:ListItem>
                                        <asp:ListItem Value="10">10</asp:ListItem>
                                        <asp:ListItem Value="11">11</asp:ListItem>
                                        <asp:ListItem Value="12">12</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList runat="server" ID="drpYearList" CssClass=" select-alt small">
                                        <asp:ListItem Value="">yıl</asp:ListItem>
                                           <asp:ListItem Value="2016">2016</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <asp:Label runat="server" ID="lblDirthDate" Style="color: red; display: none; margin-left: 130px;">'Doğum Tarihiniz' alanı zorunludur.</asp:Label>

                                <div class="form-group">
                                    <div>
                                        <label>
                                            <input type="checkbox" name="iagree" id="iagree" value="1" />
                                            Randevu hatırlatma ve hizmet sunumu için kişisel bilgilerimin
                                                kullanılmasına izin veriyorum.
                                           
                                        </label>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <asp:ScriptManager runat="server"></asp:ScriptManager>
                                    <label for="imsg" class="input-label">Güvenlik Kodu </label>
                                    <asp:UpdatePanel ID="upCaptcha" runat="server">
                                        <ContentTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Image ID="imgCaptcha" BorderWidth="1px" CssClass="captcha-image" BorderColor="DarkBlue" runat="server" />
                                                    </td>
                                                    <td style="margin-left: 11px; margin-right: -15px;">
                                                        <asp:ImageButton ID="btnYenile" ValidationGroup="2" ImageUrl="/assets/img/captcha-refresh.png" runat="server"
                                                            CssClass="captcha-button" />
                                                    </td>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="txtCaptcha" runat="server" CssClass="itext" Width="98px"></asp:TextBox></td>

                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:Label runat="server" ID="lblSecError" Style="color: red; display: none; margin-left: 0px;">'Güvenlik Kodu' alanı zorunludur.</asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:TextBox ID="hdnCaptcha" runat="server" Style="display: none;"></asp:TextBox>
                                    <asp:Literal ID="ltrCaptcha" Visible="false" runat="server"></asp:Literal>
                                    <br />
                                    <br />
                                </div>

                            </div>
                            <%--<a href="" class="btn btn-type2 btn-absolute">RANDEVU AL >></a>--%>
                            <%--</form>--%>

                            <asp:Button runat="server" CssClass="btn btn-type2 btn-absolute" ID="btnAppointment" Text="RANDEVU AL >>" OnClick="btnAppointment_Click"
                                OnClientClick="return FormControl();" />

                            <div id="divAppointmentRslt" style="padding-top: 16px; font-size: 15px;" runat="server"></div>

                        </div>


                        <div class="completed form" id="divAppointmentResult" runat="server" style="display: none;">
                            <h6>Randevunuz onaylanmıştır.</h6>
                            <p>Detaylı bilgi belirtmiş olduğunuz e-posta adresinize gönderilmiştir.</p>
                            <div class="randevu-bilgileri">
                                <div class="column">
                                    <dl>
                                        <dt>Hastane</dt>
                                        <dd id="ddHospital" runat="server"></dd>
                                    </dl>
                                    <dl>
                                        <dt>Bölüm</dt>
                                        <dd id="ddBranch" runat="server"></dd>
                                    </dl>
                                    <dl>
                                        <dt>Uzman</dt>
                                        <dd id="ddDoctor" runat="server"></dd>
                                    </dl>
                                    <dl>
                                        <dt>Randevu Tarihi & Saati</dt>
                                        <dd id="ddDateTime" runat="server"></dd>
                                    </dl>
                                </div>
                                <div class="column">
                                    <dl>
                                        <dt>TC Kimlik Numarası</dt>
                                        <dd id="ddIdentityNo" runat="server"></dd>
                                    </dl>
                                    <dl>
                                        <dt>Telefon Numarası	</dt>
                                        <dd id="ddGSM" runat="server"></dd>
                                    </dl>
                                    <dl>
                                        <dt>Randevu Numarası</dt>
                                        <dd id="ddAppointmentNo" runat="server"></dd>
                                    </dl>
                                </div>
                                <div class="clear"></div>
                                <p>&nbsp;</p>
                                <%--<a href="javascript:;" class="btn btn-type2 fright"><i class="ico-printer rimg"></i>YAZDIR</a>
                                    <a href="javascript:;"  class="btn btn-type2 fright"><i class="ico-save rimg"></i>KAYDET</a>--%>

                                <p class="notes">
                                    Randevunuzu değiştirmek veya iptal etmek için çağrı merkezimizi
                                    <br>
                                    (0850 222 2 548) aramanız gerekmektedir.
                                                <br>
                                    E-Randevu sistemini kullandığınız için teşekkür eder, sağlıklı günler dileriz.
                                </p>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="notes">
                    <%--<div class="user fright">Aktif 22 kullanıcı</div>--%>
                    <strong>Not:</strong> Hızlı Randevu işlemleri sonunda size Randevu Numarası verilir ve Eposta gönderilir.<br>
                    <%-- Muayene ücret bilgisini Çağrı Merkezimizden (444 44 84) alabilirsiniz.--%>
                </div>
            </div>
            <p>&nbsp;</p>

        </div>
        <style>
            .drpSelect {
                width: 302px;
                height: 43px;
                background: url("/assets/img/site/btn-select.png") no-repeat 100% 0;
                padding: 7px;
                background-position: 97%;
                -webkit-appearance: none;
                border: solid 1px #c5d0db;
                border-radius: 5px;
            }

                 .drpSelectDoctor {
                width: 302px;
                height: 43px;
                background: url("/assets/img/site/btn-select.png") no-repeat 100% 0;
                padding: 7px;
                background-position: 97%;
                -webkit-appearance: none;
                border: solid 1px #c5d0db;
                border-radius: 5px;
            }
        </style>
        <div class="sub-content red-border">

            <h5>Randevu İptali</h5>
            <hr class="red-hr" />

            <div class="column">

                <div class="form-group">
                    <label for="ihospital" class="input-label">Hastane seçiniz</label>

                    <asp:DropDownList runat="server" ID="drpCancelHospitalList" CssClass="drpSelect">
                        <asp:ListItem Text="Hastane Seçiniz" Value=""></asp:ListItem>
                    </asp:DropDownList>
                </div>

                <div class="form-group">
                    <label for="ino" class="input-label">Randevu Numaranız </label>

                    <asp:TextBox runat="server" ID="txtCancelAppointmentNo" CssClass="itext" placeholder="randevu numaranız"></asp:TextBox>
                </div>

                <div class="form-group">
                    <label for="itck" class="input-label">TC Kimlik Numarası</label>

                    <asp:TextBox runat="server" ID="txtCancelIdentityNo" CssClass="itext" MaxLength="11" placeholder="tc kimlik numaranız"></asp:TextBox>
                </div>


                <asp:Button runat="server" CssClass="btn btn-type2 fright" ID="btnCancelAppointment" Text="Randevu iptal et" OnClick="btnCancelAppointment_Click" />

            </div>
            <div id="divResult" style="padding-top: 16px; font-size: 15px;" runat="server"></div>



            <div class="clear"></div>
        </div>



    </div>

    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="/assets/js/plugins.js"></script>
    <script src="/assets/js/liv_global.js"></script>


    <script type="text/javascript">
        document.getElementById('mnOnline').className = "selected";
    </script>
    <script>
        $(document).ready(function () {
            $("#tab3 p").css('display', 'none');
            $("#tab3 .btn.btn-type2.btn-absolute").css('display', 'none');
            $("#tab3 #ContentPlaceHolder1_drpDoctorList").css('display', 'none');
            $("#tab3 .customselect").css('display', 'none');


            $("#ContentPlaceHolder1_lnkTab3").click(function () {

                $("#tab3 p").css('display', 'block');
                $("#tab3 .btn.btn-type2.btn-absolute").css('display', 'block');
                $("#tab3 #Content_drpDoctorList").css('display', 'block');
                $("#tab3 .drpSelectDoctor").css('display', 'block');
            });            $("#ContentPlaceHolder1_lnkTab1").click(function () {
                $("#tab3 p").css('display', 'none');
                $("#tab3 .btn.btn-type2.btn-absolute").css('display', 'none');
                $("#tab3 #Content_drpDoctorList").css('display', 'none');
                $("#tab3 .drpSelectDoctor").css('display', 'none');
            });
            $("#ContentPlaceHolder1_lnkTab2").click(function () {
                $("#tab3 p").css('display', 'none');
                $("#tab3 .btn.btn-type2.btn-absolute").css('display', 'none');
                $("#tab3 #Content_drpDoctorList").css('display', 'none');
                $("#tab3 .drpSelectDoctor").css('display', 'none');
            });
            $("#ContentPlaceHolder1_lnkTab4").click(function () {
                $("#tab3 p").css('display', 'none');
                $("#tab3 .btn.btn-type2.btn-absolute").css('display', 'none');
                $("#tab3 #Content_drpDoctorList").css('display', 'none');
                $("#tab3 .drpSelectDoctor").css('display', 'none');
            });
            $("#ContentPlaceHolder1_lnkTab5").click(function () {
                $("#tab3 p").css('display', 'none');
                $("#tab3 .btn.btn-type2.btn-absolute").css('display', 'none');
                $("#tab3 #Content_drpDoctorList").css('display', 'none');
                $("#tab3 .drpSelectDoctor").css('display', 'none');
            });
        });

    </script>
    <script type="text/javascript">

        $(window).load(function () {
            SetClass2(document.getElementById('ContentPlaceHolder1_hdTabId').value);
        });

        function SetClass2(id) {
            if (id != "") {
                for (var i = 1; i <= 5; i++) {
                    var current_tab = "lnkTab" + id;
                    var tab = "lnkTab" + i;
                    if (current_tab == tab) {
                        document.getElementById('ContentPlaceHolder1_lnkTab' + id).className = "select";
                        document.getElementById('ContentPlaceHolder1_lnkTab' + id).click();
                    } else {
                        document.getElementById('ContentPlaceHolder1_lnkTab' + i).className = "";
                    }
                }
            }
        }

    </script>

    <script type="text/javascript" src="/assets/mask/jquery.maskedinput.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#ContentPlaceHolder1_txtIdentityNo').mask('11111111111');
            $('#ContentPlaceHolder1_txtCancelIdentityNo').mask('11111111111');

        });
    </script>
</asp:Content>
