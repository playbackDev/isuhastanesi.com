﻿var middleContent = $('.middle-content');

middleContent.each(function () {
    $(this).css("margin-top", ($(this).parent().parent().height() - $(".sublinks").height() - $(this).outerHeight()) -50);
});

Accordion();

function Accordion() {
    var btnAccordion = $('.accordion-title'),
        accordionContent = $('.accordion-content');
        
    var doctorAcc = $('#divDoctorAccordion .accordion-title');
    doctorAcc.first().hide();
    doctorAcc.first().next().show();
           
    btnAccordion.on('click', function () {
            var a = $(this).find("a:first-child"); 
            var p = $(this).parents('.accordion-item');
            if(a.css("display")==="inline-block" && a.attr("href") !== undefined){
             location.href=a.attr("href");
             return false;
            }
        
            if(p.hasClass("open")){
                accordionContent.slideUp();
                p.removeClass("open");
            } else{
                $('.accordion-item.open').removeClass("open");
                    accordionContent.slideUp();
                btnAccordion.show(); 
                $(this).parents('.accordion-item').addClass("open");  
                $(this).next().slideDown().promise().done(function(){
                    $('html, body').animate({ scrollTop: $(this).offset().top-74 }, 'slow');

                });
            }
    });
}

function Accordionyed() {
    var btnAccordion = $('.accordion-title'),
        accordionContent = $('.accordion-content');
		
	var doctorAcc = $('#divDoctorAccordion .accordion-title');
    doctorAcc.first().hide();
    doctorAcc.first().next().show();

        //btnAccordion.first().hide();
    //btnAccordion.first().next().show();
           
    btnAccordion.on('click', function () {
		 var a = $(this).find("a:first-child");
        
            if(a.css("display")==="inline-block" && a.attr("href") !== undefined){
               // a.click();
             location.href=a.attr("href");
             return false;
            }
        btnAccordion.show();
        $(this).hide();
        accordionContent.slideUp();
        $(this).next().slideDown().promise().done(function(){
            $('html, body').animate({ scrollTop: $(this).offset().top }, 'slow');

        });
    });
}


function SetHospital(hospitalId, hospitalname) {
    if (hospitalId != "" && hospitalname != "") {
        $('#Content_hdHospitalId').val(hospitalId);
        $('#Content_hdHospitalName').val(hospitalname);
    }
}

function SetDepartment(departmentId) {
    if (departmentId != "") {
        $('#Content_hdDepartmentId').val(departmentId);
    }
}

function SetDoctor(doctorId) {
    if (doctorId != "") {
        $('#Content_hdDoctorId').val(doctorId);
    }
}

function SetDateTime(datetime) {
    if (datetime != "") {
        $('#Content_hdAppointmentStartEndDateTime').val(datetime);
    }
}

if ($('.medikalteknoloji').length > 0 && $(window).width() > 480) {
    $('.medikalteknoloji').bxSlider({
        minSlides: 1,
        maxSlides: 3,
        // slideWidth: 267,
        moveSlides: 1,
        pager: false,
        controls: true
    });
}

function FormControl() {

    var result = true;

    if ($('#Content_txtIdentityNo').val() == "") {
        $('#Content_lblIdentityNo').css("display", "block");
        result = false;
    } else {
        $('#Content_lblIdentityNo').css("display", "none");
    }

    if ($('#Content_txtName').val() == "") {
        $('#Content_lblName').css("display", "block");
        result = false;
    } else {
        $('#Content_lblName').css("display", "none");
    }

    if ($('#Content_txtGSM').val() == "") {
        $('#Content_lblGSM').css("display", "block");
        result = false;
    } else {
        $('#Content_lblGSM').css("display", "none");
    }

    var rd0 = document.getElementById("Content_rdGender_0");
    var rd1 = document.getElementById("Content_rdGender_1");
    if (rd0.checked == false && rd1.checked == false) {
        $('#Content_lblGender').css("display", "block");
        result = false;
    } else {
        $('#Content_lblGender').css("display", "none");
    }

    if ($('#Content_txtEmail').val() == "") {
        $('#Content_lblEmail').css("display", "block");
        result = false;
    } else {
        $('#Content_lblEmail').css("display", "none");
    }

    if ($('#Content_txtSurname').val() == "") {
        $('#Content_lblSurname').css("display", "block");
        result = false;
    } else {
        $('#Content_lblSurname').css("display", "none");
    }

    if ($('#Content_drpDayList').val() == "" || $('#Content_drpMonthList').val() == "" || $('#Content_drpYearList').val() == "") {
        $('#Content_lblDirthDate').css("display", "block");
        result = false;
    } else {
        $('#Content_lblDirthDate').css("display", "none");
    }
	
	if ($('#Content_txtCaptcha').val() != $('#Content_hdnCaptcha').val()) {
        $('#Content_lblSecError').css("display", "block");
        result = false;
    } else {
        $('#Content_lblSecError').css("display", "none");
    }

    //if ($('#Content_drpDayList').val() == "") {
    //    $('#Content_lblDay').css("display", "block");
    //    result = false;
    //} else {
    //    $('#Content_lblDay').css("display", "none");
    //}

    //if ($('#Content_drpMonthList').val() == "") {
    //    $('#Content_lblMonth').css("display", "block");
    //    result = false;
    //} else {
    //    $('#Content_lblMonth').css("display", "none");
    //}

    //if ($('#Content_drpYearList').val() == "") {
    //    $('#Content_lbllYear').css("display", "block");
    //    result = false;
    //} else {
    //    $('#Content_lbllYear').css("display", "none");
    //}

    return result;

}
function loading() {
    removeloading();
    $('body').append('<div class="global-loading"></div>');
}

function removeloading() {
    $('.global-loading').remove();
}


function SetClass(id) {
    if (id != "") {
        for (var i = 1; i <= 5; i++) {
            var current_tab = "lnkTab" + id;
            var tab = "lnkTab" + i;
            if (current_tab == tab) {
                $('#Content_lnkTab' + id).css("class", "select");
                $('#Content_lnkTab' + id).click();
            } else {
                $('#Content_lnkTab' + i).css("class", "");
                $('#Content_lnkTab' + i).click();
            }
        }
    }
}


var Page = (function () {
    if ($('#promo').length == 0) {
        return false;
    }
    var $nav = $('#nav-dots > span'),
	$navArrows = $( '#nav-arrows' ),
        slitslider = $('#slider').slitslider({
            onBeforeChange: function (slide, pos) {

                $nav.removeClass('nav-dot-current');
                $nav.eq(pos).addClass('nav-dot-current');

            },
            autoplay: true,
            interval : 6000
        }),

        init = function () {

            initEvents();

        },
        initEvents = function () {

		$navArrows.children( ':last' ).on( 'click', function() {

								slitslider.next();
								return false;

							} );

							$navArrows.children( ':first' ).on( 'click', function() {
								
								slitslider.previous();
								return false;

							} );
            $nav.each(function (i) {

                $(this).on('click', function (event) {

                    var $dot = $(this);

                    if (!slitslider.isActive()) {

                        $nav.removeClass('nav-dot-current');
                        $dot.addClass('nav-dot-current');

                    }

                    slitslider.jump(i + 1);
                    return false;

                });

            });

        };

    return { init: init };
})();

$(function(){
 $('#btn-mobile-menu').on('click', function () {
            $('#mobile-menu-container').toggleClass('mobile-menu-container-opened');
            $(this).toggleClass('btn-mobile-menu-opened');
        });

        $('.open-ul a').on('click', function () {
            $(this).next('.sub-ul').toggleClass('display-open');
            $(this).find('img').toggleClass('ico-menu-arrow-opened');
        });
function widgetpos(){
	 var pagewidth = $('body').width(); 
    if (pagewidth > 915) {
        var left = (pagewidth - 915) / 2;
        $('#widget').css({
            "left": left + "px",
            "display": "block"
        });
    }  else{
		 $('#widget').css({ 
            "display": "none"
        });
	}

}

   /*widgetpos();
   
   $(window).resize(function(){
	    widgetpos();
   });*/

   $(document).click(function(e) {
                var p = $(e.target);
			 
			if (($(e.target).parents('#loginbox').length===0 && p.attr("id")!=="loginbox") && $('#loginbox').hasClass("opened")) {
                    $('#loginbox').removeClass("opened");
                    e.stopPropagation();
                }


   });


   widgetpos();

   $(window).resize(function () {
       widgetpos();
   });

   $('#loginbox').parent().attr("id", "membermodal");
   $(document).click(function (e) {
       var p = $(e.target);


       if (($(e.target).parents('#membermodal').length === 0 && p.attr("id") !== "membermodal")) {
           $('#loginbox').removeClass("opened");
           $('#forgotpwd').removeClass("opened");
           e.stopPropagation();
       }

   });

   

   /*$('#header_btn_open_uyelik_model').on('click', function () {
        $('.blue-form.uyelik').toggleClass('opened');
        return false;
    });*/
   $('#header_btn_open_uyelik_model').on('click', function () {
        $('#forgotpwd').removeClass("opened");

        if ($('#loginbox').hasClass("opened")) {
            $('.blue-form.uyelik').removeClass('opened');
        } else {
            $('.blue-form.uyelik').addClass('opened');
        }

        //$('.blue-form.uyelik').toggleClass('opened');
        return false;
    });
   
    $('#btn-openForgotPwd').on("click", function () {
        $('#loginbox').removeClass('opened');
        $('#forgotpwd').addClass('opened');
    });

    $('#newsletter').on('click', function () {
        $('.blue-form.ebulten').addClass('opened');
        return false;
    });

    $('#btn-ebulten-close').on('click', function () {
        $('.blue-form.ebulten').removeClass('opened');
        return false;
    });

    $('#btn-kullanim-kosullari').on('click', function () {
        $('.l-modal').addClass('opened');
        return false;
    });

    $('.btn-close-modal').on('click', function () {
        $('.l-modal').removeClass('opened');
        return false;
    });

    $('.frmPageSearch').on('click', function () {
        $('#loginbox').hasClass("opened")
        $('#loginbox').removeClass("opened");
        return false;

    });
    $('.frmPageSearch').on('click', function () {
        $('#membermodal').removeClass('opened');
        return false;

    });
    /*-------------------------------------------
    -----------------------------------------*/
    $('#report').on('click', function () {
        $('.blue-form.rapor').addClass('opened');
        return false;
    });

    $('#btn-rapor-close').on('click', function () {
        $('.blue-form.rapor').removeClass('opened');
        return false;
    });

    $('#btn-kullanim-kosullari').on('click', function () {
        $('.l-modal').addClass('opened');
        return false;
    });

    $('.btn-close-modal').on('click', function () {
        $('.l-modal').removeClass('opened');
        return false;
    });
    /*-------------------------------------------
   -----------------------------------------*/


    if ($('#promo').length > 0) {
        Page.init();
    }

    if ($('.merkezler').length > 0) {
        $('.merkezler').bxSlider({
            minSlides: 3,
            maxSlides: 5,
            slideWidth: 130,
            slideMargin: 20,
            moveSlides: 1,
            pager: false,
            controls: true
        });
    }


    if ($('select.customselect').length > 0) {
        $('select.customselect').customSelect();
    }


    $('#Content_ihastane').change(function (e) {
        e.preventDefault();
        
        var pLang = "TR";
        if (window.location.href.indexOf("/en") > -1) {
            pLang = "EN";
        }
        else if (window.location.href.indexOf("/ru") > -1) {
            pLang = "RU";
        }
        else if (window.location.href.indexOf("/ar") > -1) {
            pLang = "AR";
        }

        var postdata = "livHospitalId=" + $(this).val() + "&lang=" + pLang;
        $.ajax({
            type: "post",
            url: "/LivHospitalService.asmx/BranchList",
            dataType: "xml",
            data: postdata,
            beforeSend: function () {
                loading();
            },
            success: function (xml) {
                removeloading();
                var optn = ""; //"<option value='0'>B&ouml;l&uuml;m Se&ccedil;iniz</option>";
                if (pLang == "TR")
                {
                    optn = "<option value='0'>B&ouml;l&uuml;m Se&ccedil;iniz</option>";
                }
                else if (pLang == "EN") {
                    optn = "<option value='0'>Select Department</option>";
                }
                else if (pLang == "RU") {
                    optn = "<option value='0'>Select Department</option>";
                }
                else if (pLang == "AR") {
                    optn = "<option value='0'>Select Department</option>";
                }
                $(xml).find('Branches2').each(function () {

                    optn += "<option value='"+ $(this).find('Value').text() +"'>"+ $(this).find('Text').text() +"</option>";                    

                });

                $('#Content_ibolum').html("");
                $('#Content_ibolum').html(optn);

                $('#Content_hdChar').val("");
                Search();
            }
        });

    });

    $('#ihastane').change(function (e) {
        e.preventDefault();

        var pLang = "TR";
        if (window.location.href.indexOf("/en") > -1) {
            pLang = "EN";
        }
        else if (window.location.href.indexOf("/ru") > -1) {
            pLang = "RU";
        }
        else if (window.location.href.indexOf("/ar") > -1) {
            pLang = "AR";
        }

        var postdata = "livHospitalId=" + $(this).val() + "&lang=" + pLang;
        $.ajax({
            type: "post",
            url: "/LivHospitalService.asmx/BranchList",
            dataType: "xml",
            data: postdata,
            beforeSend: function () {
                loading();
            },
            success: function (xml) {
                removeloading();
                var optn = ""; //"<option value='0'>B&ouml;l&uuml;m Se&ccedil;iniz</option>";
                if (pLang == "TR") {
                    optn = "<option value='0'>B&ouml;l&uuml;m Se&ccedil;iniz</option>";
                }
                else if (pLang == "EN") {
                    optn = "<option value='0'>Select Department</option>";
                }
                else if (pLang == "RU") {
                    optn = "<option value='0'>Select Department</option>";
                }
                else if (pLang == "AR") {
                    optn = "<option value='0'>Select Department</option>";
                }
                $(xml).find('Branches2').each(function () {

                    optn += "<option value='" + $(this).find('Value').text() + "'>" + $(this).find('Text').text() + "</option>";                    

                });

                $('#ibolum').html("");
                $('#ibolum').html(optn);

                $('#Content_hdChar').val("");
                Search();
            }
        });

    });


    $('#Content_ibolum').change(function (e) {
        e.preventDefault();

		var pLang = "TR";
		if (window.location.href.indexOf("/en") > -1) {
			pLang = "EN";
		}
		else if (window.location.href.indexOf("/ru") > -1) {
			pLang = "RU";
		}
		else if (window.location.href.indexOf("/ar") > -1) {
			pLang = "AR";
		}
		
        //var postdata = "{ livHospitalId:" + $('#Content_ihastane').val() + ", branchId:" + $(this).val() + "}";
        //alert(postdata);
        $.ajax({
            type: "post",
            url: "/LivHospitalService.asmx/DoctorList",
            dataType: "xml",
            data: 'livHospitalId=' + $('#Content_ihastane').val() + "&branchId=" + $(this).val() + "&lang=" + pLang,
            beforeSend: function () {
                loading();

            },
            success: function (xml) {
                removeloading();
                

                var optn = ""; //"<option value='0'>Doktor Se&ccedil;iniz</option>";
                if (pLang == "TR") {
                    optn = "<option value=''>Doktor Se&ccedil;iniz</option>";
                }
                else if (pLang == "EN") {
                    optn = "<option value=''>Select Doctor</option>";
                }
                else if (pLang == "RU") {
                    optn = "<option value=''>Select Doctor</option>";
                }
                else if (pLang == "AR") {
                    optn = "<option value=''>Select Doctor</option>";
                }
                $(xml).find('Doctors2').each(function () {

                    optn += "<option value='" + $(this).find('Value').text() + "'>" + $(this).find('Text').text() + "</option>";                    

                });

                $('#Content_iname').html("");
                $('#Content_iname').html(optn);

                $('#Content_hdChar').val("");
                Search();
            }
        });

    });

    $('#ibolum').change(function (e) {
        e.preventDefault();

		var pLang = "TR";
		if (window.location.href.indexOf("/en") > -1) {
			pLang = "EN";
		}
		else if (window.location.href.indexOf("/ru") > -1) {
			pLang = "RU";
		}
		else if (window.location.href.indexOf("/ar") > -1) {
			pLang = "AR";
		}
		
        $.ajax({
            type: "post",
            url: "/LivHospitalService.asmx/DoctorList",
            dataType: "xml",
            data: 'livHospitalId=' + $('#ihastane').val() + "&branchId=" + $(this).val() + "&lang=" + pLang,
            beforeSend: function () {
                loading();
            },
            success: function (xml) {
                removeloading();
                

                var optn = ""; //"<option value='0'>Doktor Se&ccedil;iniz</option>";
                if (pLang == "TR") {
                    optn = "<option value=''>Doktor Se&ccedil;iniz</option>";
                }
                else if (pLang == "EN") {
                    optn = "<option value=''>Select Doctor</option>";
                }
                else if (pLang == "RU") {
                    optn = "<option value=''>Select Doctor</option>";
                }
                else if (pLang == "AR") {
                    optn = "<option value=''>Select Doctor</option>";
                }
                $(xml).find('Doctors2').each(function () {

                    optn += "<option value='" + $(this).find('Value').text() + "'>" + $(this).find('Text').text() + "</option>";                    

                });

                $('#iname').html("");
                $('#iname').html(optn);

                $('#Content_hdChar').val("");
                Search();
            }
        });

    });

    $('#spn #Content_iname').change(function (e) {
        $('#Content_hdChar').val("");
        GetDoctorList();
    });
    $('#Content_iname').change(function (e) {
        $('#Content_hdChar').val("");
        Search();
    });


    function GetDoctorList()
    {
        var pLang = "TR";
        if (window.location.href.indexOf("/en") > -1) {
            pLang = "EN";
        }
        else if (window.location.href.indexOf("/ru") > -1) {
            pLang = "RU";
        }
        else if (window.location.href.indexOf("/ar") > -1) {
            pLang = "AR";
        }

        if (pLang == "TR") {
            window.location = "/doktorlarimiz?hospitalName=" + (($('#Content_ihastane').val() != "0" && $('#Content_ihastane').val() != "") ? $('#Content_ihastane option:selected').text() : "") +
                                                  "&branch=" + (($('#Content_ibolum').val() != "0" && $('#Content_ibolum').val() != "") ? $('#Content_ibolum option:selected').text() : "") +
                                                  "&doctor=" + (($('#Content_iname').val() != "" && $('#Content_iname').val() != "") ? $('#Content_iname').val() : "");
        }
        else if (pLang == "EN") {
            window.location = "/en/doctors?hospitalName=" + (($('#Content_ihastane').val() != "0" && $('#Content_ihastane').val() != "") ? $('#Content_ihastane option:selected').text() : "") +
                                                  "&branch=" + (($('#Content_ibolum').val() != "0" && $('#Content_ibolum').val() != "") ? $('#Content_ibolum option:selected').text() : "") +
                                                  "&doctor=" + (($('#Content_iname').val() != "" && $('#Content_iname').val() != "") ? $('#Content_iname').val() : "");
        }
        else if (pLang == "RU") {
            window.location = "/en/doctors?hospitalName=" + (($('#ihastane').val() != "0" && $('#ihastane').val() != "") ? $('#ihastane option:selected').text() : "") +
                                                  "&branch=" + (($('#ibolum').val() != "0" && $('#ibolum').val() != "") ? $('#ibolum option:selected').text() : "") +
                                                  "&doctor=" + (($('#iname').val() != "" && $('#iname').val() != "") ? $('#iname').val() : "");
        }
        else if (pLang == "AR") {
            window.location = "/en/doctors?hospitalName=" + (($('#ihastane').val() != "0" && $('#ihastane').val() != "") ? $('#ihastane option:selected').text() : "") +
                                                  "&branch=" + (($('#ibolum').val() != "0" && $('#ibolum').val() != "") ? $('#ibolum option:selected').text() : "") +
                                                  "&doctor=" + (($('#iname').val() != "" && $('#iname').val() != "") ? $('#iname').val() : "");
        }
    }


    function Search() {
        
        var livHospitalName = "";
        if ($('#Content_ihastane option:selected').index() != 0)
            livHospitalName = $('#Content_ihastane option:selected').text();

        var branchName = "";
        if ($('#Content_ibolum option:selected').index() != 0)
            branchName = $('#Content_ibolum option:selected').text();

        var doctorName = "";
        if ($('#Content_iname').val() != '')
            doctorName = $('#Content_iname').val();
        /*if ($('#Content_iname option:selected').index() != 0)
            doctorName = $('#Content_iname option:selected').text();*/


        var pLang = "TR";
        if (window.location.href.indexOf("/en") > -1) {
            pLang = "EN";
        }
        else if (window.location.href.indexOf("/ru") > -1) {
            pLang = "RU";
        }
        else if (window.location.href.indexOf("/ar") > -1) {
            pLang = "AR";
        }
        //alert('livHospitalName=' + livHospitalName + "&branchName=" + branchName + "&doctorName=" + doctorName + "&character=" + $('#Content_hdChar').val() + "&lang=" + pLang);
        $.ajax({
            type: "post",
            url: "/LivHospitalService.asmx/SearchList",
            dataType: "xml",
            data: 'livHospitalName=' + livHospitalName + "&branchName=" + branchName + "&doctorName=" + doctorName + "&character=" + $('#Content_hdChar').val() + "&lang=" + pLang,
            beforeSend: function () {
                loading();
            },
            success: function (xml) {
                removeloading();
                var doctorList = "";
                $(xml).find('SearchResult').each(function () {

                    if (pLang == "TR") {
                        doctorList += "<li>" +
                                        "<div class='photo'><a href='/doktorlarimiz/" + $(this).find('Title_NameSurname').text() /*+ "/" + $(this).find('Id').text()*/ + "'><img src='/admin" + $(this).find('ProfileImage').text() + "' alt=''/></a></div>" +
                                        "<div class='doktor-info'>" +
                                            "<a href='/doktorlarimiz/" + $(this).find('Title_NameSurname').text() /*+ "/" + $(this).find('Id').text()*/ + "'><h5>" + $(this).find('Title').text() + " " + $(this).find('NameSurname').text() + "</h5></a>" +
                                            "<h6>" + $(this).find('HospitalName').text() + " - " + $(this).find('BranchName').text() + "</h6>" +
                                        "</div>" +
                                        //"<div class='uzmanlik'>" + $(this).find('Profession_TR').text() + "</div>" +
                                        "<div class='uzmanlik'></div>" +
                                        "<div class='links'>" +
                                            "<a href='/online-hizmetlerimiz/e-randevu' class='btn btn-default'>randevu al</a>" +
                                            "<a href='/doktorlarimiz/" + $(this).find('Title_NameSurname').text() /*+ "/" + $(this).find('Id').text()*/ + "' class='btn btn-default'>profil g&ouml;r</a>" +
                                        "</div>" +
                            "</li>";
                    }
                    else if (pLang == "EN") {
                        doctorList += "<li>" +
                                        "<div class='photo'><a href='/en/doctors/" + $(this).find('Title_NameSurname').text() /*+ "/" + $(this).find('Id').text()*/ + "'><img src='/admin" + $(this).find('ProfileImage').text() + "' alt=''/></a></div>" +
                                        "<div class='doktor-info'>" +
                                            "<a href='/en/doctors/" + $(this).find('Title_NameSurname').text() /*+ "/" + $(this).find('Id').text()*/ + "'><h5>" + $(this).find('Title').text() + " " + $(this).find('NameSurname').text() + "</h5></a>" +
                                            "<h6>" + $(this).find('HospitalName').text() + " - " + $(this).find('BranchName').text() + "</h6>" +
                                        "</div>" +
                                        //"<div class='uzmanlik'>" + $(this).find('Profession_TR').text() + "</div>" +
                                        "<div class='uzmanlik'></div>" +
                                        "<div class='links'>" +
                                            "<a href='/en/online-services/e-appointment' class='btn btn-default'>appointment</a>" +
                                            "<a href='/en/doctors/" + $(this).find('Title_NameSurname').text() /*+ "/" + $(this).find('Id').text()*/ + "' class='btn btn-default'>profile</a>" +
                                        "</div>" +
                            "</li>";
                    }
                    else if (pLang == "RU") {
                        doctorList += "<li>" +
                                        "<div class='photo'><a href='/en/doctors/" + $(this).find('Title_NameSurname').text() + "/" + $(this).find('Id').text() + "'><img src='/admin" + $(this).find('ProfileImage').text() + "' alt=''/></a></div>" +
                                        "<div class='doktor-info'>" +
                                            "<a href='/en/doctors/" + $(this).find('Title_NameSurname').text() + "/" + $(this).find('Id').text() + "'><h5>" + $(this).find('Title').text() + " " + $(this).find('NameSurname').text() + "</h5></a>" +
                                            "<h6>" + $(this).find('HospitalName').text() + " - " + $(this).find('BranchName').text() + "</h6>" +
                                        "</div>" +
                                        //"<div class='uzmanlik'>" + $(this).find('Profession_TR').text() + "</div>" +
                                        "<div class='uzmanlik'></div>" +
                                        "<div class='links'>" +
                                            "<a href='/en/online-services/e-appointment' class='btn btn-default'>appointment</a>" +
                                            "<a href='/en/doctors/" + $(this).find('Title_NameSurname').text() + "/" + $(this).find('Id').text() + "' class='btn btn-default'>profile</a>" +
                                        "</div>" +
                            "</li>";
                    }
                    else if (pLang == "AR") {
                        doctorList += "<li>" +
                                        "<div class='photo'><a href='/en/doctors/" + $(this).find('Title_NameSurname').text() + "/" + $(this).find('Id').text() + "'><img src='/admin" + $(this).find('ProfileImage').text() + "' alt=''/></a></div>" +
                                        "<div class='doktor-info'>" +
                                            "<a href='/en/doctors/" + $(this).find('Title_NameSurname').text() + "/" + $(this).find('Id').text() + "'><h5>" + $(this).find('Title').text() + " " + $(this).find('NameSurname').text() + "</h5></a>" +
                                            "<h6>" + $(this).find('HospitalName').text() + " - " + $(this).find('BranchName').text() + "</h6>" +
                                        "</div>" +
                                        //"<div class='uzmanlik'>" + $(this).find('Profession_TR').text() + "</div>" +
                                        "<div class='uzmanlik'></div>" +
                                        "<div class='links'>" +
                                            "<a href='/en/online-services/e-appointment' class='btn btn-default'>appointment</a>" +
                                            "<a href='/en/doctors/" + $(this).find('Title_NameSurname').text() + "/" + $(this).find('Id').text() + "' class='btn btn-default'>profile</a>" +
                                        "</div>" +
                            "</li>";
                    }

                });

                $('#Content_doktor_list').html("");
                $('#Content_doktor_list').html(doctorList);
            }
        });

    }


    $('#Content_btn_search_doktor').click(function (e) {
        e.preventDefault();
        
        $('#Content_hdChar').val("");

        Search();
    });

    $('#btn_search_doktor').click(function (e) {
        e.preventDefault();

        var pLang = "TR";
        if (window.location.href.indexOf("/en") > -1) {
            pLang = "EN";
        }
        else if (window.location.href.indexOf("/ru") > -1) {
            pLang = "RU";
        }
        else if (window.location.href.indexOf("/ar") > -1) {
            pLang = "AR";
        }

        if (pLang == "TR") {
            window.location = "/doktorlarimiz?hospitalName=" + (($('#Content_ihastane').val() != "0" && $('#Content_ihastane').val() != "") ? $('#Content_ihastane option:selected').text() : "") +
                                                  "&branch=" + (($('#Content_ibolum').val() != "0" && $('#Content_ibolum').val() != "") ? $('#Content_ibolum option:selected').text() : "") +
                                                  "&doctor=" + (($('#Content_iname').val() != "" && $('#Content_iname').val() != "") ? $('#Content_iname').val() : "");
        }
        else if (pLang == "EN") {
            window.location = "/en/doctors?hospitalName=" + (($('#Content_ihastane').val() != "0" && $('#Content_ihastane').val() != "") ? $('#Content_ihastane option:selected').text() : "") +
                                                  "&branch=" + (($('#Content_ibolum').val() != "0" && $('#Content_ibolum').val() != "") ? $('#Content_ibolum option:selected').text() : "") +
                                                  "&doctor=" + (($('#Content_iname').val() != "" && $('#Content_iname').val() != "") ? $('#Content_iname').val() : "");
        }
        else if (pLang == "RU") {
            window.location = "/en/doctors?hospitalName=" + (($('#ihastane').val() != "0" && $('#ihastane').val() != "") ? $('#ihastane option:selected').text() : "") +
                                                  "&branch=" + (($('#ibolum').val() != "0" && $('#ibolum').val() != "") ? $('#ibolum option:selected').text() : "") +
                                                  "&doctor=" + (($('#iname').val() != "" && $('#iname').val() != "") ? $('#iname').val() : "");
        }
        else if (pLang == "AR") {
            window.location = "/en/doctors?hospitalName=" + (($('#ihastane').val() != "0" && $('#ihastane').val() != "") ? $('#ihastane option:selected').text() : "") +
                                                  "&branch=" + (($('#ibolum').val() != "0" && $('#ibolum').val() != "") ? $('#ibolum option:selected').text() : "") +
                                                  "&doctor=" + (($('#iname').val() != "" && $('#iname').val() != "") ? $('#iname').val() : "");
        }

    });




    $('#doktor-filters-alphabetic a').click(function(e){
        e.preventDefault();

        $('#Content_ihastane').val("");
        $('#Content_ibolum').val("");
        $('#Content_iname').val("");

        $('#Content_hdChar').val($(this).data("val"));

        Search();

    });

    $('#home-doktor-filters-alphabetic a').click(function (e) {
        e.preventDefault();

        var pLang = "TR";
        if (window.location.href.indexOf("/en") > -1) {
            pLang = "EN";
        }
        else if (window.location.href.indexOf("/ru") > -1) {
            pLang = "RU";
        }
        else if (window.location.href.indexOf("/ar") > -1) {
            pLang = "AR";
        }

        if (pLang == "TR") {
            window.location = "/doktorlarimiz?hospitalName=" + (($('#Content_ihastane').val() != "0" && $('#Content_ihastane').val() != "") ? $('#Content_ihastane option:selected').text() : "") +
                                                  "&branch=" + (($('#Content_ibolum').val() != "0" && $('#Content_ibolum').val() != "") ? $('#Content_ibolum option:selected').text() : "") +
                                                  "&doctor=" + (($('#Content_iname').val() != "" && $('#Content_iname').val() != "") ? $('#Content_iname').val() : "") +
                                                     "&chr=" + $(this).data("val");
        }
        else if (pLang == "EN") {
            window.location = "/en/doctors?hospitalName=" + (($('#Content_ihastane').val() != "0" && $('#Content_ihastane').val() != "") ? $('#Content_ihastane option:selected').text() : "") +
                                                  "&branch=" + (($('#Content_ibolum').val() != "0" && $('#Content_ibolum').val() != "") ? $('#Content_ibolum option:selected').text() : "") +
                                                  "&doctor=" + (($('#Content_iname').val() != "" && $('#Content_iname').val() != "") ? $('#Content_iname').val() : "") +
                                                     "&chr=" + $(this).data("val");
        }
        else if (pLang == "RU") {
            window.location = "/en/doctors?hospitalName=" + (($('#ihastane').val() != "0" && $('#ihastane').val() != "") ? $('#ihastane option:selected').text() : "") +
                                                  "&branch=" + (($('#ibolum').val() != "0" && $('#ibolum').val() != "") ? $('#ibolum option:selected').text() : "") +
                                                  "&doctor=" + (($('#iname').val() != "" && $('#iname').val() != "") ? $('#iname').val() : "") +
                                                     "&chr=" + $(this).data("val");
        }
        else if (pLang == "AR") {
            window.location = "/en/doctors?hospitalName=" + (($('#ihastane').val() != "0" && $('#ihastane').val() != "") ? $('#ihastane option:selected').text() : "") +
                                                  "&branch=" + (($('#ibolum').val() != "0" && $('#ibolum').val() != "") ? $('#ibolum option:selected').text() : "") +
                                                  "&doctor=" + (($('#iname').val() != "" && $('#iname').val() != "") ? $('#iname').val() : "") +
                                                     "&chr=" + $(this).data("val");
        }

    });


    /**************** broşürler ***********************************/
    $('#Content_brosurlerimiz_filters_alphabetic a').click(function (e) {
        e.preventDefault();
        var chr = $(this).data("val");

        var pLang = "TR";
        if (window.location.href.indexOf("/en") > -1) {
            pLang = "EN";
        }
        else if (window.location.href.indexOf("/ru") > -1) {
            pLang = "RU";
        }
        else if (window.location.href.indexOf("/ar") > -1) {
            pLang = "AR";
        }

        if (pLang == "TR") {
            window.location = "/medya/brosurlerimiz?chr=" + chr;
        }
        else if (pLang == "EN") {
            window.location = "/en/media/brochures?chr=" + chr;
        }
        else if (pLang == "RU") {
            window.location = "/en/media/brochures?chr=" + chr;
        }
        else if (pLang == "AR") {
            window.location = "/en/media/brochures?chr=" + chr;
        }

    });

    /**************** týbbi birimler ***********************************/
    $('#tibbihizmetler-filters-alphabetic a').click(function (e) {
        e.preventDefault();
       
        var hospitalId = $('#Content_ihastane option:selected').val();
        var text = $('#Content_isearch').val();
        var chr = $(this).data("val");

        var pLang = "TR";
        if (window.location.href.indexOf("/en") > -1) {
            pLang = "EN";
        }

        if (pLang == "TR") {
            $('#Content_divContent').val("");
            window.location = "/tibbi-hizmetlerimiz?hospitalId=" + hospitalId + "&text=" + text + "&chr=" + chr;
        }
        else if (pLang == "EN") {
            $('#Content_divContent').val("");
            window.location = "/en/medical-services?hospitalId=" + hospitalId + "&text=" + text + "&chr=" + chr;
        }

        Search();

    });

    /**************** medikal teknoloji ***********************************/
    $('#medikalteknolojiler-filters-alphabetic a').click(function (e) {
        e.preventDefault();

        //var hospitalId = $('#Content_ihastane option:selected').val();
        var text = $('#Content_isearch').val();
        var chr = $(this).data("val");

        var pLang = "TR";
        if (window.location.href.indexOf("/en") > -1) {
            pLang = "EN";
        }

        if (pLang == "TR") {
            //window.location = "/medikal-teknolojilerimiz?hospitalId=" + hospitalId + "&text=" + text + "&chr=" + chr;
            window.location = "/medikal-teknolojilerimiz?text=" + text + "&chr=" + chr;
        }
        else if (pLang == "EN") {
            //window.location = "/en/medical-technology?hospitalId=" + hospitalId + "&text=" + text + "&chr=" + chr;
            window.location = "/en/medical-technology?text=" + text + "&chr=" + chr;
        }

        Search();

    });

    /**************** medikal teknoloji ***********************************/
    $('.tab a').click(function (e) {
        e.preventDefault();
        if ($(this).closest('ul').hasClass("tab-disabled")) {
            //return false;
        }

        $('.tab a.select').removeClass("select");
        $(this).addClass("select");

        $('.tabcontainer .tabcontent.select').removeClass("select");
        $('.tabcontainer .tabcontent#' + $(this).data("tab")).addClass("select");

    });

    $('.tabcontainer .tabcontent .btn-back').on("click", function (e) {
        e.preventDefault();
        $('.tab a[data-tab=' + $(this).data("tab") + ']').click();



    });

    $('.tabs a').click(function (e) {
        e.preventDefault();
        $('#widget').addClass("open");
        $('.tabs a.select').removeClass("select");
        $(this).addClass("select");

        $('.tabcontainers .tabcontent.select').removeClass("select");
        $('.tabcontainers .tabcontent#' + $(this).data("tab")).addClass("select");
        $('.sl-slider-wrapper').css("opacity",".2");

    });

    $('.radio-buttons .btn-radio').click(function () {
        $(this).parents(".radio-buttons").find(".selected").removeClass("selected");
        $(this).addClass("selected");
    });

    //$('.table2 tbody td').click(function () {
    //    $('.table2 tbody td.selected').removeClass("selected");
    //    $(this).addClass("selected");
    //});

    $('.table2 td .table2 td ').click(function () {
        $('.table2 td .table2 td.selected').removeClass("selected");
        $(this).addClass("selected");
    });




    


//});




$(document).click(function(e) {
    var p = $(e.target);

    if ($(e.target).parents('#widget').length===0)  {
        $('#widget .tabcontainers .tabcontent').removeClass("select");
        $('#widget .tabs a.select').removeClass("select");
        $('#widget').removeClass("open");

        $('.sl-slider-wrapper').css("opacity","1");
        e.stopPropagation();
    }
});
    
if($(".fancybox").length>0){
    $(".fancybox").fancybox({
        openEffect: 'none',
        closeEffect: 'none'
    });
}
     
var newsContent = $('.news-content'),
    rest = $('.rest'),
    trg = $('.news-trg-container'),
    date = $('.date'),
    h3 = $('h3'),
    h2 = $('h2');

newsContent.on('click', function () {
    rest.slideUp();
    $(this).find(rest).slideDown().promise().done(function () {
        $('html, body').animate({scrollTop: $(this).parent().parent().find('h2').offset().top - 30}, 'slow');
    });
    trg.fadeIn();
    $(this).find(trg).hide();
    h3.show();
    $(this).find(h3).hide();
    newsContent.find(date).removeClass('date-collapsed');
    $(this).find(date).addClass('date-collapsed');
    newsContent.find(h2).hide();
    $(this).find(h2).fadeIn();
});
$(document).ready(function () {
    $("body").click(function () {
        $('.frmPageSearch').animate({
            top: -100
        }, 300, function () {
            $(".searchbtn").removeClass("open");
        });
    });

    $(".searchbtn").click(function (e) {
        e.stopPropagation();
        var $this = $(this);
        if ($('.blue-form.uyelik').hasClass("opened")) {
            $('.blue-form.uyelik').removeClass('opened');
        }

        if ($this.hasClass("open")) {
            $('.frmPageSearch').animate({
                top: -100
            }, 300, function () {
                $this.removeClass("open");
            });

        } else {
            $('.frmPageSearch').animate({
                top: 28
            }, 300, function () {
                $this.addClass("open");
            });

        }
    });

    $(".frmPageSearch").click(function (e) {
        e.stopPropagation();
    });
})
  

 var offset = 300, offset_opacity = 1200, scroll_top_duration = 700, $back_to_top = $('.cd-top');


        $back_to_top.on('click', function (event) {
            event.preventDefault();
            $('body,html').animate({
                scrollTop: 0,
            }, scroll_top_duration
            );
        });

        

        $(window).scroll(function () {
            ($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
            if ($(this).scrollTop() > offset_opacity) {
                $back_to_top.addClass('cd-fade-out');
            } 
        });
});
