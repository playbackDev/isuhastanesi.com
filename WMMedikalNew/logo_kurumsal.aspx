﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="logo_kurumsal.aspx.cs" Inherits="WMMedikalNew.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="sub-page-title bg-1">
        <div class="container">
            <h1 class="middle-content" style="margin-top: 128px;"><%=Resources.Lang.Logolarimiz %> </h1>
        </div>
            <div class="sublinks">
            <div class="container">
                <ul class="clearfix">
                    <li><a href="/tanitim-filmlerimiz/<%=WMMedikalNew.cBase.Lang%>"><%=Resources.Lang.TanitimFilmlerimiz %></a></li>
                    <li><a href="/foto-galeri/<%=WMMedikalNew.cBase.Lang%>"><%=Resources.Lang.FotoGaleri %></a></li>
                    <li><a href="/video-galeri/<%=WMMedikalNew.cBase.Lang%>"><%=Resources.Lang.VideoGaleri %></a></li>
                    <li><a href="/logo-ve-kurumsal-kimlik/<%=WMMedikalNew.cBase.Lang%>"><%=Resources.Lang.LogoveKurumsal %></a></li>
                </ul>

            </div>
        </div>
    </div>

    <div class="container content">
        <div class="sub-content">

            <ul class="logo-kit">
                <asp:Repeater ID="rptLogo" runat="server">
                    <ItemTemplate>
                    <li>
                        <a href="/uploads/download/<%#Eval("download_file") %>">
                            <img src="/uploads/download/<%#Eval("image_name") %>" alt="">
                            <span class="logo-download"><%=Resources.Lang.indir %> </span>
                            <div class="clear"></div>
                        </a>
                    </li>
                        </ItemTemplate>
                </asp:Repeater>
             
                </ul>

            <div class="clear"></div>
        </div>
    </div>
</asp:Content>
