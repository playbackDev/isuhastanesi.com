﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="all_videos.aspx.cs" Inherits="WMMedikalNew.all_videos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="sub-page-title bg-1">
        <div class="container">
            <h1 class="middle-content" style="margin-top: 128px;"><%=Resources.Lang.MedyaBasin %> </h1>
        </div>

            <div class="sublinks">
            <div class="container">
                <ul class="clearfix">
                    <li><a href="/tanitim-filmlerimiz/<%=WMMedikalNew.cBase.Lang%>"><%=Resources.Lang.TanitimFilmlerimiz %></a></li>
                    <li><a href="/foto-galeri/<%=WMMedikalNew.cBase.Lang%>"><%=Resources.Lang.FotoGaleri %></a></li>
                    <li><a href="/video-galeri/<%=WMMedikalNew.cBase.Lang%>"><%=Resources.Lang.VideoGaleri %></a></li>
                    <li><a href="/logo-ve-kurumsal-kimlik/<%=WMMedikalNew.cBase.Lang%>"><%=Resources.Lang.LogoveKurumsal %></a></li>
                </ul>

            </div>
        </div>
    </div>
    <div class="container content pad4050">

        <asp:Repeater ID="rptVideo" runat="server">
            <ItemTemplate>
                <iframe class="video-full" src="https://www.youtube.com/embed/<%#Eval("video_link") %>" frameborder="0" allowfullscreen=""></iframe>
               

            </ItemTemplate>
        </asp:Repeater>


    </div>
</asp:Content>
