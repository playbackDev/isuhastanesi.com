﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="h_doctor_list.aspx.cs" Inherits="WMMedikalNew.WebForm10" %>
<%@ Register Src="~/usercontrol/pageSubMenu.ascx" TagPrefix="uc1" TagName="pagesubmenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
        <style>
        .customselect {
            font-size: 13px;
            color: #364f60;
            border: solid 1px #e1e1e1;
            background: #fff;
            padding: 10px 10px 6px;
            border-radius: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div class="sub-page-title bg-21" runat="server" id="topbanner">
        <div class="container">
           <%-- <h1 class="middle-content" style="margin-top: 128px;"><%=Resources.Lang.Doktorlarimiz %></h1>--%>
        </div>
             <uc1:pagesubmenu runat="server" ID="pagesubmenu" />
          </div>
              <div class="container content">
        <div class="sub-content-wide">
            <div class="filters">
                <div class="filters-select">
                    <h4> <asp:Literal ID="ltrHospitalName" runat="server"></asp:Literal> - <%=Resources.Lang.DoktorAra %></h4>

                        <ul>
                           
                            <li>
                                <span class="selectbox size6 alt">
                                    <asp:DropDownList ID="drpMedical" runat="server" CssClass="customselect" AutoPostBack="true" OnSelectedIndexChanged="drpMedical_SelectedIndexChanged" >
                                    </asp:DropDownList>
                                </span>
                            </li>
                            <li>
                                <span class="selectbox size6 alt">
                                    <asp:DropDownList ID="drpDoctors" runat="server" CssClass="customselect" AutoPostBack="true">
                                    </asp:DropDownList>
                                </span>
                            </li>
                      
                            <li>
                                <asp:LinkButton Text="Ara" runat="server" ID="btnSearch" CssClass="btn-search"  />
                            </li>
                        </ul>
                </div>

                <input type="hidden" id="hdChar" runat="server" />

                <div id="doktor-filters-alphabetic" class="filters-alphabetic">
                    <h5><%=Resources.Lang.Alfabetik %></h5>
                    <ul>

                        <asp:Repeater ID="rptLetters" runat="server">
                            <ItemTemplate>
                                <li>
                                 <asp:LinkButton Text='<%#Eval("letter") %>' ID="btnLetters" runat="server" CommandArgument='<%#Eval("letter") %>' OnCommand="btnLetters_Command" />
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>

            </div>
            
                         <ul id="doktor_list" class="doktor-list" runat="server">
                                <center>
                        <span style="color:#90032B;font-size:16px;margin-top:20px;"><asp:Literal ID="ltrUyari" runat="server"></asp:Literal></span>
                            </center>
                    <asp:Repeater runat="server" ID="rptDoctorList" >
                        <ItemTemplate>
                             <li>
                                <div class="photo">
                                    <a href="/doktorlarimiz/<%#WMMedikalNew.cExtensions.ToURL(Eval("DoktorAdi").ToString())%>-<%#Eval("doktor_id") %>/<%#Eval("Dil") %>">
                                   
                                    <img src="<%# Eval("ResimAdi") != null &&  Eval("ResimAdi") != ""  && Eval("ResimAdi") != " " ? "../"+Eval("ResimAdi") :  "/admin/resimyok.png" %> "/>

                                       

                                    </a>

                                </div>

                                <div class="doktor-info">
                                    <a href="/doktorlarimiz/<%#WMMedikalNew.cExtensions.ToURL(Eval("DoktorAdi").ToString())%>-<%#Eval("doktor_id") %>/<%#Eval("Dil") %>"><h5> 
                                        <%#Eval("Unvan") %>  <%#Eval("DoktorAdi") %></h5></a>
                                    <h6><%#Eval("HastaneAdi") %> - <%#Eval("BolumAdi") %></h6>
                                </div>

                                <div class="uzmanlik">
                            
                                </div>
                                <div class="links">
                                        <a href="/e-services/hospitalName=<%#Eval("HastaneAdi") %>&hospitalId=<%#Eval("HastaneServisID") %>&branchName=<%#Eval("BolumAdi") %>&branchId=<%#Eval("BolumServisID") %>&doctorName=<%#Eval("DoktorAdi") %>&doctorId=<%#Eval("DoktorServisID") %>&tab=4/<%#Eval("Dil") %>" class="btn btn-default"><%=Resources.Lang.RandevuAl2 %></a>
                                    <a href="/doktorlarimiz/<%#WMMedikalNew.cExtensions.ToURL(Eval("DoktorAdi").ToString())%>-<%#Eval("doktor_id") %>/<%#Eval("Dil") %>" class="btn btn-default"><%=Resources.Lang.ProfiliGor %></a>
                
                                   
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            <div>
                


            </div>
        </div>
    </div>
  <%--      <script src="/assets/js/jquery-1.9.1.min.js"></script>
    <script src="/assets/js/plugins.js"></script>
    <script src="/assets/js/liv_global.js"></script>--%>
</asp:Content>
