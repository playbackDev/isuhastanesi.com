﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="hospital_detail.aspx.cs" Inherits="WMMedikalNew.hospital_detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="sub-page-title" runat="server" id="topbanner">
    </div>
    <div class="section-line">
        <div class="breadcrumb2">
            <a href="/"><%=Resources.Lang.Anasayfa %> | </a><a href="#">Hastanemiz</a>
        </div>
    </div>

    <div class="container content">
        <div class="sub-content">
            <h1 class="huge"><asp:Literal ID="ltrHospitalName" runat="server"></asp:Literal></h1>
            <div style="height: 200px;">
                <div class="owl-carousel owl-theme" id="owl-hospital">
                    <asp:Repeater ID="PageGallery" runat="server">
                        <ItemTemplate>
                            <div>
                                <a href="<%#Eval("image_name") %>" class="fancy" rel="gallery001" style="display: block; height: 292px; width: 412px;">
                                    <img src="<%#Eval("image_name") %>" style="height: 146px; width: 216px" alt="">
                                </a>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div class="content-section">
                <div class="content" style="background: none;">
                    <asp:Literal ID="ltrDescription" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
    </div>
    <script src="carousel/owl.carousel.js"></script>
    <link href="carousel/owl.carousel.css" rel="stylesheet" />

    <script>
        $(document).ready(function () {
            //$("#owl-hospital").owlCarousel({
            //    autoPlay: 3000,
            //    items: 4,
            //    navigation: true,
            //    controls: true,margin:10
            //});
            $('#owl-hospital').owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                controls: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: true
                    },
                    600: {
                        items: 3,
                        nav: true
                    },
                    1000: {
                        items: 4,
                        nav: true
                    }
                }
            })


            $(".fancy").fancybox({
                'transitionIn': 'elastic',
                'transitionOut': 'elastic',
                'speedIn': 600,
                'speedOut': 200,
                'overlayShow': false
            });
        })
    </script>
</asp:Content>
