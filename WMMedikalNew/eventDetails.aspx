﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="eventDetails.aspx.cs" Inherits="WMMedikalNew.eventDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%--<div class="sub-page-title" runat="server" id="topbanner">
        <div class="container">
        </div>
    </div>--%>

    <div class="section-line">
        <div class="breadcrumb2">
            <a href="/"><%=Resources.Lang.Anasayfa %> | </a><a href="#">Duyuru Detayları</a>
        </div>
    </div>

    <style>
        .sub-content .content-section p {
            font-size: 15px;
            font: 600;
        }

        .sub-content .content-section a h2 {
            font-size: 15px;
        }
    </style>

    <div class="container content">
        <div class="sub-content">
            <h1 class="huge">
                <asp:Literal ID="ltrHospitalName" runat="server"></asp:Literal></h1>
            <div class="content-section">

                <div class="content" style="background: none;">
                    <a>
                        <h2 runat="server" id="txtTitle"></h2>
                    </a>
                    <hr />

                    <p style="float: right" runat="server" id="txtTime"></p>

                    <br />
                    <asp:Literal ID="ltrImgBig" runat="server" />
                    <asp:Label ID="lblContent" runat="server" Text="Label"></asp:Label>


                </div>
            </div>
        </div>
    </div>
</asp:Content>
