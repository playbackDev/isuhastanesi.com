﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="press_in_medical.aspx.cs" Inherits="WMMedikalNew.WebForm3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="sub-page-title bg-2">
        <div class="container">
            <h1 class="middle-content" style="margin-top: 128px;"><%=Resources.Lang.BasindaBiz %> </h1>
        </div>
    </div>
    <div class="container content">
    <div class="sub-content">
        <asp:Repeater ID="rptPress" runat="server">
            <ItemTemplate>
                    <a class="fancybox gallery-item basin-item" rel="gallery2" href="/uploads/press/<%#Eval("image_name") %>" title="">
                        <img src="/uploads/press/kucuk/<%#Eval("image_name") %>" alt="">
                        <div class="basin-description">
                            <h5>
                                
                            </h5>
                            <p>
                                
                            </p>
                            <span>
                               <%#Eval("date") %>  • <%#Eval("press_name") %>
                            </span>
                        </div>
                    </a>
                </ItemTemplate>
                </asp:Repeater>
        <div class="clear"></div>
    </div>
        </div>
</asp:Content>
