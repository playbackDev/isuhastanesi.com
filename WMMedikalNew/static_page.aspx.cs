﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class WebForm15 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["page_id"] != null)
            {
                int PageID = Request.QueryString["page_id"].toint();
                //var PageDetail = db.page_langs.Select(s => new { 
                //s.id,
                //s.page_id,
                //s.page_name,
                //s.lang,
                //s.parent_id,
                //s.pages.page_detail_lang.FirstOrDefault().description
                
                //}).Where(q => q.page_id == PageID && q.lang == Lang && q.parent_id == 999).FirstOrDefault();

                var SayfaDetay = db.Sayfalar_Dil.Select(s => new
                {
                    s.sayfa_id,
                    s.sayfa_adi,
                    s.Sayfalar.ust_id,
                    s.dil,
                    s.Sayfalar.Sayfa_Detay_Dil.FirstOrDefault().description
                }).Where(q => q.sayfa_id == PageID && q.dil == Lang && q.ust_id == 999).FirstOrDefault();

                if (SayfaDetay !=null)
                {
                    ltrPageName.Text = SayfaDetay.sayfa_adi;
                    ltrPageName2.Text = SayfaDetay.sayfa_adi;
                    ltrDescription.Text = SayfaDetay.description;
                }            
               


                var SabitSayfalar = db.Sayfalar.Select(s => new
                {
                    sayfa_id = s.id,
                    link = s.Sayfalar_Dil.FirstOrDefault(q => q.dil == "TR").sayfa_adi,
                    s.ust_id,
                    s.Sayfalar_Dil.FirstOrDefault().dil,
                    s.Sayfalar_Dil.FirstOrDefault(q => q.dil == Lang).sayfa_adi

                }).Where(f => f.ust_id == 999).ToList();


                rptSabit.DataSource = SabitSayfalar;
                rptSabit.DataBind();


            }
        }
    }
}