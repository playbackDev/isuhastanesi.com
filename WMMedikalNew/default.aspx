﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="WMMedikalNew._default" ValidateRequest="false" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="/assets/css/jquery.bxslider.css" />
    <link href="/assets/plugins/flexslider.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
           <asp:HiddenField runat="server" ID="hdTabId" />
    <div class="sliderContent">
        <section id="promo" class="home-slider1">

            <div class="flexslider" style="overflow: hidden; width: 100%; border: 0 !important;">
                <ul class="slides">
                    <asp:Repeater ID="rptSlide" runat="server">
                        <ItemTemplate>
                            <li><a href="<%#Eval("link") %>">
                               <img src="<%#Eval("image_url") %>" draggable="false" class="desktop" /></a>
                                <img src="<%#Eval("image_url_mobile") %>" draggable="false" class="mobile" />
                            </li>             
                        </ItemTemplate>
                    </asp:Repeater>
             
                </ul>
            </div>
        </section>

    </div>

    <section class="newsContent">
        <div class="container">
            <div class="news">
               <ul>
                    <li><h2>Sağlık Köşesi</h2></li>
                    <asp:Repeater ID="rptNews" runat="server"><ItemTemplate>

                    <li>
                        <a href="/s-<%# Eval("seoURL") %>/<%#Eval ("news_id") %>">
                            <div class="media-left">
                                <img src="<%#Eval("news_image") %>" alt=""/>
                            </div>
                            <div class="media-body">
                                <h3><%#Eval("news_title") %></h3>
                                <span class="newsDate"><%#Eval("created_time") %></span>
                                <p><%#Eval("news_short") %></p>
                            </div>
                        </a>
                    </li>
                   </ItemTemplate></asp:Repeater>
                </ul>
                <a href="/saglik-kosesi" class="allButton">Tümünü Gör</a>
                
            </div>
            <div class="guncel">
                <ul>
                    <li><h2>Duyurular</h2></li>
                     <asp:Repeater ID="rptDuyuru" runat="server"><ItemTemplate>
                    <li>
                       <a href="/d-<%# Eval("seoURL") %>/<%#Eval("id") %>">
                            <span class="newsDate"><%#Eval("created_time") %></span>
                            <p><%#Eval("news_title") %></p>
                      </a>
                    </li>
                   </ItemTemplate>
                         </asp:Repeater>
                </ul>
                <a href="/duyurular" class="allButton">Tüm Duyurular</a>
            </div>
        </div>
    </section>

  


        <div class="clear"></div>
  
    <script src="/assets/js/jquery-1.9.1.min.js"></script>
    <script src="/assets/js/plugins.js"></script>
    <script src="/assets/js/jquery.bxslider.min.js"></script>
    <script src="/assets/js/modernizr.custom.79639.js"></script>
    <script src="/assets/js/jquery.slitslider.js"></script>

    <script src="/assets/js/liv_global.js"></script>
    <script src="/carousel/owl.carousel.js"></script>

    <script type="text/javascript">

        $(window).load(function () {
            SetClass2(document.getElementById('ContentPlaceHolder1_hdTabId').value);
        });

        function SetClass2(id) {
            if (id != "") {
                for (var i = 1; i <= 3; i++) {
                    var current_tab = "ContentPlaceHolder1_lnkTab" + id;
                    var tab = "ContentPlaceHolder1_lnkTab" + i;
                    if (current_tab == tab) {
                        var clss = "tab" + id;
                        //document.getElementById('lnkTab' + id).className = clss;
                        document.getElementById('ContentPlaceHolder1_lnkTab' + id).click();
                    } else {
                        //document.getElementById('lnkTab' + i).className = "";
                    }
                }
            }
        }

        function SetAppointment() {
            window.location = "/online-hizmetlerimiz/e-randevu?hospitalId=" + document.getElementById('drpAppointmentHospitalList').value +
                                                    "&branchId=" + document.getElementById('drpAppointmentBranchList').value +
                                                    "&doctorId=" + document.getElementById('drpAppointmentDoctorList').value;
        }

    </script>

    <script type="text/javascript" src="/assets/mask/jquery.maskedinput.js"></script>
    <script src="/assets/plugins/jquery.flexslider.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $('#txtIdentityNo').mask('11111111111');
            $('#Content_txtIdentityNo').mask('11111111111');
            $('#Content_txtCancelIdentityNo').mask('11111111111');

        });
    </script>
    <script>
        $(document).ready(function (e) {

            if (checkCookie() == false) {
                setCookie("cookiePopup", 1, 0.001);
                $('#popup').animate({ "top": "10%", "marginTop": "30px" }, 500);
            }
            else { $('#popup,#ContentPlaceHolder1_overlayPop').css('display', 'none'); }

            getCookie("cookiePopup");
            $('#closePop,#ContentPlaceHolder1_overlayPop').click(function () {
                $('#popup,#ContentPlaceHolder1_overlayPop').css('display', 'none');
            });
            $("#owl-demo").owlCarousel({
                autoPlay: 3000,
                items: 3,



            });

        });

        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        }
        function checkCookie() {
            var cookieName = getCookie("cookiePopup");
            if (cookieName != "") {
                return true;
            } else {
                return false;

            }
        }
        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
            }
            return "";
        }
    </script>
    <script>
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                directionNav: true,
                controlNav: false,
                slideshow: true
            });
        });</script>
    <div id="overlayPop" runat="server" visible="false">
        <div id="popup">
            <a id="closePop" href="#">Kapat</a>
            <asp:Literal ID="ltrPopup" runat="server"></asp:Literal>

        </div>
    </div>

    <style>
        .flex-viewport {
            width: 100%;
        }

        #ContentPlaceHolder1_overlayPop {
            position: fixed;
            width: 100%;
            height: 100vh;
            background: #000000;
            opacity: 0.9;
            top: 0;
            margin: 0;
            z-index: 9999;
        }

        .bx-controls-direction {
            display: none;
        }

        #popup {
            width: 60%;
            height: auto;
            background: transparent;
            color: #fff;
            /* padding: 20px; */
            position: relative;
            top: 0%;
            /* left: 25%; */
            margin-top: -500px;
            /* margin-left: -250px; */
            /* z-index: 91; */
            margin: 10% auto;
        }

            #popup h1 {
                margin: 0px;
                padding: 0px;
            }

        .pop-img {
            display: block;
            width: 100%;
        }

        #owl-demo .item li {
            height: 204px;
            width: 267px !important;
            float: left;
            padding: 16px;
            box-sizing: border-box;
            list-style: none;
            background: url("/assets/img/site/medikalteknoloji.png") no-repeat 100% 0;
        }

        #owl-demo .item img {
            display: block;
            width: 107px;
            height: auto;
            margin: 0 auto;
        }
    </style>

    <style>
        .customselect1 {
            font-size: 13px;
            color: #364f60;
            border: solid 1px #e1e1e1;
            background: #fff;
            padding: 10px 10px 6px;
            border-radius: 5px;
        }

        .flex-next {
            font-size: 0;
            background: url(/assets/plugins/next.png)no-repeat 100% 100%;
            background-size: 100%;
        }

        .flex-prev {
            font-size: 0;
            background: url(/assets/plugins/prev.png)no-repeat 100% 100%;
            background-size: 100%;
        }

        .flex-next:before {
            display: none !important;
        }

        .flex-prev:before {
            display: none !important;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('#btn-mobile-menu').on('click', function () {
                $('#mobile-menu-container').toggleClass('mobile-menu-container-opened');
                $(this).toggleClass('btn-mobile-menu-opened');
            });

            $('.open-ul a').on('click', function () {
                $(this).next('.sub-ul').toggleClass('display-open');
                $(this).find('img').toggleClass('ico-menu-arrow-opened');
            });
        });
    </script>
</asp:Content>
