﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class WMMedikal : System.Web.UI.MasterPage
    {
        cBase cb = new cBase();
        string new_page_url = string.Empty;
        string lang;
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                form1.Action = Request.RawUrl;
                lang = cBase.Lang;

                if (!Page.IsPostBack)
                {
                    var _sosyal = cb.db.sosyal_ikon.FirstOrDefault();

                    if (_sosyal.facebook != String.Empty)
                    {
                        facebookID.HRef = _sosyal.facebook;
                        LabelFace.Visible = true;
                    }
                    if (_sosyal.twitter != String.Empty)
                    {
                        twitterID.HRef = _sosyal.twitter;
                        LabelTwitter.Visible = true;
                    }


                    if (_sosyal.instagram != String.Empty)
                    {
                        instagramID.HRef = _sosyal.instagram;
                        LabelInstagram.Visible = true;
                    }

                    if (_sosyal.youtube != String.Empty)
                    {
                        youtubeID.HRef = _sosyal.youtube;
                        LabelYoutube.Visible = true;
                    }



                    var _UpdatePageTR =cb.db.Sayfa_Detay_Dil.Where(q=> q.id == 19).FirstOrDefault();
                    kariyerlink.HRef = _UpdatePageTR.description;

                    var _centers = cb.db.TibbiBolumler_Dil
                        .Where(q => q.TibbiBolumler.isMerkez && q.Dil == "TR")
                        .OrderBy(o => o.TibbiBolumler.displayOrder)
                        .Select(s => new
                        {
                            s.BolumID,
                            s.TibbiBolumler.seoUrl,
                            s.BolumAdi
                        })
                        .ToList();

                    if (_centers.Count > 0)
                    {
                        rptCenters.DataSource = _centers;
                        rptCenters.DataBind();
                        rptCenters2.DataSource = _centers;
                        rptCenters2.DataBind();
                    }
                    
                    //var Sayfalar = cb.db.Sayfalar.Select(s => new
                    //{
                    //    sayfa_id = s.id,
                    //    s.is_active,
                    //    link = s.sayfa_adi,
                    //    s.sayfa_tipi,
                    //    s.ust_id,
                    //    s.Sayfalar_Dil.FirstOrDefault(q => q.dil == lang).sayfa_adi
                    //}).Where(q => q.is_active == true && q.ust_id == 0).ToList();

                    //rptMainMenu.DataSource = Sayfalar;
                    //rptMainMenu.DataBind();



                    //var TopMenu = cb.db.Sayfalar.Select(s => new
                    //{
                    //    sayfa_id = s.id,
                    //    s.is_active,
                    //    link = s.sayfa_adi,
                    //    s.sayfa_tipi,
                    //    s.ust_id,
                    //    s.Sayfalar_Dil.FirstOrDefault(q => q.dil == "TR").sayfa_adi
                    //}).Where(q => q.is_active == true && q.ust_id == 777).OrderByDescending(o => o.sayfa_adi).ToList();

                    //rptTopMenu.DataSource = TopMenu;
                    //rptTopMenu.DataBind();



                    //var SabitSayfalar = cb.db.Sayfalar.Select(s => new
                    //{
                    //    sayfa_id = s.id,
                    //    link = s.Sayfalar_Dil.FirstOrDefault(q => q.dil == "TR").sayfa_adi,
                    //    s.ust_id,
                    //    s.Sayfalar_Dil.FirstOrDefault().dil,
                    //    s.Sayfalar_Dil.FirstOrDefault(q => q.dil == lang).sayfa_adi

                    //}).Where(f => f.ust_id == 999).ToList();


                    //rptSabit.DataSource = SabitSayfalar;
                    //rptSabit.DataBind();

                }
            }
            catch (DbEntityValidationException hata)
            {
                //foreach (var ht in hata.EntityValidationErrors)
                //{
                //    Response.Write(ht.Entry.Entity.GetType().Name + " " + ht.Entry.State);

                //    foreach (var valerror in ht.ValidationErrors)
                //    {
                //        Response.Write(valerror.PropertyName + " " + valerror.ErrorMessage);
                //    }
                //}
            }

        }

        //protected void rptMainMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    try
        //    {
        //        HtmlGenericControl divControl = e.Item.FindControl("alt_menu") as HtmlGenericControl;
        //        HyperLink hypParentPageLink = (HyperLink)e.Item.FindControl("hypParentPageLink");
               
        //        //short ParentID = DataBinder.Eval(e.Item.DataItem, "page_id").toshort();
        //        int SayfaTipi = DataBinder.Eval(e.Item.DataItem, "sayfa_tipi").toint();

        //        int sayfa_id = DataBinder.Eval(e.Item.DataItem, "sayfa_id").toint();

        //        switch (SayfaTipi)
        //        {
        //            case 1:
        //                hypParentPageLink.NavigateUrl = "/" + lang + "/hastanelerimiz/";
        //                break;

        //            case 2:
        //                hypParentPageLink.NavigateUrl = "/" + lang + "/tibbi-hizmetlerimiz/";
        //                break;

        //            case 3:
        //                hypParentPageLink.NavigateUrl = "/" + lang + "/doktorlarimiz/";
        //                break;

        //            case 4:
        //                hypParentPageLink.NavigateUrl = "/" + lang + "/e-services/";
        //                break;

        //            case 11:
        //                hypParentPageLink.NavigateUrl = "/" + lang + "/vm-medical-park-liv-concept/";
        //                break;

        //        }

        //        //var Hastaneler = cb.db.Hastaneler.ToList();

        //        //if (Hastaneler != null)
        //        //{
        //        //    if (SayfaTipi == 1)
        //        //    {
        //        //        rptSubMenu.DataSource = Hastaneler.ToList();
        //        //        rptSubMenu.DataBind();
        //        //    }
        //        //    else
        //        //        divControl.Visible = false;
        //        //}


        //    }

        //    catch (DbEntityValidationException hata)
        //    {
        //        foreach (var ht in hata.EntityValidationErrors)
        //        {
        //            Response.Write(ht.Entry.Entity.GetType().Name + " " + ht.Entry.State);
        //            foreach (var valerror in ht.ValidationErrors)
        //            {
        //                Response.Write(valerror.PropertyName + " " + valerror.ErrorMessage);
        //            }
        //        }
        //    }
        //}

      

        //protected void rptSubSubMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    try
        //    {
        //        HyperLink hypPageLink = (HyperLink)e.Item.FindControl("hypPageLink");
        //        int SayfaTipi = DataBinder.Eval(e.Item.DataItem, "sayfa_tipi").toint();
        //        string link = DataBinder.Eval(e.Item.DataItem, "link").ToString();
        //        string hastane_adi = DataBinder.Eval(e.Item.DataItem, "hastane_adi").ToString();
              
       
        //        int sayfa_id = DataBinder.Eval(e.Item.DataItem, "sayfa_id").toint();

        //        switch (SayfaTipi)
        //        {
        //            case 1:
        //                new_page_url = DataBinder.Eval(e.Item.DataItem, "page_url").ToString();
        //                hypPageLink.NavigateUrl = "/" + lang + "/hastane-detay/" + new_page_url + "-" + sayfa_id;
        //                break;

        //            case 2:
        //                new_page_url = DataBinder.Eval(e.Item.DataItem, "page_url").ToString();
        //                hypPageLink.NavigateUrl = "/" + lang + "/m-" + new_page_url + "-" + sayfa_id;
        //                break;

        //            case 3:
        //                new_page_url = DataBinder.Eval(e.Item.DataItem, "page_url").ToString();
        //                hypPageLink.NavigateUrl = "/" + lang + "/" + new_page_url + "-" + sayfa_id;
        //                break;

        //            case 4:
        //                new_page_url = DataBinder.Eval(e.Item.DataItem, "page_url").ToString();
        //                hypPageLink.NavigateUrl = "/" + lang + "/krm-" + new_page_url + "-" + sayfa_id;
        //                break;
        //            case 5:
        //                new_page_url = DataBinder.Eval(e.Item.DataItem, "page_url").ToString();
        //                hypPageLink.NavigateUrl = "/" + lang + "/fotograf/" + new_page_url + "-" + sayfa_id;
        //                break;
 
        //            case 6:
        //                hypPageLink.NavigateUrl = "/" + lang + "/e-services";
        //                break;
                    
        //            case 7:
        //                new_page_url = DataBinder.Eval(e.Item.DataItem, "page_url").ToString();
        //                hypPageLink.NavigateUrl = "/" + lang + "/vmkl-" + new_page_url + "-" + sayfa_id;
        //                break;



        //        }
        //    }
        //    catch (DbEntityValidationException hata)
        //    {
        //        foreach (var ht in hata.EntityValidationErrors)
        //        {
        //            Response.Write(ht.Entry.Entity.GetType().Name + " " + ht.Entry.State);
        //            foreach (var valerror in ht.ValidationErrors)
        //            {
        //                Response.Write(valerror.PropertyName + " " + valerror.ErrorMessage);
        //            }
        //        }
        //    }


       

        //}

        //protected void rptTopMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    HyperLink hypTopMenuLink = (HyperLink)e.Item.FindControl("hypTopMenuLink");
        //    string PageName = DataBinder.Eval(e.Item.DataItem, "link").ToString();
        //    int SayfaTipi = DataBinder.Eval(e.Item.DataItem, "sayfa_tipi").toint();
        //    int sayfa_id = DataBinder.Eval(e.Item.DataItem, "sayfa_id").toint();
        //    switch (SayfaTipi)
        //    {
        //        case 5:
        //            hypTopMenuLink.NavigateUrl = "/" + lang + "/vm-medical-park-mission-and-vision-" + sayfa_id;
        //            break;
        //        case 99:
        //            hypTopMenuLink.NavigateUrl = "http://www.medicalparkik.com/";
        //            break;
        //    }
        //}





        //protected void lnkSearch_Click(object sender, EventArgs e)
        //{
        //    if (txtSearch.Text.Trim() != null)
        //    {
        //        Response.Redirect("/search.aspx?text=" + txtSearch.Text.Trim());
        //    }
        //}
    }
}