﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class video_list : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var VideoList = db.video_gallery.Where(q=> q.is_active).ToList();

            if (VideoList.Count > 0)
            {
                rptGaleri.DataSource = VideoList;
                rptGaleri.DataBind();
            }
            
        }
    }
}