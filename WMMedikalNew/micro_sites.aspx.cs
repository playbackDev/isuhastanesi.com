﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class WebForm1 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hypBreadcrumb.NavigateUrl = Lang+"/micro_siteler";

            var _SiteList = db.micro_sites_lang.Where(q => q.lang == Lang && q.micro_sites.is_active).
                 Select(s => new { 
                     s.id,
                     s.site_name,
                    s.micro_sites.site_image,
                     s.micro_sites.site_url
                 }).ToList();

            if(_SiteList.Count > 0 )
            {
                rptSite.DataSource = _SiteList;
                rptSite.DataBind();
            }
          
        }
    }
}