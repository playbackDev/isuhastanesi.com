﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class news1 : cBase
    {
        int newsID;
        protected void Page_Load(object sender, EventArgs e)
        {


            if (Request.QueryString["id"] != null)
            {
                newsID = Request.QueryString["id"].toshort();
                //Lang = Request.QueryString["Lang"].ToString();
                if (!IsPostBack)
                {
                    var _NewsList = db.news_lang.Where(q => q.is_active && q.news_id == newsID)
                        .Select(s => new
                        {
                            s.id,
                            s.news_content,
                            s.news_title,
                            s.news.news_image,
                            s.news.seoPageTitle,
                            s.news.seoMetaDesc,
                            s.news.seoKeywords,
                            s.news.bigImg
                        }).FirstOrDefault();

                    if (_NewsList != null)
                    {
                        SetMetaTags(this.Page, _NewsList.seoPageTitle, _NewsList.seoMetaDesc, _NewsList.seoKeywords);
                        ltrTitle.Text = _NewsList.news_title;
                        ltrContent.Text = _NewsList.news_content;

                        if (_NewsList.bigImg != null || _NewsList.bigImg != "")
                            ltrImgBig.Text = "<img src='" + _NewsList.bigImg + "' />";
                        //rptNewList.DataSource = _NewsList;
                        //rptNewList.DataBind();
                    }
                    hypBreadCrumb.NavigateUrl = Lang + "/saglik-kosesi";


                    //var _eventList2 = db.news.FirstOrDefault(q => q.type == 1);

                    //if (_eventList2.img_path != null && _eventList2.img_path != "")
                    //{
                    //    topbanner.Style.Add("background-image", "../" + _eventList2.img_path);
                    //    topbanner.Style.Add("background-position-y", "-31px");
                    //}
                    //else
                    //    topbanner.Style.Add("height", "0");
                }
            }
        }
    }
}