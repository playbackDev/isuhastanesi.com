﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="video_list.aspx.cs" Inherits="WMMedikalNew.video_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="sub-page-title bg-1">
        <div class="container">
            <h1 class="middle-content" style="margin-top: 128px;"><%=Resources.Lang.MedyaBasin %> </h1>
        </div>

            <div class="sublinks">
            <div class="container">
                <ul class="clearfix">
                    <li><a href="/tanitim-filmlerimiz/<%=WMMedikalNew.cBase.Lang%>"><%=Resources.Lang.TanitimFilmlerimiz %></a></li>
                    <li><a href="/foto-galeri/<%=WMMedikalNew.cBase.Lang%>"><%=Resources.Lang.FotoGaleri %></a></li>
                    <li><a href="/video-galeri/<%=WMMedikalNew.cBase.Lang%>"><%=Resources.Lang.VideoGaleri %></a></li>
                    <li><a href="/logo-ve-kurumsal-kimlik/<%=WMMedikalNew.cBase.Lang%>"><%=Resources.Lang.LogoveKurumsal %></a></li>
                </ul>

            </div>
        </div>
    </div>
    <div class="container content pad20">
 
        <ul class="gallery-items">
            <asp:Repeater ID="rptGaleri" runat="server">
                <ItemTemplate>
            <li class="">
               <iframe width="100%" height="240" src="<%#Eval ("video_link") %>" frameborder="0" allowfullscreen=""></iframe>
            </li>

                    </ItemTemplate>
            </asp:Repeater>

        </ul>

    </div>


  
</asp:Content>
