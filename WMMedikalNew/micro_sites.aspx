﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="micro_sites.aspx.cs" Inherits="WMMedikalNew.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="sub-page-title bg-2">
        <div class="container">
            <h1 class="middle-content" style="margin-top: 128px;"><%=Resources.Lang.MikroSiteler %></h1>
        </div>
    </div>

    <div class="section-line">
        <div class="breadcrumb2">
            <a href="/">anasayfa &gt; </a> 
            <asp:HyperLink ID="hypBreadcrumb" runat="server"><%=Resources.Lang.MikroSiteler %></asp:HyperLink>

                                          
        </div>
    </div>
    <div class="container">
        <div class="micro-site">
            <ul class="micro-list">
                <asp:Repeater ID="rptSite" runat="server">
                    <ItemTemplate>
                <li>
                    <a href="<%#Eval("site_url") %>" target="_blank">
                    <img src="<%#Eval("site_image") %>" alt="<%#Eval("site_name") %>" style="max-width:300px;"></a>
                    <p><%#Eval("site_name") %></p>
                </li>
                        </ItemTemplate>
                </asp:Repeater>

                  
            </ul>
        </div>
       
    </div>
</asp:Content>
