﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="search.aspx.cs" Inherits="WMMedikalNew.WebForm16" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
           <div class="sub-page-title bg-1">
        <div class="container">
            <h1 class="middle-content">ARAMA SONUÇLARI</h1>
        </div>

        <div class="sublinks">
            <div class="container">
                <%--<uc1:AboutMenu ID="AboutMenu" runat="server" />--%>
            </div>
        </div>
    </div>

    <div class="container content pad4050">
        <h5 class="header-line">"<% = (Request.QueryString["text"] != null) ? Request.QueryString["text"].ToString() : "" %>" için arama sonuçları</h5>
        <br /><br />

        <asp:Repeater ID="rptSearchList" runat="server">
            <ItemTemplate>
                <a href="<%#Eval("Link") %>" style="text-decoration:underline;"><%#Eval("Title") %></a><br />
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Content>
