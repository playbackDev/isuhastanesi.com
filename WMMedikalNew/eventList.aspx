﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="eventList.aspx.cs" Inherits="WMMedikalNew.eventList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%--<div class="sub-page-title" runat="server" id="topbanner">
        <div class="container">
        </div>
    </div>--%>

    <div class="section-line">
        <div class="breadcrumb2">
            <a href="/">Anasayfa |</a><a href="/duyurular">Duyurular</a>
        </div>
    </div>


    <style>
        .accordion-title-new {
            display: block;
            background-color: #fff;
        }

            .accordion-title-new h2 {
                text-align: center;
                padding: 20px 0;
                background-color: #fff;
                cursor: pointer;
                font-size: 16px;
                color: #90032B;
                border-bottom: 1px solid #90032B;
            }
    </style>
    <div class="container">



        <div class="page-wrapper">

            <div class="accordion-title-new">
                <h2>DUYURULAR</h2>
            </div>

            <div class="etkinlik white">

                <asp:Repeater ID="rptEventList" runat="server" OnItemDataBound="rptEventList_ItemDataBound">
                    <ItemTemplate>
                        <a href="/d-<%# Eval("seoURL") %>/<%#Eval("id") %>" style="margin-left: 20px">
                            <asp:Literal ID="ltrImg" runat="server" />
                            <div class="etkinlik-title">
                                <p><%#Eval("news_title") %> </p>
                            </div>

                            <div class="etkinlik-date">
                                <p><%#Eval("created_time", "{0:d}") %> </p>
                            </div>
                        </a>
                        <hr style="background-color: #90032B; border: 0; height: 1px; background-image: -webkit-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0); background-image: -moz-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0); background-image: -ms-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0); background-image: -o-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0); clear: left; width: 80%;" />

                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>

    </div>

</asp:Content>
