﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class WebForm14 : cBase
    {
        int PageID;
        int HospitalID; 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["page_id"] != "")
            {
                PageID = Request.QueryString["page_id"].toint();
                var PageDetail = db.Hastane_Sayfalar_Dil.Select(s => new
                {
                    s.Hastane_Sayfalar.hastane_id,
                    s.sayfa_id,
                    s.dil,
                    s.Hastane_Sayfalar.Hastaneler.HastaneAdi,
                    s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == Lang).top_banner,
                    s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == Lang).meta_description
                }).Where(q => q.sayfa_id == PageID && q.dil == Lang).FirstOrDefault();

            
                if(PageDetail !=null)
                {
                    pagesubmenu.HospitalID = PageDetail.hastane_id.toint();
                    Page.MetaDescription = PageDetail.meta_description;
                    if (PageDetail.top_banner != null && PageDetail.top_banner != "")
                    {
                        topbanner.Style.Add("background-image", "../"+PageDetail.top_banner);
                        topbanner.Style.Add("background-position-y", "-31px");
                    }
                    else
                        topbanner.Style.Add("height", "0");

                    var _galeri = db.gallery.Where(q => q.is_active && q.hospital_id == PageDetail.hastane_id).ToList();

                    if (_galeri.Count > 0)
                    {
                        rptGaleri.DataSource = _galeri;
                        rptGaleri.DataBind();
                    }

                }
            }
          

        }
    }
}