﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class WebForm13 : cBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["page_id"] != null)
            {
                int PageID = Request.QueryString["page_id"].toint();
                //var PageDetail = db.page_detail_lang.Where(q => q.page_id == PageID && q.lang == Lang).FirstOrDefault();

                var SayfaDetay = db.Sayfa_Detay_Dil.Where(q => q.sayfa_id == PageID && q.lang == Lang).FirstOrDefault();

                if (SayfaDetay != null)
                {
                    ltrPageName.Text = SayfaDetay.Sayfalar.Sayfalar_Dil.FirstOrDefault().sayfa_adi;
                    ltrDescription.Text = SayfaDetay.description;
                }
                else
                    Response.Redirect("/",false);


            }
        }
    }
}