﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class WebForm4 : cBase
    {
        List<string> lets = new List<string> { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "Ö", "P", "R", "S", "T", "U", "V", "Y", "Z" };
        protected void Page_Load(object sender, EventArgs e)
        {
            var _HakkimizdDetay = db.Hastane_Sayfalar_Dil
                .Select(s => new
                {
                    s.sayfa_adi,
                    s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == "TR").description,
                    s.sayfa_id,
                    s.Hastane_Sayfalar.Hastaneler.HastaneAdi,
                    hastane_id = s.Hastane_Sayfalar.Hastaneler.id,
                    s.dil,
                    s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == "TR").top_banner,
                    s.Hastane_Sayfalar.Hastane_Sayfa_Detay_Dil.FirstOrDefault(q => q.lang == "TR").meta_description,
                    GalleryImage = s.Hastane_Sayfalar.gallery.Select(s2 => new
                    {
                        ImageID = s2.id,
                        s2.image_name,
                        active_status = (s2.is_active) ? "Aktif" : "Pasif",
                        s2.Hastaneler.HastaneAdi
                    })
                }).FirstOrDefault(q => q.sayfa_id == 2);

            if (_HakkimizdDetay != null)
            {
                //pagesubmenu.HospitalID = _HakkimizdDetay.hastane_id.toint();
                Page.MetaDescription = _HakkimizdDetay.meta_description.ToString();

                if (_HakkimizdDetay.top_banner != null && _HakkimizdDetay.top_banner != "")
                {
                    topbanner.Style.Add("background-image", "../" + _HakkimizdDetay.top_banner);
                    topbanner.Style.Add("background-position-y", "-31px");
                }
                else
                    topbanner.Style.Add("height", "0");
            }

            if (!IsPostBack)
            {
                DataLoad("");
                LoadLetters();
            }

        }
        private void DataLoad(string letter)
        {
            try
            {
                var _MedicalServices = db.TibbiBolumler_Dil.Select(s => new
                {
                    BolumID = s.TibbiBolumler.id,
                    s.BolumAdi,
                    s.Dil,
                    HastaneID = s.TibbiBolumler.Hastaneler.id,
                    s.TibbiBolumler.Hastaneler.HastaneAdi,
                    s.TibbiBolumler.displayOrder,
                    s.TibbiBolumler.seoUrl
                });


                _MedicalServices = _MedicalServices.Where(q => q.Dil == "TR").OrderBy(o => o.displayOrder);

                if (letter != "")
                {
                    _MedicalServices = _MedicalServices.Where(w => w.BolumAdi.StartsWith(letter)).OrderBy(o => o.displayOrder);

                    if (_MedicalServices != null)
                    {
                        lets.Clear();
                        lets.Add(letter);
                    }

                }
                //else
                //{
                //    if (drpHospitals.SelectedValue.toint() > 0)
                //    {
                //        int hospital_id = drpHospitals.SelectedValue.toint();
                //        _MedicalServices = _MedicalServices.Where(w => w.HastaneID == hospital_id);
                //    }

                //    if (txtSearch.Text != "")
                //    {
                //        string word = txtSearch.Text;
                //        lets.Clear();
                //        lets.Add(letter);
                //        _MedicalServices = _MedicalServices.Where(w => w.BolumAdi.Contains(word));
                //    }
                //}



                List<LETTERS> cats = new List<LETTERS>();


                foreach (var item in lets)
                {
                    LETTERS L = new LETTERS();

                    var letter_branches = _MedicalServices.Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi, display_order = s.displayOrder, url = s.seoUrl });

                    if (item == "C")
                    {
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith("C") || w.BolumAdi.StartsWith("Ç")).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi, display_order = s.displayOrder, url = s.seoUrl });
                        L.letter = "C-Ç";
                    }
                    else if (item == "G")
                    {
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith("G") || w.BolumAdi.StartsWith("Ğ")).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi, display_order = s.displayOrder, url = s.seoUrl });
                        L.letter = "G-Ğ";
                    }
                    else if (item == "I")
                    {
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith("I") || w.BolumAdi.StartsWith("İ")).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi, display_order = s.displayOrder, url = s.seoUrl });
                        L.letter = "I-İ";
                    }
                    else if (item == "O")
                    {
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith("O") || w.BolumAdi.StartsWith("Ö")).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi, display_order = s.displayOrder, url = s.seoUrl });
                        L.letter = "O-Ö";
                    }
                    else if (item == "S")
                    {
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith("S") || w.BolumAdi.StartsWith("Ş")).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi, display_order = s.displayOrder, url = s.seoUrl });
                        L.letter = "S-Ş";
                    }
                    else if (item == "U")
                    {
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith("U") || w.BolumAdi.StartsWith("Ü")).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi, display_order = s.displayOrder, url = s.seoUrl });
                        L.letter = "U-Ü";
                    }
                    else
                    {
                        L.letter = item;
                        letter_branches = _MedicalServices.Where(w => w.BolumAdi.StartsWith(item)).Select(s => new BRANCH { id = s.BolumID, medical_services_name = s.BolumAdi, hospital_name = s.HastaneAdi, display_order = s.displayOrder, url = s.seoUrl });
                    }

                    L.services_list = letter_branches.ToList().OrderBy(o => o.display_order).ToList();

                    if (L.services_list.Count > 0)
                        cats.Add(L);
                }

                if (cats.Count > 0)
                {
                    ltrUyari.Text = "";
                    rptLettersCats.BindData(cats);
                }
                else
                {
                    ltrUyari.Text = "Sonuç Bulunamadı";
                    rptLettersCats.ClearData();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        public void LoadLetters()
        {

            var harf_listesi = new List<LETTERS>();
            foreach (var item in lets)
            {
                harf_listesi.Add(new LETTERS { letter = item });
            }

            //rptLetters.BindData(harf_listesi);
        }

        protected class LETTERS
        {
            public string letter { get; set; }
            public List<BRANCH> services_list { get; set; }
        }

        protected class BRANCH
        {
            public int id { get; set; }
            public string medical_services_name { get; set; }
            public string hospital_name { get; set; }
            public short display_order { get; set; }
            public string url { get; set; }
        }

        protected void btnSearchMedUnit_Click(object sender, EventArgs e)
        {
            try
            {
                DataLoad("");
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        protected void rptBrn_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                Literal ltrLink = (Literal)e.Item.FindControl("ltrLink");
                Literal ltrUyari = (Literal)e.Item.FindControl("ltrUyari");

                int id = DataBinder.Eval(e.Item.DataItem, "id").toint();
                string BolumAdi = DataBinder.Eval(e.Item.DataItem, "medical_services_name").ToString();
                string HastaneAdi = DataBinder.Eval(e.Item.DataItem, "hospital_name").ToString();

                if (BolumAdi != null || BolumAdi != "" || BolumAdi != " ")

                    ltrLink.Text = "<a href=" + "/tibbi-hizmetler/" + DataBinder.Eval(e.Item.DataItem, "url") + "/" + id + ">" + BolumAdi + "</a>";
                else
                    ltrLink.Text = "Sonuç Bulunamadı";



            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }


        protected void btnLetters_Command(object sender, CommandEventArgs e)
        {
            DataLoad(e.CommandArgument.ToString());
        }


    }
}