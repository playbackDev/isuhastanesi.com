﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WMMedikal.Master" AutoEventWireup="true" CodeBehind="static_page.aspx.cs" Inherits="WMMedikalNew.WebForm15" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="sub-page-title bg-2">
        <div class="container">
            <h1 class="middle-content" style="margin-top: 128px;"><asp:Literal ID="ltrPageName2" runat="server"></asp:Literal></h1>
        </div>
    </div>

    <div class="section-line">
        <div class="breadcrumb2">
            <a href="/<%=WMMedikalNew.cBase.Lang%>"><%=Resources.Lang.Anasayfa %> &gt; </a><a href="#"> <asp:Literal ID="ltrPageName" runat="server"></asp:Literal></a>
        </div>
    </div>
    <div class="hasta-hak-menu">
        <div class="container">
             <ul>
              
              
                  <asp:Repeater ID="rptSabit" runat="server">
                            <ItemTemplate>
                            <li>     <a href="/hasta-ve-ziyaretciler/<%#WMMedikalNew.cExtensions.ToURL(Eval("link").ToString())%>-<%#Eval("sayfa_id") %>/<%=WMMedikalNew.cBase.Lang%>"><%#Eval("sayfa_adi") %></a></li>
                      
                            </ItemTemplate>
                        </asp:Repeater>
            </ul>
        </div>
    </div>
    <div class="container">
       <div class="basinda-medical">
           <div class="rules">
                  <asp:Literal ID="ltrDescription" runat="server"></asp:Literal>
           </div>
        
           </div>
    </div>
</asp:Content>
