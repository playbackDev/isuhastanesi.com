﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WMMedikalNew
{
    public partial class WebForm8 : cBase
    {
        int HospitalID = 0;
        int ParentID = 0;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["page_id"] != null)
            {
                int PageID = Request.QueryString["page_id"].toint();

                var PageDetail = db.page_detail_lang
                .Select(s => new
                {
                    s.video_url,
                    s.description,
                    s.lang,
                    s.page_id,
                    s.pages.hospital_id,
                    s.pages.parent_id,

                })
                .Where(q => q.page_id == PageID && q.lang == Lang)
                .FirstOrDefault();

                if (PageDetail != null)
                {
                    pagesubmenu.HospitalID = PageDetail.hospital_id.toint();
                    pagesubmenu.ParentID = PageDetail.parent_id.toint();

                    if (PageDetail.video_url == "" || PageDetail.video_url == null)
                        video_div.Visible = false;
                    else
                    {
                        iframe_video.Src = PageDetail.video_url;
                    }
                    ltrDescription.Text = PageDetail.description;
                }
                else
                {
                    video_div.Visible = false;
                    ltrDescription.Text = "İçerik Eklenmemiş";
                }
            }
            




        }


    }
}