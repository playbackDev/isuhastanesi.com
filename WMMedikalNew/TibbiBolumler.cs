//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WMMedikalNew
{
    using System;
    using System.Collections.Generic;
    
    public partial class TibbiBolumler
    {
        public TibbiBolumler()
        {
            this.Doktorlar = new HashSet<Doktorlar>();
            this.TibbiBolumler_Dil = new HashSet<TibbiBolumler_Dil>();
            this.doktorlar_ek_bolumler = new HashSet<doktorlar_ek_bolumler>();
        }
    
        public int id { get; set; }
        public string BolumAdi { get; set; }
        public int HastaneID { get; set; }
        public int ServisID { get; set; }
        public string Resim { get; set; }
        public bool is_manuel { get; set; }
        public short displayOrder { get; set; }
        public string seoPageTitle { get; set; }
        public string seoMetaDesc { get; set; }
        public string seoKeywords { get; set; }
        public string seoUrl { get; set; }
        public bool isMerkez { get; set; }
    
        public virtual ICollection<Doktorlar> Doktorlar { get; set; }
        public virtual Hastaneler Hastaneler { get; set; }
        public virtual ICollection<TibbiBolumler_Dil> TibbiBolumler_Dil { get; set; }
        public virtual ICollection<doktorlar_ek_bolumler> doktorlar_ek_bolumler { get; set; }
    }
}
