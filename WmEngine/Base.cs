﻿using System;
using System.Collections.Generic;
using System.Drawing;

using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WmEngine
{
    class Base
    {
     
        public static string _logTxtPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\";
        //public static string _imagePath = _physicalPath + @"\uploads\products\";
        public static string _imagePath="";
        public static void WriteLog(string Log)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(_logTxtPath + "log-" + DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year + ".txt", true))
                {
                    Log = "[root@zbyr] # " + Log + " / " + DateTime.Now;

                    Console.WriteLine(Log);
                    writer.WriteLine(Log);
                }
            }
            catch { }
        }
        public static string CreateRandomValue(int Length, bool CharactersB, bool CharactersS, bool Numbers, bool SpecialCharacters)
        {
            string characters_b = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string characters_s = "abcdefghijklmnopqrstuvwxyz";
            string numbers = "0123456789";
            string special_characters = "-_*+/";
            string allowedChars = String.Empty;

            if (CharactersB)
                allowedChars += characters_b;

            if (CharactersS)
                allowedChars += characters_s;

            if (Numbers)
                allowedChars += numbers;

            if (SpecialCharacters)
                allowedChars += special_characters;

            char[] chars = new char[Length];
            Random rd = new Random();

            for (int i = 0; i < Length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }
        public static bool DownloadImage(string Url, string NewFileName)
        {
            try
            {
                string localFilename = _imagePath + @"1000x1500\" + NewFileName;

                if (Url.StartsWith("//"))
                    Url = "http:" + Url;

                using (WebClient client = new WebClient())
                {
                    client.DownloadFile(Url, localFilename);
                }

                return true;
            }
            catch (Exception ex)
            {
                Base.WriteLog("Error while downloading Image (" + Url + "): " + ex.Message);
                if (ex.InnerException != null)
                    Base.WriteLog(ex.InnerException.Message);

                return false;
            }
        }
     
    }
}
