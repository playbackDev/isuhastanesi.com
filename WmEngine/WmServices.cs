﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WmEngine.LivHospitalERandevu1;
using WmEngine.LivHospitalESonuc1;

namespace WmEngine
{
    class WmServices
    {
        public class ExistingHospital
        {
            public int HospitalID;
            public string HospitalName;
            public string Hostname;

        }

        private WMMedikalDBEntities db = new WMMedikalDBEntities();

        List<ExistingHospital> _existingHospital = new List<ExistingHospital>();
        randevufiltre eRandevu = new randevufiltre();
        TestResultService eSonuc = new TestResultService();
        public string IpAddress = "85.105.102.223";
        public string strUsername = "vmmedic";
        public string strPassword = "alpark";
        string SessionCode = string.Empty;
        public WmServices()
        {
            //GetHospitalList();
            //GetMedicalServiceList();
            GetDoctorList();
        }

        private void GetDoctorList()
        {
            SessionCode = eRandevu.GetSession(strUsername, strPassword, IpAddress, "");

            //var _doctors = db.Doktorlar.Select(s => new
            //{
            //    ServisHastaneID = s.Hastaneler.ServisHastaneID,
            //    HastaneID = s.Hastaneler.id,
            //    BolumID = s.TibbiBolumler.id,
            //    ServisBolumID = s.TibbiBolumler.ServisID,
            //    s.ServisID,
            //}).ToList();

            var _doctors = db.TibbiBolumler.Select(s => new
            {
                ServisBolumID = s.ServisID,
                s.Hastaneler.ServisHastaneID,
                s.HastaneID,
                s.id

            }).ToList();

            if (_doctors.Count() > 0)
            {
                foreach (var HastaneRow in _doctors)
                {
                    DataTable doctorTable = eRandevu.GetDoctors(SessionCode, HastaneRow.ServisHastaneID, HastaneRow.ServisBolumID);

                    foreach (DataRow row in doctorTable.Rows)
                    {
                        Doktorlar _existingDoctors = _isDoctorsExists(Convert.ToInt32(row[0].ToString()), HastaneRow.ServisBolumID, HastaneRow.ServisHastaneID);

                        if (_existingDoctors == null)
                        {
                            //Console.WriteLine("Yok DoktorID " + Convert.ToInt32(row[0].ToString()));
                            if (_createNewDoctor(Convert.ToInt32(row[0].ToString()), row[4].ToString(), row[5].ToString(), HastaneRow.HastaneID, HastaneRow.id))
                            {

                                //    //bool _createNewDoctor(int ServisID, string DoctorName, int HospitalID, int MedicalServiceID) 
                                //    //DoctorId = row[0].ToString(),
                                //    //DepartmentId = row[1].ToString(),
                                //    //DoctorName = row[2].ToString(),
                                //    //BranchId = row[3].ToString(),
                                //    //Title = row[4].ToString()

                                Console.WriteLine("Doktor Eklendi - Doktor Servis ID : " + row[0].ToString() + " Doktor Adı : " + row[2].ToString());
                            }
                        }
                    }
                }
            }
            else
            {

                //DataTable doctorTable = eRandevu.GetDoctors(SessionCode, item.ServisHastaneID, item.ServisBolumID);

                //foreach (DataRow row in doctorTable.Rows)
                //{
                //    Doktorlar _existingDoctors = _isDoctorsExists(Convert.ToInt32(row[0].ToString()), item.ServisBolumID, item.ServisHastaneID);

                //    if (_existingDoctors == null)
                //    {
                //        //Console.WriteLine("Yok DoktorID " + Convert.ToInt32(row[0].ToString()));
                //        if (_createNewDoctor(Convert.ToInt32(row[0].ToString()), row[2].ToString(), item.HastaneID, item.BolumID))
                //        {
                //            //    //bool _createNewDoctor(int ServisID, string DoctorName, int HospitalID, int MedicalServiceID) 
                //            //    //DoctorId = row[0].ToString(),
                //            //    //DepartmentId = row[1].ToString(),
                //            //    //DoctorName = row[2].ToString(),
                //            //    //BranchId = row[3].ToString(),
                //            //    //Title = row[4].ToString()

                //            Console.WriteLine("Doktor Eklendi - Doktor Servis ID : " + row[0].ToString() + " Doktor Adı : " + row[2].ToString());
                //        }
                //    }
                //}
            }

        }

        Doktorlar _isDoctorsExists(int DoctorServisID, int BolumServisID, int BolumHastaneID)
        {
            try
            {
                Doktorlar _doktorlar = db.Doktorlar.Where(q => q.ServisID == DoctorServisID && q.TibbiBolumler.ServisID == BolumServisID && q.Hastaneler.ServisHastaneID == BolumHastaneID).FirstOrDefault();
                if (_doktorlar != null)
                    return _doktorlar;
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        private void GetMedicalServiceList()
        {
            SessionCode = eRandevu.GetSession(strUsername, strPassword, IpAddress, "");
            List<Hastaneler> _hospitals = db.Hastaneler.ToList();

            foreach (var item in _hospitals)
            {
                DataTable departmentTable = eRandevu.GetDepartments(SessionCode, item.ServisHastaneID);
                foreach (DataRow row in departmentTable.Rows)
                {
                    TibbiBolumler _existingMedicalServices = _isMedicalServicesExists(Convert.ToInt32(row[0].ToString()), item.ServisHastaneID);

                    if (_existingMedicalServices == null)
                    {
                        if (_createNewMedicalServices(Convert.ToInt32(row[0].ToString()), row[1].ToString(), item.id, item.ServisHastaneID))
                        {
                            Console.WriteLine("Tıbbi Bölüm Eklendi - Bölüm Servis ID : " + row[0].ToString() + " Bölüm Adı : " + row[1].ToString());

                            //DataTable doctorTable = eRandevu.GetDoctors(SessionCode, item.ServisHastaneID, Convert.ToInt32(row[0].ToString()));

                            //foreach (DataRow _doctor_item in doctorTable.Rows)
                            //{
                            //    Doktorlar _existingDoctor = _isDoctorExisting(Convert.ToInt32(_doctor_item[0].ToString()));

                            //    if (_existingDoctor == null)
                            //    {
                            //        if (_createNewDoctor(Convert.ToInt32(_doctor_item[0].ToString()), _doctor_item[2].ToString(), item.ServisHastaneID, Convert.ToInt32(row[0].ToString())))
                            //        {
                            //            Console.WriteLine("Doktor Eklendi - Doktor Servis ID : " + _doctor_item[0].ToString() + " Doktor Adı : " + row[2].ToString());
                            //        }
                            //    }
                            //}
                        }
                    }
                    else
                    {
                        if (_updateMedicalServices(Convert.ToInt32(row[0].ToString()), row[1].ToString(), item.id))
                        {
                            Console.WriteLine("Tıbbi Bölüm Güncellendi - Bölüm Servis ID : " + row[0].ToString() + " Bölüm Adı : " + row[1].ToString());
                        }
                    }
                }
            }

        }

        private Doktorlar _isDoctorExisting(int DoctorServisID)
        {
            try
            {
                Doktorlar _doktorlar = db.Doktorlar.Where(q => q.ServisID == DoctorServisID).FirstOrDefault();
                if (_doktorlar != null)
                    return _doktorlar;
                else
                    return null;
            }
            catch { return null; }
        }

        private void GetHospitalList()
        {
            try
            {
                SessionCode = eRandevu.GetSession(strUsername, strPassword, IpAddress, "");

                if (SessionCode != null)
                {
                    DataTable hospitalTable = eRandevu.GetHospitals(SessionCode.ToString());
                    foreach (DataRow row in hospitalTable.Rows)
                    {
                        Hastaneler _existingHospital = _isHospitalExists(Convert.ToInt32(row[0].ToString()));

                        if (_existingHospital == null)
                        {
                            if (_createNewHospital(row[1].ToString(), Convert.ToInt32(row[0].ToString()), SessionCode))
                            {
                                Console.WriteLine("Hastane Eklendi - HastaneID : " + row[0].ToString() + " HastaneAdı : " + row[1].ToString());
                            }
                        }
                        else
                        {
                            if (_updateHospital(row[1].ToString(), Convert.ToInt32(row[0].ToString())))
                            {
                                Console.WriteLine("Hastane Güncellendi - HastaneID : " + row[0].ToString() + " HastaneAdı : " + row[1].ToString());
                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        bool _createNewHospital(string HospitalName, int HospitalServiceID, string SessionCode)
        {
            Hastaneler _newHospital = new Hastaneler();
            _newHospital.HastaneAdi = HospitalName;
            _newHospital.ServisHastaneID = HospitalServiceID;
            db.Hastaneler.Add(_newHospital);

            if (db.SaveChanges() > 0)
            {

                DataTable departmentTable = eRandevu.GetDepartments(SessionCode, HospitalServiceID);

                foreach (DataRow row in departmentTable.Rows)
                {
                    TibbiBolumler _existingMedicalServices = _isMedicalServicesExists(Convert.ToInt32(row[0].ToString()), HospitalServiceID);

                    if (_existingMedicalServices == null)
                    {
                        if (_createNewMedicalServices(Convert.ToInt32(row[0].ToString()), row[1].ToString(), _newHospital.id, HospitalServiceID))
                        {
                            Console.WriteLine("Eklendi -Tıbbi Bölüm ID : " + row[0].ToString() + " Bölüm Adı : " + row[1].ToString());

                        }

                    }
                    else
                    {
                        if (_updateMedicalServices(Convert.ToInt32(row[0].ToString()), row[1].ToString(), _newHospital.id))
                        {
                            Console.WriteLine("Güncellendi -Tıbbi Bölüm ID : " + row[0].ToString() + " Bölüm Adı : " + row[1].ToString());
                        }
                    }

                }
                return true;
            }
            else
                return false;
        }

        bool _updateHospital(string HospitalName, int HospitalServiceID)
        {
            using (WMMedikalDBEntities db = new WMMedikalDBEntities())
            {

                Hastaneler _newHospital = db.Hastaneler.Where(q => q.ServisHastaneID == HospitalServiceID).FirstOrDefault();
                _newHospital.HastaneAdi = HospitalName;

                if (db.SaveChanges() > 0)
                    return true;
                else
                    return false;
            }
        }
        bool _createNewMedicalServices(int ServisID, string ServiceName, int HospitalID, int HospitalServisID)
        {
            TibbiBolumler _newMedicalService = new TibbiBolumler();
            _newMedicalService.BolumAdi = ServiceName;
            _newMedicalService.ServisID = ServisID;
            _newMedicalService.HastaneID = HospitalID;
            _newMedicalService.is_manuel = false;

            db.TibbiBolumler.Add(_newMedicalService);

            TibbiBolumler_Dil _newMedicalServiceLangTR = new TibbiBolumler_Dil();
            _newMedicalServiceLangTR.Aciklama = "";
            _newMedicalServiceLangTR.BolumAdi = ServiceName;
            _newMedicalServiceLangTR.BolumID = _newMedicalService.id;
            _newMedicalServiceLangTR.Dil = "TR";

            db.TibbiBolumler_Dil.Add(_newMedicalServiceLangTR);

            TibbiBolumler_Dil _newMedicalServiceLangEN = new TibbiBolumler_Dil();
            _newMedicalServiceLangEN.Aciklama = "";
            _newMedicalServiceLangEN.BolumAdi = ServiceName;
            _newMedicalServiceLangEN.BolumID = _newMedicalService.id;
            _newMedicalServiceLangEN.Dil = "EN";

            db.TibbiBolumler_Dil.Add(_newMedicalServiceLangEN);




            if (db.SaveChanges() > 0)
            {

                DataTable doctorTable = eRandevu.GetDoctors(SessionCode, HospitalServisID, ServisID);

                foreach (DataRow _doctor_item in doctorTable.Rows)
                {
                    Doktorlar _existingDoctor = _isDoctorExisting(Convert.ToInt32(_doctor_item[0].ToString()));

                    if (_existingDoctor == null)
                    {
                        if (_createNewDoctor(Convert.ToInt32(_doctor_item[0].ToString()), _doctor_item[4].ToString(), _doctor_item[5].ToString(), HospitalID, _newMedicalService.id))
                        {
                            Console.WriteLine("Doktor Eklendi - Doktor Servis ID : " + _doctor_item[0].ToString() + " Doktor Adı : " + _doctor_item[2].ToString());
                        }

                    }
                    else
                    {
                        //if (_updateDoctor(Convert.ToInt32(_doctor_item[0].ToString()), _doctor_item[2].ToString(), HospitalID, _newMedicalService.id))
                        //{
                        //    Console.WriteLine("Doktor Güncellendi - Doktor Servis ID : " + _doctor_item[0].ToString() + " Doktor Adı : " + _doctor_item[2].ToString());
                        //}
                    }
                }
                return true;
            }

            else
                return false;
        }

        bool _createNewDoctor(int ServisID, string Unvan,string DoctorName, int HospitalID, int MedicalServiceID)
        {
            Doktorlar _doktorlar = new Doktorlar();
            _doktorlar.DoktorAdi = DoctorName;
            _doktorlar.ServisID = ServisID;
            _doktorlar.HastaneID = HospitalID;
            _doktorlar.Unvan = Unvan;
            _doktorlar.TibbiBolumID = MedicalServiceID;

            db.Doktorlar.Add(_doktorlar);


            Doktorlar_Dil _doktorlarDilTR = new Doktorlar_Dil();
            _doktorlarDilTR.doktor_id = _doktorlar.id;
            _doktorlarDilTR.dil = "TR";

            db.Doktorlar_Dil.Add(_doktorlarDilTR);

            Doktorlar_Dil _doktorlarDilEN = new Doktorlar_Dil();
            _doktorlarDilEN.doktor_id = _doktorlar.id;
            _doktorlarDilEN.dil = "EN";



            db.Doktorlar_Dil.Add(_doktorlarDilEN);
            if (db.SaveChanges() > 0)
                return true;
            else
                return false;
       


        }

        //bool _updateDoctor(int ServisID,string   int HospitalID, int MedicalServiceID)
        //{
        //    var _updateDoctor = db.Doktorlar
        //        .Select(s => new { 

        //        }).Where(q => q.ServisID == ServisID && q.HastaneID == HospitalID && q.TibbiBolumID == MedicalServiceID);

        //    //Doktorlar _doktorlar = new Doktorlar();
        //    //_doktorlar.DoktorAdi = DoctorName;
        //    //_doktorlar.ServisID = ServisID;
        //    //_doktorlar.HastaneID = HospitalID;
        //    //_doktorlar.TibbiBolumID = MedicalServiceID;

        //    //db.Doktorlar.Add(_doktorlar);

        //    //Doktorlar_Dil _doktorlarDilTR = new Doktorlar_Dil();
        //    //_doktorlarDilTR.doktor_id = _doktorlar.id;
        //    //_doktorlarDilTR.dil = "TR";

        //    //db.Doktorlar_Dil.Add(_doktorlarDilTR);

        //    //Doktorlar_Dil _doktorlarDilEN = new Doktorlar_Dil();
        //    //_doktorlarDilEN.doktor_id = _doktorlar.id;
        //    //_doktorlarDilEN.dil = "EN";

        //    //db.Doktorlar_Dil.Add(_doktorlarDilEN);


        //    if (db.SaveChanges() > 0)
        //        return true;
        //    else
        //        return false;
        //}
        bool _updateMedicalServices(int ServisID, string ServiceName, int HospitalID)
        {
            TibbiBolumler _MedicalServices = db.TibbiBolumler.Where(q => q.ServisID == ServisID).FirstOrDefault();
            _MedicalServices.BolumAdi = ServiceName;
            _MedicalServices.HastaneID = HospitalID;
            _MedicalServices.is_manuel = false;

            if (db.SaveChanges() > 0)
                return true;
            else
                return false;
        }

        Hastaneler _isHospitalExists(int HospitalServisID)
        {
            try
            {
                Hastaneler _hospital = db.Hastaneler.Where(q => q.ServisHastaneID == HospitalServisID).FirstOrDefault();
                if (_hospital != null)
                    return _hospital;
                else
                    return null;
            }
            catch { return null; }
        }

        TibbiBolumler _isMedicalServicesExists(int MedicalServisID, int HospitalID)
        {
            try
            {
                TibbiBolumler _MedicalServices = db.TibbiBolumler.Where(q => q.ServisID == MedicalServisID && q.Hastaneler.ServisHastaneID == HospitalID).FirstOrDefault();
                if (_MedicalServices != null)
                    return _MedicalServices;
                else
                    return null;
            }
            catch { return null; }
        }


        public class HospitalList
        {
            public string HospitalId { get; set; }
            public string HospitalName { get; set; }
        }

        public class DepartmentList
        {
            public string DepartmentId { get; set; }
            public string DepartmentName { get; set; }
        }

        public class DoctorList
        {
            public string DoctorId { get; set; }
            public string DepartmentId { get; set; }
            public string BranchId { get; set; }
            public string DoctorName { get; set; }
            public string Title { get; set; }
        }
    }



}
